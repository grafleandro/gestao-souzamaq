<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'CoordMec',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
     * 'smart-style-{SKIN#}'
     * 'smart-rtl'         - Switch theme mode to RTL
     * 'menu-on-top'       - Switch to top navigation (no DOM change required)
     * 'no-menu'			  - Hides the menu completely
     * 'hidden-menu'       - Hides the main menu but still accessable by hovering over left edge
     * 'fixed-header'      - Fixes the header
     * 'fixed-navigation'  - Fixes the main menu
     * 'fixed-ribbon'      - Fixes breadcrumb
     * 'fixed-page-footer' - Fixes footer
     * 'container'         - boxed layout mode (non-responsive: will not work with fixed-navigation & fixed-ribbon)
     * */

    'layout' => 'menu-on-top',
];
