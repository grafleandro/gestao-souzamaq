<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */
    [
        'text'      => 'Início',
        'icon'      => 'home',
        'class'     => 'active',
        'title'     => 'Início',
        'url'       => '/home',
    ],
    [
        'text'        => 'Empresa',
        'icon'        => 'institution',
        'class'       => '',
        'title'       => 'Empresa',
        'submenu'     => [
            [
                'text'  => 'Empresa',
                'title' => 'Empresa',
                'class' => '',
                'submenu'     => [
                    [
                        'text'  => 'Cadastrar',
                        'title' => 'Empresa',
                        'url'   => '/empresa/empresa/cadastrar',
                    ],
                    [
                        'text'  => 'Listar',
                        'title' => 'Empresa',
                        'url'   => '/empresa/empresa/listar',
                    ],
                    [
                        'text'  => 'Conta',
                        'title' => 'Empresa',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Endereço',
                        'title' => 'Empresa',
                        'url'   => '#',
                    ],
                ]
            ],
            [
                'text'  => 'Cliente',
                'title' => 'Cliente',
                'class' => '',
                'submenu'     => [
                    [
                        'text'  => 'Cadastro',
                        'title' => 'Cadastro',
                        'url'   => '/empresa/cliente/cadastrar',
                    ],
                    [
                        'text'  => 'Listar',
                        'title' => 'Listar',
                        'url'   => '/empresa/cliente/listar',
                    ],
                ]
            ],
            [
                'text'  => 'Fornecedores',
                'title' => 'Empresa',
                'class' => '',
                'submenu'     => [
                    [
                        'text'  => 'Fornecedores',
                        'title' => 'Fornecedores',
                        'url'   => '/empresa/fornecedor/fornecedor/cadastrar',
                    ],
                    [
                        'text'  => 'Lançamentos',
                        'title' => 'Fornecedores',
                        'url'   => '/empresa/fornecedor/fornecedor/lancamento',
                    ],
                ]
            ],
            [
                'text'  => 'Colaborador',
                'title' => 'user',
                'class' => '',
                'submenu'     => [
                    [
                        'text'  => 'Cadastro',
                        'title' => 'Colaborador',
                        'url'   => '/empresa/colaborador/cadastrar',
                    ],
                    [
                        'text'  => 'Listar',
                        'title' => 'Colaborador',
                        'url'   => '/empresa/colaborador/listar',
                    ],
                    [
                        'text'  => 'Conta Corrente',
                        'title' => 'Colaborador',
                        'url'   => '#',
                    ],
                ]
            ],
            [
                'text'  => 'Usuários',
                'title' => 'Empresa',
                'class' => '',
                'submenu'     => [
                    [
                        'text'  => 'Cadastrar',
                        'title' => 'Usuário',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Editar',
                        'title' => 'Usuário',
                        'url'   => '#',
                    ],
                ]
            ],
            [
                'text'  => 'Caixas',
                'title' => 'Empresa',
                'class' => '',
                'submenu'     => [
                    [
                        'text'  => 'Cadastrar',
                        'title' => 'Caixas',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Configuração',
                        'title' => 'Caixas',
                        'url'   => '#',
                    ],
                ]
            ],
        ]
    ],
    [
        'text'        => 'Oficina',
        'icon'        => 'gears',
        'class'       => '',
        'title'       => 'Oficina',
        'submenu'     => [
            [
                'text'  => 'Check-List',
                'title' => 'Oficina',
                'class' => '',
                'url'   => '#',
                'submenu'     => [
                    [
                        'text'  => 'Interno',
                        'title' => 'Check-List',
                        'url'   => '/oficina/checklist/interno',
                    ],
                    [
                        'text'  => 'Externo',
                        'title' => 'Check-List',
                        'url'   => '/oficina/checklist/externo',
                    ],
                ]
            ],
            [
                'text'  => 'Almoxarifado',
                'title' => 'Oficina',
                'class' => '',
                'url'   => '#',
                'submenu'     => [
                    [
                        'text'  => 'Ferramentas',
                        'title' => 'Almoxarifado',
                        'url'   => '/oficina/almoxarifado/ferramentas',
                    ],
                ]
            ],
            [
                'text'  => 'Serviços',
                'title' => 'Oficina',
                'class' => '',
                'url'   => '/oficina/servico',
            ],
            [
                'text'  => 'Maquinas',
                'title' => 'Oficina',
                'class' => '',
                'url'   => '#',
            ],
            [
                'text'  => 'Agenda de Serviço',
                'title' => 'Oficina',
                'class' => '',
                'url'   => '#',
            ],
            [
                'text'  => 'Orçamento / O.S',
                'title' => 'Oficina',
                'class' => '',
                'url'   => '#',
            ],
            [
                'text'  => 'Veículos',
                'title' => 'Oficina',
                'class' => '',
                'url'   => '/oficina/veiculo',
            ],
        ]
    ],
    [
        'text'        => 'Estoque',
        'icon'        => 'cubes',
        'class'       => '',
        'title'       => 'Estoque',
        'submenu'     => [
            [
                'text'  => 'Compras',
                'title' => 'Estoque',
                'url'   => '#',
                'submenu'     => [
                    [
                        'text'  => 'Estoque Minímo',
                        'title' => 'Compras',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Pedido de Compra',
                        'title' => 'Compras',
                        'url'   => '#',
                    ],
                ]
            ],
            [
                'text'  => 'Balanço',
                'title' => 'Estoque',
                'url'   => '#',
            ],
            [
                'text'  => 'Lançamento',
                'title' => 'Estoque',
                'url'   => '#',
            ],
            [
                'text'  => 'Transferência de Estoque',
                'title' => 'Estoque',
                'url'   => '#',
            ],
            [
                'text'  => 'Ajuste de Estoque',
                'title' => 'Estoque',
                'url'   => '#',
            ],
            [
                'text'  => 'Categorias',
                'title' => 'Estoque',
                'url'   => '/estoque/categoria',
            ],
        ]
    ],
    [
        'text'        => 'Vendas',
        'icon'        => 'shopping-cart',
        'class'       => '',
        'title'       => 'Vendas',
        'submenu'     => [
            [
                'text'  => 'Vendas',
                'title' => 'Vendas',
                'url'   => '#',
            ],
            [
                'text'  => 'Devolução',
                'title' => 'Vendas',
                'url'   => '#',
            ],
            [
                'text'  => 'Cancelamento',
                'title' => 'Vendas',
                'url'   => '#',
            ],
        ]
    ],
    [
        'text'        => 'Relatórios',
        'icon'        => 'bar-chart-o',
        'class'       => '',
        'title'       => 'Relatórios',
        'submenu'     => [
            [
                'text'  => 'Clientes',
                'title' => 'Relatórios',
                'url'   => '#',
                'submenu'     => [
                    [
                        'text'  => 'Cadastrados',
                        'title' => 'Clientes',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Aniversariates',
                        'title' => 'Clientes',
                        'url'   => '#',
                    ],
                ]
            ],
            [
                'text'  => 'Financeiro',
                'title' => 'Relatório',
                'url'   => '#',
                'submenu'     => [
                    [
                        'text'  => 'Contas a Pagar',
                        'title' => 'Financeiro',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Contas a Receber',
                        'title' => 'Financeiro',
                        'url'   => '#',
                    ],
                ],
            ],
            [
                'text'  => 'Caixa',
                'title' => 'Relatório',
                'url'   => '#',
                'submenu'     => [
                    [
                        'text'  => 'Movimento Diário',
                        'title' => 'Caixa',
                        'url'   => '#',
                    ],
                ],
            ],
            [
                'text'  => 'Serviço',
                'title' => 'Relatório',
                'url'   => '#',
                'submenu'     => [
                    [
                        'text'  => 'Ordem de Serviço',
                        'title' => 'Serviço',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Comissão',
                        'title' => 'Serviço',
                        'url'   => '#',
                    ],
                ],
            ],
            [
                'text'  => 'Estoque',
                'title' => 'Relatório',
                'url'   => '#',
                'submenu'     => [
                    [
                        'text'  => 'Pedido de Compra',
                        'title' => 'Estoque',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Entrada de Produto',
                        'title' => 'Estoque',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Devolução de Produto',
                        'title' => 'Estoque',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Produto sem Movimentação',
                        'title' => 'Estoque',
                        'url'   => '#',
                    ],
                ],
            ],
            [
                'text'  => 'Forncedores',
                'title' => 'Relatório',
                'url'   => '#',
            ],
            [
                'text'  => 'Despesas',
                'title' => 'Relatório',
                'url'   => '#',
            ],
            [
                'text'  => 'Conta Corrente Funcionário',
                'title' => 'Relatório',
                'url'   => '#',
            ],
            [
                'text'  => 'Vendas',
                'title' => 'Relatório',
                'url'   => '#',
            ],
        ]
    ],
    [
        'text'        => 'Requisição',
        'icon'        => 'file-text-o',
        'class'       => '',
        'title'       => 'Requisição',
        'submenu'     => [
            [
                'text'  => 'Vale',
                'title' => 'Requisição',
                'url'   => '#',
            ],
            [
                'text'  => 'Produto e Serviço',
                'title' => 'Requisição',
                'url'   => '#',
                'submenu'     => [
                    [
                        'text'  => 'Cadastrar',
                        'title' => 'Produto e Serviço',
                        'url'   => '/requisicao/produto-servico/cadastrar',
                    ],
                    [
                        'text'  => 'Listar',
                        'title' => 'Produto e Serviço',
                        'url'   => '#',
                    ]
                ]
            ],
        ]
    ],
    [
        'text'        => 'Financeiro',
        'icon'        => 'dollar',
        'class'       => '',
        'title'       => 'Financeiro',
        'submenu'     => [
            [
                'text'  => 'Contas a Pagar',
                'title' => 'Financeiro',
                'url'   => '#',
                'submenu'     => [
                    [
                        'text'  => 'Tipos de Documento',
                        'title' => 'Contas a Pagar',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Lançamento de Título',
                        'title' => 'Contas a Pagar',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Baixa',
                        'title' => 'Contas a Pagar',
                        'url'   => '#',
                    ],
                ],
            ],
            [
                'text'  => 'Contas a Receber',
                'title' => 'Financeiro',
                'url'   => '#',
                'submenu'     => [
                    [
                        'text'  => 'Lançamento',
                        'title' => 'Contas a Receber',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Recebimento',
                        'title' => 'Contas a Receber',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Cobrança',
                        'title' => 'Contas a Receber',
                        'url'   => '#',
                    ],
                ],
            ],
            [
                'text'  => 'Movimentação de Caixa',
                'title' => 'Financeiro',
                'url'   => '#',
                'submenu'     => [
                    [
                        'text'  => 'Abertura',
                        'title' => 'Mov. de Cx.',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Lançamento Avulço',
                        'title' => 'Mov. de Cx.',
                        'url'   => '#',
                    ],
                    [
                        'text'  => 'Fechamento',
                        'title' => 'Mov. de Cx.',
                        'url'   => '#',
                    ],
                ],
            ],
        ]
    ],
    [
        'text'      => 'Almoxarifado',
        'icon'      => 'archive',
        'class'     => '',
        'title'     => 'Almoxarifado',
        'url'       => '#',
    ],
    [
        'text'      => 'Configuração',
        'icon'      => 'gear',
        'class'     => '',
        'title'     => 'Configuração',
        'url'       => '#',
        'submenu'     => [
            [
                'text'  => 'Função',
                'title' => 'Configuração',
                'url'   => '/configuracao/funcao',
            ],
            [
                'text'  => 'Notificação',
                'title' => 'Configuração',
                'url'   => '/configuracao/notificao',
            ],
        ]
    ],
    [
        'text'      => 'Sair',
        'icon'      => 'sign-out',
        'class'     => '',
        'title'     => 'Sair',
        'url'       => 'logout',
    ],
    [
        'text'      => 'Ajuda',
        'icon'      => 'question',
        'class'     => '',
        'title'     => 'Ajuda',
        'url'       => '#',
    ],
];
