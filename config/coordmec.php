<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => env('APP_NAME', 'CoordMec'),

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | 'smart-style-{SKIN#}'
    |
    */

    'skin' => 'red',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    * 'smart-rtl'         - Switch theme mode to RTL
	| 'menu-on-top'       - Switch to top navigation (no DOM change required)
	| 'no-menu'			  - Hides the menu completely
	| 'hidden-menu'       - Hides the main menu but still accessable by hovering over left edge
	| 'fixed-header'      - Fixes the header
	| 'fixed-navigation'  - Fixes the main menu
	| 'fixed-ribbon'      - Fixes breadcrumb
	| 'fixed-page-footer' - Fixes footer
    | 'container'         - boxed layout mode (non-responsive: will not work with fixed-navigation & fixed-ribbon)
    |
    */

    'layout' => 'menu-on-top',

    'chart-information-page' => false,

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    */

    'plugins' => [
        'fullcalendar'  => true,
        'jvectormap'    => false,
        'smartchat'     => false,
        'voice-command' => false,
        'flot-chart-plugin'    => false,
    ],
];
