<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormaPagamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param int $emprId
     * @return void
     */
    public function run(int $emprId = 0)
    {
        $string     = file_get_contents("storage/seed/forma_pagamento.json");
        $dadosFormPgto  = json_decode($string, true);

        $formPgtoModel = [];

        if($emprId) {
            foreach ($dadosFormPgto['forma_pgamento'] as $index => $value) {
                $formPgtoModel[] = [
                    'cofp_titulo' => $value['cofp_titulo'],
                    'cofp_descricao' => $value['cofp_descricao'],
                    'cofp_avista' => $value['cofp_avista'],
                    'cofp_parcelas' => $value['cofp_parcelas'],
                    'cofp_troco' => $value['cofp_troco'],
                    'cofp_desconto_max' => $value['cofp_desconto_max'],
                    'cofp_cod_nfe' => $value['cofp_cod_nfe'],
                    'empr_id' => $emprId,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ];
            }

            DB::table('configuracao_forma_pagamento')->insert($formPgtoModel);
        }
    }
}
