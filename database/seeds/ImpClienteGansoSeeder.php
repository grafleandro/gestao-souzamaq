<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use League\Csv\Reader;
use App\Utils\Mask;
use App\Utils\ConsultarCepUtils;

/**
 * IMPORTACAO DOS DADOS DOS CLIENTES PARA SISTEMA GANSO
*/
class ImpClienteGansoSeeder extends Seeder
{
    const EMPRE_ID = null;
    const AMBIENTE = 'mysql'; // mysql(local) ou producao

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \League\Csv\Exception
     */
    public function run()
    {
        $this->importDataFromCsv();
    }

    /**
     * @throws \League\Csv\Exception
     */
    private function importDataFromCsv()
    {
        $csv = Reader::createFromPath(getcwd() . '/storage/seed/cliente_umtrato.csv')
            ->setDelimiter(',')
            ->setHeaderOffset(0);

        $cliente    = [];

        foreach ($csv as $index => $record) {
            $endereco= [];
            $contato = [];

            $contato[] = [
                'cont_cel_1' => $record['CELULAR'] ?? null,
                'cont_cel_2' => $record['FAX'] ?? null,
                'cont_tel_fixo' => $record['TELEFONE'] ?? null,
                'cont_email' => $record['EMAIL'] ?? null,
            ];

            /** Salvando os dados do Contato */
            DB::connection(self::AMBIENTE)->table('contato')->insert($contato);
            $cont_id = DB::connection(self::AMBIENTE)->getPdo()->lastInsertId();

            $dadosEnd = ConsultarCepUtils::getCep(Mask::removerMascara($record['CEP']));

            if($dadosEnd['success'] && isset($dadosEnd['endereco']['status']) && $dadosEnd['endereco']['status']) {
                $endereco[] = [
                    'ende_logradouro_titulo' => $dadosEnd['endereco']['address'],
                    'ende_bairro' => $dadosEnd['endereco']['district'],
                    'ende_cidade' => $dadosEnd['endereco']['city'],
                    'ende_estado' => $dadosEnd['endereco']['state'],
                    'ende_complemento' => '',
                    'ende_numero' => $record['NUMERO'] ?? null,
                    'ende_cep' => Mask::removerMascara($dadosEnd['endereco']['code']),
                    'enlo_id' => '24',
                ];

                /** Salvando os dados do Endereco */
                DB::connection(self::AMBIENTE)->table('endereco')->insert($endereco);
                $ende_id = DB::connection(self::AMBIENTE)->getPdo()->lastInsertId();
            } else {
                $ende_id = null;
            }

            $cliente[] = [
                'clie_nome_razao_social' => $record['NOME'],
                'clie_nome_fantasia' => $record['nome'] ?? $record['RAZAO_SOCIAL'],
                'clie_cpf_cnpj' => $record['CPF'] ?? $record['CNPJ'],
                'clie_rg_insc_estadual' => $record['RG'] ?? $record['INSC_ESTADUAL'],
                'clie_tipo' => ($record['CNPJ']) ? '4' : '3',
                'clie_status' => (isset($record['SITUACAO']) && $record['SITUACAO'] == 'A') ? '1' : '0',
                'clie_nascimento' => (empty($record['NASCIMENTO'])) ? null : Carbon::createFromFormat('d/m/Y', $record['NASCIMENTO'])->format('Y-m-d'),
                'clie_limite' => $record['LIMITE'] ?? null,
                'clie_insc_municipal' => $record['INSC_MUNICIPAL'] ?? null,
                'ende_id' => $ende_id ?? null,
                'cont_id' => $cont_id ?? null,
                'empr_id' => self::EMPRE_ID,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];

            if($index % 100 == 0){

                DB::connection(self::AMBIENTE)->table('cliente')->insert($cliente);
                $cliente = [];

                echo($index);
                break;
            }

        }

//        DB::connection(self::AMBIENTE)->table('cliente')->insert($cliente);

        echo 'Terminou ;-) ...';
    }
}
