<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use League\Csv\Reader;
use Symfony\Component\Console\Output\ConsoleOutput;

class FiscalNcmCestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \League\Csv\Exception
     */
    public function run()
    {
        $this->importDataFromCsv();
    }

    private function insertFakeData()
    {
        DB::table('fiscal_ncm')->insert([
            'finc_cod_ncm' => '45646546456',
            'finc_cod_cest' => null,
            'finc_descricao' => 'Apenas testando',
            'finc_ipi' => '5.9',
            'finc_inicio_vigencia' => '2018-06-11',
            'finc_fim_vigencia' => '2018-06-11',
            'finc_gtin_producao' => '2018-06-11',
            'finc_gtin_homologacao' => '2018-06-11',
            'finc_observacao' => 'descricao',
            'unme_id' => 10,
            'created_at' => \Carbon\Carbon::now(),
        ]);
    }

//        finc_id
//        finc_cod_ncm
//        finc_cod_cest
//        finc_descricao
//        finc_ipi
//        finc_inicio_vigencia
//        finc_fim_vigencia
//        finc_unid_medida
//        finc_gtin_producao
//        finc_gtin_homologacao
//        finc_observacao
//        created_at
//        updated_at

    //    NCM                   0
    //    Categoria	            1
    //    Descrição	            2
    //    IPI	                3
    //    Início da Vigência	4
    //    Fim da Vigência	    5
    //    uTrib	                6
    //    Descrição da uTrib	7
    //    GTIN Produção	        8
    //    GTIN Homologação	    9
    //    Observação            10


    /**
     * @throws \League\Csv\Exception
     */
    private function importDataFromCsv()
    {
        $unidadeMedida = \App\Model\UnidadeMedidaModel::get()->toArray();
        $csv = Reader::createFromPath(getcwd() . '/storage/seed/tabela_ncm.csv')
            ->setDelimiter(';');

        $dataNCM        = [];
        $dataCategoria  = [];

        foreach ($csv as $index => $record) {

            if($index == 0){
                continue;
            }

            $dataNCM[] = [
                'finc_cod_ncm' => $record[0],
                'finc_cod_cest' => null,
                'finc_descricao' => rtrim(explode("-", $record[2])[1] ?? null),
                'finc_ipi' => $record[3],
                'finc_inicio_vigencia' => (empty($record[4])) ? null : \Carbon\Carbon::createFromFormat('d/m/Y', $record[4])->format('Y-m-d'),
                'finc_fim_vigencia' => (empty($record[5])) ? null : \Carbon\Carbon::createFromFormat('d/m/Y', $record[5])->format('Y-m-d'),
                'finc_gtin_producao' => (empty($record[8])) ? null : \Carbon\Carbon::createFromFormat('d/m/Y', $record[8])->format('Y-m-d'),
                'finc_gtin_homologacao' => (empty($record[9])) ? null : \Carbon\Carbon::createFromFormat('d/m/Y', $record[9])->format('Y-m-d'),
                'finc_observacao' => $record[10],
                'unme_id' => $this->getUnidMedida($record[6], $unidadeMedida),
                'created_at' => \Carbon\Carbon::now(),
            ];

            if ($index % 100 == 0){
                DB::table('fiscal_ncm')->insert($dataNCM);
                $dataNCM = [];
                echo $index;
            }
        }

        DB::table('fiscal_ncm')->insert($dataNCM);
    }

    /**
     * @param string $string
     * @param array $unidadeMedida
     * @return bool
     */
    private function getUnidMedida($string = '', $unidadeMedida = [])
    {
        foreach ($unidadeMedida as $value){
            if(strncasecmp($string, $value['unme_sigla'] , strlen($string)) == 0){
                return $value['unme_id'];
            }

            if(strncasecmp($string, $value['unme_titulo'] , strlen($string)) == 0){
                return $value['unme_id'];
            }
        }
    }
}
