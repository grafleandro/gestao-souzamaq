$(function(){
    jQueryOrdemServico.init();
});

let jQueryOrdemServico = (function(){
    let $dadosItem = {};
    let valorProduto = 0;
    let valorServico = 0;
    let valorTotalProduto = 0;
    let valorTotalServico = 0;
    let totalOS = 0;
    let $table = '<table id="tabela-ordem-servico" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th class="text-center" style="width: 5%; text-align: center">Nº</th>'
                            +'<th class="text-center">Cliente</th>'
                            +'<th class="text-center"> Veiculo / Placa</th>'
                            +'<th class="text-center"> Serviço</th>'
                            +'<th class="text-center"> Situação</th>'
                            +'<th class="text-center">Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    return {

        init: function(){
            if($('#form_ordem_servico').attr('method') ==='put' && sessionStorage.getItem("servico_os"))
            {
                this.inserirDadosOS();
            }else {
                sessionStorage.clear();
            }

            if($('.table-listar-ordem-servico').length) {
                let $setTable = $('.table-listar-ordem-servico');

                $setTable.empty();

                $setTable.html($table);

                this.listarOrdemServico();
            }

        },

        listarOrdemServico: function(){

            $("#tabela-ordem-servico").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/oficina/ordem_servico/tabela',
                    data:{
                        _token: csrfToken,
                        situacao: $('#situacao').val(),
                    },
                },
                columns: [
                    { data: "orse_numero",},
                    { data: "clie_id", name:"ordemServicoCliente.clie_nome_razao_social"},
                    { data: "veic_id", name:"ordemServicoVeiculo.veic_placa" || "ordemServicoVeiculo.veic_modelo"},
                    { data: "servico", orderable: false, searchable: false },
                    { data: "situacao", className: 'pull-center', orderable: false, searchable: false },
                    { data: "action", className: 'pull-center', orderable: false, searchable: false},
                ]
            });
        },

        selecionarFormaPgto: function($event){
            let optionSelected = $("option:selected", $event);

            /* Desbloqueia botao para poder calcular */
            $("#venda-btn-confirmar").removeAttr('disabled');

            // Quando selecionado uma opcao de PGTO que possui parcelas, eh carregado a quantidade de BUTTONS para selecionar
            if(optionSelected.data('parcelas')){
                $("#venda-parcelas").removeClass('hidden').empty();

                for(i = 1; i <= parseInt(optionSelected.data('parcelas')); i++){
                    $("#venda-parcelas").append('<button type="button" class="btn btn-default btn-md btn-circle margin-right-20 margin-top-5" data-value="'+ i +'" onclick="jQueryVendaFinalizar.calcularQtdParcelas($(this))">'+ i +'X</button>')
                }
            }else{
                $("#venda-parcelas").addClass('hidden').empty();
            }

            $("#venda-total-pagar").focus();
            this.calcularDescDinheiro($("#venda-desconto-din"));
        },

        selecionaChecklist: function() {
                        let $modal = $('#modal-info');
                        let $table = '<table id="tabela-checklist-interno" name="id="tabela-checklist-interno"" class="table table-bordered table-striped" width="100%">'
                            +'<thead>'
                            +'<tr>'
                            +'<th data-hide="phone" class="text-center" style="width: 5%; text-align: center">OS</th>'
                            +'<th data-hide="phone" class="text-center">Cliente</th>'
                            +'<th data-class="expand" class="text-center"> Veiculo / Placa</th>'
                            +'<th data-class="expand" class="text-center" style="width: 30%"> Serviço</th>'
                            +'<th data-hide="phone" class="text-center"> Previção de Entrega</th>'
                            +'</tr>'
                            +'</thead>'
                            +'<tbody>'
                            +'</tbody>'
                            +'</table>';


                        $modal.find('.modal-body').empty();
                        $modal.find('.modal-footer').empty();

                        $modal.find('.modal-title').html('Selecionar o Checklist');

                        $modal.find('.modal-dialog').addClass('modal-lg');

                        $modal.find('.modal-body').html('<div class="box-body table-responsive table-listar-checklist-interno">\n' +
                            '                        </div>');

                        $modal.modal('show');

                        let $setTable = $('.table-listar-checklist-interno');

                        $setTable.empty();

                        $setTable.html($table);

                        let table = $("#tabela-checklist-interno").DataTable({
                            destroy: true,
                            searching: true,
                            serverSide: true,
                            processing: true,
                            select: true,
                            ajax: {
                                url: '/oficina/ordem_servico/checklist'
                            },
                            columns: [
                                { data: "ckli_id" },
                                { data: "cliente" },
                                { data: "veiculo" },
                                { data: "ckli_servico" },
                                { data: "prev_entrega" },
                            ]
                        });

            table.on( 'select', function ( e, dt, type, indexes ) {
                    let checklist = table.rows( indexes ).data().toArray()[0];
                    let mecanicos = [];

                    $('#nome_cliente').val(checklist['cliente']);
                    $('#nome_veiculo').val(checklist['veiculo']);
                    $('#dt_entrega').val(checklist['prev_entrega']);

                    $('#descricao_servico').val(checklist['ckli_servico']);
                    $('#id_cliente').val(checklist['clie_id']);
                    $('#id_veiculo').val(checklist['veic_id']);
                    $('#checklist_id').val(checklist['ckli_id']);

                    $modal.modal('hide');
                } );

        },

        addProdutoTabela: function($event){
            let status = true;

            if(typeof $dadosItem === 'undefined' || $.isEmptyObject($dadosItem)){
                swal({
                    title: 'Adicionar Produto',
                    text: 'Você precisa selecionar um produto!',
                    icon: "warning",
                }).then(function(){
                    $("#produto").focus();
                });

                check = false;
            }

            if(!$("#prod_qtd").val().length){
                swal({
                    title: 'Quantidade do Produto',
                    text: 'Você precisa informar a quantidade deste produto!',
                    icon: "warning",
                }).then(function(){
                    $("#prod_qtd").focus();
                });

                status = false;
            }

            if(status){
                valorProduto = $dadosItem.produto_valores.prva_preco_venda;
                valorTotalProduto = valorTotalProduto + $dadosItem.produto_valores.prva_preco_venda * parseFloat($("#prod_qtd").val().replace('R$', '').replace('.', '').replace(',', '.'));
                totalOS = valorTotalProduto + valorTotalServico;

                $("#tabela-ordem-servico-produtos").append('<tr>' +
                    '<td class="text-align-center">'+ $dadosItem.prod_id +'</td>' +
                    '<td>'+ $dadosItem.prod_titulo +'</td>' +
                    '<td class="text-align-center">'+ $("#prod_qtd").val() +'</td>' +
                    '<td class="text-align-center">'+ accounting.formatMoney(valorProduto, "R$ ", 2, ".", ",") + '</td>' +
                    '<td class="text-align-center">'+ accounting.formatMoney($dadosItem.produto_valores.prva_preco_venda * parseFloat($("#prod_qtd").val().replace('R$', '').replace('.', '').replace(',', '.')), "R$ ", 2, ".", ",") +'</td>' +
                    '<td class="text-align-center">' +
                    '<button class="btn btn-default btn-circle" data-index="'+ $dadosItem.prod_id +'" onclick="jQueryOrdemServico.removerProdutoTabela($(this))"><i class="fa fa-trash"></i></button>' +
                    '</td>' +
                    '</tr>'
                );

                /* Adicionar produto a Session Storage */
                if (Modernizr.localstorage) {
                    let produto_OS = [];

                    if(sessionStorage.getItem("produto_os")){
                        produto_OS = Object.values(JSON.parse(sessionStorage.getItem('produto_os')));

                        sessionStorage.removeItem("produto_os");
                    }

                    produto_OS.push({
                        vend_id: $dadosItem.prod_id,
                        produto: $dadosItem,
                        quantidade: parseFloat($("#prod_qtd").val().replace(',', '.')),
                    });

                    $('#num_produto')[0].textContent = produto_OS.length;

                    sessionStorage.setItem("produto_os", JSON.stringify(produto_OS));
                }

                $('#total_produto')[0].textContent = accounting.formatMoney(valorTotalProduto, "R$ ", 2, ".", ",");

                $('#total_os')[0].textContent = accounting.formatMoney(totalOS, "R$ ", 2, ".", ",");

                $("#produto").val('').focus();

                $("#prod_qtd").val('');
            }
        },

        inserirDadosOS: function($event){
            valorTotalProduto = os.orse_total_produtos;
            totalOS = os.orse_total_os;
            valorTotalServico = os.orse_total_servico;

            if(os.ordem_servico_produto){
                let qtd = 0;
                $.map(os.ordem_servico_produto, function ($dadosItem, $index) {
                    $("#tabela-ordem-servico-produtos").append('<tr>' +
                        '<td class="text-align-center">'+ $dadosItem.prod_id +'</td>' +
                        '<td>'+ $dadosItem.produto.prod_titulo +'</td>' +
                        '<td class="text-align-center">'+ accounting.formatMoney( $dadosItem.prmo_qtd , "", 2, ".", ",") +'</td>' +
                        '<td class="text-align-center">'+ accounting.formatMoney($dadosItem.prmo_valor, "R$ ", 2, ".", ",") + '</td>' +
                        '<td class="text-align-center">'+ accounting.formatMoney($dadosItem.prmo_valor * $dadosItem.prmo_qtd, "R$ ", 2, ".", ",") +'</td>' +
                        '<td class="text-align-center">' +
                        '<button class="btn btn-default btn-circle" data-index="'+ $dadosItem.prod_id +'" onclick="jQueryOrdemServico.removerProdutoTabela($(this))"><i class="fa fa-trash"></i></button>' +
                        '</td>' +
                        '</tr>'
                    );
                    qtd++;
                });


                 $('#num_produto')[0].textContent = qtd;

                $("#produto").val('');

                $("#prod_qtd").val('');
            }

            if(os.ordem_servico_servicos){
                let qtd = 0;
                $.map(os.ordem_servico_servicos, function ($dadosItem, $index) {
                    $("#tabela-ordem-servico").append('<tr>' +
                        '<td class="text-align-center">'+ $dadosItem.serv_id +'</td>' +
                        '<td>'+ $dadosItem.servico.serv_titulo +'</td>' +
                        '<td class="text-align-center">'+ accounting.formatMoney( $dadosItem.osrs_qtd_servico , "", 2, ".", ",") +'</td>' +
                        '<td class="text-align-center">'+ accounting.formatMoney($dadosItem.osrs_qtd_servico * $dadosItem.osrs_valor_servico, "R$ ", 2, ".", ",") + '</td>' +
                        '<td class="text-align-center">' +
                        '<button class="btn btn-default btn-circle" data-servico="'+ $dadosItem.serv_id +'" onclick="jQueryOrdemServico.removerServicoTabela($(this))"><i class="fa fa-trash"></i></button>' +
                        '</td>' +
                        '</tr>'
                    );
                    qtd ++;
                });

                $('#num_servico')[0].textContent = qtd;

                $("#servico").val('');

                $("#serv_qtd").val('');
            }


        },

        removerProdutoTabela: function($event){
            let indexProduto= $event.data('index');

            setTimeout(function(){
                if(sessionStorage.getItem("produto_os")) {
                    let produto_OS = Object.values(JSON.parse(sessionStorage.getItem('produto_os')));

                    produto_OS = produto_OS.filter(function(value, index){
                        if(value.produto.prod_id == indexProduto){

                            if(typeof value.quantidade!=='undefined') {
                                valorTotalProduto = valorTotalProduto - value.quantidade * value.produto.produto_valores.prva_preco_venda;
                                totalOS = totalOS - value.quantidade * value.produto.produto_valores.prva_preco_venda;
                            }else{
                                valorTotalProduto = valorTotalProduto - value.osrp_prod_qtd * value.osrp_prod_valor;
                                totalOS = totalOS - value.osrp_prod_qtd * value.osrp_prod_valor;
                            }
                            $('#total_produto')[0].textContent = accounting.formatMoney(valorTotalProduto, "R$ ", 2, ".", ",");

                            $('#total_os')[0].textContent = accounting.formatMoney(totalOS, "R$ ", 2, ".", ",");

                            $('#num_produto')[0].textContent = parseInt($('#num_produto')[0].textContent) - 1;
                        }
                        return (value.produto.prod_id !== indexProduto)
                    });

                    sessionStorage.removeItem("produto_os");
                    sessionStorage.setItem("produto_os", JSON.stringify(produto_OS));
                }
            },0);

            /* Remover a linha da tabela */
            $event.closest('tr').remove();

        },

        confereEstoqueProduto:function($event){

            if($dadosItem){

                    if($("#prod_qtd").val().replace(',', '.') > $dadosItem.produto_estoque.prest_estoq_atual){
                        swal({
                            title: 'Quantidade do Produto',
                            text: 'O produto '+ $dadosItem.prod_titulo +' esta com o estoque abaixo da quantidade solicitada! \n\n Quantidade em estoque: ' + $dadosItem.produto_estoque.prest_estoq_atual,
                            icon: "warning",
                        }).then(function(){
                            $("#prod_qtd").focus();
                        });

                        status = false;
                    }


            }
        },

        autocompleteProduto: function($event){
            let $parent = this;

            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        if(request.term.length === 2 && /\s/g.test(request.term)){
                            request.term = request.term.replace(/\s/g, '*');
                        }
                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/empresa/produto",
                            data: {
                                type: 'prod',
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.produto.length){

                                        response($.map(data.produto, function (item) {

                                            return {
                                                label: item.prod_id + ' - ' + item.prod_titulo,
                                                value: item.prod_id+ ' - ' + item.prod_titulo,
                                                id: item.prod_id,
                                                produto: item
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item Não Encontrato',
                                            value: 'Item Não Encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar Produto', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar Produto', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $dadosItem = ui.item.produto;
                    }
                }
            });
        },

        addServicoTabela: function($event){
            let status = true;

            if(typeof $dadosItem === 'undefined' || $.isEmptyObject($dadosItem)){
                swal({
                    title: 'Adicionar Produto',
                    text: 'Você precisa selecionar um servico!',
                    icon: "warning",
                }).then(function(){
                    $("#servico").focus();
                });

                status = false;
            }

            if(!$("#serv_qtd").val().length){
                swal({
                    title: 'Quantidade do Produto',
                    text: 'Você precisa informar a quantidade deste serviço!',
                    icon: "warning",
                }).then(function(){
                    $("#serv_qtd").focus();
                });

                status = false;
            }

            if(status){
                valorServico = $dadosItem.serv_valor;
                valorTotalServico = valorTotalServico + valorServico * parseFloat($("#serv_qtd").val());
                totalOS = valorTotalProduto + valorTotalServico;

                $("#tabela-ordem-servico").append('<tr>' +
                    '<td class="text-align-center">'+ $dadosItem.serv_id +'</td>' +
                    '<td>'+ $dadosItem.serv_titulo +'</td>' +
                    '<td class="text-align-center">'+ $("#serv_qtd").val() +'</td>' +
                    '<td class="text-align-center">'+ accounting.formatMoney(valorServico, "R$ ", 2, ".", ",") + '</td>' +
                    '<td class="text-align-center">' +
                    '<button class="btn btn-default btn-circle" data-servico="'+ $dadosItem.serv_id +'" onclick="jQueryOrdemServico.removerServicoTabela($(this))"><i class="fa fa-trash"></i></button>' +
                    '</td>' +
                    '</tr>'
                );

                /* Adicionar produto a Session Storage */
                if (Modernizr.localstorage) {
                    let servico_OS = [];

                    if(sessionStorage.getItem("servico_os")){
                        servico_OS = Object.values(JSON.parse(sessionStorage.getItem('servico_os')));

                        sessionStorage.removeItem("servico_os");
                    }

                    servico_OS.push({
                        serv_id: $dadosItem.serv_id,
                        servico: $dadosItem,
                        quantidade: parseInt($("#serv_qtd").val().replace(',', '.')),
                        // valor: parseFloat(())
                    });

                    $('#num_servico')[0].textContent = servico_OS.length;

                    sessionStorage.setItem("servico_os", JSON.stringify(servico_OS));
                }

                $('#total_servico')[0].textContent = accounting.formatMoney(valorTotalServico, "R$ ", 2, ".", ",");

                $('#total_os')[0].textContent = accounting.formatMoney(totalOS, "R$ ", 2, ".", ",");

                /*Adicionando os produtos cas exista cadastrados no servico*/
                if($dadosItem.servico_produto.length){
                    $dadosItem.servico_produto.map(function (produto) {
                        valorProduto = produto.produto_valores.prva_preco_venda;
                        valorTotalProduto = valorTotalProduto + valorProduto * parseFloat(produto.sepr_qtd);
                        totalOS = valorTotalProduto + valorTotalServico;

                        $("#tabela-ordem-servico-produtos").append('<tr>' +
                            '<td class="text-align-center">'+ produto.prod_id +'</td>' +
                            '<td>'+ produto.produto.prod_titulo +'</td>' +
                            '<td class="text-align-center">'+ produto.sepr_qtd +'</td>' +
                            '<td class="text-align-center">'+ accounting.formatMoney(valorProduto, "R$ ", 2, ".", ",") + '</td>' +
                            '<td class="text-align-center">'+ accounting.formatMoney(valorProduto * produto.sepr_qtd, "R$ ", 2, ".", ",") +'</td>' +
                            '<td class="text-align-center">' +
                            '<button class="btn btn-default btn-circle" data-index="'+ produto.prod_id +'" onclick="jQueryOrdemServico.removerProdutoTabela($(this))"><i class="fa fa-trash"></i></button>' +
                            '</td>' +
                            '</tr>'
                        );

                        /* Adicionar produto a Session Storage */
                        if (Modernizr.localstorage) {
                            let produto_OS = [];

                            if(sessionStorage.getItem("produto_os")){
                                produto_OS = Object.values(JSON.parse(sessionStorage.getItem('produto_os')));

                                sessionStorage.removeItem("produto_os");
                            }

                            produto_OS.push({
                                vend_id: produto.prod_id,
                                produto: produto,
                                quantidade: parseFloat(produto.sepr_qtd),
                            });

                            $('#num_produto')[0].textContent = produto_OS.length;

                            sessionStorage.setItem("produto_os", JSON.stringify(produto_OS));
                        }

                        $('#total_produto')[0].textContent = accounting.formatMoney(valorTotalProduto, "R$ ", 2, ".", ",");

                        $('#total_os')[0].textContent = accounting.formatMoney(totalOS, "R$ ", 2, ".", ",");

                        $("#produto").val('');

                        $("#prod_qtd").val('');
                    })

                }

                $("#servico").val('').focus();

                $("#serv_qtd").val('');
            }
        },

        removerServicoTabela: function($event){
            let indexServico= $event.data('servico');

            setTimeout(function(){
                if(sessionStorage.getItem("servico_os")) {
                    let servico_OS = Object.values(JSON.parse(sessionStorage.getItem('servico_os')));

                    servico_OS = servico_OS.filter(function(value, index){
                        if(value.serv_id == indexServico){

                            if(typeof value.quantidade!=='undefined') {
                                valorTotalServico = valorTotalServico - value.quantidade * value.servico.serv_valor;
                                totalOS = totalOS - value.quantidade * value.servico.serv_valor;
                            }else{
                                valorTotalServico = valorTotalServico - value.osrs_qtd_servico * value.osrs_valor_servico;
                                totalOS = totalOS - value.osrs_qtd_servico * value.osrs_valor_servico;
                            }

                            $('#total_servico')[0].textContent = accounting.formatMoney(valorTotalServico, "R$ ", 2, ".", ",");

                            $('#total_os')[0].textContent = accounting.formatMoney(totalOS, "R$ ", 2, ".", ",");

                            $('#num_servico')[0].textContent = parseInt($('#num_servico')[0].textContent) - 1;
                        }
                        return (value.serv_id !== indexServico)
                    });

                    sessionStorage.removeItem("servico_os");
                    sessionStorage.setItem("servico_os", JSON.stringify(servico_OS));
                }
            },0);

            /* Remover a linha da tabela */
            $event.closest('tr').remove();

        },

        autocompleteServico: function($event){
            let $parent = this;

            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){

                        if(request.term.length === 2 && /\s/g.test(request.term)){
                            request.term = request.term.replace(/\s/g, '*');
                        }

                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/oficina/servico",
                            data: {
                                type: 'serv',
                                search_value: request.term
                            },
                            success: function(data){
                                if(data.success){
                                    if(data.servico.length){

                                        response($.map(data.servico, function (item) {

                                            return {
                                                label: item.serv_id + ' - ' + item.serv_titulo,
                                                value: item.serv_id+ ' - ' + item.serv_titulo,
                                                id: item.serv_id,
                                                servico: item
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item Não Encontrato',
                                            value: 'Item Não Encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar Serviço', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar Serviço', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $dadosItem = ui.item.servico;
                    }

                    $("#serv_valor").val(accounting.formatMoney($dadosItem.serv_valor, "R$ ", 2, ".", ","));
                    $("#serv_qtd").val('1,00');
                }
            });
        },

        deletarOrdemServico: function ($this) {
            if(typeof $this.data('os') !== 'undefined' && $this.data('os') !== ''){

                swal({
                    title: "Excluir Ordem de Serviço",
                    text: "Deseja realmente excluir esta Ordem de Serviço ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/oficina/ordem_servico/' + $this.data('os'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Ordem de Serviço", "Ordem de Serviço excluida com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir Ordem de Serviço", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Ordem de Serviço", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },

        finalizarOS: function($event){
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '/oficina/ordem_servico/finalizar_os',
                data: {},
                success: function (data) {
                    let $modal = $("#modal-info");

                    if(!$modal.find('.modal-dialog').hasClass('modal-xl')){
                        $modal.find('.modal-dialog').addClass('modal-xl');
                    }

                    $modal.find('.modal-title').text('FINALIZAR ORDEM DE SERVIÇO');
                    $modal.find('.modal-body').addClass('no-padding').html(data.view);

                    $modal.modal({
                        backdrop: 'static',
                        keyboard: false
                    });

                    jQueryOSFinalizar.init();
                },
                error: function (xhr) {

                }
            });
        },

        salvarOS: function($event){
            let dadosSalvar = {};


            if (Modernizr.localstorage) {
                if (sessionStorage.getItem("produto_os")) {
                    let produtos_OS = JSON.parse(sessionStorage.getItem('produto_os'));
                    let produtos = [];

                    $.map(produtos_OS, function ( item, index) {
                        produtos.push({
                            prod_id: item.produto.prod_id,
                            prod_valor: item.produto.produto_valores ? item.produto.produto_valores.prva_preco_venda : item.osrp_prod_valor,
                            prod_qtd: parseFloat(item.quantidade ? item.quantidade : item.osrp_prod_qtd),
                        });
                    });


                    dadosSalvar.produto     = produtos;
                }

                if (sessionStorage.getItem("servico_os")) {
                    let servicos_OS = JSON.parse(sessionStorage.getItem('servico_os'));
                    let servicos = [];


                    $.map(servicos_OS, function (item, index) {
                        servicos.push({
                            serv_id: item.serv_id,
                            serv_valor: item.servico ? item.servico.serv_valor : item.osrs_valor_servico,
                            serv_qtd: parseFloat(item.quantidade ? item.quantidade : item.osrs_qtd_servico),
                        });
                    });


                    dadosSalvar.servicos     = servicos;
                }

                dadosSalvar.id_cliente = $('#id_cliente').val();
                dadosSalvar.id_veiculo = $('#id_veiculo').val();
                dadosSalvar.checklist_id = $('#checklist_id').val();
                dadosSalvar.situacao = $('#situacao').val();
                dadosSalvar.dt_entrega = $('#dt_entrega').val();
                dadosSalvar.mecanico = $('#mecanicos').val();
                dadosSalvar.observacao = $("#observacao").val();
                dadosSalvar.desc_servico = $("#descricao_servico").val();

                dadosSalvar.total_os = totalOS;
                dadosSalvar.totalProduto  = valorTotalProduto;
                dadosSalvar.totalservico  = valorTotalServico;

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: $('#form_ordem_servico').attr('method'),
                    dataType: 'json',
                    url: $('#form_ordem_servico').attr('action'),
                    data: dadosSalvar,
                    beforeSend: function(data){
                        $("#venda-btn-finalizar").html('<i class="fa fa-spinner fa-spin"></i> Salvando').attr('disabled', 'disabled');
                    },
                    success: function(data){
                        $("#venda-btn-finalizar").html('<i class="fa fa-check"></i> Finalizar').removeAttr('disabled');

                        if(data.success){
                            swal({
                                title: 'Ordem de Serviço',
                                text: "Dados salvos com sucesso. \nDeseja imprimir essta ordem de serviço ?",
                                icon: "success",
                                buttons: ["Não", "Sim"],
                            }).then((isConfirm) => {
                                if (isConfirm) {
                                    var win = window.open((data.impressao), '_blank');
                                    if (win) {
                                        //Browser has allowed it to be opened
                                        win.focus();
                                    }
                                }

                                sessionStorage.clear();

                                location.reload();
                            });

                        } else {
                            swal('Ordem de Serviço', data.alert, {icon: "warning"});
                        }
                    },
                    error: function(xhr){
                        $("#venda-btn-finalizar").html('<i class="fa fa-check"> Finalizar</i>').removeAttr('disabled');

                        console.log(xhr);
                    }
                })
            }
        },

        getProduto: function () {
            return valorTotalProduto;
        },

        getServico: function () {
            return valorTotalServico;
        }

    }
})(jQuery);
