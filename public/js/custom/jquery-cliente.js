$(function(){
    jQueryCliente.init();
});

let jQueryCliente = (function(){
    let $table = '<table id="tabela-cliente" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th>ID</th>'
                            +'<th>Tipo do Cliente</th>'
                            +'<th>Situação</th>'
                            +'<th>Nome / Razão Social</th>'
                            +'<th>CPF / CNPJ</th>'
                            +'<th>Contato</th>'
                            +'<th class="pull-center">Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    return{
        init: function(){
            if($('.table-cliente-listar').length) {
                let $setTable = $('.table-cliente-listar');

                $setTable.empty();

                $setTable.html($table);

                this.listarCliente();
            }
        },
        listarCliente: function(){
            $("#tabela-cliente").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/empresa/cliente/tabela'
                },
                columns: [
                    { data: "clie_id"},
                    { data: "clie_tipo"},
                    { data: "clie_situacao"},
                    { data: "clie_nome_razao"},
                    { data: "clie_cpf_cnpj"},
                    { data: "clie_contato"},
                    { data: "action" },
                ]
            });
        },
        carregarTipoCliente: function($event){

            if($event.val() === PESSOA_FISICA){
                if($(".form-fisica").hasClass('hidden')){
                    $(".form-fisica").removeClass('hidden');
                    $(".form-juridica").addClass('hidden');
                } else {
                    $(".form-juridica").addClass('hidden');
                }
            } else if($event.val() === PESSOA_JURIDICA){
                if($(".form-juridica").hasClass('hidden')){
                    $(".form-juridica").removeClass('hidden');
                    $(".form-fisica").addClass('hidden');
                } else {
                    $(".form-fisica").addClass('hidden');
                }
            }
        },
        deletarCliente: function($event){
            if(typeof $event.data('cliente') !== 'undefined' && $event.data('cliente') !== ''){
                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/empresa/cliente/' + $event.data('cliente'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Cliente", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Excluir Cliente", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Cliente", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        autocomplete: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){

                        if(request.term.length === 2 && /\s/g.test(request.term)){
                            request.term = request.term.replace(/\s/g, '*');
                        }

                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/empresa/cliente",
                            data: {
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.cliente.length){

                                        response($.map(data.cliente, function (item) {

                                            let nomeRazaoSocial = (item.clie_nome_fantasia) ? item.clie_nome_fantasia : item.clie_nome_razao_social;

                                            return {
                                                label: nomeRazaoSocial,
                                                value: nomeRazaoSocial,
                                                id: item.clie_id
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Adicionar Cliente',
                                            value: 'Adicionar Cliente',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Cliente', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Cliente', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $("#id_cliente").val(ui.item.id);
                    }else{
                        $.ajax({
                            type: 'get',
                            dataType: 'json',
                            url: '/oficina/checklist/interno/cadastrar_cliente',
                            success: function(data){
                                if(data.success){
                                    let $modal = $('#modal-info');

                                    $modal.find('.modal-body').empty();
                                    $modal.find('.modal-footer').empty();

                                    $modal.find('.modal-title').html('Cadastrar Cliente');

                                    $modal.find('.modal-dialog').addClass('modal-lg');

                                    $modal.find('.modal-body').html(data.view);
                                    // $modal.find('.modal-footer').html('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>' +
                                    //     '<button class="btn btn-primary" data-title="Salvar Cliente" data-dismiss="modal" onclick="this.salvarCliente($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>');


                                    $modal.modal('show');
                                }
                            },
                            error: function(xhr){
                                swal("Editar Mecânica", xhr.responseJSON.message, {icon: "danger"});
                            }
                        });
                    }
                }
            });
        }
    }
})(jQuery);
