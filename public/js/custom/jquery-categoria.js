$(function(){
    jQueryCategoria.init();
});

let jQueryCategoria = (function(){
    let $table = '<table id="tabela-categoria" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th data-hide="phone">ID</th>'
                            +'<th data-class="expand"> Título</th>'
                            +'<th data-hide="phone"> Descrição</th>'
                            +'<th>Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';
    return {
        init: function () {
            if($('.table-categoria-listar').length) {
                let $setTable = $('.table-categoria-listar');

                $setTable.empty();

                $setTable.html($table);

                this.listarCategoria();
            }
        },
        listarCategoria: function(){

            $("#tabela-categoria").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/estoque/categoria/tabela'
                },
                columns: [
                    { data: "cate_id" },
                    { data: "cate_titulo" },
                    { data: "cate_descricao" },
                    { data: "action" },
                ]
            });
        },
        deletarCategoria: function($event){
            if(typeof $event.data('categoria') !== 'undefined' && $event.data('categoria') !== ''){
                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/estoque/categoria/' + $event.data('categoria'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Categoria", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Excluir Categoria", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Categoria", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        editarCategoria: function($event){
            let $objForm = $("#form-categoria");

            /* Trocando Valores da URL */
            $objForm.attr('action', "/estoque/categoria/" + $event.data('categoria'));

            /* Trocando o metodo de envio */
            $objForm.attr('method', 'put');

            /* Alterando Titulo */
            $objForm.closest('article').find('header > h2').text('Categoria - Editar Categoria');

            /* Carregando Dados */
            $("[name='categoria_titulo']").val($event.closest('tr').find('td').eq(1).text()).focus(); // Titulo
            $("[name='categoria_descricao']").val($event.closest('tr').find('td').eq(2).text()); // Descricao

            /* Alterar mensagens do Formulario */
            $objForm.find('button.btn-primary').attr('data-title', 'Atualizar Categoria');
            $objForm.find('button.btn-primary').attr('data-loading-text', 'Atualizando Dados...');
        }
    }
})(jQuery);
