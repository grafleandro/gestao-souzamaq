// url base
var base_url = $("body").data('url');

/*Certificado */
let csrfToken = $('meta[name="csrf-token"]').attr('content');

/* Loading */
var loading = loading || (function ($) {
    'use strict';

    // Creating modal dialog's DOM
    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">'
        +'<div class="modal-dialog modal-m">'
        +'<div class="modal-content">'
        +'<div class="modal-header">'
        +'<h3 style="margin:0;"></h3>'
        +'</div>'
        +'<div class="modal-body">'
        +'<div class="progress progress-striped active" style="margin-bottom:0;">'
        +'<div class="progress-bar" style="width: 100%"></div>'
        +'</div>'
        +'</div>'
        +'</div>'
        +'</div>'
        +'</div>');

    return {
        /**
         * Opens our dialog
         * @param message Custom message
         * @param options Custom options:
         * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
            // Assigning defaults
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Loading';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
            }, options);

            // Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            // Adding callbacks
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            // Opening dialog
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        }
    };

})(jQuery);

function removerMaskDinheiro (valor){
    return parseFloat(valor.replace('R$', '').replace('.', '').replace(',', '.'));
}

/* Validacao Input */
$(":input").blur(function(event){
    /* Capturando dados do Input */
    let formGroup = $(this).closest('section');
    let label = formGroup.find('.label');

    if(formGroup.hasClass('has-error') && $(this).val().length){
        label.prepend('<i class="fa fa-check"></i> ');
        formGroup.removeClass('has-error').addClass('has-success');
        formGroup.find('.help-block').remove();
    }
});


(function ($) {
    $.fn.serializeFormJSON = function () {

        let o = {};
        let a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $.fn.clearForm = function () {
        let a = this.serializeArray();

        $.each(a, function () {
            this.value = '';
        });
    };
})(jQuery);
