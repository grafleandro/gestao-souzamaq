$(function(){
    jQueryPermissao.init(colaboradorMenus);
});

let jQueryPermissao = (function(){
    let arrayMenu = [];

    return{
        init: function(colaboradorMenus){
            arrayMenu = colaboradorMenus;
        },
        save: function($event){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                dataType: 'json',
                url: '/empresa/colaborador/permissao',
                data: {permissao: arrayMenu, cola_id: $("#cola_id").val()},
                beforeSend: function(){
                    jQueryLoader.fadeIn();
                },
                success: function(data){
                    jQueryLoader.fadeOut();

                    if(data.success){
                        swal({
                            title: 'Permissão do Usuário',
                            text: 'Dados salvos com sucesso.',
                            icon: "success",
                            type: "success"
                        }).then(function(){
                            if(typeof data.reload === 'undefined' && !data.reload ) {
                                location.href = '/empresa/colaborador/listar';
                            }
                        });
                    }else{
                        swal($loading_title, data.alert, {icon: "warning"});
                    }
                },
                error: function(xhr){
                    jQueryLoader.fadeOut();
                }
            })
        },
        addMenu: function($event){

            if($event.attr('data-status') === 'true'){
                this.removeMenu(parseInt($event.val()));

                $event.attr('data-status', 'false');
            } else {
                if(this.checkExistKey(parseInt($event.val()))) {
                    arrayMenu.push(parseInt($event.val()));
                    $event.attr('data-status', 'true');
                }
            }

            this.addFather($event.closest('li').offsetParent());

            console.log(arrayMenu);
        },
        removeMenu: function(idMenu){
            arrayMenu = arrayMenu.filter(function(value, index){
                return (value !== idMenu);
            });
        },
        addFather: function($father){

            if(typeof $father.data('id') !== "undefined"){
                if(this.checkExistKey(parseInt($father.data('id')))) {
                    arrayMenu.push($father.data('id'));
                }
            }

            if($father.closest('li').length){
                this.addFather($father.closest('li').offsetParent());
            }

            return false;
        },
        checkExistKey: function(newKey){
            let status = true;

            $.each(arrayMenu, function(index, value){
                if(value === newKey){
                    status = false;
                    return false;
                }
            });

            return status;
        }
    }

})(jQuery);
