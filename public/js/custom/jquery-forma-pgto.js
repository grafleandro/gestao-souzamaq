
let jQueryFormaPgto = (function(){
    return{
        salvarForm: function($event){
            if(jQueryForm.validateInput($( $("#form-forma-pagamento")[0]).find(':input'), 'Forma de Pagamento')){
                return;
            }

            // action do form
            let $url = $("#form-forma-pagamento").attr('action');

            /* Metodo de Envio */
            let $method = (typeof $("#form-forma-pagamento").attr('method') !== 'undefined') ? $("#form-forma-pagamento").attr('method') : 'POST';

            let $dados_form = $("#form-forma-pagamento").serializeFormJSON();

            let $loading_title = 'Forma de Pagamento';

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: $method, //tipo que esta sendo enviado post ou get
                data: $dados_form,//conteudo do form
                url: $url, //endereço para onde vai
                dataType: 'json',//tipo do retorno
                beforeSend: function () {
                    loading.show($loading_title, {dialogSize: 'sm'});
                },
                success: function (data) {
                    loading.hide();

                    if(data.success){
                        swal({
                            title: $loading_title,
                            text: 'Dados salvos com sucesso.',
                            icon: "success",
                        }).then(function(){

                            $("#form-forma-pagamento").addClass('hidden').fadeOut('slow');

                            location.reload();
                        });
                    }else{
                        swal($loading_title, data.alert, {icon: "warning"});
                    }
                },
                error: function (xhr) {
                    loading.hide();

                    swal($loading_title, "Erro ao validar informação", {icon: "danger"});
                }
            });
        },
        possuiJuros: function($event){
            if($event.val() === BOOL_SIM){
                $("#form-juros-parcela").removeAttr('disabled').focus();
            } else if($event.val() === BOOL_NAO){
                $("#form-juros-parcela").attr('disabled', true);
            }
        },
        possuiMarquininha: function($event){
            if($event.val() === BOOL_SIM){
                $("#form-fornecedor").removeAttr('disabled').focus();
                $("#form-porcentagem-parcela").removeAttr('disabled');
            } else if($event.val() === BOOL_NAO){
                $("#form-fornecedor").attr('disabled', true);
                $("#form-porcentagem-parcela").attr('disabled', true);
            }
        },
        deletarFormaPgto: function ($this) {
            if(typeof $this.data('form-pgto') !== 'undefined' && $this.data('form-pgto,') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/configuracao/forma_pagamento/' + $this.data('form-pgto'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Forma de Pagamento", "Item excluido com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir Forma de Pagamento", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Usuário", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
    }
})(jQuery);
