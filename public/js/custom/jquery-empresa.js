$(function(){
    jQueryEmpresa.init();
});

let jQueryEmpresa = (function(){
    let $table = '<table id="tabela-empresa" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th data-hide="phone">ID</th>'
                            +'<th data-class="expand"> Nome</th>'
                            +'<th data-class="expand"> CNPJ</th>'
                            +'<th data-hide="phone"> Contato</th>'
                            +'<th>Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    return {
        init: function(){
            if($('.table-funcao-listar-empresa').length) {
                let $setTable = $('.table-funcao-listar-empresa');

                $setTable.empty();

                $setTable.html($table);

                this.listarEmpresa();
            }
        },
        listarEmpresa: function(){

            $("#tabela-empresa").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/empresa/empresa/tabela'
                },
                columns: [
                    { data: "empr_id" },
                    { data: "empr_nome" },
                    { data: "empr_cnpj" },
                    { data: "cont_id" },
                    { data: "action" },
                ]
            });
        },
        deletarEmpresa: function ($this) {
            if(typeof $this.data('empresa') !== 'undefined' && $this.data('empresa') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/empresa/empresa/' + $this.data('empresa'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Empresa", "Item excluido com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir Empresa", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Empresa", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },

        editarEmpresa: function($event){
            location.href = '/empresa/empresa/' + $event.data('empresa') + '/edit';
        },
    }
})(jQuery);
