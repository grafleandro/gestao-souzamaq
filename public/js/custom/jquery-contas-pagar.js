$(function(){
    jQueryContasPagar.init();
});


let jQueryContasPagar = (function(){
    let $table = '<table id="contas-pagar" class="display projects-table table table-striped table-bordered table-hover" cellspacing="0" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th>#</th>'
                            +'<th>Descrição</th>'
                            +'<th>Categoria</th>'
                            +'<th>Centro de Custo</th>'
                            +'<th>Data de Vencimento</th>'
                            +'<th>Valor</th>'
                            +'<th class="pull-center">Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';


    let objRowParcela = [];

    return{
        init: function(){
            if($('.table-contas-pagar').length) {
                let $setTable = $('.table-contas-pagar');

                $setTable.empty();

                $setTable.html($table);

                this.listarContasPagar();
            }
        },
        listarContasPagar: function(){

            $("#contas-pagar").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/financeiro/contas-pagar/tabela'
                },
                columns: [
                    { data: "copa_id"},
                    { data: "copa_descricao"},
                    { data: "copc_titulo"},
                    { data: "cocc_titulo"},
                    { data: "copa_dt_vencimento"},
                    { data: "copa_valor"},
                    { data: "action", className: 'pull-center'},
                ]
            });
        },
        visualizarConta: function($event){

            if(typeof $event.data('conta') !== 'undefined' && $event.data('conta')) {

                $.get('http://localhost:8080/financeiro/contas-pagar/?visualizar=' + $event.data('conta'), function (data) {

                    if (data.success) {
                        let $modal = $("#modal-info");

                        $modal.find('.modal-body').empty();
                        $modal.find('.modal-footer').empty();

                        $modal.find('.modal-dialog').addClass('modal-lg');

                        $modal.find('.modal-title').text('Visualizar Dados da Conta');

                        $modal.find('.modal-body').html(data.view);

                        $modal.find('.modal-footer').append('<button class="btn btn-primary"><i class="fa fa-print"></i> Imprimir</button>');
                        $modal.find('.modal-footer').append('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>');

                        $modal.modal('show');
                    }
                });
            }
        },
        possuiParcelas: function($event){
            console.log($event.val());
            if(parseInt($event.val())){
                $("#cp_qtd_parcelas").removeAttr('disabled').focus();
                $("#btn-gerar-parcelas").removeClass('hidden');
            } else {
                $("#cp_qtd_parcelas").attr('disabled', 'disabled');
                $("#btn-gerar-parcelas").addClass('hidden');
            }
        },
        deletarConta: function($event){
            if(typeof $event.data('conta') !== 'undefined' && $event.data('conta') !== ''){
                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/financeiro/contas-pagar/' + $event.data('conta'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Conta à Pagar", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Excluir Conta à Pagar", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Conta à Pagar", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        gerarParcelas: function($event){
            let qtdParcelas = $("#cp_qtd_parcelas").val();
            let valorTotal  = removerMaskDinheiro($("#cp_valor_total").val());
            let parcelas = (valorTotal/qtdParcelas);
            let dtVencimento= $("#cp_dt_vencimento").val();
            let vencimentos = new Date();

            $("#table-contas-pagar-parcelas").removeClass('hidden').find('tbody').empty();

            for(let i = 1; i <= qtdParcelas; i++){
                let objDataVenc = new Date($.datepicker.parseDate('dd/mm/yy', dtVencimento));

                let tagTr = '<tr>' +
                                '<td class="pull-center">'+ i +'/'+ qtdParcelas +'</td>'+
                                '<td class="pull-center">'+ accounting.formatMoney(parcelas, "R$ ", 2, ".", ",") +'</td>'+
                                '<td class="pull-center">'+ objDataVenc.toLocaleDateString() +'</td>'+
                                '<td class="pull-center"><button type="button" class="btn btn-default btn-circle" onclick="jQueryContasPagar.editarParcela($(this))"><i class="fa fa-edit"></i></button></td>';
                            '</tr>';

                $("#table-contas-pagar-parcelas>tbody").append(tagTr);

                vencimentos.setDate(objDataVenc.getDate()+30);

                dtVencimento = vencimentos.toLocaleDateString();
            }
        },
        atualizarValorParcelas: function($event){
            let valorTotal      = removerMaskDinheiro($("#cp_valor_total").val());
            let valorParcela    = removerMaskDinheiro($event.val());
            let qtdParcelas     = $("#cp_qtd_parcelas").val();
            let valorParcelaAtualizado = (valorTotal-valorParcela)/(qtdParcelas-1);
            let indexLinha      = $event.closest('tr').index();

            /* Atualiza os outros valores das parcelas */
            $event.closest('tbody').find('tr').map(function(index, value){
                if(index !== indexLinha){
                    $(value).find('td:eq(1)').text(accounting.formatMoney(valorParcelaAtualizado, "R$ ", 2, ".", ","))
                }
            });
        },
        editarParcela: function($event){
            let tr = $event.closest('tr');

            if(objRowParcela.length && objRowParcela.index() === tr.index()){
                return false;
            }

            let trChild = tr.find('td');

            let childValor   = $(trChild[1]).text();
            let childDtVenc  = $(trChild[2]).text();

            /** Caso exista algum item jah selecionado, este passa a esconder os inputs */
            this.bloquearCampoEditar($(tr));

            $(trChild[1]).html('<input class="form-control input-align-center" id="valor-parcela" name="valor-parcela" value="'+ childValor +'" onblur="jQueryContasPagar.atualizarValorParcelas($(this))">');
            $(trChild[2]).html('<label class="input-group">' +
                                    '<input type="text" id="vencimento-parcela" name="vencimento-parcela" class="form-control input-datepicker input-align-center" data-dateformat="dd/mm/yy" value="'+ childDtVenc +'">' +
                                    '<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>' +
                                '</label>');

            $('.input-datepicker').datepicker({
                language:'pt-BR',
                autoclose: true,
                todayHighlight: true
            });

            $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

            $("#vencimento-parcela").datepicker({setDate: new Date($.datepicker.parseDate('dd/mm/yy', childDtVenc))});

            $("#valor-parcela").maskMoney({
                prefix: 'R$ ',
                decimal: ',',
                thousands: '.'
            });

            objRowParcela = $(tr);
        },
        bloquearCampoEditar: function(objRowParcelaAtual){
            if(objRowParcela.length && objRowParcela.index() !== objRowParcelaAtual.index()){
                let trChild = objRowParcela.find('td');
                let childValor   = $(trChild[1]).find('input').val();
                let childDtVenc  = $(trChild[2]).find('input').val();

                $(trChild[1]).empty().text(childValor);
                $(trChild[2]).empty().text(childDtVenc);
            }
        },
        salvar: function($event){
            let dadosSalvar = $event.closest('form').serializeFormJSON();

            /* Selecionar dados das Parcelas */
            if(parseInt(dadosSalvar.cp_parcelas)){
                let parcelas = [];

                $("#table-contas-pagar-parcelas>tbody>tr").map(function(index, value){
                    parcelas.push({
                        valor: removerMaskDinheiro($(value).find('td:eq(1)').text()),
                        data_vencimento: $(value).find('td:eq(2)').text()
                    })
                });

                dadosSalvar.parcelas = parcelas;
            }

            if(jQueryForm.validateInput($event.closest('form').find(':input'), 'Contas à Pagar')){
                return;
            }

            /* Metodo de Envio */
            let $method = (typeof $event.closest('form').attr('method') !== 'undefined') ? $event.closest('form').attr('method') : 'POST';

            $.ajax({
                type: $method,
                dataType: 'json',
                url: $event.closest('form').attr('action'),
                data: dadosSalvar,
                beforeSend: function () {
                    loading.show('Contas à Pagar', {dialogSize: 'sm'});
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        swal({
                            title: 'Contas à Pagar',
                            text: 'Dados salvos com sucesso.',
                            icon: "success",
                        }).then(function(){
                            if(typeof data.url !== 'undefined' && data.url){
                                location.href = data.url;
                            } else {
                                location.reload();
                            }
                        });
                    } else {
                        swal("Contas à Pagar", data.alert, {icon: "warning"});
                    }
                },
                error: function(xhr){
                    loading.hide();

                    if(xhr.responseJSON.errors){
                        let message = "";

                        $.each(xhr.responseJSON.errors, function(index, error){

                            if(error.length){
                                $.each(error, function(k, text){
                                    message += text + '\n';
                                });
                            }
                        });

                        swal("Contas à Pagar", message, {icon: "warning"});
                    }
                }
            })
        }
    }
})(jQuery);
