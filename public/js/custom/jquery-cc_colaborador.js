$(function(){
    jQueryCCcolaborador.init();
});

let jQueryCCcolaborador = (function(){
    let $table = '<table id="tabela-extrato" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th class="pull-center">#</th>'
                            +'<th width="15%" class="pull-center">Tipo</th>'
                            +'<th>Origem</th>'
                            +'<th >Descrição</th>'
                            +'<th>Valor</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';


    let statusDisplayFiltro = false;

    return {
        init: function(){
            if($('.table-listar-cc-colaborador').length) {
                let $setTable = $('.table-listar-cc-colaborador');

                $setTable.empty();

                $setTable.html($table);

                this.listarExtrato();
            }

        },
        listarExtrato: function(filter = {}){
            $("#tabela-extrato").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/empresa/colaborador/conta_corrente/tabela',
                    data: filter
                },
                columns: [
                    { data: "cocc_id", className:"no-border"},
                    { data: "cocc_tipo", className:"no-border"},
                    { data: "cocc_origem", orderable: false, searchable: false, className: "pull-center no-border"},
                    { data: "cocc_descricao", className: "pull-center no-border"},
                    { data: "cocc_valor", className:"no-border" },
                ],

            });
        },
        filtro: function($event){
            if(statusDisplayFiltro){
                $("#form-extrato-filtro").fadeOut('slow');

                statusDisplayFiltro = false;
            } else {
                $("#form-extrato-filtro").fadeIn('slow');

                statusDisplayFiltro = true;
            }
        },
        filtroListar: function($event){
            let $form = $event.closest('form');

            this.listarExtrato($($form).serializeFormJSON());
        },

        salvaEmprestimo: function($this) {

            let $refresh_content = $this.data('refresh');
            let $text_action = $this.html();

            let $loading = $this.data('loading');
            let $loading_text = $this.data('loading-text');
            let $loading_title = $this.data('title');

            if(typeof $this.data('content') !== undefined && $this.data('content')){
                let formContent = $this.data('content');

                $form = $(formContent);
            }else{
                // localizar form se tiver
                $form = $this.closest('form');
            }

            if ($form[0]) {

                // dados que serão enviados para o metodo
                let $dados_form = $form.serialize();


                if(jQueryForm.validateInput($($form[0]).find(':input'), $loading_title)){
                    return;
                }

                // action do form
                let $url = $form.attr('action');

                /* Metodo de Envio */
                let $method = (typeof $form.attr('method') !== 'undefined') ? $form.attr('method') : 'POST';

                if (typeof $url !== 'undefined') {

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: $method, //tipo que esta sendo enviado post ou get
                        data: $dados_form,//conteudo do form
                        url: $url, //endereço para onde vai
                        dataType: 'json',//tipo do retorno
                        beforeSend: function () {
                            loading.show($loading_title, {dialogSize: 'sm'});
                        },
                        success: function (data) {
                            $this.html($text_action);

                            loading.hide();

                            if(data.success){
                                swal({
                                    title: 'Ordem de Serviço',
                                    text: "Dados salvos com sucesso. \nDeseja imprimir termo de emprestimo ordem de serviço ?",
                                    icon: "success",
                                    buttons: ["Não", "Sim"],
                                }).then((isConfirm) => {
                                    if (isConfirm) {
                                        if(data.impressao) {
                                            var win = window.open((data.impressao), '_blank');
                                            if (win) {
                                                win.focus();
                                            }
                                        }
                                    }


                                    location.reload();
                                });
                            }else{
                                swal($loading_title, data.alert, {icon: "warning"});
                            }
                        },
                        error: function (xhr) {
                            loading.hide();

                            swal($loading_title, "Erro ao validar informação", {icon: "danger"});
                        }
                    });
                } else {
                    swal("Erro", "Url não informada", {icon: "danger"});
                }
            } else {
                //console.log('Nenhum form foi encontrado!');
            }

        },

        detalhe: function($event){
            $.get( "/almoxarifado/detalhe?almo=" + $event.data('almox'), function(data) {
                if(data.success) {
                    let $modal = $("#modal-info");

                    $modal.find('.modal-body').empty();
                    $modal.find('.modal-footer').empty();

                    $modal.find('.modal-dialog').addClass('modal-lg');

                    $modal.find('.modal-title').text('Detalhamento de Empréstimo');

                    $modal.find('.modal-body').html(data.view);

                    $modal.find('.modal-footer').append('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>');

                    $modal.modal('show');
                }
            });
        },
        informacao: function($event){
            $.get( "/almoxarifado/informacao", function(data) {
                if (data.success) {
                    let $modal = $("#modal-info");

                    $modal.find('.modal-body').empty();
                    $modal.find('.modal-footer').empty();

                    $modal.find('.modal-dialog').addClass('modal-lg');

                    $modal.find('.modal-title').text('Informações');

                    $modal.find('.modal-body').addClass('no-padding').html(data.view);

                    $modal.find('.modal-footer').append('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>');

                    $modal.modal('show');
                }
            });
        },
        deletarAlmoxItem: function($event){
            if(typeof $event.data('almox') !== 'undefined' && $event.data('almox') !== ''){
                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/almoxarifado/' + $event.data('almox'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Almoxarifado", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Almoxarifado", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Almoxarifado", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
    }
})(jQuery);
