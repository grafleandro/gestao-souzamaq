$(function(){
    jQueryGrupo.init();
});

let jQueryGrupo = (function(){
    let $table = '<table id="tabela-grupo" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th>ID</th>'
                            +'<th>Titulo</th>'
                            +'<th>Descrição</th>'
                            +'<th>Qtd. Produtos</th>'
                            +'<th class="pull-center">Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    return {
        init: function(){
            if($('.table-grupo').length) {
                let $setTable = $('.table-grupo');

                $setTable.empty();

                $setTable.html($table);

                this.listarGrupo();
            }
        },
        listarGrupo: function(){
            $("#tabela-grupo").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/empresa/grupo/tabela'
                },
                columns: [
                    { data: "grup_id"},
                    { data: "grup_titulo"},
                    { data: "grup_descricao"},
                    { data: "qtd_produto"},
                    { data: "action" },
                ]
            });
        },
        deletarGrupo: function($event){
            if(typeof $event.data('grupo') !== 'undefined' && $event.data('grupo') !== ''){
                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/empresa/grupo/' + $event.data('grupo'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Grupo", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Excluir Grupo", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Grupo", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        novoGrupo: function($event){
            swal({
                icon: '/img/icon/plus.svg',
                title: 'Novo Grupo',
                text: 'Informa o nome do novo Grupo.',
                content: {
                    element: "input",
                    attributes: {
                        placeholder: "Novo Grupo",
                        type: "text",
                    },
                },
                buttons: ['Cancelar', 'Salvar']
            }).then(function(inputValue){
                let inputRef = $event.data('content');

                if(inputValue){
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "post",
                        dataType: 'json',
                        url: '/empresa/grupo',
                        data: {
                            gp_titulo: inputValue,
                            gp_descricao: ''
                        },
                        success: function(data){
                            if(data.success){
                                $("#"+inputRef).val(inputValue);
                                $("#grup_id").val(data.success);
                            }else{
                                swal('Novo Grupo', data.alert, {icon: "warning"});
                            }
                        },
                        error: function(xhr){
                            console.log(xhr);

                            swal('Novo Grupo', "Erro ao salvar o Novo Grupo!", {icon: "danger"});
                        }
                    })
                }
            });
        },
        autocomplete: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        if(request.term.length === 2 && /\s/g.test(request.term)){
                            request.term = request.term.replace(/\s/g, '*');
                        }

                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/empresa/grupo",
                            data: {
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.grupo.length){

                                        response($.map(data.grupo, function (item) {

                                            return {
                                                label: item.grup_titulo,
                                                value: item.grup_titulo,
                                                id: item.grup_id,
                                                descricao: item.grup_descricao,
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item Não Encontrato',
                                            value: 'Item Não Encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar Grupo', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar Grupo', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                open: function(event, ui){
                    $('.ui-autocomplete').append('<li class="autocomplete-ui-add" onclick="jQueryGrupo.novoGrupo($(this))" data-content="'+ $event.attr('id') +'"><i class="fa fa-plus-circle"></i> Novo Registro</a></li>');
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $("#grup_id").val(ui.item.id);
                    }
                }
            });
        }
    }
})(jQuery);
