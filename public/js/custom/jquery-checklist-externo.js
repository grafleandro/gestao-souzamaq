$(function(){
    jQueryCheckListExterno.init();
});

let jQueryCheckListExterno = (function(){
    let $table = '<table id="tabela-checklist-externo" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th data-hide="phone">OS</th>'
                            +'<th data-hide="phone">Cliente</th>'
                            +'<th data-class="expand"> Placa</th>'
                            +'<th data-class="expand"> Descrição</th>'
                            +'<th data-hide="phone"> Responsável</th>'
                            +'<th>Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    return {
        init: function(){
            if($('.table-listar-checklist-externo').length) {
                let $setTable = $('.table-listar-checklist-externo');

                $setTable.empty();

                $setTable.html($table);

               this.listarCheckListExterno();
            }
        },

        listarCheckListExterno: function(){

            $("#tabela-checklist-externo").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/oficina/checklist/externo/tabela'
                },
                columns: [
                    { data: "chex_num_os" },
                    { data: "chex_cliente" },
                    { data: "chex_placa" },
                    { data: "chex_descricao" },
                    { data: "chex_responsavel" },
                    { data: "action" },
                ]
            });
        },

        deletarCheckListExterno: function ($this) {
            if(typeof $this.data('externo') !== 'undefined' && $this.data('externo') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/oficina/checklist/externo/' + $this.data('externo'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir CheckList Externo", "Item excluido com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir CheckList Externo", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir CheckList Externo", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },

        // editarEmpresa: function($event){
        //     location.href = '/empresa/colaborador/' + $event.data('colaborador') + '/edit';
        // },
    }
})(jQuery);
