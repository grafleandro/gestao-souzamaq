$(function(){
    jQueryConta.initTables();
});

let jQueryConta = (function(){
    let $formPgto = null;
    let $contaBancaria = null;

    let actionFormaPgto = $('#form-forma-pagamento').attr('action');

    return{
        initTables: function(){
            this.tabelaFormaPgto();
            this.tabelaContaBancaria();
        },
        tabelaContaBancaria: function(){
            if($('#tabela-conta-bancaria').length) {

                $contaBancaria = $("#tabela-conta-bancaria").DataTable({
                    destroy: true,
                    searching: true,
                    serverSide: true,
                    processing: true,
                    ajax: {
                        url: '/configuracao/conta_bancaria/tabela'
                    },
                    columns: [
                        { data: "cocb_id" },
                        { data: "cocb_tipo_conta" },
                        { data: "banc_id" },
                        { data: "cocb_agencia", className: "pull-center" },
                        { data: "cocb_conta_corrente", className: "pull-center"},
                        { data: "cocb_conta_poupanca", className: "pull-center"},
                        { data: "action", className: "pull-center"},
                    ]
                });
            }
        },
        tabelaFormaPgto: function(){
            if($('#tabela-form-pgto').length) {

                $formPgto = $("#tabela-form-pgto").DataTable({
                    destroy: true,
                    searching: true,
                    serverSide: true,
                    processing: true,
                    ajax: {
                        url: '/configuracao/forma_pagamento/tabela'
                    },
                    columns: [
                        { data: "cofp_id" },
                        { data: "cofp_titulo" },
                        { data: "cofp_descricao" },
                        { data: "cofp_tipo_pgto", className: "pull-center" },
                        { data: "cofp_parcelas", className: "pull-center"},
                        { data: "action" },
                    ]
                });
            }
        },
        editarFormaPgto: function($event){
            let tr = $event.closest('tr');
            let dadosPgto = $formPgto.row(tr).data();

            $('#form-forma-pagamento').removeClass('hidden').fadeIn('slow').clearForm();

            $("#form-titulo").val(dadosPgto.cofp_titulo).focus();
            $("#form-descricao").val(dadosPgto.cofp_descricao);
            $("#form-parcelas").val(dadosPgto.cofp_parcelas);
            $("#form-limite-parcela").val( accounting.formatMoney(dadosPgto.cofp_limite_parcela, "R$ ", 2, ".", ",")).removeAttr('disabled');
            $("#form-juros-parcela").val(dadosPgto.cofp_juros_parcela);
            $("#form-porcentagem-parcela").val(dadosPgto.cofp_maquininha_fornecedor);

            if(dadosPgto.conta_bancaria !== null){
                $("#conta_bancaria").val(dadosPgto.conta_bancaria.cocb_id);
            } else {
                $("#conta_bancaria").val(0);
            }

            if(dadosPgto.cofp_maquininha_fornecedor){
                $("#form-porcentagem-parcela").removeAttr('disabled');
            }

            if(parseInt(dadosPgto.cofp_avista)){
                $("#form-tipo-pgto-aprazo").removeAttr('checked');
                $("#form-parcelas").attr('disabled', 'disabled');

                $("#form-tipo-pgto-avista").attr('checked', 'checked');
            } else {
                $("#form-tipo-pgto-avista").removeAttr('checked');
                $("#form-parcelas").removeAttr('disabled');

                $("#form-tipo-pgto-aprazo").attr('checked', 'checked');
            }

            if(parseInt(dadosPgto.cofp_troco)){
                $("#form-troco-sim").attr('checked', 'checked');
                $("#form-troco-nao").removeAttr('checked');
            } else {
                $("#form-troco-nao").attr('checked', 'checked');
                $("#form-troco-sim").removeAttr('checked');
            }

            if(parseInt(dadosPgto.cofp_juros_parcela)){
                $("#form-possui-juros-sim").attr('checked', 'checked');
                $("#form-possui-juros-nao").removeAttr('checked');
            } else {
                $("#form-possui-juros-nao").attr('checked', 'checked');
                $("#form-possui-juros-sim").removeAttr('checked');
            }

            if(parseInt(dadosPgto.cofp_maquininha_fornecedor)){
                $("#form-possui-maquininha-sim").attr('checked', 'checked');
                $("#form-possui-maquininha-nao").removeAttr('checked');
            } else {
                $("#form-possui-maquininha-nao").attr('checked', 'checked');
                $("#form-possui-maquininha-sim").removeAttr('checked');
            }

            /* Alterando o cabecalho do formulario */
            $('#form-forma-pagamento').attr('method', 'put');
            $('#form-forma-pagamento').attr('action', actionFormaPgto + '/' + dadosPgto.cofp_id);
        },
        desabilitarCampo: function($event){
            if(parseInt($event.val())){
                $("#prod-parcelas").attr('disabled', 'disabled');
            } else {
                $("#prod-parcelas").removeAttr('disabled').focus();
            }
        },
        possuiJuros: function($event){
            if($event.val() === BOOL_SIM){

            } else if($event.val() === BOOL_NAO){

            }
        }
    }

})(jQuery);
