let jQueryComissao = (function(){
    return {
        cotaEmpresa: function($event){
            let cotaComissao = parseFloat($event.val().replace(' %', '').replace(',', '.'));

            if(cotaComissao > 100){
                swal('Comissão', 'O valor da comissão, não pode exceder o total de 100%', 'warning');
                $event.val('').focus();

                return false;
            }

            let cotaEmpresa = (100-cotaComissao).toFixed(2).replace('.', ',') + "%";

            $("#cota_empresa").val(cotaEmpresa);
        },
        informacao: function($event){
            let $modal = $("#modal-info");

            $modal.find('.modal-footer').empty();
            $modal.find('.modal-body').empty();

            $modal.find('.modal-title').text('Informações - Comissão');
            let body = '<p class="align-text-center"><strong>COTAS</strong>: As COTAS são as partes nas quais serão destinadas a COMISSÃO e EMPRESA. Ao informar qual a porcentagem no qual a empresa deseja destinar ao serviço de comissão, este por sua vez será aplicado a todo e qualquer VALOR que esteja vinculado a COMISSÃO.</p>';
                body += '<p class="align-text-center">Por exemplo: A empresa deseja dedicar ao serviço de COMISSÃO 70% do valor, ficando para a empresa 30% (atenção, esse valor não refere-se ao lucro, mas sim o valor do PRODUTO/SERVIÇO).</p>';
                body += '<p class="align-text-center">Desta forma, ao aplicar-se está regra ao serviço de valor R$ 100,00, será destina aos colaboradores R$ 70,00 para que seja trabalhado a divisão ou não da comissão, com os envolvidos, ficando R$ 30,00 destinado a empresa.</p>';
                body += '<p class="align-text-center"><strong>Atenção</strong>, é importante destacar, que este valor não necessariamente precisa ser utilizar em seu total para comissão, só será destinado ao colaborador, o valor no qual foi definido em cadastro.</p>';
            $modal.find('.modal-body').html(body);
            $modal.find('.modal-footer').html('<button class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>');

            $modal.modal('show');
        }
    }
})(jQuery);
