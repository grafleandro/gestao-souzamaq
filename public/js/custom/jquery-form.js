
let jQueryForm = (function () {

    // url base
    let base_url = $("body").data('url');

    // constroller
    let $control;

    // metodo da controller
    let $method;

    // area para informação de carregamento
    let $loading;

    // texto de carregamento
    let $loading_text;

    // area onde será inserido o conteudo
    let $target_content;

    // url completa para a requisicao
    let $url;

    let $form;
    /* Passa um referencia dos dados que deveram ser salvos */
    let $content = null;
    /* Variaveis de comparacoes */
    let _KEYWORD    = "keyword";
    let _FORM_ADV   = "form-adv";

    return {
        send_form: function($this) {

            let $refresh_content = $this.data('refresh');
            let $text_action = $this.html();

            let $loading = $this.data('loading');
            let $loading_text = $this.data('loading-text');
            let $loading_title = $this.data('title');

            if(typeof $this.data('content') !== undefined && $this.data('content')){
                let formContent = $this.data('content');

                $form = $(formContent);
            }else{
                // localizar form se tiver
                $form = $this.closest('form');
            }

            if ($form[0]) {

                // dados que serão enviados para o metodo
                let $dados_form = $form.serialize();

                if($('#mecanicos').val() && !$('#cad_nome').length && !$('#cad_modelo').length){
                    $dados_form += '&colaboradores='+$('#mecanicos').val().toString();
                }

                if(jQueryForm.validateInput($($form[0]).find(':input'), $loading_title)){
                    return;
                }

                // action do form
                let $url = $form.attr('action');

                /* Metodo de Envio */
                let $method = (typeof $form.attr('method') !== 'undefined') ? $form.attr('method') : 'POST';

                if (typeof $url !== 'undefined') {

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: $method, //tipo que esta sendo enviado post ou get
                        data: $dados_form,//conteudo do form
                        url: $url, //endereço para onde vai
                        dataType: 'json',//tipo do retorno
                        beforeSend: function () {
                        	loading.show($loading_title, {dialogSize: 'sm'});
                        },
                        success: function (data) {
                            $this.html($text_action);

                            loading.hide();

                            if(data.success){
                                swal({
                                    title: $loading_title,
                                    text: 'Dados salvos com sucesso.',
                                    icon: "success",
                                }).then(function(){
                                    if(typeof data.url !== 'undefined' && data.url){
                                        location.href = data.url;
                                    }else if(typeof data.reload !== 'undefined' && data.reload ) {
                                        location.reload();
                                    }
                                });
                            }else{
                                swal($loading_title, data.alert, {icon: "warning"});
                            }
                       },
                       error: function (xhr) {
                           loading.hide();

                           if(xhr.responseJSON.errors){
                               let alerta = '';

                               $.each(xhr.responseJSON.errors, function(i, errors){
                                   $.each(errors, function(j, message){
                                       alerta += message;
                                   });
                               });

                               swal($loading_title, alerta , {icon: "error"});
                           }
                       }
                    });
                } else {
                    swal("Erro", "Url não informada", {icon: "error"});
                }
            } else {
                //console.log('Nenhum form foi encontrado!');
            }

        },
        send_form_file: function($this) {
            let $refresh_content;
            let $text_action = $this.html();

            let $loading_text = $this.data('loading-text');
            let $loading_title = $this.data('title');

            if(typeof $this.data('content') !== undefined && $this.data('content')){
                let formContent = $this.data('content');

                $form = $(formContent);
            }else{
                // localizar form se tiver
                $form = $this.closest('form');
            }

            if ($form[0]) {
                let $dados_form = new FormData($form[0]);

                $refresh_content = $form.find($this.data('refresh'));
                $loading = $form.find($this.data('loading'));

                /* Metodo de Envio */
                let $method = (typeof $form.attr('method') !== 'undefined') ? $form.attr('method') : 'POST';

                $dados_form.append('_method', $method);

                /* action do form */
                let $url = $form.attr('action');

                if (typeof $url !== 'undefined') {

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'POST',
                        data: $dados_form,
                        url: $url,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                        	loading.show($loading_text, {dialogSize: 'sm'});
                        },
                        success: function (data) {
                        	 $this.html($text_action);

                             loading.hide();

                            if(data.success){
                                swal({
                                    title: $loading_title,
                                    text: 'Dados salvos com sucesso.',
                                    icon: "success",
                                    type: "success"
                                }).then(function(){
                                    location.reload();
                                });
                            }else{
                                swal($loading_title, data.alert, {icon: "warning"});
                            }
                        },
                        error: function (xhr) {
                            loading.hide();

                            let message = '';

                            if(xhr.responseJSON.errors){
                                $.each(xhr.responseJSON.errors, function(index, item){
                                    message += item[0] + '\n';
                                })
                            } else {
                                message = "Erro ao validar informação"
                            }

                            swal($loading_title, message, {icon: "error"});
                        }
                    });
                } else {
                    swal("Erro", "Url não informada", {icon: "error"});
                }
            } else {
                //console.log('Nenhum form foi encontrado!');
            }

        },
        validateInput: function($inputs, $title){
            let status = false;

            $.each($inputs, function(index, input){
                /* Capturando dados do Input */
                let formGroup = $(input).closest('section');
                let label = formGroup.find('label');

                if(input.required && !$(input).val().length){
                    /* Ativando alertas */
                    formGroup.addClass('has-error');

                    if(!formGroup.find('.help-block').length){
                        formGroup.append('<span class="help-block">Campo Obrigatório!</span>');
                    }

                    status = true;
                }else if(input.required && $(input).val().length && formGroup.hasClass('has-error')){
                    label.prepend('<i class="fa fa-check"></i> ');
                    formGroup.removeClass('has-error').addClass('has-success');
                    formGroup.find('.help-block').remove();
                }
            });

            if(status){
                swal($title, 'Você possui campos que precisam ser preenchidos.', {icon: "warning"});
            }

            return status;
        },
        imprimir:function ($this) {

            if(typeof $this.data('imprimi') !== 'undefined' && $this.data('imprimi') !== '' && typeof $this.data('url') !== 'undefined' && $this.data('url') !== '' ){

                $url = $this.data('url');

                $.ajax({
                    type: "get",
                    dataType: "json",
                    url: $url,
                    data:{
                        _token: csrfToken,
                        referecia: $this.data('imprimi')
                    },
                    beforeSend: function () {
                        loading.show('Gerando o relatório...', {dialogSize: 'sm'});
                    },
                    success: function (data) {
                        loading.hide();

                        let $modal = $('#modal-info');

                        $modal.find('.modal-body').empty();
                        $modal.find('.modal-footer').empty();

                        $modal.find('.modal-title').html('Visualização');

                        $modal.find('.modal-dialog').addClass('modal-lg');

                        $modal.find('.modal-body').html('<iframe  height="500px" width="100%" src="'+ data.pdf +'"></iframe>')

                        $modal.modal('show');
                    },
                    error: function ($xhr) {
                        console.log($xhr);
                    }
                });
            }
        },

    };
})(jQuery);
