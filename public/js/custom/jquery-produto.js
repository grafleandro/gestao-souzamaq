$(function(){
    jQueryProduto.init();
});

let jQueryProduto = (function(){
    let $table = '<table id="tabela-cliente" class="table table-bordered table-striped" width="100%">'
                +'<thead>'
                    +'<tr>'
                        +'<th>N.C.M</th>'
                        +'<th>Código de Barra</th>'
                        +'<th>Produto</th>'
                        +'<th>Valor de Venda</th>'
                        +'<th class="pull-center">Estoque</th>'
                        +'<th class="pull-center">Ação</th>'
                    +'</tr>'
                +'</thead>'
                +'<tbody>'
                +'</tbody>'
                +'</table>';

    return{
        init: function(){
            if($('.table-listar-produto').length) {
                let $setTable = $('.table-listar-produto');

                $setTable.empty();

                $setTable.html($table);

                this.listarProduto();
            }
        },
        listarProduto: function(){
            $("#tabela-cliente").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/empresa/produto/tabela'
                },
                columns: [
                    { data: "finc_cod_ncm"},
                    { data: "prod_cod_barra"},
                    { data: "prod_titulo"},
                    { data: "prva_preco_venda"},
                    { data: "prest_estoq_atual"},
                    { data: "action" },
                ]
            });
        },
        precoVenda: function($event){
            $("#prod_preco_sugerido").val('');
            $("#prod_markup").val('');
        },
        precoSugerido: function($event){
            let parent = this;

            setTimeout(function(){

                if($("#prod_markup").val().replace(/\D/g, '').length) {
                    let rg = /\,+\d/g;
                    let valor = $event.val();
                    let precoSugerido = 0;

                    if(!$("#prod_custo").val().replace(/\D/g, '').length){
                        swal({
                            title: 'Valor de Custo',
                            text: 'Você precisa informar o valor de Custo do Produto!',
                            icon: 'warning',
                            buttons: false,
                            timer: 3000,
                        });

                        $("#prod_custo").focus();
                        $event.val('');
                    }

                    if(rg.test(valor)){
                        /* Numero decimal (com a virgula) */
                        let valorPerc = valor.replace('%', '');
                        valorPerc = parseFloat(valorPerc.replace(',', '.'));

                        precoSugerido = parent.calcularPrecoSugerido($("#prod_custo").val().replace('R$', ''), valorPerc);
                    }else{
                        /* Numero Inteiro (sem a virgula) */
                        let valorPerc = parseInt(valor.replace('%', ''));

                        precoSugerido = parent.calcularPrecoSugerido($("#prod_custo").val().replace('R$', ''), valorPerc);
                    }

                    $("#prod_preco_sugerido").val(precoSugerido.toFixed(2)).maskMoney('mask');

                    /* Limpa o campo de Preco de Venda, deixando apenas um campo preenchido */
                    $("#prod_preco_venda").val('');
                    swal({
                        title: 'Preço Sugerido',
                        text: 'O Preço Sugerido é calculado com base no Markup informado. Caso este preço lhe satisfaz, NÃO PRECISE PREENCHER O PREÇO DE VENDA. Ao informar o Preço de Venda, o Preço Sugerido será ignorado.',
                        icon: 'warning',
                        button: 'Fechar'
                    });
                } else {
                    $("#prod_preco_sugerido").val('');
                }

            }, 200);
        },
        calcularPrecoSugerido: function(valorCusto, markup){
            let rg = /\,+\d/g;

            if(rg.test(valorCusto)){
                valorCusto = parseFloat(valorCusto.replace(',', '.'));

                return (valorCusto + (valorCusto * (markup / 100)));
            } else {
                valorCusto = parseInt(valorCusto);

                return (valorCusto + (valorCusto * (markup / 100)));
            }
        },
        markup: function(){
            swal({
                title: 'Saiba Mais',
                text: 'Markup ou Mark Up é um índice aplicado sobre o custo de um produto ou serviço para a formação do preço de venda',
                icon: 'info',
                buttons: 'Fechar',
            });
        },
        deletarProduto: function($event){
            if(typeof $event.data('produto') !== 'undefined' && $event.data('produto') !== ''){
                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/empresa/produto/' + $event.data('produto'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Produto", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Excluir Produto", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Produto", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        autocompleteProduto: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/empresa/produto",
                            data: {
                                type: 'prod',
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.produto.length){

                                        response($.map(data.produto, function (item) {

                                            return {
                                                label: item.prod_id + ' - ' + item.prod_titulo,
                                                value: item.prod_id+ ' - ' + item.prod_titulo,
                                                id: item.prod_id,
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item Não Encontrato',
                                            value: 'Item Não Encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar Produto', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar Produto', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        // item selecionado
                    }
                }
            });
        },
        autocompleteTribNCM: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/empresa/produto",
                            data: {
                                type: 'ncm',
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.ncm.length){

                                        response($.map(data.ncm, function (item) {

                                            return {
                                                label: item.finc_cod_ncm + ' - ' + item.finc_descricao,
                                                value: item.finc_cod_ncm + ' - ' + item.finc_descricao,
                                                id: item.finc_id,
                                                descricao: item.finc_descricao,
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item Não Encontrato',
                                            value: 'Item Não Encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar NCM', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar NCM', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $("#trib_ncm_codigo").val(ui.item.id);
                        $("#trib_ncm_desc").val(ui.item.descricao);
                    }
                }
            });
        },
        adicionarQuantidade: function($event){
            swal({
                title: 'Aumentar Estoque',
                text: 'O valor informar a baixo, será somado a quantidade do estoque atual.',
                content: {
                    element: "input",
                    attributes: {
                        placeholder: "Informar Quantidade",
                        type: "text",
                    },
                },
                buttons: ['Cancelar', 'Salvar']
            }).then((element) => {

                if (element) {
                    $.ajax({
                        // headers: { 'X-CSRF-TOKEN' : csrfToken },
                        type: "post",
                        dataType: "json",
                        url: '/empresa/produto/',
                        data:{
                            _token: csrfToken,
                            atualizar_estoque: 1,
                            prod_id: $event.data('produto'),
                            prod_estoq_atual: (parseInt($event.data('estoque'))+parseInt(element))
                        },
                        beforeSend: function () {
                            loading.show('Atualizando Dados...', {dialogSize: 'sm'});
                        },
                        success: function(data){
                            loading.hide();

                            if(data.success){
                                swal({
                                    title: 'Atualizando Produto',
                                    text: 'Item atualizado com sucesso!',
                                    icon: 'success',
                                    buttons: 'Fechar',
                                }).then(function(){
                                    location.reload();
                                });
                            }else{
                                swal("Atualizar Produto", data.alert, {icon: "warning"});
                            }
                        },
                        error: function (xhr) {
                            loading.hide();

                            swal("Atualizar Produto", xhr.responseJSON.message, {icon: "warning"});
                        }
                    });
                }
            });
        }
    }
})(jQuery);
