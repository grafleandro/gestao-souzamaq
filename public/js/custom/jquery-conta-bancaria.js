$(function(){

});

let jQueryContaBancaria = (function(){
    return {
        tipoConta: function($event){
            debugger;
            if($event.val() === CB_CONTA_CORRENTE){
                $("#cb-variacao").closest('section').addClass('hidden');
                $("#cb-conta-polpanca").closest('section').addClass('hidden');

                $("#cb-agencia").closest('section').removeClass('col-2').addClass('col-3');
                $("#cb-conta-corrente").closest('section').removeClass('hidden');
            } else if($event.val() === CB_CONTA_POUPANCA){
                $("#cb-conta-corrente").closest('section').addClass('hidden');

                $("#cb-agencia").closest('section').removeClass('col-3').addClass('col-2');
                $("#cb-variacao").closest('section').removeClass('hidden');
                $("#cb-conta-polpanca").closest('section').removeClass('hidden');
            }
        },
        autocompleteBanco: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        if(request.term.length === 2 && /\s/g.test(request.term)){
                            request.term = request.term.replace(/\s/g, '*');
                        }

                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/configuracao/conta_bancaria",
                            data: {
                                search_value: request.term,
                                autocomplete: 'banco'
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.bancos.length){

                                        response($.map(data.bancos, function (item) {

                                            return {
                                                label: item.banc_codigo + ' - ' + item.banc_titulo,
                                                value: item.banc_codigo + ' - ' + item.banc_titulo,
                                                id: item.banc_id,
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item Não Encontrato',
                                            value: 'Item Não Encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar Grupo', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar Grupo', "Erro ao validar informação", {icon: "error"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $("#banc-id").val(ui.item.id);
                    }
                }
            });
        }
    }
})(jQuery);
