$(function(){
    $.key('f1', function(event) {
        event.preventDefault();

        $('.nav-tabs a[href="#s1"]').tab('show');

        $('#nome_cliente').focus();
    });

    $.key('f2', function(event) {
        event.preventDefault();

        $('.nav-tabs a[href="#s2"]').tab('show');

        $('#servico').focus();
    });

    $.key('f3', function(event) {
        event.preventDefault();

        $('.nav-tabs a[href="#s3"]').tab('show');

        $('#produto').focus();
    });

    $.key('f4', function(event) {
        event.preventDefault();

        $('.nav-tabs a[href="#s4"]').tab('show');

        $('#observacao').focus();
    });

    $.key('f7', function(event) {
        event.preventDefault();

        $('#id_cliente').val('');
        $('#id_veiculo').val('');
        $('#checklist_id').val('');
        $('#nome_cliente').val('');
        $('#nome_veiculo').val('');
        $('#servico').val('');
        $('#serv_qtd').val('');
        $('#produto').val('');
        $('#prod_qtd').val('');
        $('#situacao').val('');
        $('#dt_entrega').val('');

        $.each($('#mecanicos').val(), function(index, item){
            $('#mecanicos').multiselect("deselectOption",item);
        });
        $('#mecanicos').val('');
        $("#observacao").val('');
        $("#descricao_servico").val('');

        $('#total_produto')[0].textContent = accounting.formatMoney(0, "R$ ", 2, ".", ",");
        $('#total_servico')[0].textContent = accounting.formatMoney(0, "R$ ", 2, ".", ",");
        $('#total_os')[0].textContent = accounting.formatMoney(0, "R$ ", 2, ".", ",");

        jQueryOrdemServico.totalOS = 0;

        jQueryOrdemServico.valorTotalProduto = 0;

        jQueryOrdemServico.valorTotalServico = 0;

        $("#tabela-ordem-servico-produtos tbody").empty();
        $("#tabela-ordem-servico tbody").empty();

        $('#num_servico')[0].textContent = '0';
        $('#num_produto')[0].textContent = '0';

        sessionStorage.clear();
    });

    $.key('f8', function(event) {
        event.preventDefault();

        jQueryOrdemServico.salvarOS();
    });

    $.key('f9', function(event) {
        event.preventDefault();

        jQueryOrdemServico.finalizarOS();
    });
});

