$(function(){
    jQueryMarca.init();
});

let jQueryMarca = (function(){
    let $table = '<table id="tabela-marca" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th>ID</th>'
                            +'<th>Titulo</th>'
                            +'<th>Descrição</th>'
                            +'<th>Qtd. Produtos</th>'
                            +'<th class="pull-center">Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    return {
        init: function(){
            if($('.table-marca').length) {
                let $setTable = $('.table-marca');

                $setTable.empty();

                $setTable.html($table);

                this.listarGrupo();
            }
        },
        listarGrupo: function(){
            $("#tabela-marca").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/empresa/marca/tabela'
                },
                columns: [
                    { data: "marc_id"},
                    { data: "marc_titulo"},
                    { data: "marc_descricao"},
                    { data: "qtd_produto"},
                    { data: "action" },
                ]
            });
        },
        deletarMarca: function($event){
            if(typeof $event.data('marca') !== 'undefined' && $event.data('marca') !== ''){
                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/empresa/marca/' + $event.data('marca'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Grupo", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Excluir Grupo", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Grupo", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        autocomplete: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term.length === 2 && /\s/g.test(request.term)){
                        request.term = request.term.replace(/\s/g, '*');
                    }

                    if(request.term !== 0){
                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/empresa/marca",
                            data: {
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.marca.length){

                                        response($.map(data.marca, function (item) {

                                            return {
                                                label: item.marc_titulo,
                                                value: item.marc_titulo,
                                                id: item.marc_id,
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item Não Encontrato',
                                            value: 'Item Não Encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar Marca', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar Marca', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $("#marc_id").val(ui.item.id);
                    }
                }
            });
        }
    }
})(jQuery);
