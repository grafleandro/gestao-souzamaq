$(function(){
    jQueryAlmoxarifado.init();
});

let jQueryAlmoxarifado = (function(){
    let $table = '<table id="tabela-almox" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th class="pull-center">#</th>'
                            +'<th width="15%" class="pull-center">Qtd. Disponível</th>'
                            +'<th>Item</th>'
                            +'<th width="15%">Unid. de Medida</th>'
                            +'<th>Finalidade</th>'
                            +'<th class="pull-center" width="15%">Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    let statusDisplayFiltro = false;

    return {
        init: function(){
            if($('.table-almoxarifado').length) {
                let $setTable = $('.table-almoxarifado');

                $setTable.empty();

                $setTable.html($table);

                this.listarAlmox();
            }

            this.finalidade($("input[name='almo_finalidade']:checked"));
        },
        listarAlmox: function(filter = {}){
            $("#tabela-almox").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/almoxarifado/tabela',
                    data: filter
                },
                columns: [
                    { data: "almo_id", className: "pull-center"},
                    { data: "almo_qtd", orderable: false, searchable: false, className: "pull-center"},
                    { data: "almo_titulo", name: "almo_titulo"},
                    { data: "unme_titulo", name: "unidadeMedida.unme_titulo"},
                    { data: "almo_finalidade"},
                    { data: "action", className: "pull-center", searchable: false},
                ],

            });
        },
        calcularTotal: function($event){
            if($("#almo_valor").val() && $("#almo_qtd").val()){
                let valorItem   = removerMaskDinheiro($("#almo_valor").val());
                let qtdItem     = parseInt($("#almo_qtd").val());
                let valorTotal  = (valorItem*qtdItem);

                $("#almo_valor_total").val(accounting.formatMoney(valorTotal, "R$ ", 2, ".", ","))
            } else {
                swal('Almoxarifado', 'Você precisa informar o Valor do Item e a Quantidade, para calcular o Valor Total!', 'warning');
            }
        },
        multa: function($event){
            $event.val($("#almo_valor").val());
        },
        filtro: function($event){
            if(statusDisplayFiltro){
                $("#form-almox-filtro").fadeOut('slow');

                statusDisplayFiltro = false;
            } else {
                $("#form-almox-filtro").fadeIn('slow');

                statusDisplayFiltro = true;
            }
        },
        filtroListar: function($event){
            let $form = $event.closest('form');

            this.listarAlmox($($form).serializeFormJSON());
        },
        imprimirRelatorio:function ($event) {

            let impressaoRelatorio = window.open('/almoxarifado/imprimir_relatorio?' + $('form').serialize(), '_blank');
            impressaoRelatorio.focus();
        },
        finalidade: function($event){

            if($event.val() === FINA_CONSUMO){
                $("#div-fina-consumo").removeClass('hidden');
                $("#div-fina-emprestimo").addClass('hidden');
            } else if($event.val() === FINA_EMPRESTIMO){
                $("#div-fina-consumo").addClass('hidden');
                $("#div-fina-emprestimo").removeClass('hidden');
            }
        },
        emprestimoQtd: function($event){
            let qtdItem = $event.data('item');
            let qtdInformado = $event.val();

            if(qtdItem < qtdInformado){
                swal({
                    title: 'Empréstimo',
                    text: 'A quantidade solicitado, é maior do que a quantidade disponível!\n\nQuantidade Disponivel: ' + qtdItem + '\nQuantidade Informado: ' + qtdInformado,
                    icon: 'warning',
                }).then(function(){
                    $event.val('').focus();
                });
            }
        },
        emprestimoDarBaixa: function($event){
            swal({
                title: "Dar Baixa",
                text: "Deseja dar Baixa neste Item ?",
                icon: "warning",
                buttons: ["Não", "Sim"],
            }).then((isConfirm) => {
                if (isConfirm) {

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'put',
                        dataType: 'json',
                        url: '/almoxarifado-emprestimo/' + $event.data('emprestimo'),
                        data: {
                            status: 0,
                            _method: 'pull'
                        },
                        beforeSend: function(){
                            jQueryLoader.fadeIn();
                        },
                        success: function(data){
                            jQueryLoader.fadeOut();

                            if(data.success){
                                swal({
                                    title: 'Dar Baixa',
                                    text: 'Baixa realizada com sucesso.',
                                    icon: "success",
                                }).then(function(){
                                    $event.closest('.detalhe-item').remove();

                                    location.reload();
                                });
                            }
                        },
                        error: function(xhr){
                            jQueryLoader.fadeOut();

                            console.log(xhr);
                        }
                    });


                    // $.post('/almoxarifado-emprestimo/' + $event.data('emprestimo'), {
                    //     _token: $('meta[name="csrf-token"]').attr('content'),
                    //     _method: 'pull',
                    // }, function(data){
                    //
                    // });
                }
            });
        },
        emprestimoDestino: function($event){
            if($event.val() === DEST_INTERNO){
                $("#almo_empre_responsavel").closest('section').addClass('hidden');
                $("#almo_empre_colaborador").closest('section').removeClass('hidden');
            } else if($event.val() === DEST_EXTERNO){
                $("#almo_empre_responsavel").closest('section').removeClass('hidden');
                $("#almo_empre_colaborador").closest('section').addClass('hidden');
            }
        },
        emprestimo: function($event){

            if($event.data('item') === 0){
                swal('Empréstimo', 'Não possui itens disponíveis!', 'warning');
                return false;
            }

            $.get( "/almoxarifado/emprestimo?almo=" + $event.data('almox') + "&item=" + $event.data('item'), function(data) {
                if(data.success) {
                    let $modal = $("#modal-info");

                    $modal.find('.modal-body').empty();
                    $modal.find('.modal-footer').empty();

                    $modal.find('.modal-title').text('Realizar Empréstimo');

                    $modal.find('.modal-body').html(data.view);

                    $modal.find('.modal-footer').append('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>');
                    $modal.find('.modal-footer').append('<button class="btn btn-primary" data-title="Empréstimo" data-loading-text="Salvando dados..." data-content="#form-almox-emprestimo" data-dismiss="modal" onclick="jQueryAlmoxarifado.salvaEmprestimo($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>');

                    $modal.modal('show');
                }
            });
        },

        salvaEmprestimo: function($this) {

            let $refresh_content = $this.data('refresh');
            let $text_action = $this.html();

            let $loading = $this.data('loading');
            let $loading_text = $this.data('loading-text');
            let $loading_title = $this.data('title');

            if(typeof $this.data('content') !== undefined && $this.data('content')){
                let formContent = $this.data('content');

                $form = $(formContent);
            }else{
                // localizar form se tiver
                $form = $this.closest('form');
            }

            if ($form[0]) {

                // dados que serão enviados para o metodo
                let $dados_form = $form.serialize();


                if(jQueryForm.validateInput($($form[0]).find(':input'), $loading_title)){
                    return;
                }

                // action do form
                let $url = $form.attr('action');

                /* Metodo de Envio */
                let $method = (typeof $form.attr('method') !== 'undefined') ? $form.attr('method') : 'POST';

                if (typeof $url !== 'undefined') {

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: $method, //tipo que esta sendo enviado post ou get
                        data: $dados_form,//conteudo do form
                        url: $url, //endereço para onde vai
                        dataType: 'json',//tipo do retorno
                        beforeSend: function () {
                            loading.show($loading_title, {dialogSize: 'sm'});
                        },
                        success: function (data) {
                            $this.html($text_action);

                            loading.hide();

                            if(data.success){
                                swal({
                                    title: 'Ordem de Serviço',
                                    text: "Dados salvos com sucesso. \nDeseja imprimir termo de emprestimo ordem de serviço ?",
                                    icon: "success",
                                    buttons: ["Não", "Sim"],
                                }).then((isConfirm) => {
                                    if (isConfirm) {
                                        if(data.impressao) {
                                            var win = window.open((data.impressao), '_blank');
                                            if (win) {
                                                win.focus();
                                            }
                                        }
                                    }


                                    location.reload();
                                });
                            }else{
                                swal($loading_title, data.alert, {icon: "warning"});
                            }
                        },
                        error: function (xhr) {
                            loading.hide();

                            swal($loading_title, "Erro ao validar informação", {icon: "danger"});
                        }
                    });
                } else {
                    swal("Erro", "Url não informada", {icon: "danger"});
                }
            } else {
                //console.log('Nenhum form foi encontrado!');
            }

        },

        detalhe: function($event){
            $.get( "/almoxarifado/detalhe?almo=" + $event.data('almox'), function(data) {
                if(data.success) {
                    let $modal = $("#modal-info");

                    $modal.find('.modal-body').empty();
                    $modal.find('.modal-footer').empty();

                    $modal.find('.modal-dialog').addClass('modal-lg');

                    $modal.find('.modal-title').text('Detalhamento de Empréstimo');

                    $modal.find('.modal-body').html(data.view);

                    $modal.find('.modal-footer').append('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>');

                    $modal.modal('show');
                }
            });
        },
        informacao: function($event){
            $.get( "/almoxarifado/informacao", function(data) {
                if (data.success) {
                    let $modal = $("#modal-info");

                    $modal.find('.modal-body').empty();
                    $modal.find('.modal-footer').empty();

                    $modal.find('.modal-dialog').addClass('modal-lg');

                    $modal.find('.modal-title').text('Informações');

                    $modal.find('.modal-body').addClass('no-padding').html(data.view);

                    $modal.find('.modal-footer').append('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>');

                    $modal.modal('show');
                }
            });
        },
        deletarAlmoxItem: function($event){
            if(typeof $event.data('almox') !== 'undefined' && $event.data('almox') !== ''){
                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/almoxarifado/' + $event.data('almox'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Almoxarifado", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Almoxarifado", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Almoxarifado", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
    }
})(jQuery);
