$(function(){

    $.key('f8', function(event) {
        event.preventDefault();

        jQueryVenda.cancelarVenda();
    });

    $.key('f9', function(event) {
        event.preventDefault();

        $('#submit').click()
    });

    jQueryCheckListInterno.init();
});

let jQueryCheckListInterno = (function(){
    let $table = '<table id="tabela-checklist-interno" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th data-hide="phone" class="text-center" style="width: 5%; text-align: center">OS</th>'
                            +'<th data-hide="phone" class="text-center">O.S Externa</th>'
                            +'<th data-hide="phone" class="text-center">Cliente</th>'
                            +'<th data-class="expand" class="text-center"> Veiculo / Placa</th>'
                            +'<th data-class="expand" class="text-center" style="width: 30%"> Serviço</th>'
                            +'<th data-hide="phone" class="text-center"> Status</th>'
                            +'<th class="text-center">Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    let statusDisplayFiltro = false;

    return {
        init: function(){
            if($('.table-listar-checklist-interno').length) {
                let $setTable = $('.table-listar-checklist-interno');

                $setTable.empty();

                $setTable.html($table);

               this.listarCheckListInterno();
            }
        },
        listarCheckListInterno: function(filter = {}){

            $("#tabela-checklist-interno").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/oficina/checklist/interno/tabela',
                    data: filter,
                },
                columns: [
                    { data: "ckli_id" },
                    { data: "ckli_os_externo" },
                    { data: "clie_id"},
                    { data: "veic_id" },
                    { data: "ckli_servico" },
                    { data: "ckli_status" },
                    { data: "action" },
                ]
            });
        },
        filtro: function($event){
            if(statusDisplayFiltro){
                $("#form-almox-filtro").fadeOut('slow');

                statusDisplayFiltro = false;
            } else {
                $("#form-almox-filtro").fadeIn('slow');

                statusDisplayFiltro = true;
            }
        },
        filtroListar: function($event){
            let $form = $event.closest('form');

            this.listarCheckListInterno($($form).serializeFormJSON());
        },
        deletarCheckListInterno: function ($this) {
            if(typeof $this.data('interno') !== 'undefined' && $this.data('interno') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/oficina/checklist/interno/' + $this.data('interno'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir CheckList Externo", "Item excluido com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir CheckList Externo", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir CheckList Externo", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        imprimirRelatorio:function ($this) {

            let impressaoRelatorio = window.open('/oficina/checklist/interno/imprimir_relatorio?' + $('form').serialize(), '_blank');
            impressaoRelatorio.focus();
        },
        cadastro: function($this) {

            let $refresh_content = $this.data('refresh');
            let $text_action = $this.html();

            let $loading = $this.data('loading');
            let $loading_text = $this.data('loading-text');
            let $loading_title = $this.data('title');

            if(typeof $this.data('content') !== undefined && $this.data('content')){
                let formContent = $this.data('content');

                $form = $(formContent);
            }else{
                // localizar form se tiver
                $form = $this.closest('form');
            }

            if ($form[0]) {

                // dados que serão enviados para o metodo
                let $dados_form = $form.serialize();

                if(jQueryForm.validateInput($($form[0]).find(':input'), $loading_title)){
                    return;
                }

                // action do form
                let $url = $form.attr('action');

                /* Metodo de Envio */
                let $method = (typeof $form.attr('method') !== 'undefined') ? $form.attr('method') : 'POST';

                if (typeof $url !== 'undefined') {

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: $method, //tipo que esta sendo enviado post ou get
                        data: $dados_form,//conteudo do form
                        url: $url, //endereço para onde vai
                        dataType: 'json',//tipo do retorno
                        beforeSend: function () {
                            loading.show($loading_title, {dialogSize: 'sm'});
                        },
                        success: function (data) {
                            $this.html($text_action);

                            loading.hide();

                            if(data.success){
                                let $modal = $('#modal-info');
                                swal({
                                    title: $loading_title,
                                    text: 'Dados salvos com sucesso.',
                                    icon: "success",
                                    timer: 2000,
                                }).then();

                                $modal.modal('hide');

                                $modal.find('.modal-body').empty();
                                $modal.find('.modal-footer').empty();

                                if(typeof data.veiculo !== undefined && data.veiculo) {
                                    $('#nome_veiculo').val(data.veiculo.veic_marca.toUpperCase() + ' / ' + data.veiculo.veic_modelo.toUpperCase() + '  -  ' + data.veiculo.veic_placa.toUpperCase());
                                    $("#id_veiculo").val(data.veiculo.veic_id);
                                }else{
                                    $('#nome_veiculo').val(data.servico.veic_marca.toUpperCase() + ' / ' + data.servico.veic_modelo.toUpperCase() + '  -  ' + data.servico.veic_placa.toUpperCase());
                                    $("#id_veiculo").val(data.servico.veic_id);
                                    $("#nome_cliente").val(data.servico.clie_nome_razao_social);
                                    $("#id_cliente").val(data.servico.clie_id);
                                }

                            }else{
                                swal($loading_title, data.alert, {icon: "warning"});
                            }
                        },
                        error: function (xhr) {
                            loading.hide();

                            swal($loading_title, "Erro ao validar informação", {icon: "danger"});
                        }
                    });
                } else {
                    swal("Erro", "Url não informada", {icon: "danger"});
                }
            } else {
                //console.log('Nenhum form foi encontrado!');
            }

        },
    }
})(jQuery);
