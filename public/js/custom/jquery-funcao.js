$(function(){
    jQueryFuncao.init();
});

let jQueryFuncao = (function(){
    let $table = '<table id="tabela-funcao" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th data-hide="phone">ID</th>'
                            +'<th data-class="expand"> Título</th>'
                            +'<th data-class="expand"> Sigla</th>'
                            +'<th data-hide="phone"> Descrição</th>'
                            +'<th class="pull-center">Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    return {
        init: function(){
            if($('.table-funcao-listar').length) {
                let $setTable = $('.table-funcao-listar');

                $setTable.empty();

                $setTable.html($table);

                this.listarCategoria();
            }
        },
        listarCategoria: function(){

            $("#tabela-funcao").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/configuracao/funcao/tabela'
                },
                columns: [
                    { data: "func_id" },
                    { data: "func_nome" },
                    { data: "func_sigla" },
                    { data: "func_descricao" },
                    { data: "action", className: 'pull-center'},
                ]
            });
        },
        deletarFuncao: function($event){
            if(typeof $event.data('funcao') !== 'undefined' && $event.data('funcao') !== ''){
                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/configuracao/funcao/' + $event.data('funcao'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Função", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Excluir Função", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Função", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        editarFuncao: function($event){
            let $objForm = $("#form-funcao");

            /* Trocando Valores da URL */
            $objForm.attr('action', "/configuracao/funcao/" + $event.data('funcao'));

            /* Trocando o metodo de envio */
            $objForm.attr('method', 'put');

            /* Alterando Titulo */
            $objForm.closest('article').find('header > h2').text('Função - Editar Função');

            /* Carregando Dados */
            $("[name='funcao_titulo']").val($event.closest('tr').find('td').eq(1).text()).focus(); // Titulo
            $("[name='funcao_descricao']").val($event.closest('tr').find('td').eq(3).text()); // Descricao

            /* Alterar mensagens do Formulario */
            $objForm.find('button.btn-primary').attr('data-title', 'Atualizar Função');
            $objForm.find('button.btn-primary').attr('data-loading-text', 'Atualizando Dados...');
        }
    }
})(jQuery);
