$(document).bind('keydown', function(event) {

    if( event.which === 121 ) {
        $("#os-btn-cancelar").trigger('click');
        event.preventDefault();
    }

    if( event.which === 122) {
        $('#os-desconto-porc').focus();
        event.preventDefault();
    }

    if( event.which === 123) {
        jQueryOSFinalizar.finalizarOS();
        event.preventDefault();
    }


});



let jQueryOSFinalizar = (function(){

    let indexPgto       = 0;
    let totalOS      = 0; // Valor total da OS
    let totalPagamento  = 0; // Montante que jah foi contabilizado a ser pago
    let totalDesconto   = 0;

    let objPagamento    = {};

    return{
        init: function(){
            totalOS = 0;

            if (Modernizr.localstorage) {
                if (sessionStorage.getItem("servico_os")) {

                    totalOS = $('#total_os')[0].textContent.replace(',', '.').replace('R$', '');
                    // $.each(vendaBalcao, function(index, item){
                    //     let desconto = '';
                    //     let totalValorItem = (parseFloat(item.quantidade)*item.produto.produto_valores.prva_preco_venda);
                    //
                    //     if(item.desconto.prefix === '%'){
                    //         desconto    ='-' + item.desconto.valor + '%';
                    //     } else if (item.desconto.prefix === 'R$'){
                    //         desconto    = '-R$ ' + item.desconto.valor;
                    //     }
                    //
                    //     totalVenda = (totalVenda + totalValorItem);
                    // });

                    // Informa o valor total da vendo nos campo A SER PAGO e o TOTAL A PAGAR
                    $("#os-fina-total").text(accounting.formatMoney(totalOS, "R$ ", 2, ".", ","));
                    $("#os-fina-subtotal").text(accounting.formatMoney(totalOS, "R$ ", 2, ".", ","));
                    $("#os-total-pagar").val(accounting.formatMoney(totalOS, "R$ ", 2, ".", ","));
                }
            }
        },

        selecionarFormaPgto: function($event){
            let optionSelected = $("option:selected", $event);

            /* Desbloqueia botao para poder calcular */
            $("#os-btn-confirmar").removeAttr('disabled');

            // Quando selecionado uma opcao de PGTO que possui parcelas, eh carregado a quantidade de BUTTONS para selecionar
            if(optionSelected.data('parcelas')){
                $("#os-parcelas").removeClass('hidden').empty();

                for(i = 1; i <= parseInt(optionSelected.data('parcelas')); i++){
                    $("#os-parcelas").append('<button type="button" class="btn btn-default btn-md btn-circle margin-right-20 margin-top-5" data-value="'+ i +'" onclick="jQueryOSFinalizar.calcularQtdParcelas($(this))">'+ i +'X</button>')
                }
            }else{
                $("#os-parcelas").addClass('hidden').empty();
            }

            $("#os-total-pagar").focus();
            this.calcularDescDinheiro($("#os-desconto-din"));
        },
        calcularPgto: function($event){
            let optionSelected = $("option:selected", $("#os-form-pgto"));
            let totalPagar  = this.osTotalPagar();
            // let trocoPgto   = parseInt(optionSelected.data('troco'));

            /* Caso exista desconto, eh acrescentado ao valor total */
            let totalDesconto = (typeof objPagamento.desconto !== "undefined") ? objPagamento.desconto : 0;

            totalPagamento = totalPagamento + totalDesconto;

            // Verifica se o valor que deseja pagar, eh maior do que o valor que ainda resta ser pago.
            if((totalOS - totalPagamento) < totalPagar){
                swal({
                    title: "Finalizar Ordem de Serviço",
                    text: "O valor informado é maior do que o valor a ser pago. E não é possível gerar troco para está forma de Pagamento !",
                    icon: "warning",
                    type: "warning"
                }).then(function(){
                    $("#os-total-pagar").focus();
                });
            } else {
                totalPagamento = (totalPagamento + totalPagar);

                let osTotalPagar = (totalOS - totalPagamento);

                // Verifica se possui Troco
                // if (trocoPgto) {
                //     this.calcularTroco(); // Calcula o valor do troco
                // }

                let parcelas = '';
                let parcelasX = '';

                // Verifica se a forma de pgto selecionada, possui parcelas a serem calculadas
                if($("#os-parcelas-qtd").val() !== 'undefined'){
                    parcelas = $("#os-parcelas-qtd").val() ? parseInt($("#os-parcelas-qtd").val()) : null;
                    parcelasX = (parcelas) ? '('+ parcelas +'x)' : '';

                    $("#os-parcelas-qtd").val('0');
                }

                $("#os-pagamento").closest('fieldset').removeClass('hidden');

                $("#os-pagamento").append('<div class="row no-margin-left sale-panel-details">' +
                                                '<div class="col-md-6">' +
                                                    '<button class="btn btn-danger btn-circle" data-troco="'+ 0.0 +'" data-value="'+ totalPagar +'" data-index="'+ indexPgto +'" onclick="jQueryVendaFinalizar.removePagamentos($(this))"><i class="fa fa-trash"></i></button>' +
                                                    '<span><strong> - ' + optionSelected.text() + '</strong><i>'+ parcelasX +'</i></span>' +
                                                '</div>' +
                                                '<div class="col-md-6 pull-center">' +
                                                    '<span> ' + accounting.formatMoney(totalPagar, "R$ ", 2, ".", ",") + '</span>' +
                                                '</div>' +
                                            '</div>');

                // Contabiliza os valores a SEREM PAGOS e o que JA FOI PAGO
                $("#os-fina-pagamentos").text(accounting.formatMoney(totalPagamento, "R$ ", 2, ".", ","));


                if(osTotalPagar <= 0){
                    $("#os-total-pagar").val(accounting.formatMoney(0, "R$ ", 2, ".", ",")).attr('disabled', 'disabled');
                    $("#os-btn-confirmar").attr('disabled', 'disabled');
                    $("#os-desconto-din").attr('disabled', 'disabled');
                    $("#os-desconto-porc").attr('disabled', 'disabled');
                    $("#os-parcelas").addClass('hidden').empty();
                } else {
                    $("#os-total-pagar").val(accounting.formatMoney(osTotalPagar, "R$ ", 2, ".", ","));
                }

                // Gravando dados
                if(!indexPgto){
                    objPagamento.formas_pgto = [];
                }

                objPagamento.formas_pgto[indexPgto] = {valor: totalPagar, tipo_pgto: parseInt(optionSelected.val()), parcela: parcelas};

                indexPgto++;
            }
        },
        calcularDescPorcet: function($event){
            let valor   = $event.val().replace(',', '.').replace('%', '');

            if(!valor){
                return false;
            }

            let desconto= (totalOS*(parseFloat(valor)/100));

            totalDesconto = (totalOS-(totalOS*(parseFloat(valor)/100)));

            $("#os-fina-desconto").text(accounting.formatMoney(desconto, "R$ ", 2, ".", ","));
            $("#os-fina-total").text(accounting.formatMoney(totalDesconto, "R$ ", 2, ".", ","));
            $("#os-total-pagar").val(accounting.formatMoney(totalDesconto, "R$ ", 2, ".", ","));
            $("#os-desconto-din").val(accounting.formatMoney(desconto, "R$ ", 2, ".", ","))

            objPagamento.desconto = parseFloat(desconto);
        },
        calcularDescDinheiro: function($event){
            let desconto = $event.val().replace(',', '.').replace('R$', '');

            if(!desconto){
                return false;
            }

            let valorPorcent = ((parseFloat(desconto)*100)/totalOS);

            totalDesconto = (totalOS-parseFloat(desconto));

            $("#os-fina-desconto").text(accounting.formatMoney(desconto, "R$ ", 2, ".", ","));
            $("#os-fina-total").text(accounting.formatMoney(totalDesconto, "R$ ", 2, ".", ","));
            $("#os-total-pagar").val(accounting.formatMoney(totalDesconto, "R$ ", 2, ".", ","));
            $("#os-desconto-porc").val(accounting.formatMoney(valorPorcent, {
                symbol: '%',
                format: '%v%s',
                decimal: ',',
                thousand: '.',
                precision: 2
            }));

            objPagamento.desconto = parseFloat(desconto);
        },
        calcularQtdParcelas: function($event){
            $("#os-parcelas-qtd").val($event.data('value'));
        },
        removePagamentos: function($event){
            let valor       = $event.data('value');
            let indexPgto   = $event.data('index');
            let vendaTotalPagar = this.vendaTotalPagar();

            if(!vendaTotalPagar){
                $("#venda-total-pagar").removeAttr('disabled', 'disabled');
                $("#venda-btn-confirmar").removeAttr('disabled', 'disabled');
                $("#venda-desconto-din").removeAttr('disabled', 'disabled');
                $("#venda-desconto-porc").removeAttr('disabled', 'disabled');
            }

            totalPagamento  = (totalPagamento-valor) + (typeof objPagamento.desconto !== "undefined") ? objPagamento.desconto : 0;

            // Recalcula o valor do Troco
            this.calcularTroco();

            // Contabiliza os valores a SEREM PAGOS e o que JA FOI PAGO com base na forma de PGTO que foi removida
            $("#venda-fina-pagamentos").text(accounting.formatMoney(totalPagamento, "R$ ", 2, ".", ","));
            $("#venda-total-pagar").val(accounting.formatMoney((totalVenda-totalPagamento), "R$ ", 2, ".", ","));

            // Exclui o item de Pagamento
            $event.closest('.sale-panel-details').remove();

            // Remove o item do vetor de pagamento
            objPagamento.formas_pgto.splice(objPagamento.formas_pgto.indexOf(indexPgto), 1);

            // Oculta novamente o campo de Pagamentos
            if(totalPagamento <= 0){
                $("#venda-pagamento").closest('fieldset').addClass('hidden');
            }
        },
        osTotalPagar: function(){
            let totalPagar  = $("#os-total-pagar").val().replace('R$', '').replace('.', '').replace(',', '.');

            return parseFloat(totalPagar);
        },
        finalizarOS: function($event){
            let dadosSalvar = {};

            if(typeof objPagamento.formas_pgto === "undefined" || !objPagamento.formas_pgto.length){
                swal({
                    title: 'Forma de Pagamento',
                    text: 'Você precisa selecionar uma Forma de Pagamento!',
                    icon: "warning",
                }).then(function(){
                    $("#os-form-pgto").trigger('click');
                });

                return false;
            }

            if (Modernizr.localstorage) {

                let pagamento   = [];

                if (sessionStorage.getItem("produto_os")) {
                    let produtos_OS = JSON.parse(sessionStorage.getItem('produto_os'));
                    let produtos = [];

                    $.map(produtos_OS, function ( item, index) {
                        produtos.push({
                            prod_id: item.produto.prod_id,
                            prod_valor: item.produto.produto_valores ? item.produto.produto_valores.prva_preco_venda : item.osrp_prod_valor,
                            prod_qtd: parseFloat(item.quantidade ? item.quantidade : item.osrp_prod_qtd),
                        });
                    });


                    dadosSalvar.produto     = produtos;
                }

                if (sessionStorage.getItem("servico_os")) {
                    let servicos_OS = JSON.parse(sessionStorage.getItem('servico_os'));
                    let servicos = [];


                    $.map(servicos_OS, function (item, index) {
                        servicos.push({
                            serv_id: item.serv_id,
                            serv_valor: item.servico ? item.servico.serv_valor : item.osrs_valor_servico,
                            serv_qtd: parseFloat(item.quantidade ? item.quantidade : item.osrs_qtd_servico),
                        });
                    });


                    dadosSalvar.servicos     = servicos;
                }

                dadosSalvar.id_cliente = $('#id_cliente').val();
                dadosSalvar.id_veiculo = $('#id_veiculo').val();
                dadosSalvar.checklist_id = $('#checklist_id').val();
                dadosSalvar.situacao = $('#situacao').val();
                dadosSalvar.dt_entrega = $('#dt_entrega').val();
                dadosSalvar.mecanico = $('#mecanicos').val();
                dadosSalvar.observacao = $("#observacao").val();
                dadosSalvar.desc_servico = $("#descricao_servico").val();

                dadosSalvar.total_os = totalOS;
                dadosSalvar.totalProduto  = jQueryOrdemServico.getProduto();
                dadosSalvar.totalservico  = jQueryOrdemServico.getServico();

                if($('#form_ordem_servico').attr('method') === 'put'){
                    dadosSalvar.edit = $("#form_ordem_servico").attr('action').split('/')[5];
                }



                $.each(objPagamento.formas_pgto, function(index, pgto){
                    pagamento.push({
                        valor: pgto.valor,
                        tipo_pgto: pgto.tipo_pgto,
                        parcela: pgto.parcela,
                        desconto: totalOS - totalDesconto
                    });
                });

                dadosSalvar.pagamento   = pagamento;
                dadosSalvar.total_pgto  = (totalDesconto) ? totalDesconto : totalOS ;

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'get',
                    dataType: 'json',
                    url: '/oficina/ordem_servico/create',
                    data: dadosSalvar,
                    beforeSend: function(data){
                        $("#os-btn-finalizar").html('<i class="fa fa-spinner fa-spin"></i> Salvando').attr('disabled', 'disabled');
                    },
                    success: function(data){
                        $("#os-btn-finalizar").html('<i class="fa fa-check"></i> Finalizar').removeAttr('disabled');

                        if(data.success){
                            swal({
                                title: 'Ordem de Serviço',
                                text: 'Finalizada com sucesso.',
                                icon: "success",
                                type: "success"
                            }).then(function(){
                                sessionStorage.clear();

                                location.reload();
                            });
                        } else {
                            swal('Ordem de Serviço', data.alert, {icon: "warning"});
                        }
                    },
                    error: function(xhr){
                        $("#os-btn-finalizar").html('<i class="fa fa-check"> Finalizar</i>').removeAttr('disabled');

                        console.log(xhr);
                    }
                })
            }
        },
    }
})(jQuery);
