$(function(){
    jQueryInventario.init();
});

let jQueryInventario = (function(){
    let $table = '<table id="tabela-inventario" class="table table-bordered table-striped align-vertical" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th class="pull-center">Arquivo</th>'
                            +'<th>Código de Barra</th>'
                            +'<th>Quantidade</th>'
                            +'<th>Valor Unitário</th>'
                            +'<th class="pull-center">Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    return {
        init: function(){
            if($('.table-inventario').length) {
                let $setTable = $('.table-inventario');

                $setTable.empty();

                $setTable.html($table);

                this.listarInventario();
            }
        },
        listarInventario: function(){
            $("#tabela-inventario").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/empresa/inventario/tabela'
                },
                columns: [
                    { data: "arquivo", name: "inve_arquivo", className: "pull-center"},
                    { data: "inve_cod_barra"},
                    { data: "inve_qtd"},
                    { data: "inve_valor_unit"},
                    { data: "action", className: "pull-center"},
                ]
            });
        },
        possuiManutencao: function($event){
            if($event.val() === BOOL_SIM){
                $("#inv_intevalo_manutencao").closest('section').removeClass('hidden').focus();
                $("#inv_ultima_manutencao").closest('section').removeClass('hidden').focus();
            } else {
                $("#inv_intevalo_manutencao").closest('section').addClass('hidden').focus();
                $("#inv_ultima_manutencao").closest('section').addClass('hidden').focus();
            }
        },
        possuiGarantia: function($event){
            if($event.val() === BOOL_SIM){
                $("#inv_periodo_garantia").closest('section').removeClass('hidden');
            } else {
                $("#inv_periodo_garantia").closest('section').addClass('hidden');
            }
        },
        deletarInventario: function ($this) {
            if(typeof $this.data('inventario') !== 'undefined' && $this.data('inventario') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/empresa/inventario/' + $this.data('inventario'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Item", "Item excluido com sucesso do Inventário!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir Item", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Item", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
    }
})(jQuery);
