$(function(){
    jQueryFabricante.init();
});

let jQueryFabricante = (function(){
    let $table = '<table id="tabela-fabricante" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th>Código</th>'
                            +'<th>Tipo do Fabricante</th>'
                            +'<th>Nome / Razão Social</th>'
                            +'<th>CPF / CNPJ</th>'
                            +'<th>Contato</th>'
                            +'<th class="pull-center">Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    return {
        init: function(){
            if($('.table-fabricante').length) {
                let $setTable = $('.table-fabricante');

                $setTable.empty();

                $setTable.html($table);

                this.listarCliente();
            }
        },
        listarCliente: function(){
            $("#tabela-fabricante").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/empresa/fabricante/tabela'
                },
                columns: [
                    { data: "fabr_id"},
                    { data: "fabr_tipo"},
                    { data: "fabr_nome_razao"},
                    { data: "fabr_cpf_cnpj"},
                    { data: "fabr_contato"},
                    { data: "action" },
                ]
            });
        },
        deletarFabricante: function($event){
            if(typeof $event.data('fabricante') !== 'undefined' && $event.data('fabricante') !== ''){
                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/empresa/fabricante/' + $event.data('fabricante'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Fabricante", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Excluir Fabricante", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Cliente", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        carregarTipo: function($event){

            if($event.val() === PESSOA_FISICA){
                if($(".form-fisica").hasClass('hidden')){
                    $(".form-fisica").removeClass('hidden');
                    $(".form-juridica").addClass('hidden');
                } else {
                    $(".form-juridica").addClass('hidden');
                }
            } else if($event.val() === PESSOA_JURIDICA){
                if($(".form-juridica").hasClass('hidden')){
                    $(".form-juridica").removeClass('hidden');
                    $(".form-fisica").addClass('hidden');
                } else {
                    $(".form-fisica").addClass('hidden');
                }
            }
        },
        autocomplete: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        if(request.term.length === 2 && /\s/g.test(request.term)){
                            request.term = request.term.replace(/\s/g, '*');
                        }

                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/empresa/fabricante",
                            data: {
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.fabricante.length){

                                        response($.map(data.fabricante, function (item) {

                                            return {
                                                label: item.fabr_nome_nome_fantazia,
                                                value: item.fabr_nome_nome_fantazia,
                                                id: item.fabr_id,
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item Não Encontrato',
                                            value: 'Item Não Encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar Fabricante', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar Fabricante', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $("#fabr_id").val(ui.item.id);
                    }
                }
            });
        }
    }
})(jQuery);
