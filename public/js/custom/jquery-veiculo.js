$(function(){
    jQueryVeiculo.init();
});

let jQueryVeiculo = (function(){
    let tableDetails = null;

    return{
        init: function(){
            let $parent = this;

            if($('#table-veiculo').length) {
                // clears the variable if left blank
                tableDetails = $('#table-veiculo').DataTable({
                    sDom: "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    ajax: "/oficina/veiculo/tabela",
                    bDestroy: true,
                    oLanguage: {
                        sSearch: '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                    },
                    columns: [
                        {
                            "class": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {data: "cliente"},
                        {data: "marca_modelo"},
                        {data: "placa"},
                        {data: "veic_chassi"},
                        {data: "action", className: 'pull-center'}
                    ],
                    fnDrawCallback: function (oSettings) {
                        runAllCharts()
                    }
                });
            }

            // Add event listener for opening and closing details
            $('#table-veiculo tbody').on('click', 'td.details-control', function () {
                let tr = $(this).closest('tr');
                let row = tableDetails.row( tr );

                if( row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    // Open this row
                    row.child( $parent.format(row.data()) ).show();
                    tr.addClass('shown');
                }
            });
        },
        format: function(row){
            // `d` is the original data object for the row
            return '<table cellpadding="5" cellspacing="0" border="0" class="table table-hover table-condensed table-responsive">'+
                        '<tr>'+
                            '<td style="width:100px">Cliente:</td>'+
                            '<td>'+ row.cliente +'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>Marca/Modelo:</td>'+
                            '<td>'+ row.marca_modelo +'</td>'+
                        '</tr>'+
                        '<tr>'+
                            '<td>Placa:</td>'+
                            '<td>'+ row.placa +'</td>'+
                        '</tr>'+
                        // '<tr>'+
                        //     '<td>Ultima Manutenção:</td>'+
                        //     '<td>12/02/2017</td>'+
                        // '</tr>'+
                    '</table>';
        },
        cadastrar: function(){
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '/oficina/veiculo/cadastrar',
                success: function(data){
                    if(data.success){
                        let $modal = $('#modal-info');

                        $modal.find('.modal-body').empty();
                        $modal.find('.modal-footer').empty();

                        $modal.find('.modal-title').html('Cadastrar Veículo');

                        $modal.find('.modal-dialog').addClass('modal-lg');

                        $modal.find('.modal-body').html(data.view);
                        $modal.find('.modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>')
                        $modal.find('.modal-footer').append('<button type="button" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Salvar</button>')

                        $modal.modal('show');
                    }
                },
                error: function(xhr){
                    swal("Editar Mecânica", xhr.responseJSON.message, {icon: "danger"});
                }
            });
        },
        deletarVeiculo: function($this){
            if(typeof $this.data('veiculo') !== 'undefined' && $this.data('veiculo') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/oficina/veiculo/' + $this.data('veiculo'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Veículo", "Item excluido com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir Veículo", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Veículo", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        autocomplete: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        if(request.term.length === 2 && /\s/g.test(request.term)){
                            request.term = request.term.replace(/\s/g, '*');
                        }
                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/oficina/veiculo",
                            data: {
                                search_value: request.term,
                                id_cliente: $('#id_cliente').val(),
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.veiculo.length){

                                        response($.map(data.veiculo, function (item) {
                                            return {
                                                label: item.veic_marca.toUpperCase() + ' / ' + item.veic_modelo.toUpperCase() + '  -  ' + item.veic_placa.toUpperCase(),
                                                value: item.veic_marca.toUpperCase() + ' / ' + item.veic_modelo.toUpperCase() + '  -  ' + item.veic_placa.toUpperCase(),
                                                cliente: (item.veiculo_cliente.clie_nome_fantasia) ? item.veiculo_cliente.clie_nome_fantasia : item.veiculo_cliente.clie_nome_razao_social,
                                                id_clie: item.veiculo_cliente.clie_id,
                                                id: item.veic_id
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Adicionar Veiculo',
                                            value: 'Adicionar Veiculo',
                                            id: 0,
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal($loading_title, data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal($loading_title, "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 7,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $("#id_veiculo").val(ui.item.id);
                        $("#nome_cliente").val(ui.item.cliente);
                        $("#id_cliente").val(ui.item.id_clie);

                    }else{
                        if ($('#id_cliente').val() > 0) {
                            $.ajax({
                                type: 'get',
                                dataType: 'json',
                                url: '/oficina/checklist/interno/cadastrar_veiculo',
                                data: {
                                    id_cliente: $('#id_cliente').val(),
                                },
                                success: function (data) {
                                    if (data.success) {
                                        let $modal = $('#modal-info');

                                        $modal.find('.modal-body').empty();
                                        $modal.find('.modal-footer').empty();

                                        $modal.find('.modal-title').html('Cadastrar Veiculo');

                                        $modal.find('.modal-dialog').addClass('modal-lg');

                                        $modal.find('.modal-body').html(data.view);

                                        $modal.modal('show');
                                    }
                                },
                                error: function (xhr) {
                                    swal("Editar Mecânica", xhr.responseJSON.message, {icon: "danger"});
                                }
                            });
                        }else{
                                swal('Cadastro de Veículo', "Para cadastrar um novo veículo é necessário selecionar um cliente", {icon: "danger"});
                        }
                    }
                }
            });
        },
    }
})(jQuery);
