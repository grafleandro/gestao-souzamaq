$(function(){
    jQueryTributacao.init();
});

let jQueryTributacao = (function(){

    let $table = '<table id="tabela-trib-ncm" class="table table-bordered table-striped" width="100%">'
                +'<thead>'
                    +'<tr>'
                        +'<th>N.C.N</th>'
                        +'<th>Descrição</th>'
                        +'<th>Qtd. Produtos</th>'
                        +'<th>Início da Vigência</th>'
                        +'<th class="pull-center">Ação</th>'
                    +'</tr>'
                +'</thead>'
                +'<tbody>'
                +'</tbody>'
                +'</table>';
    return{
        init: function(){
            if($('.table-listar-ncm').length) {
                let $setTable = $('.table-listar-ncm');

                $setTable.empty();

                $setTable.html($table);

                this.listarNCM();
            }
        },
        listarNCM: function(){
            $("#tabela-trib-ncm").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/configuracao/tributacao/tabela'
                },
                columns: [
                    { data: "finc_cod_ncm"},
                    { data: "finc_descricao"},
                    { data: "qtd_produto"},
                    { data: "finc_inicio_vigencia"},
                    { data: "action" },
                ]
            });
        },
        detalhe: function(){
            swal({
                html: true,
                title: "Tributação Viculado ao NCM",
                text: "O NCM é a Nomenclatura Comum do Mercosul. É um código composto por 8 ou 2 digitos estabelecido pelo governo brasileiro para identificar a natureza das mercadorias.",
                icon: "warning",
            });
        },
        deletarTributo: function($event){
            if(typeof $event.data('ncm') !== 'undefined' && $event.data('ncm') !== ''){
                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/configuracao/tributacao/' + $event.data('ncm'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir N.C.M", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Excluir N.C.M", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir N.C.M", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        autocompleteNCM: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        if(request.term.length === 2 && /\s/g.test(request.term)){
                            request.term = request.term.replace(/\s/g, '*');
                        }

                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/configuracao/tributacao",
                            data: {
                                type: 'ncm',
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.ncm.length){

                                        response($.map(data.ncm, function (item) {

                                            return {
                                                label: item.finc_cod_ncm + ' - ' + item.finc_descricao,
                                                value: item.finc_cod_ncm + ' - ' + item.finc_descricao,
                                                id: item.finc_id,
                                                descricao: item.finc_descricao,
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item Não Encontrato',
                                            value: 'Item Não Encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar NCM', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar NCM', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $("#trib-ncm-codigo").val(ui.item.id);
                        $("#trib-ncm-desc").val(ui.item.descricao);
                    }
                }
            });
        }
    }
})(jQuery);
