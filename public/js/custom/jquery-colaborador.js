$(function(){
    jQueryColaborador.init();
});

let jQueryColaborador = (function(){
    let $table = '<table id="tabela-colaborador" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th>ID</th>'
                            +'<th>Situação</th>'
                            +'<th>Nome</th>'
                            +'<th>CPF</th>'
                            +'<th>Contato</th>'
                            +'<th class="pull-center">Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    return {
        init: function(){
            if($('.table-listar-colaborador').length) {
                let $setTable = $('.table-listar-colaborador');

                $setTable.empty();

                $setTable.html($table);

               this.listarColaborador();
            }
        },
        listarColaborador: function(){

            $("#tabela-colaborador").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                searchDelay: 350,
                ajax: {
                    url: '/empresa/colaborador/tabela'
                },
                columns: [
                    { data: "cola_id",  },
                    { data: "cola_status" },
                    { data: "cola_nome" },
                    { data: "cola_cpf" },
                    { data: "cont_id" },
                    { data: "action" },
                ]
            });
        },
        deletarColaborador: function ($this) {
            if(typeof $this.data('colaborador') !== 'undefined' && $this.data('colaborador') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/empresa/colaborador/' + $this.data('colaborador'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Usuário", "Item excluido com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir Usuário", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Usuário", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        editarEmpresa: function($event){
            location.href = '/empresa/colaborador/' + $event.data('colaborador') + '/edit';
        },
        autocomplete: function($event){

            if(!$event.val().length)
            {
                $("#id_colaborador").val('');
            }

            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){

                        if(request.term.length === 2 && /\s/g.test(request.term)){
                            request.term = request.term.replace(/\s/g, '*');
                        }

                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/empresa/colaborador",
                            data: {
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.colaborador.length){

                                        response($.map(data.colaborador, function (item) {
                                            return {
                                                label: item.cola_nome,
                                                value: item.cola_nome,
                                                id: item.cola_id
                                            };
                                        }));
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Colaborador', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Colaborador', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $("#id_colaborador").val(ui.item.id);
                    }
                }
            });
        }
    }
})(jQuery);
