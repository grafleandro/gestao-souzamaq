$(".mask-cpf").focusout(function(){
    var element;
    element = $(this);
    element.unmask();
    cpf_cnpj = element.val().replace(/\D/g, '');

    element.mask("999.999.999-99");

}).trigger('focusout');

$(".mask-cnpj").focusout(function(){
    var element;
    element = $(this);
    element.unmask();
    element.val().replace(/\D/g, '');

    element.mask("99.999.999/99?99-99");

}).trigger('focusout');

$(".mask-tributo").focusout(function(){
    var element;
    element = $(this);
    element.unmask();
    element.val().replace(/\D/g, '');

    element.mask("99?9.9?9%");

}).trigger('focusout');


$(".mask-placa").focusout(function(){
    var element;
    element = $(this);
    element.unmask();
    element.val().replace(/\D/g, '');

    element.mask("aaa-9999");

}).trigger('focusout');


$(".telephone").focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    element.val().replace(/\D/g, '');

    element.mask("(99) 9999-9999");
}).trigger('focusout');

$(".cell_phone_1").focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
        element.mask("(99) 9 9999-999?9");
    } else {
        element.mask("(99) 9 999-9999?9");
    }
}).trigger('focusout');

$(".cell_phone_2").focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
        element.mask("(99) 9 9999-999?9");
    } else {
        element.mask("(99) 9 999-9999?9");
    }
}).trigger('focusout');

$(".mask-date").focusout(function(){
    let date, element;
    let year = (new Date).getFullYear();
    element = $(this);
    element.unmask();
    // date = element.val().replace(/\D/g, '');
    date = element.val().split('/');

    if(typeof date[0] !== "undefined" && date[0] === 2 && date[0] < 1 || date[0] > 31){
        swal('Calendário', 'O dia tem que ser Válido: 01 à 31');
    } else if(typeof date[1] !== "undefined" && date[1] === 2 && date[1] < 1 || date[1] > 12){
        swal('Calendário', 'O mês tem que ser Válido: 01 à 12');
    } else if(typeof date[2] !== "undefined" && date[2] === 4 && date[2] < 1990 || date[2] > year){
        swal('Calendário', 'O Ano tem que ser Válido!');
        element.val('').focus();
    }

    element.mask('99/99/9999');
}).trigger('focusout');

$(".mask-integer").keyup(function(){
    let integer = $(this).val().replace(/[^\d]/,'');
    $(this).val(integer);
});

$('.mask-percent').keyup(function(){
    let element = $(this);

    element.unmask();

    element.maskMoney({
        suffix: '%',
        decimal: ',',
    });
});

$('.mask-decimal').keyup(function(){
    let element = $(this);

    element.unmask();

    element.maskMoney({
        decimal: ',',
    });
});

if(typeof $(".mask-money").maskMoney !== "undefined"){
    $(".mask-money").maskMoney({
        prefix: 'R$ ',
        decimal: ',',
        thousands: '.'
    });
}

