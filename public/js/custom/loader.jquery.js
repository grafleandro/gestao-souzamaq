


let jQueryLoader = (function(){
    return {
        fadeIn: function(){
            $(".loader").removeClass('hidden').fadeIn('slow');
        },
        fadeOut: function(){
            $(".loader").addClass('hidden').fadeOut('slow');
        }
    }
})(jQuery);
