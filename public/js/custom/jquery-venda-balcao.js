$(function(){
    $.key('f2', function(event) {
        event.preventDefault();

        jQueryVenda.produtoBuscar();
    });

    $.key('f3', function(event) {
        event.preventDefault();

        jQueryVenda.produtoQuantidade();
    });

    $.key('f4', function(event) {
        event.preventDefault();

        jQueryVenda.produtoDesconto();
    });

    $.key('f6', function(event) {
        event.preventDefault();

        jQueryVenda.produtoAdicionar();
    });

    $.key('f8', function(event) {
        event.preventDefault();

        jQueryVenda.cancelarVenda();
    });

    $.key('f9', function(event) {
        event.preventDefault();

        jQueryVenda.finalizarVenda();
    });

    jQueryVenda.init();
});

let jQueryVenda = (function(){
    let totalVenda      = 0; // Valor total da compra
    let $produto        = {}; // Dados do Produto selecionado

    return{
        init: function($event, update = false){
            if(update){
                totalVenda = 0;
            }

            $("#table-vend-item tbody").empty();

            if (Modernizr.localstorage) {
                if(sessionStorage.getItem("venda")){
                    let vendaBalcao = JSON.parse(sessionStorage.getItem('venda'));
                    let desconto    = '';

                    $.each(vendaBalcao, function(index, item){
                        let totalValorItem = (parseFloat(item.quantidade)*item.produto.produto_valores.prva_preco_venda);

                        if(item.desconto.prefix === '%'){
                            desconto    ='-' + item.desconto.valor + '%';
                        } else if (item.desconto.prefix === 'R$'){
                            desconto    = '-R$ ' + item.desconto.valor;
                        }

                        $("#table-vend-item tbody").append('<tr data-produto="'+ item.produto.prod_id +'" onclick="jQueryVenda.produtoEditarTabela($(this))"> ' +
                                '<td>'+ accounting.formatMoney(item.quantidade, "", 2, ".", ",") +'</td>' +
                                '<td>'+ item.produto.prod_titulo +'</td>' +
                                '<td><span style="color: red">'+ desconto +'</span></td>' +
                                '<td class="pull-center">'+ accounting.formatMoney(totalValorItem, "R$ ", 2, ".", ",") +'</td>' +
                            '</tr>'
                        );

                        totalVenda = (totalVenda + totalValorItem);
                    });

                    $(".sale-total-price").find('value').text(accounting.formatMoney(totalVenda, "", 2, ".", ","));

                    this.produtoBuscar();
                }
            }
        },
        produtoBuscar: function($event){
            $produto = null;

            $("#vend_buscar").val("").trigger('click');
            $("#vend_prod_codigo").val("");
            $("#vend_qtd").val("").attr('readonly');
            $("#vend_valor_unit").val("");
            $("#vend_desconto").val("").attr('readonly');
            $("#vend_total").val("");

            $("#vend_buscar").focus();
        },
        produtoQuantidade: function($event){
            $("#vend_qtd").focus();
        },
        produtoDesconto: function($event){
            $("#vend_desconto").focus();
        },
        produtoAdicionar: function($event){
            $("#btn-vend-add").trigger('click');
        },
        produtoEditarTabela: function($event){
            if(sessionStorage.getItem("venda")) {
                let vendaBalcao = JSON.parse(sessionStorage.getItem('venda'));

                $.each(vendaBalcao, function(index, item){
                    if(item.produto.prod_id === $event.data('produto')){
                        let valorTotal = (item.produto.produto_valores.prva_preco_venda*parseFloat(item.quantidade));

                        $("#vend_buscar").val(item.produto.prod_id + ' - ' + item.produto.prod_titulo);
                        $("#vend_prod_codigo").val(item.produto.prod_id);
                        $("#vend_qtd").val(accounting.formatMoney(item.quantidade, "", 2, ".", ",")).removeAttr('readonly').focus();
                        $("#vend_valor_unit").val(accounting.formatMoney(item.produto.produto_valores.prva_preco_venda, "R$ ", 2, ".", ","));
                        $("#vend_desconto").val(accounting.formatMoney(0, "R$ ", 2, ".", ",")).removeAttr('readonly');
                        $("#vend_total").val( accounting.formatMoney(valorTotal, "R$ ", 2, ".", ",")).removeAttr('readonly');

                        $produto = item.produto;
                    }
                });
            }

            $("#btn-vend-add").addClass('hidden');
            $("#btn-vend-atualizar").removeClass('hidden');
            $("#btn-vend-excluir").removeClass('hidden');

            $event.closest('tr').remove();
        },
        vendaAdicionar: function($event){
            let desconto = $("#vend_desconto").val();
            let descPrefix  = '';
            let descValor   = 0;

            if(!$produto){
                swal('Adicionar Produto', 'Você não possui nenhum Produto selecionado!', {icon: "warning"});
            }

            if(jQueryVenda.atualizarProdutoTabela()){
                return false;
            }

            if($("#vend-tipo-desconto").data('tipo') === '%') {
                descValor   = desconto;
                desconto    ='-' + desconto + '%';
                descPrefix  = '%';
            } else if ($("#vend-tipo-desconto").data('tipo') === 'R$') {
                descValor   = desconto;
                desconto    = '-R$ ' + desconto;
                descPrefix  = 'R$';
            }

            $("#table-vend-item tbody").append('<tr data-produto="'+ $produto.prod_id +'" onclick="jQueryVenda.produtoEditarTabela($(this))">' +
                                                    '<td>'+ $("#vend_qtd").val() +'</td>' +
                                                    '<td>'+ $produto.prod_titulo +'</td>' +
                                                    '<td><span style="color: red">'+ desconto +'</span></td>' +
                                                    '<td class="pull-center">'+ accounting.formatMoney($produto.produto_valores.prva_preco_venda, "R$ ", 2, ".", ",") +'</td>' +
                                               '</tr>');

            /* Calcular o total da compra */
            let qtdProd  = ($("#vend_qtd").val().length) ? $("#vend_qtd").val().replace(',', '.') : 1;
            let subTotal = ($produto.produto_valores.prva_preco_venda * parseFloat(qtdProd));
            let total    = $("#vend-total-balcao").find('value').text().length ? parseFloat($("#vend-total-balcao").find('value').text().replace(',', '.')) : 0;
            let calcularValorTotal = (total+subTotal) + totalVenda;

            $("#vend-total-balcao").find('value').text(accounting.formatMoney(calcularValorTotal, "", 2, ".", ","));

            $(".sale-total-price").find('value').text(accounting.formatMoney(calcularValorTotal, "", 2, ".", ","));

            /* Adicionar produto a Session Storage */
            if (Modernizr.localstorage) {
                let venda = [];

                if(sessionStorage.getItem("venda")){
                    venda = JSON.parse(sessionStorage.getItem('venda'));

                    sessionStorage.removeItem("venda");
                }

                venda.push({
                    vend_id: $produto.prod_id,
                    produto: $produto,
                    desconto: {prefix: descPrefix, valor: descValor},
                    quantidade: $("#vend_qtd").val().replace(',', '.'),
                });

                sessionStorage.setItem("venda", JSON.stringify(venda));
            }

            this.produtoBuscar();
        },
        calcularSubTotalQtd: function($event){

            if(parseFloat($("#vend_qtd").val()) > $produto.produto_estoque.prest_estoq_atual){
                swal('Estoque', 'A quantidade informada, não possui em estoque!', {icon: "warning"});
                return false;
            }

            let subTotal = ($produto.produto_valores.prva_preco_venda*parseFloat($event.val().replace(',', '.')));

            $("#vend_total").val(accounting.formatMoney(subTotal, "R$ ", 2, ".", ","));
        },
        calcularSubTotalDesc: function($event){
            let qtdProd  = ($("#vend_qtd").val().length) ? $("#vend_qtd").val() : 1;
            let subTotal = ($produto.produto_valores.prva_preco_venda * parseFloat(qtdProd));

            if($("#vend-tipo-desconto").data('tipo') === 'R$'){
                let desc = parseFloat($.trim($event.val().replace('R$', '').replace(',', '.')));
                subTotal = (subTotal-desc);

            } else if($("#vend-tipo-desconto").data('tipo') === '%'){
                let desc = parseFloat($.trim($event.val().replace(',', '.')));
                subTotal = (subTotal-(subTotal*(desc/100)));
                debugger;
            }

            $("#vend_total").val(accounting.formatMoney(subTotal, "R$ ", 2, ".", ","));
        },
        mudarTipoDesconto: function($event){
            let tipoDesc = $("#vend-tipo-desconto").data('tipo');

            if($event.data('desconto') != tipoDesc){
                $("#vend-tipo-desconto").text($event.data('desconto'));
                $("#vend-tipo-desconto").data('tipo', $event.data('desconto'));
            }
        },
        finalizarVenda: function($event){
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '/venda/balcao/finalizar_venda',
                data: {},
                success: function (data) {
                    let $modal = $("#modal-info");

                    if(!$modal.find('.modal-dialog').hasClass('modal-xl')){
                        $modal.find('.modal-dialog').addClass('modal-xl');
                    }

                    $modal.find('.modal-title').text('FINALIZAR VENDA');
                    $modal.find('.modal-body').addClass('no-padding').html(data.view);

                    $modal.modal({
                        backdrop: 'static',
                        keyboard: false
                    });

                    jQueryVendaFinalizar.init();
                },
                error: function (xhr) {

                }
            });
        },
        cancelarVenda: function($event){
            let parent = this;
            swal({
                title: "Cancelar Venda",
                text: "Deseja realmente cancelar esta venda ?",
                icon: "error",
                buttons: ["Não", "Sim"],
            }).then((isConfirm) => {
                if (isConfirm) {
                    $("#table-vend-item tbody").empty();

                    parent.produtoBuscar();

                    sessionStorage.clear();
                }
            });
        },
        atualizarProdutoTabela: function(){
            if(sessionStorage.getItem("venda")) {
                let vendaBalcao = JSON.parse(sessionStorage.getItem('venda'));
                let objProduto  = {};
                let atualizar   = false;

                $.each(vendaBalcao, function (index, item) {
                    if(item.produto.prod_id === $produto.prod_id){
                        let desconto = $("#vend_desconto").val();
                        let descPrefix  = '';
                        let descValor   = 0;

                        if($("#vend-tipo-desconto").data('tipo') === '%') {
                            descValor   = desconto;
                            descPrefix  = '%';
                        } else if ($("#vend-tipo-desconto").data('tipo') === 'R$') {
                            descValor   = desconto;
                            descPrefix  = 'R$';
                        }

                        objProduto = {
                            vend_id: item.produto.prod_id,
                            produto: item.produto,
                            desconto: {prefix: descPrefix, valor: descValor},
                            quantidade: $("#vend_qtd").val().replace(',', '.'),
                        };

                        // vendaBalcao.splice(index, 1);
                        atualizar = true;

                    }
                });

                /* Atualizar os dados na Storage */
                if(atualizar) {
                    /* Remover Item selecionado */
                    vendaBalcao = vendaBalcao.filter(function(value, index){
                        return (value.produto.prod_id !== $produto.prod_id)
                    });

                    vendaBalcao.push(objProduto);

                    sessionStorage.removeItem("venda");
                    sessionStorage.setItem("venda", JSON.stringify(vendaBalcao));

                    $("#btn-vend-add").removeClass('hidden');
                    $("#btn-vend-atualizar").addClass('hidden');
                    $("#btn-vend-excluir").addClass('hidden');

                    this.init(null, true);
                }

                return atualizar;
            }
        },
        excluirProdutoTabela: function($excluirProduto){
            let parent = this;
            swal({
                title: "Excluir Produto",
                text: "Deseja realmente excluir este Produto?",
                icon: "error",
                buttons: ["Não", "Sim"],
            }).then((isConfirm) => {
                if (isConfirm) {

                    setTimeout(function(){
                        if(sessionStorage.getItem("venda")) {
                            let vendaBalcao = JSON.parse(sessionStorage.getItem('venda'));

                            vendaBalcao = vendaBalcao.filter(function(value, index){
                                return (value.produto.prod_id !== $produto.prod_id)
                            });

                            sessionStorage.removeItem("venda");
                            sessionStorage.setItem("venda", JSON.stringify(vendaBalcao));
                        }
                    },0);

                    $("#btn-vend-add").removeClass('hidden');
                    $("#btn-vend-atualizar").addClass('hidden');
                    $("#btn-vend-excluir").addClass('hidden');

                    parent.produtoBuscar();
                }
            });
        },
        autocompleteProduto: function($event){
            let parent = this;

            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/empresa/produto",
                            data: {
                                type: 'prod',
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.produto.length){

                                        response($.map(data.produto, function (item) {

                                            return {
                                                label: item.prod_id + ' - ' + item.prod_titulo,
                                                value: item.prod_id+ ' - ' + item.prod_titulo,
                                                id: item.prod_id,
                                                produto: item
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item Não Encontrato',
                                            value: 'Item Não Encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar Produto', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar Produto', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $produto = ui.item.produto;

                        $("#vend_prod_codigo").val(ui.item.produto.prod_id);
                        $("#vend_qtd").val(accounting.formatMoney(1.0, "", 2, ".", ",")).removeAttr('readonly').focus();
                        $("#vend_valor_unit").val(accounting.formatMoney(ui.item.produto.produto_valores.prva_preco_venda, "R$ ", 2, ".", ","));
                        $("#vend_desconto").val(accounting.formatMoney(0.0, "", 2, ".", ",")).removeAttr('readonly');
                        $("#vend_total").val(accounting.formatMoney(ui.item.produto.produto_valores.prva_preco_venda, "R$ ", 2, ".", ","))
                    }
                }
            });
        }
    }
})(jQuery);
