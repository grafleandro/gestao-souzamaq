$(document).ready(function(){
	// Limpa valores do formulário de cep.
	$("[name='cep']").val();
    // $("[name='street']").val();
    // $("[name='neighborhood']").val();
    $("[name='cidade']").val();
    $("[name='estado']").val();
    
	$("#cep").blur(function() {
		//Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "" && cep.length <= 8) {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                // $("[name='logradouro']").val("...")
                // $("[name='bairro']").val("...")
                $("[name='cidade']").val("...")
                $("[name='estado']").val("...")

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        // $("[name='logradouro']").val(dados.logradouro);
                        $("[name='bairro']").val(dados.bairro);
                        $("[name='cidade']").val(dados.localidade);
                        $("[name='estado']").val(dados.uf);
                        //$("#ibge").val(dados.ibge);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }

        $("#number").focus();
	});
});

function limpa_formulário_cep() {
	$("[name='cep']").val();
    // $("[name='logradouro']").val();
    $("[name='bairro']").val();
    $("[name='cidade']").val();
    $("[name='estado']").val();
}
