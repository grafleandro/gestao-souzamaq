$(function(){
    jQueryCapturePhoto.init();
});

let jQueryCapturePhoto = (function(){
    return {
        init: function(){
            if(jQuery.browser.mobile) {
                let upBtn = document.getElementById('up-button');
                let upFile = document.getElementById('up-file');

                upBtn.addEventListener('change', function (e) {
                    let file = e.target.files[0];
                    // Do something with the image file.
                    upFile.src = URL.createObjectURL(file);

                    // if(jQuery.browser.mobile){
                    //     let imgHeigth   = $(frame).height();
                    //     let imgWidth    = $(frame).width();
                    //
                    //     $(frame).rotate({angle:90});
                    // }
                });
            }
        },
        isPortrait: function(){
            return window.innerHeight > window.innerWidth;
        },
        isLandscape: function(){
            return (window.orientation === 90 || window.orientation === -90);
        }
    }
})(jQuery);
