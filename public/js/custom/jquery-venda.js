$(function(){
    jQueryVenda.init();
});

let jQueryVenda = (function(){
    let $table = '<table id="tabela-venda" class="table table-bordered table-striped" width="100%">'
                    +'<thead>'
                        +'<tr>'
                            +'<th class="pull-center">Ação</th>'
                            +'<th>#</th>'
                            +'<th>Vendedor(a)</th>'
                            +'<th>Valor</th>'
                            +'<th>Parcelas</th>'
                            +'<th>Forma de Pagamento</th>'
                            +'<th width="30%">Produto</th>'
                            +'<th class="pull-center">Status</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    +'</tbody>'
                +'</table>';

    return{
        init: function(){
            if($('.table-venda-listar').length) {
                let $setTable = $('.table-venda-listar');

                $setTable.empty();

                $setTable.html($table);

                this.listarVenda();
            }
        },
        listarVenda: function(){
            $("#tabela-venda").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: '/venda/balcao/tabela',
                },
                columns: [
                    { data: "action", className: 'pull-center', orderable: false},
                    { data: "veba_id"},
                    { data: "colaborador.cola_nome"},
                    { data: "veba_venda_total"},
                    { data: "parcelas", orderable: false, searchable: false},
                    { data: "forma_pgto", orderable: false, searchable: false},
                    { data: "produto", orderable: false, searchable: false},
                    { data: "vest_id", className: 'pull-center', name: 'status.vest_titulo'},
                ]
            });
        },
        cancelarVenda: function($event){

            if(typeof $event.data('venda') !== 'undefined' && $event.data('venda') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente CANCELAR esta venda?",
                    icon: "error",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "put",
                            dataType: "json",
                            url: '/venda/balcao/' + $event.data('venda'),
                            data:{
                                _token: csrfToken,
                                venda_status: VEND_STATUS.VEND_CANCELADA
                            },
                            beforeSend: function () {
                                loading.show('Cancelando venda...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal({
                                        title: "Cancelar Venda",
                                        text: 'Venda cancelada com sucesso!',
                                        icon: "success",
                                    }).then(function(){
                                        location.reload();
                                    });

                                }else{
                                    swal("Cancelar Venda", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Cancelar Venda", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        deletarVenda: function($event){
            debugger;
            if(typeof $event.data('venda') !== 'undefined' && $event.data('venda') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Ao Deletar está venda, ela será removida da base de dados. Deseja continuar ?",
                    icon: "error",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: "delete",
                            dataType: "json",
                            url: '/venda/balcao/' + $event.data('venda'),
                            data: {},
                            beforeSend: function () {
                                loading.show('Deletando venda...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    $event.closest('tr').remove();

                                    swal("Deletar Venda", "Venda deletada com sucesso!", {icon: "success"});
                                }else{
                                    swal("Deletar Venda", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Deletar Venda", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
    }
})(jQuery);
