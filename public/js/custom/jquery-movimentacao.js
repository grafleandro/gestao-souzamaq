$(function(){
    jQueryMovimentacao.init();
});

let jQueryMovimentacao = (function(){
    let $table = '<table id="tabela-movimentacao" class="table table-bordered table-striped align-vertical" width="100%">'
                        +'<thead>'
                            +'<tr>'
                                +'<th>#</th>'
                                +'<th>Produto</th>'
                                +'<th class="pull-center">Valor</th>'
                                +'<th class="pull-center">Cadastrado Em</th>'
                                +'<th class="pull-center">Estoque Atual</th>'
                                +'<th class="pull-center">Detalhe</th>'
                            +'</tr>'
                        +'</thead>'
                        +'<tbody>'
                        +'</tbody>'
                    +'</table>';

    return{
        init: function(){
            if($('.table-movimentacao').length) {
                let $setTable = $('.table-movimentacao');

                $setTable.empty();

                $setTable.html($table);

                this.listarMovimentacao();
            }
        },
        listarMovimentacao: function(){

            $('#tabela-movimentacao').dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: "/estoque/movimentacao/produtos",
                },
                columns: [
                    { data: "codigo"},
                    { data: "produto"},
                    { data: "valor_venda", className: "pull-center"},
                    { data: "dt_cadastro", className: "pull-center"},
                    { data: "estoque", className: "pull-center"},
                    { data: "action", className: "pull-center"},
                ]
            });
        }
    }
})(jQuery);
