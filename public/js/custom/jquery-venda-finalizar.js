$(document).bind('keydown', function(event) {

    if(event.shiftKey && event.which === 67 ) {
        $("#venda-btn-cancelar").trigger('click');
    }

    if(event.shiftKey && event.which === 70) {
        jQueryVendaFinalizar.finalizarVenda();
    }
});



let jQueryVendaFinalizar = (function(){

    let indexPgto       = 0;
    let totalVenda      = 0; // Valor total da compra
    let totalPagamento  = 0; // Montante que jah foi contabilizado a ser pago
    let totalDesconto   = 0;
    let objPagamento    = {};

    return{
        init: function(){
            totalVenda = 0;

            if (Modernizr.localstorage) {
                if (sessionStorage.getItem("venda")) {
                    let vendaBalcao = JSON.parse(sessionStorage.getItem('venda'));

                    $.each(vendaBalcao, function(index, item){
                        let desconto = '';
                        let totalValorItem = (parseFloat(item.quantidade)*item.produto.produto_valores.prva_preco_venda);

                        if(item.desconto.prefix === '%'){
                            desconto    ='-' + item.desconto.valor + '%';
                        } else if (item.desconto.prefix === 'R$'){
                            desconto    = '-R$ ' + item.desconto.valor;
                        }

                        $("#table-vend-finalizar-item tbody").append('<tr> ' +
                            '<td>'+ item.quantidade +'</td>' +
                            '<td>'+ item.produto.prod_titulo +'</td>' +
                            '<td><span style="color: red">'+ desconto +'</span></td>' +
                            '<td class="pull-center">'+ accounting.formatMoney(totalValorItem, "R$ ", 2, ".", ",") +'</td>' +
                            '</tr>'
                        );

                        totalVenda = (totalVenda + totalValorItem);
                    });

                    // Informa o valor total da vendo nos campo A SER PAGO e o TOTAL A PAGAR
                    $("#venda-fina-total").text(accounting.formatMoney(totalVenda, "R$ ", 2, ".", ","));
                    $("#venda-fina-subtotal").text(accounting.formatMoney(totalVenda, "R$ ", 2, ".", ","));
                    $("#venda-total-pagar").val(accounting.formatMoney(totalVenda, "R$ ", 2, ".", ","));
                }
            }
        },
        selecionarFormaPgto: function($event){
            let optionSelected = $("option:selected", $event);

            /* Desbloqueia botao para poder calcular */
            $("#venda-btn-confirmar").removeAttr('disabled');

            // Quando selecionado uma opcao de PGTO que possui parcelas, eh carregado a quantidade de BUTTONS para selecionar
            if(optionSelected.data('parcelas')){
                $("#venda-parcelas").removeClass('hidden').empty();

                for(i = 1; i <= parseInt(optionSelected.data('parcelas')); i++){
                    $("#venda-parcelas").append('<button type="button" class="btn btn-default btn-md btn-circle margin-right-20 margin-top-5" data-value="'+ i +'" onclick="jQueryVendaFinalizar.calcularQtdParcelas($(this))">'+ i +'X</button>')
                }
            }else{
                $("#venda-parcelas").addClass('hidden').empty();
            }

            $("#venda-total-pagar").focus();
            this.calcularDescDinheiro($("#venda-desconto-din"));
        },
        calcularPgto: function($event){
            let optionSelected = $("option:selected", $("#venda-form-pgto"));
            let totalPagar  = this.vendaTotalPagar();
            let trocoPgto   = parseInt(optionSelected.data('troco'));

            /* Caso exista desconto, eh acrescentado ao valor total */
            let totalDesconto = (typeof objPagamento.desconto !== "undefined") ? objPagamento.desconto : 0;

            totalPagamento = totalPagamento + totalDesconto;

            // Verifica se o valor que deseja pagar, eh maior do que o valor que ainda resta ser pago.
            if((totalVenda - totalPagamento) < totalPagar && !trocoPgto){
                swal({
                    title: "Confirmar Venda",
                    text: "O valor informado é maior do que o valor a ser pago. E não é possível gerar troco para está forma de Pagamento !",
                    icon: "warning",
                    type: "warning"
                }).then(function(){
                    $("#venda-total-pagar").focus();
                });
            } else {

                totalPagamento = (totalPagamento + totalPagar);

                let vendaTotalPagar = (totalVenda - totalPagamento);

                // Verifica se possui Troco
                if (trocoPgto) {
                    this.calcularTroco(); // Calcula o valor do troco
                }

                let parcelas = '';
                let parcelasX = '';

                // Verifica se a forma de pgto selecionada, possui parcelas a serem calculadas
                if($("#venda-parcelas-qtd").val().length && $("#venda-parcelas-qtd").val() !== 0){
                    parcelas = parseInt($("#venda-parcelas-qtd").val());
                    parcelasX = (parcelas) ? '('+ parcelas +'x)' : '';

                    $("#venda-parcelas-qtd").val('0');
                }

                $("#venda-pagamento").closest('fieldset').removeClass('hidden');

                $("#venda-pagamento").append('<div class="row no-margin-left sale-panel-details">' +
                                                '<div class="col-md-6">' +
                                                    '<button class="btn btn-danger btn-circle" data-troco="'+ trocoPgto +'" data-value="'+ totalPagar +'" data-index="'+ indexPgto +'" onclick="jQueryVendaFinalizar.removePagamentos($(this))"><i class="fa fa-trash"></i></button>' +
                                                    '<span><strong> - ' + optionSelected.text() + '</strong><i>'+ parcelasX +'</i></span>' +
                                                '</div>' +
                                                '<div class="col-md-6 pull-center">' +
                                                    '<span> ' + accounting.formatMoney(totalPagar, "R$ ", 2, ".", ",") + '</span>' +
                                                '</div>' +
                                            '</div>');

                // Contabiliza os valores a SEREM PAGOS e o que JA FOI PAGO
                $("#venda-fina-pagamentos").text(accounting.formatMoney(totalPagamento, "R$ ", 2, ".", ","));


                if(vendaTotalPagar <= 0){
                    $("#venda-total-pagar").val(accounting.formatMoney(0, "R$ ", 2, ".", ",")).attr('disabled', 'disabled');
                    $("#venda-btn-confirmar").attr('disabled', 'disabled');
                    $("#venda-desconto-din").attr('disabled', 'disabled');
                    $("#venda-desconto-porc").attr('disabled', 'disabled');
                    $("#venda-parcelas").addClass('hidden').empty();
                } else {
                    $("#venda-total-pagar").val(accounting.formatMoney(vendaTotalPagar, "R$ ", 2, ".", ","));
                }

                // Gravando dados
                if(!indexPgto){
                    objPagamento.formas_pgto = [];
                }

                objPagamento.formas_pgto[indexPgto] = {valor: totalPagar, tipo_pgto: parseInt(optionSelected.val()), parcela: parcelas};

                indexPgto++;
            }
        },
        calcularTroco: function(){
            let troco;

            if(totalPagamento <= totalVenda){
                troco = 0;
            } else {
                troco = (totalPagamento-totalVenda);
            }

            $("#venda-fina-troco").text(accounting.formatMoney(troco, "R$ ", 2, ".", ","));
        },
        calcularDescPorcet: function($event){
            let valor   = $event.val().replace(',', '.').replace('%', '');

            if(!valor){
                return false;
            }

            let desconto= (totalVenda*(parseFloat(valor)/100));

            totalDesconto = (totalVenda-(totalVenda*(parseFloat(valor)/100)));

            $("#venda-fina-desconto").text(accounting.formatMoney(desconto, "R$ ", 2, ".", ","));
            $("#venda-fina-total").text(accounting.formatMoney(totalDesconto, "R$ ", 2, ".", ","));
            $("#venda-total-pagar").val(accounting.formatMoney(totalDesconto, "R$ ", 2, ".", ","));
            $("#venda-desconto-din").val(accounting.formatMoney(desconto, "R$ ", 2, ".", ","))

            objPagamento.desconto = parseFloat(desconto);
        },
        calcularDescDinheiro: function($event){
            let desconto = $event.val().replace(',', '.').replace('R$', '');

            if(!desconto){
                return false;
            }

            let valorPorcent = ((parseFloat(desconto)*100)/totalVenda);

            totalDesconto = (totalVenda-parseFloat(desconto));

            $("#venda-fina-desconto").text(accounting.formatMoney(desconto, "R$ ", 2, ".", ","));
            $("#venda-fina-total").text(accounting.formatMoney(totalDesconto, "R$ ", 2, ".", ","));
            $("#venda-total-pagar").val(accounting.formatMoney(totalDesconto, "R$ ", 2, ".", ","));
            $("#venda-desconto-porc").val(accounting.formatMoney(valorPorcent, {
                symbol: '%',
                format: '%v%s',
                decimal: ',',
                thousand: '.',
                precision: 2
            }));

            objPagamento.desconto = parseFloat(desconto);
        },
        calcularQtdParcelas: function($event){
            $("#venda-parcelas-qtd").val($event.data('value'));
        },
        removePagamentos: function($event){
            let valor       = $event.data('value');
            let indexPgto   = $event.data('index');
            let vendaTotalPagar = this.vendaTotalPagar();

            if(!vendaTotalPagar){
                $("#venda-total-pagar").removeAttr('disabled', 'disabled');
                $("#venda-btn-confirmar").removeAttr('disabled', 'disabled');
                $("#venda-desconto-din").removeAttr('disabled', 'disabled');
                $("#venda-desconto-porc").removeAttr('disabled', 'disabled');
            }

            totalPagamento  = (totalPagamento-valor) + (typeof objPagamento.desconto !== "undefined") ? objPagamento.desconto : 0;

            // Recalcula o valor do Troco
            this.calcularTroco();

            // Contabiliza os valores a SEREM PAGOS e o que JA FOI PAGO com base na forma de PGTO que foi removida
            $("#venda-fina-pagamentos").text(accounting.formatMoney(totalPagamento, "R$ ", 2, ".", ","));
            $("#venda-total-pagar").val(accounting.formatMoney((totalVenda-totalPagamento), "R$ ", 2, ".", ","));

            // Exclui o item de Pagamento
            $event.closest('.sale-panel-details').remove();

            // Remove o item do vetor de pagamento
            objPagamento.formas_pgto.splice(objPagamento.formas_pgto.indexOf(indexPgto), 1);

            // Oculta novamente o campo de Pagamentos
            if(totalPagamento <= 0){
                $("#venda-pagamento").closest('fieldset').addClass('hidden');
            }
        },
        vendaTotalPagar: function(){
            let totalPagar  = $("#venda-total-pagar").val().replace('R$', '').replace('.', '').replace(',', '.');

            return parseFloat(totalPagar);
        },
        finalizarVenda: function($event){
            let dadosSalvar = {};

            if(typeof objPagamento.formas_pgto === "undefined" || !objPagamento.formas_pgto.length){
                swal({
                    title: 'Forma de Pagamento',
                    text: 'Você precisa selecionar uma Forma de Pagamento!',
                    icon: "warning",
                }).then(function(){
                    $("#venda-form-pgto").trigger('click');
                });

                return false;
            }

            if (Modernizr.localstorage) {
                if (sessionStorage.getItem("venda")) {
                    let vendaBalcao = JSON.parse(sessionStorage.getItem('venda'));
                    let produtos    = [];
                    let pagamento   = [];

                    $.each(vendaBalcao, function (index, item) {
                        produtos.push({
                            prod_id: item.produto.prod_id,
                            prod_valor: item.produto.produto_valores.prva_preco_venda,
                            prod_qtd: parseFloat(item.quantidade),
                        });
                    });

                    $.each(objPagamento.formas_pgto, function(index, pgto){
                        pagamento.push({
                            valor: pgto.valor,
                            tipo_pgto: pgto.tipo_pgto,
                            parcela: pgto.parcela
                        });
                    });

                    dadosSalvar.produto     = produtos;
                    dadosSalvar.pagamento   = pagamento;
                    dadosSalvar.total_venda = totalVenda;
                    dadosSalvar.total_pgto  = totalPagamento;
                    dadosSalvar.clie_consumidor = parseInt($("#clie_consumidor").val());
                }

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'post',
                    dataType: 'json',
                    url: '/venda/balcao',
                    data: dadosSalvar,
                    beforeSend: function(data){
                        $("#venda-btn-finalizar").html('<i class="fa fa-spinner fa-spin"></i> Salvando').attr('disabled', 'disabled');
                    },
                    success: function(data){
                        $("#venda-btn-finalizar").html('<i class="fa fa-check"></i> Finalizar').removeAttr('disabled');

                        if(data.success){
                            swal({
                                title: 'Venda Balcão',
                                text: 'Dados salvos com sucesso.',
                                icon: "success",
                                type: "success"
                            }).then(function(){
                                sessionStorage.clear();

                                location.reload();
                            });
                        } else {
                            swal('Venda Balcão', data.alert, {icon: "warning"});
                        }
                    },
                    error: function(xhr){
                        $("#venda-btn-finalizar").html('<i class="fa fa-check"> Finalizar</i>').removeAttr('disabled');

                        console.log(xhr);
                    }
                })
            }
        }
    }
})(jQuery);
