$(function(){
    jQueryServicos.init();
});

let jQueryServicos = (function(){
    let $tableProduto = '<table class="table table-hover tabela-produto">' +
                            '<thead>' +
                                '<tr>' +
                                    '<th class="pull-center">#</th>' +
                                    '<th>Produto</th>' +
                                    '<th class="pull-center">Quantidade</th>' +
                                    '<th class="pull-center">Valor</th>' +
                                    '<th>Ação</th>' +
                                '</tr>' +
                            '</thead>' +
                            '<tbody>' +
                            '</tbody>' +
                        '</table>';

    let $tableServico = '<table id="tabela-servico" class="display projects-table table table-striped table-bordered table-hover" cellspacing="0" width="100%">'
                            +'<thead>'
                                +'<tr>'
                                    +'<th></th>'
                                    +'<th>Código</th>'
                                    +'<th>Título</th>'
                                    +'<th>ISS</th>'
                                    +'<th>Valor</th>'
                                    +'<th class="pull-center">Ação</th>'
                                +'</tr>'
                            +'</thead>'
                            +'<tbody>'
                            +'</tbody>'
                        +'</table>';

    let $dadosProduto = {};

    let tableDetails = null;

    return {
        init: function(){
            if($(".table-servico-produto").length && $("#form_servico").attr('method') !== 'put'){
                let $tbProduto = $(".table-servico-produto");

                $tbProduto.empty();

                $tbProduto.html($tableProduto);
            }

            if($('.table-listar-servico').length) {
                let $setTable = $('.table-listar-servico');

                $setTable.empty();

                $setTable.html($tableServico);

                this.listarServico();
            }
        },
        listarServico(){
            let $parent = this;

            tableDetails = $('#tabela-servico').DataTable({
                sDom: "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                ajax: "/oficina/servico/tabela",
                bDestroy: true,
                "oLanguage": {
                    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                },
                "columns": [
                    {
                        "class": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    { "data": "serv_id", "class": 'pull-center'},
                    { "data": "serv_titulo" },
                    { "data": "serv_aliq_iss", "class": 'pull-center'},
                    { "data": "serv_valor", "class": 'pull-center'},
                    { "data": "action" }
                ],
                "order": [[1, 'asc']],
                "fnDrawCallback": function( oSettings ) {
                    runAllCharts()
                }
            });

            $('#tabela-servico tbody').on('click', 'td.details-control', function () {
                let tr = $(this).closest('tr');
                let row = tableDetails.row( tr );

                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    row.child( $parent.formatDetails(row.data()) ).show();
                    tr.addClass('shown');
                }
            });
        },
        formatDetails: function ( row ) {
            console.log(row);
            let produtoDetails = '';

            $.each(row.servico_produto, function(index, produto){
                let produtoValor = accounting.formatMoney(produto.produto_valores.prva_preco_venda, "R$ ", 2, ".", ",");

                produtoDetails += '<tr>'+
                                    '<td style="width:100px"><strong>Produto:</strong></td>'+
                                    '<td style="width:200px">'+ produto.produto.prod_titulo +'</td>'+
                                    '<td style="width:100px"><strong>Quantidade:</strong></td>'+
                                    '<td style="width:100px">'+ produto.sepr_qtd +'</td>'+
                                    '<td style="width:100px"><strong>Valor Unitário:</strong></td>'+
                                    '<td>'+ produtoValor +'</td>'+
                                '</tr>';
            });

            return '<table cellpadding="5" cellspacing="0" border="0" class="table table-hover table-condensed">'+ produtoDetails +'</table>';
        },
        addProdutoTabela: function($event){
            let status = true;

            if(typeof $dadosProduto === 'undefined'){
                swal({
                    title: 'Adicionar Produto',
                    text: 'Você precisa selecionar um produto!',
                    icon: "warning",
                }).then(function(){
                    $("#serv_produto").focus();
                });

                status = false;
            }

            if(!$("#serv_prod_qtd").val().length){
                swal({
                    title: 'Quantidade do Produto',
                    text: 'Você precisa informar a quantidade deste produto!',
                    icon: "warning",
                }).then(function(){
                    $("#serv_prod_qtd").focus();
                });

                status = false;
            }

            if(status){
                let valorProduto = accounting.formatMoney($dadosProduto.produto_valores.prva_preco_venda, "R$ ", 2, ".", ",");
                let indexAtualTb = this.procurarProxIndexValido(parseInt($("input[name^=tabela_prod_qtd]").length/2));

                $(".tabela-produto").append('<tr>' +
                                                '<td>'+ $dadosProduto.prod_id +'</td>' +
                                                '<td>'+ $dadosProduto.prod_titulo +'</td>' +
                                                '<td class="pull-center">'+ $("#serv_prod_qtd").val() +'</td>' +
                                                '<td class="pull-center">'+ valorProduto +'</td>' +
                                                '<td>' +
                                                    '<button class="btn btn-default btn-circle" data-index="'+ indexAtualTb +'" onclick="jQueryServicos.removerProdutoTabela($(this))"><i class="fa fa-trash"></i></button>' +
                                                '</td>' +
                                            '</tr>'
                );

                $("#dados_tabela").append('<input type="hidden" class="tabela-produto-'+ indexAtualTb +'" id="tabela_prod_qtd" name="tabela_prod_qtd['+ indexAtualTb +'][prod_id]" value="'+ $dadosProduto.prod_id +'">')
                $("#dados_tabela").append('<input type="hidden" class="tabela-produto-'+ indexAtualTb +'" id="tabela_prod_qtd" name="tabela_prod_qtd['+ indexAtualTb +'][qtd]" value="'+ $("#serv_prod_qtd").val() +'">')

                $("#serv_produto").val('').focus();
                $("#serv_prod_qtd").val('');
            }
        },
        removerProdutoTabela: function($event){
            let indexProduto= $event.data('index');

            /* Remover a linha da tabela */
            $event.closest('tr').remove();

            /* Remover Inputs */
            $(".tabela-produto-" + indexProduto).remove();
        },
        procurarProxIndexValido: function(index){
            if(!$(".tabela-produto-" + index).length){
                return index;
            }

            return this.procurarProxIndexValido(index+1);
        },
        deletarServico: function($event){
            if(typeof $event.data('servico') !== 'undefined' && $event.data('servico') !== ''){
                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/oficina/servico/' + $event.data('servico'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Serviço", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Excluir Serviço", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Serviço", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        autocompleteProduto: function($event){
            let $parent = this;

            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        if(request.term.length === 2 && /\s/g.test(request.term)){
                            request.term = request.term.replace(/\s/g, '*');
                        }
                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/empresa/produto",
                            data: {
                                type: 'prod',
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.produto.length){

                                        response($.map(data.produto, function (item) {

                                            return {
                                                label: item.prod_id + ' - ' + item.prod_titulo,
                                                value: item.prod_id+ ' - ' + item.prod_titulo,
                                                id: item.prod_id,
                                                produto: item
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item Não Encontrato',
                                            value: 'Item Não Encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar Produto', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar Produto', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $dadosProduto = ui.item.produto;
                        $("#serv_prod_qtd").focus();
                    }
                }
            });
        },

        autocompleteServico: function($event){
            let $parent = this;

            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){

                        if(request.term.length === 2 && /\s/g.test(request.term)){
                            request.term = request.term.replace(/\s/g, '*');
                        }

                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/oficina/servico",
                            data: {
                                search_value: request.term
                            },
                            success: function(data){
                                if(data.success){
                                    if(data.produto.length){

                                        response($.map(data.produto, function (item) {

                                            return {
                                                label: item.prod_id + ' - ' + item.prod_titulo,
                                                value: item.prod_id+ ' - ' + item.prod_titulo,
                                                id: item.prod_id,
                                                produto: item
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item Não Encontrato',
                                            value: 'Item Não Encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar Produto', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar Produto', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 20,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $dadosProduto = ui.item.produto;
                        $("#serv_prod_qtd").focus();
                    }
                }
            });
        },
    }
})(jQuery);
