<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes();

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/home', 'HomeController@index')->name('home');

/** MENU EMPRESA */
Route::group(['namespace' => 'Empresa'], function() {

    Route::group(['prefix' => 'empresa'], function () {

        /** CRUD CLIENTE */
        Route::resource('cliente', 'ClienteController');

        Route::resource('empresa', 'EmpresaController');

        Route::resource('usuario', 'UsuarioController');

        /** CRUD FABRICANTE */
        Route::resource('fabricante', 'ProdutoFabricanteController');

        /** CRUD COLABORADOR */
        Route::resource('colaborador', 'ColaboradorController');

        /** CRUD COLABORADOR PERMISSAO */
        Route::resource('colaborador/permissao', 'ColaboradorPermissaoController');

        /** CRUD CONTA CORRENTE COLABORADOR */
        Route::resource('colaborador/conta_corrente', 'CCColaboradorController');

        /** CRUD PRODUTO */
        Route::resource('produto', 'ProdutoController');

        /** CRUD Grupo */
        Route::resource('grupo', 'ProdutoGrupoController');

        /** CRUD MARCA */
        Route::resource('marca', 'ProdutoMarcaController');

        /** CRUD INVENTARIO */
        Route::resource('inventario', 'InventarioController');

        Route::group(['prefix' => 'fornecedor'], function () {
            /** CRUD Fornecedor*/
            Route::resource('fornecedor', 'FornecedorController');
        });

    });
});

/** MENU OFICINA */
Route::group(['namespace' => 'Oficina'], function() {

    Route::group(['prefix' => 'oficina'], function () {

        /** SUB-MENU CHECK-LIST */
        Route::group(['prefix' => 'checklist'], function () {

            /** CRUD CHECK-LIST INTERNO*/
            Route::resource('interno', 'CheckListInternoController');

            /** CRUD CHECK-LIST EXTERNO*/
            Route::resource('externo', 'CheckListExternoController');
        });

        /** SUB-MENU ALMOXARIFADO */
        Route::group(['prefix' => 'almoxarifado'], function () {
            /** CRUD FERRAMENTAS */
            Route::resource('ferramentas', 'AlmoxarifadoFerramentasController');
        });

        /** CRUD SERVICO */
        Route::resource('servico', 'ServicoController');

        /** CRUD ORDEM DE SERVICO */
        Route::resource('ordem_servico', 'OrdemServicoController');

        /** CRUD VEICULO */
        Route::resource('veiculo', 'VeiculoController');
    });
});

/** MENU ESTOQUE*/
Route::group(['namespace' => 'Estoque'], function () {

    Route::group(['prefix' => 'estoque'], function () {

        /** CRUD - CATEGORIA */
        Route::resource('categoria', 'CategoriaController');

        /** RELATORIOS DE MOVIMENTACAO DE PRODUTOS */
        Route::resource('movimentacao', 'MovimentacaoController');
    });
});

/** MENU VENDA */
Route::group(['namespace' => 'Venda'], function() {

    Route::group(['prefix' => 'venda'], function () {

        /** CRUD - BALCAO */
        Route::resource('balcao', 'BalcaoController');

        /** ALTERACAO E MANIPULACAO DOS DADOS DA VENDA */
        Route::resource('listar', 'VendaController');
    });
});

/** MENU REQUISICAO */
Route::group(['namespace' => 'Requisicao'], function () {

    Route::group(['prefix' => 'requisicao'], function () {

        /** CRUD - REQUISICAO DE PRODUTO E SERVICO*/
        Route::resource('produto-servico', 'RequisicaoProdutoServicoController');
    });
});

/** MENU FINANCEIRO*/
Route::group(['namespace' => 'Financeiro'], function () {

    Route::group(['prefix' => 'financeiro'], function () {

        /** CRUD - CATEGORIA */
        Route::resource('contas-pagar', 'ContasAPagarController');
    });
});

/** MENU ALMOXARIFADO */
Route::group(['namespace' => 'Almoxarifado'], function () {

    /** CRUD - ALMOXARIFADO*/
    Route::resource('almoxarifado', 'AlmoxarifadoController');

    /** CRUD - ALMOXARIFADO EMPRESTIMO */
    Route::resource('almoxarifado-emprestimo', 'AlmoxarifadoEmprestimoController');
});

/** MENU CONFIGURACAO */
Route::group(['namespace' => 'Configuracao'], function () {

    Route::group(['prefix' => 'configuracao'], function () {

        /** CRUD COLABORADOR - FUNCAO */
        Route::resource('funcao', 'ColaboradorFuncaoController');

        /** CRUD TRIBUTACAO - FUNCAO */
        Route::resource('tributacao', 'TributacaoController');

        /** CRUD FORMA DE PAGAMENTO*/
        Route::resource('forma_pagamento', 'FormaPgtoController');

        /** CRUD CONTA DA EMPRESA */
        Route::resource('conta', 'ContaController');

        /** CRUD CONTA BANCARIA*/
        Route::resource('conta_bancaria', 'ContaBancariaController');

        /** CRUD CENTRO DE CUSTO */
        Route::resource('centro/custo', 'CentroCustoController');

        /** CRUD COMISSAO */
        Route::resource('comissao', 'ComissaoController');
    });
});

