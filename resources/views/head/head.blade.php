@yield('smart_css')

<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('vendor/smartadmin/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('vendor/smartadmin/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('vendor/smartadmin/css/smartadmin-production-plugins.min.css') }}">
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('vendor/smartadmin/css/smartadmin-production.min.css') }}">
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('vendor/smartadmin/css/smartadmin-skins.min.css') }}">
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('vendor/smartadmin/css/smartadmin-rtl.min.css') }}">

<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('vendor/smartadmin/css/demo.min.css') }}">

<!-- CUSTOM JS -->
<link rel="stylesheet" href="{{ asset('css/style.css') }}">

@stack('css')
