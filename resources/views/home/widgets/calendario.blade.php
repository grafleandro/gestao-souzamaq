<!-- NEW WIDGET START -->
<article class="col-xs-12 col-sm-6 col-md-8">

    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget" id="wid-id-{{ \App\Utils\HomeUtils::CALENDARIO }}">
        <header>
            <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
            <h2>Eventos </h2>
        </header>

        <!-- widget div-->
        <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->
                <input class="form-control" type="text">
            </div>
            <!-- end widget edit box -->

            <!-- widget content -->
            <div class="widget-body no-padding">
                <!-- content goes here -->
                <div class="widget-body-toolbar">

                    <div id="calendar-buttons">

                        <div class="btn-group">
                            <a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-prev"><i class="fa fa-chevron-left"></i></a>
                            <a href="javascript:void(0)" class="btn btn-default btn-xs" id="btn-next"><i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
                <div id="calendar"></div>

                <!-- end content -->
            </div>
            <!-- end widget content -->

        </div>
        <!-- end widget div -->

    </div>
    <!-- end widget -->

</article>
<!-- WIDGET END -->

@push('scripts')
    <script>
        /*
         * FULL CALENDAR JS
         */

        if ($("#calendar").length) {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var calendar = $('#calendar').fullCalendar({
                editable : true,
                draggable : true,
                selectable : false,
                selectHelper : true,
                unselectAuto : false,
                disableResizing : false,
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                height: "auto",
                header : {
                    left : 'title', //,today
                    center : 'prev, next, today',
                    right : 'month, agendaWeek, agenDay' //month, agendaDay,
                },
                select : function(start, end, allDay) {
                    var title = prompt('Event Title:');
                    if (title) {
                        calendar.fullCalendar('renderEvent', {
                                title : title,
                                start : start,
                                end : end,
                                allDay : allDay
                            }, true // make the event "stick"
                        );
                    }
                    calendar.fullCalendar('unselect');
                },

                events : [{
                    title : 'All Day Event',
                    start : new Date(y, m, 1),
                    description : 'long description',
                    className : ["event", "bg-color-greenLight"],
                    icon : 'fa-check'
                }, {
                    title : 'Long Event',
                    start : new Date(y, m, d - 5),
                    end : new Date(y, m, d - 2),
                    className : ["event", "bg-color-red"],
                    icon : 'fa-lock'
                }, {
                    id : 999,
                    title : 'Repeating Event',
                    start : new Date(y, m, d - 3, 16, 0),
                    allDay : false,
                    className : ["event", "bg-color-blue"],
                    icon : 'fa-clock-o'
                }, {
                    id : 999,
                    title : 'Repeating Event',
                    start : new Date(y, m, d + 4, 16, 0),
                    allDay : false,
                    className : ["event", "bg-color-blue"],
                    icon : 'fa-clock-o'
                }, {
                    title : 'Birthday Party',
                    start : new Date(y, m, d + 1, 19, 0),
                    end : new Date(y, m, d + 1, 22, 30),
                    allDay : false,
                    className : ["event", "bg-color-darken"]
                }, {
                    title : 'Smartadmin Open Day',
                    start : new Date(y, m, 28),
                    end : new Date(y, m, 29),
                    className : ["event", "bg-color-darken"]
                }],


                eventRender : function(event, element, icon) {
                    if (!event.description == "") {
                        element.find('.fc-title').append("<br/><span class='ultra-light'>" + event.description + "</span>");
                    }
                    if (!event.icon == "") {
                        element.find('.fc-title').append("<i class='air air-top-right fa " + event.icon + " '></i>");
                    }
                }
            });

        };

        /* hide default buttons */
        $('.fc-toolbar .fc-right, .fc-toolbar .fc-center').hide();

        // calendar prev
        $('#calendar-buttons #btn-prev').click(function() {
            $('.fc-prev-button').click();
            return false;
        });

        // calendar next
        $('#calendar-buttons #btn-next').click(function() {
            $('.fc-next-button').click();
            return false;
        });

        // calendar today
        $('#calendar-buttons #btn-today').click(function() {
            $('.fc-button-today').click();
            return false;
        });

        // calendar month
        $('#mt').click(function() {
            $('#calendar').fullCalendar('changeView', 'month');
        });

        // calendar agenda week
        $('#ag').click(function() {
            $('#calendar').fullCalendar('changeView', 'agendaWeek');
        });

        // calendar agenda day
        $('#td').click(function() {
            $('#calendar').fullCalendar('changeView', 'agendaDay');
        });
    </script>
@endpush
