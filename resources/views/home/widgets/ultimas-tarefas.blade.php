<!-- NEW WIDGET START -->
<article class="col-xs-12 col-sm-6 col-md-4">

    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget" id="wid-id-{{ \App\Utils\HomeUtils::ULTIMAS_TAREFAS }}">
        <header>
            <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
            <h2>Últimas Terafas Realizadas </h2>
        </header>

        <!-- widget div-->
        <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->
                <input class="form-control" type="text">
            </div>
            <!-- end widget edit box -->

            <!-- widget content -->
            <div class="widget-body">
                <!-- this is what the user will see -->
                <div class="panel panel-default">
                    <div class="panel-body status">
                        <div class="who clearfix">
                            <h4>Últimas Tarefas</h4>
                        </div>

                        <div class="who clearfix">
                            <img src="img/avatars/2.png" alt="img" class="busy">
                            <span class="name font-sm"> <span class="text-muted">Posted by</span> <b> Karrigan Mean <span class="pull-right font-xs text-muted"><i>3 minutes ago</i></span> </b>
                                        <br>
                                        <a href="javascript:void(0);" class="font-md">Business Requirement Docs</a> </span>
                        </div>

                        <div class="who clearfix">
                            <img src="img/avatars/3.png" alt="img" class="offline">
                            <span class="name font-sm"> <span class="text-muted">Posted by</span> <b> Alliz Yaen <span class="pull-right font-xs text-muted"><i>2 days ago</i></span> </b>
                                        <br>
                                        <a href="javascript:void(0);" class="font-md">Maecenas nec odio et ante tincidun</a> </span>
                        </div>

                        <div class="who clearfix">
                            <img src="img/avatars/4.png" alt="img" class="away">
                            <span class="name font-sm"> <span class="text-muted">Posted by</span> <b> Barley Kartzukh <span class="pull-right font-xs text-muted"><i>1 month ago</i></span> </b>
                                        <br>
                                        <a href="javascript:void(0);" class="font-md">Tincidun nec Gasket Mask </a> </span>
                        </div>

                    </div>

                </div>

            </div>
            <!-- end widget content -->

        </div>
        <!-- end widget div -->

    </div>
    <!-- end widget -->

</article>
<!-- WIDGET END -->
