@extends('layouts.page')

@section('content')
    <!-- row -->
    <div class="row">

        <!-- col -->
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-home"></i>
                Painel Administrativo
                <span>Dashboard</span>
            </h1>
        </div>
        <!-- end col -->

        <!-- right side of the page with the sparkline graphs -->
        <!-- col -->
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            <!-- sparks -->
            <ul id="sparks">
                <li class="sparks-info">
                    <h5> O.S Abertas<span class="txt-color-blue">{{$qtdOs}}</span></h5>
                    {{--<div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">--}}
                        {{--456, 154, 354, 45, 256--}}
                    {{--</div>--}}
                </li>
                <li class="sparks-info" title="{{$periodo}}">
                    @if($crescimento > 0)
                        <h5>Comparativo mês anterior <span class="txt-color-green"><i class="fa fa-arrow-circle-up" data-rel="bootstrap-tooltip" title="Comparativo do Mês Anterior - Mesmo perído"></i> {{\App\Utils\Mask::porcentagem($crescimento)}}</span></h5>
                    @elseif($crescimento == 0)
                        <h5>Comparativo mês anterior <span class="txt-color-yellow"><i class="fa fa-minus" data-rel="bootstrap-tooltip" title="Comparativo do Mês Anterior - Mesmo perído"></i> {{\App\Utils\Mask::porcentagem($crescimento)}}</span></h5>
                    @else
                        <h5>Comparativo mês anterior <span class="txt-color-red"><i class="fa fa-arrow-circle-down" data-rel="bootstrap-tooltip" title="Comparativo do Mês Anterior - Mesmo perído"></i> {{\App\Utils\Mask::porcentagem($crescimento)}}</span></h5>
                    @endif
                </li>
                <li class="sparks-info" title="{{$periodo}}">
                    <h5> Vendas Realizadas<span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;{{$qtdVendas}}</span></h5>
                </li>
            </ul>
            <!-- end sparks -->
        </div>
        <!-- end col -->

    </div>
    <!-- end row -->

    <!--
                The ID "widget-grid" will start to initialize all widgets below
                You do not need to use widgets if you dont want to. Simply remove
                the <section></section> and you can use wells or panels instead
                -->

    <!-- widget grid -->
    <section id="widget-grid" class="">
        <div class="row">
            <!-- a blank row to get started -->
            <div class="col-sm-12">
                <!-- your contents here -->
            </div>
        </div>
        <!-- end row -->
    </section>
    <!-- end widget grid -->
@endsection
