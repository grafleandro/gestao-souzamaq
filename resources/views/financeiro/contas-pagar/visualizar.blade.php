<fieldset>
    <legend>Dados da Conta</legend>
    <div class="row">
        <div class="col-md-3">
            <label><strong>Descrição:</strong></label>
        </div>
        <div class="col-md-9">
            {{ $conta['copa_descricao'] }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label><strong>Categoria:</strong></label>
        </div>
        <div class="col-md-9">
            @if(isset($conta['categoria']) && !empty($conta['categoria']))
                {{ $conta['categoria']['copc_titulo'] }} <i>{{ $conta['categoria']['copc_descricao'] }}</i>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label><strong>Conta:</strong></label>
        </div>
        <div class="col-md-9">
            AG: {{ $conta['conta_bancaria']['cocb_agencia'] }}
            {{ ($conta['conta_bancaria']['cocb_tipo_conta'] == \App\Utils\ConfiguracaoUtils::CB_CONTA_CORRENTE) ? ' / CC: ' . $conta['conta_bancaria']['cocb_conta_corrente'] : 'CP: ' . $conta['conta_bancaria']['cocb_conta_poupanca'] }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label><strong>Centro de Custo:</strong></label>
        </div>
        <div class="col-md-9">
            @if(isset($conta['centro_custo']) && !empty($conta['centro_custo']))
                {{ $conta['centro_custo']['cocc_titulo'] }} - {{ $conta['centro_custo']['cocc_descricao'] }}
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label><strong>Data do Lançamento:</strong></label>
        </div>
        <div class="col-md-9">
            {{ \Carbon\Carbon::parse($conta['created_at'])->format('d/m/Y') }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label><strong>Data da Competência:</strong></label>
        </div>
        <div class="col-md-9">
            {{ \Carbon\Carbon::parse($conta['copa_dt_competencia'])->format('d/m/Y') }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label><strong>Data do Vencimento:</strong></label>
        </div>
        <div class="col-md-9">
            {{ \Carbon\Carbon::parse($conta['copa_dt_vencimento'])->format('d/m/Y') }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label><strong>Valor Total:</strong></label>
        </div>
        <div class="col-md-9">
            {{ \App\Utils\Mask::dinheiro($conta['copa_valor']) }}
        </div>
    </div>
</fieldset>

@if(!empty($conta['parcelas']))
    <fieldset>
        <legend>Parcelas</legend>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="pull-center">Parcela</th>
                    <th class="pull-center">Vencimento</th>
                    <th class="pull-center">Valor</th>
                    <th class="pull-center">Status</th>
                </tr>
            </thead>
            <tbody><div class="badge bg-color-greenLight pull-left"></div>
                @foreach($conta['parcelas'] as $index => $parcela)
                    <tr>
                        <td class="pull-center">{{ ($index+1) }}/{{ count($conta['parcelas']) }}</td>
                        <td class="pull-center">{{ \Carbon\Carbon::parse($parcela['copp_dt_vencimento'])->format('d/m/Y') }}</td>
                        <td class="pull-center">{{ \App\Utils\Mask::dinheiro($parcela['copp_valor']) }}</td>
                        @if($parcela['copp_status'])
                            <td class="pull-center"><span class="badge bg-color-greenLight">Pago</span></td>
                        @else
                            <td class="pull-center"><span class="badge bg-color-red">Não Pago</span></td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </fieldset>
@endif
