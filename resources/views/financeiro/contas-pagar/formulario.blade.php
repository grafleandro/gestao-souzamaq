@if(isset($contasPagar['copa_id']))
    <form id="form-contas-pagar" class="smart-form" action="{{ url('financeiro/contas-pagar/' . $contasPagar['copa_id']) }}" method="put">
@else
    <form id="form-contas-pagar" class="smart-form" action="{{ url('financeiro/contas-pagar/') }}" method="post">
@endif
    <fieldset>
        <div class="row">
            <section class="col col-3">
                <label class="label">Descrição</label>
                <label class="input">
                    <input type="text" id="cp_descricao" name="cp_descricao" value="{{ $contasPagar['copa_descricao'] ?? '' }}" required>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Categoria</label>
                <label class="input">
                    <select id="cp_categoria" name="cp_categoria">
                        <option value="0" selected="selected">-- Selecionar --</option>
                        @foreach($categoria as $index => $value)
                            <option value="{{ $value['copc_id'] }}" data-description="{{$value['copc_descricao']}}" {{(isset($contasPagar['copc_id'])) ? 'selected' : ''}}>{{ $value['copc_titulo'] }}</option>
                        @endforeach
                    </select>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Conta</label>
                <label class="input">
                    <select class="form-control form-cascade-control" name="cp_banco" required>
                        <option selected="selected" value="">-- Selecionar --</option>
                        @foreach($conta as $index => $value)
                            <option value="{{ $value['cocb_id'] }}" {{(isset($contasPagar['conta_bancaria']['banc_id'])) ? 'selected' : ''}}>{{ $value['banco']['banc_titulo'] }} / AG: {{ $value['cocb_agencia'] }}</option>
                        @endforeach
                    </select>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Centro de Custo</label>
                <label class="select">
                    {{ \App\Utils\FormUtils::select($centro_custo, 'cp_centro_custo', 'cp_centro_custo', 'form-control form-cascade-control', ($contasPagar['centro_custo']['cocc_id']) ?? null, false, array(), '-- Selecionar --', true, 'cocc_id', 'cocc_titulo') }}
                    <i></i>
                </label>
            </section>

        </div>

        <div class="row">
            <section class="col col-3">
                <label class="label">Data Competência</label>
                <label class='input-group date'>
                    <input type='text' name='cp_dt_competencia' id='cp_dt_competencia' class="form-control input-datepicker" value="{{(isset($contasPagar['cp_dt_competencia'])) ? \Carbon\Carbon::parse($contasPagar['cp_dt_competencia'])->format('d/m/Y') : '' }}" />
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Data de Vencimento</label>
                <label class='input-group date'>
                    <input type='text' name='cp_dt_vencimento' id='cp_dt_vencimento' class="form-control input-datepicker" value="{{(isset($contasPagar['copa_dt_vencimento'])) ? \Carbon\Carbon::parse($contasPagar['copa_dt_vencimento'])->format('d/m/Y') : '' }}" />
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Valor Total</label>
                <label class="input">
                    <input class="mask-money" type="text" id="cp_valor_total" name="cp_valor_total" value="{{ isset($contasPagar['copa_valor']) ? \App\Utils\Mask::dinheiro($contasPagar['copa_valor']) ?? '' : '' }}" required>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Possui Parcelas ?</label>
                <div class="inline-group">
                    @if(isset($contasPagar['copa_parcelas']) && $contasPagar['copa_parcelas'])
                        <label class="radio">
                            <input type="radio" value="1" id="cp_parcelas" name="cp_parcelas" checked="checked" onclick="jQueryContasPagar.possuiParcelas($(this))">
                            <i></i>Sim
                        </label>
                        <label class="radio">
                            <input type="radio" value="0" id="cp_parcelas"  name="cp_parcelas" onclick="jQueryContasPagar.possuiParcelas($(this))">
                            <i></i>Não
                        </label>
                    @else
                        <label class="radio">
                            <input type="radio" value="1" id="cp_parcelas" name="cp_parcelas" onclick="jQueryContasPagar.possuiParcelas($(this))">
                            <i></i>Sim
                        </label>
                        <label class="radio">
                            <input type="radio" value="0" id="cp_parcelas"  name="cp_parcelas" checked="checked" onclick="jQueryContasPagar.possuiParcelas($(this))">
                            <i></i>Não
                        </label>
                    @endif
                </div>
            </section>
        </div>

        <div class="row">
            <section class="col col-3">
                <label class="label">Quantidade de Parcelas</label>
                <label class="input">
                    <input type="text" id="cp_qtd_parcelas" name="cp_qtd_parcelas" value="{{ $contasPagar['copa_parcelas_qtd'] ?? '' }}" {{isset($contasPagar['copa_parcelas']) && $contasPagar['copa_parcelas'] ? '' : 'disabled'}}>
                </label>
            </section>

            <section class="col col-9">
                <label class="label">Observação</label>
                <label class="input">
                    <input type="text" id="cp_observacao" name="cp_observacao" value="{{ $contasPagar['copa_observacao'] ?? '' }}">
                </label>
            </section>
        </div>

        <div class="row">
            <section class="col col-6">
                <div class="box-body table-responsive">
                    <table id="table-contas-pagar-parcelas" class="table table-bordered table-striped {{isset($contasPagar['copa_parcelas']) && $contasPagar['copa_parcelas'] ? '' : 'hidden'}}" width="100%">
                        <thead>
                            <tr>
                                <th class="pull-center">Parcela</th>
                                <th class="pull-center">Valor</th>
                                <th class="pull-center">Data de Vencimento</th>
                                <th class="pull-center">Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($contasPagar['copa_parcelas']) && $contasPagar['copa_parcelas'])
                                @foreach($contasPagar['parcelas'] as $index => $parcela)
                                    <tr>
                                        <td class="pull-center">{{($index+1)}}</td>
                                        <td class="pull-center">{{\App\Utils\Mask::dinheiro($parcela['copp_valor'])}}</td>
                                        <td class="pull-center">{{\Carbon\Carbon::parse($parcela['copp_dt_vencimento'])->format('d/m/Y')}}</td>
                                        <td class="pull-center">
                                            <button type="button" class="btn btn-default btn-circle" onclick="jQueryContasPagar.editarParcela($(this))"><i class="fa fa-edit"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </fieldset>
    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="copc_id" name="copc_id" value="0">
    <footer>
        <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button id="btn-gerar-parcelas" type="button" class="btn btn-success {{isset($contasPagar['copa_parcelas']) && $contasPagar['copa_parcelas'] ? '' : 'hidden'}}" onclick="jQueryContasPagar.gerarParcelas($(this))"><i class="fa fa-table"></i> Gerar Parcelas</button>
        <button type="button" name="submit" class="btn btn-primary" data-title="Veículo" data-loading-text="Salvando dados..." onclick="jQueryContasPagar.salvar($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
    </footer>
</form>

@push('scripts')
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.accounting.min.js') }}></script>
    <script type="text/javascript" src="{{ asset('js/vendor/jquery.ddslick.min.js') }}"></script>

    <script>
        $('#cp_categoria').ddslick({
            width: '100%',
            showSelectedHTML: false,
            onSelected: function(data){
                $("#copc_id").val(data.selectedData.value)
            }
        });
    </script>

@endpush
