@extends('layouts.page')

@push('css')
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false">
        @if(isset($contasPagar['copa_id']))
            @include('header.header-content', ['title' => 'Contas à Pagar - Cadastrar', 'action' => ['financeiro.contas-pagar.header-menu-btn', 'financeiro.contas-pagar.header-menu-btn-list']])
        @else
            @include('header.header-content', ['title' => 'Contas à Pagar - Cadastrar', 'action' => 'financeiro.contas-pagar.header-menu-btn-list'])
        @endif

        <!-- widget div-->
            <div>
                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">
                    @include('financeiro.contas-pagar.formulario')
                </div>
            </div>
        </div>
    </article>
@endsection

@push('scripts')
    <script type="text/javascript" src={{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}></script>
    <script type="text/javascript" src={{ asset('bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-calendario.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.accounting.min.js') }}></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-contas-pagar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/jquery.ddslick.min.js') }}"></script>

    <script>
        $('#cp_categoria').ddslick({
            width: '100%',
            showSelectedHTML: false,
            onSelected: function(data){
                $("#copc_id").val(data.selectedData.value)
            }
        });
    </script>

@endpush

