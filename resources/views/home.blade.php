@extends('layouts.page')

@section('content')
    <!-- row -->
    <div class="row">

        <!-- col -->
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">

                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-home"></i>
                Painel Administrativo
                <span>Dashboard</span>
            </h1>
        </div>
        <!-- end col -->

        <!-- right side of the page with the sparkline graphs -->
        <!-- col -->
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            <!-- sparks -->
            <ul id="sparks">
                <li class="sparks-info">
                    <h5> O.S Abertas<span class="txt-color-blue">452</span></h5>
                    <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
                        456, 154, 354, 45, 256
                    </div>
                </li>
                <li class="sparks-info">
                    <h5>Comparativo mês anterior <span class="txt-color-purple"><i class="fa fa-arrow-circle-up" data-rel="bootstrap-tooltip" title="Increased"></i> 25%</span></h5>
                    <div class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm"></div>
                </li>
                <li class="sparks-info">
                    <h5> Vendas Realizadas <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;1254</span></h5>
                    <div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm"></div>
                </li>
            </ul>
            <!-- end sparks -->
        </div>
        <!-- end col -->

    </div>
    <!-- end row -->

    <!--
                The ID "widget-grid" will start to initialize all widgets below
                You do not need to use widgets if you dont want to. Simply remove
                the <section></section> and you can use wells or panels instead
                -->

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">
            @include('home.widgets.lembretes')
            @include('home.widgets.calendario')
        </div>
        <!-- end row -->

        <div class="row">
            @include('home.widgets.ultimas-tarefas')
        </div>

        <!-- row -->

        <div class="row">

            <!-- a blank row to get started -->
            <div class="col-sm-12">
                <!-- your contents here -->
            </div>

        </div>

        <!-- end row -->

    </section>
    <!-- end widget grid -->
@endsection
