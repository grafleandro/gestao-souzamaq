<form role="form" class="smart-form">
    <div class="row">
        <div class="col col-6">
            <fieldset>
                <legend>Formas de Pagamento</legend>

                <label class="label">Selecionar</label>
                <label class="input">
                    <select class="form-control" id="os-form-pgto" name="os-form-pgto" onchange="jQueryOSFinalizar.selecionarFormaPgto($(this))">
                        <option value="">Selecione</option>
                        @foreach($forma_pgto as $index => $value)
                            <option data-parcelas="{{ $value['cofp_parcelas'] }}" data-troco="{{ $value['cofp_troco'] }}" value="{{ $value['cofp_id'] }}">{{ $value['cofp_titulo'] }}</option>
                        @endforeach
                    </select>
                </label>

                <div class="col col-12 no-padding-left margin-top-10 hidden" id="os-parcelas">
                </div>

                <div class="col col-8 no-padding-left margin-top-10">
                    <label class="label">TOTAL À PAGAR</label>
                    <label class="input">
                        <input class="input-lg mask-money" type="text" id="os-total-pagar" name="os-total-pagar" placeholder="R$ 0,00">
                    </label>
                </div>

                <div class="col col-4 margin-top-34 no-padding-right">
                    <button type="button" class="btn btn-default btn-lg" id="os-btn-confirmar" disabled="disabled" onclick="jQueryOSFinalizar.calcularPgto($(this))"><i class="fa fa-check"></i> Confirmar</button>
                </div>

            </fieldset>
        </div>
        <div class="col col-6">
            <fieldset>
                <legend>Descontos</legend>
                <section class="col col-6">
                    <label class="label">Porcentagem</label>
                    <label class="input">
                        <input class="mask-percent" type="text" id="os-desconto-porc" name="os-desconto-porc" onblur="jQueryOSFinalizar.calcularDescPorcet($(this))" placeholder="%">
                    </label>
                </section>
                <section class="col col-6">
                    <label class="label">Monetário</label>
                    <label class="input">
                        <input class="mask-money" type="text" id="os-desconto-din" name="os-desconto-din" onblur="jQueryOSFinalizar.calcularDescDinheiro($(this))" placeholder="R$">
                    </label>
                </section>
            </fieldset>



            <fieldset>
                <legend>Total</legend>
                <div class="row no-margin-left sale-panel-details">
                    <div class="col-md-6">
                        <span><strong>SUBTOTAL</strong></span>
                    </div>
                    <div class="col-md-6 pull-center">
                        <span id="os-fina-subtotal">R$ 0,00</span>
                    </div>
                </div>
                <div class="row no-margin-left sale-panel-details">
                    <div class="col-md-6">
                        <span><strong>DESCONTOS</strong></span>
                    </div>
                    <div class="col-md-6 pull-center">
                        <span id="os-fina-desconto">R$ 0,00</span>
                    </div>
                </div>
                <div class="row no-margin-left sale-panel-details">
                    <div class="col-md-6">
                        <span><strong>TOTAL A PAGAR</strong></span>
                    </div>
                    <div class="col-md-6 pull-center">
                        <span id="os-fina-total">R$ 0,00</span>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <input type="hidden" id="os-parcelas-qtd" name="os-parcelas-qtd" value="">

    <footer class="pull-right no-margin">
        <button type="button" name="submit" class="btn btn-default btn-md" data-dismiss="modal" id="os-btn-cancelar"><i class="fa fa-times"></i> Cancelar</button>
        <button type="button" name="submit" class="btn btn-success btn-md" id="os-btn-finalizar" onclick="jQueryOSFinalizar.finalizarOS($(this))"><i class="fa fa-check"></i> Finalizar</button>
    </footer>
</form>

<div class="col-md-12 sale-shortcut-key margin-top-10">
    <strong> F10 - Cancelar | F11 - Definir Desconto| F12 - Finalizar</strong>
</div>

<script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
<script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
<script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
<script type="text/javascript" src="{{ asset('js/custom/jquery-app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom/jquery-form.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom/jquery.key.js') }}"></script>
