<!-- NEW COL START -->
<article class="col col-sm-12">

        <!-- widget div-->
        <div>

            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->

            </div>
            <!-- end widget edit box -->

            <!-- widget content -->
            <div class="widget-body">

                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive table-produto-listar">
                        <table id="tabela-ordem-servico-produtos" class="table table-bordered table-striped" width="100%">
                            <thead>
                                 <tr>
                                    <th data-hide="phone" class="text-center" style="width: 5%; text-align: center">#</th>
                                    <th data-hide="phone" class="text-center">Produto</th>
                                    <th data-class="expand" class="text-center" style="width: 10%">Quantidade</th>
                                    <th data-class="expand" class="text-center" style="width: 15%">Valor Unit.</th>
                                    <th data-class="expand" class="text-center" style="width: 20%">Total</th>
                                    <th class="text-center" style="width: 10%">Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <!-- end widget content -->

        </div>
        <!-- end widget div -->
    <!-- end widget -->
</article>

