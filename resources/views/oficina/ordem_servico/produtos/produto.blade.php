    <fieldset>
        <div class="row">
            <section class="col col-sm-7">
                <label class="label">Produto</label>
                <label class="input">
                    <input type="text" id="produto" name="produto" placeholder="Selecionar Produto..." onfocus="jQueryOrdemServico.autocompleteProduto($(this))">
                </label>
            </section>
            <section class="col col-5">
                <div class="col col-8">
                    <label class="label">Quantidade</label>
                    <label class="input">
                        <input type="text" class="mask-decimal sale-input-rigth" id="prod_qtd" name="prod_qtd" onchange="jQueryOrdemServico.confereEstoqueProduto($(this))">
                    </label>
                </div>
                <div class="col col-4">
                    <button type="button" class="btn btn-primary btn-circle btn-margin-top" onclick="jQueryOrdemServico.addProdutoTabela()"><i class="fa fa-plus"></i></button>
                </div>
            </section>
        </div>


        <div class="row">
            @include('oficina.ordem_servico.produtos.pecas-listar')
        </div>
    </fieldset>