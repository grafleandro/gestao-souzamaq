<!-- NEW COL START -->
<article class="col col-md-12 col-sm-12 no-padding-top">
    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false">
        @include('header.header-content', ['title' => 'Ordem de Serviço', 'action' => 'oficina.ordem_servico.header-menu-btn-list'])


        <!-- widget div-->

            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">

                    <ul id="tab_ordem_servico" class="nav nav-tabs bordered">
                        <li class="active">
                            <a href="#s1" data-toggle="tab"> <i class="fa fa-fw fa-lg fa-user"></i>Cliente</a>
                        </li>
                        <li>
                            <a href="#s2" data-toggle="tab" id=""><i class="fa fa-fw fa-lg fa-wrench"></i> Serviços <span class="badge bg-color-blue txt-color-white" id="num_servico">0</span></a>
                        </li>
                        <li>
                            <a href="#s3" data-toggle="tab"><i class="fa fa-fw fa-lg fa-gear"></i> Produtos<span class="badge bg-color-blue txt-color-white" id="num_produto">0</span></a>
                        </li>
                        <li>
                            <a href="#s4" data-toggle="tab"><i class="fa fa-fw fa-lg fa-reorder"></i> Observações</a>
                        </li>
                    </ul>
                    <div class="col-md-12 sale-shortcut-key margin-top-10">
                        <strong>F1 - Cliente | F2 - Serviços | F3 - Produtos | F4 - Observações | F7 - Limpar | F8 - Salvar | F9 - Finalizar</strong>
                    </div>
                    <form role="form" id="form_ordem_servico" {{ (!isset($os['orse_id'])) ? 'method=post action=' . url('oficina/ordem_servico') : 'method=put action=' . url('oficina/ordem_servico').'/'.$os['orse_id']}} class="smart-form">
                        <div id="content_ordem_servico" class="tab-content padding-10">
                            <div class="tab-pane fade in active" id="s1">
                                @include('oficina.ordem_servico.cliente-veiculo.cliente-veiculo')
                            </div>
                            <div class="tab-pane fade" id="s2">
                                @include('oficina.ordem_servico.servico.servicos')
                            </div>
                            <div class="tab-pane fade" id="s3">
                                @include('oficina.ordem_servico.produtos.produto')
                            </div>
                            <div class="tab-pane fade" id="s4">
                                @include('oficina.ordem_servico.obs.observacao')
                            </div>
                        </div>
                        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="id_cliente" name="id_cliente" value="{{ $os['clie_id'] ?? '' }}">
                        <input type="hidden" id="id_veiculo" name="id_veiculo" value="{{ $os['veic_id'] ?? '' }}">
                        <input type="hidden" id="checklist_id" name="checklist_id" value="{{ $os['ckli_id'] ?? '' }}">

                    </form>
                    <table class="table table-hover padding-top-10">
                        <tbody>
                            <tr>
                                <td colspan="4">Total de Produtos</td>
                                <td class="text-right"><strong id="total_produto" name="total_produto">{{isset($os['orse_total_produtos']) ? \App\Utils\Mask::dinheiro($os['orse_total_produtos']) : 'R$ 0,00'}}</strong></td>
                            </tr>
                            <tr>
                                <td colspan="4">Total de Serviços</td>
                                <td class="text-right"><strong id="total_servico" name="total_servico">{{isset($os['orse_total_servico']) ? \App\Utils\Mask::dinheiro($os['orse_total_servico']) : 'R$ 0,00'}}</strong></td>
                            </tr>
                            <tr>
                                <td colspan="4">Total a Pagar</td>
                                <td class="text-right" ><strong><span id="total_os" name="total_os">{{isset($os['orse_total_os']) ? \App\Utils\Mask::dinheiro($os['orse_total_os']) : 'R$ 0,00'}}</span></strong></td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    @include('oficina.ordem_servico.acoes')
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

    </div>

    <!-- end widget -->
</article>

@push('scripts')

    <script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src={{ asset('js/vendor/modernizr-custom.js') }}></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-checklist-interno.js') }}"></script>
@endpush
