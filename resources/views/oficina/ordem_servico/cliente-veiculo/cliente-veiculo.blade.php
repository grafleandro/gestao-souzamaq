{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: lms--}}
 {{--* Date: 25/10/18--}}
 {{--* Time: 23:32--}}
 {{--*/--}}

    <fieldset>
        <div class="row">
            @if(@isset($os))
            <section class="col col-6">
                <label class="label">Cliente</label>
                <label class="input">
                    <input type="text" id="nome_cliente" name="nome_cliente" onfocus="jQueryCliente.autocomplete($(this))" value="{{ (!empty($os['ordem_servico_cliente']['clie_nome_fantasia'])) ? $os['ordem_servico_cliente']['clie_nome_fantasia']:  $os['ordem_servico_cliente']['clie_nome_razao_social'] }}">
                </label>
            </section>
            @else
                <section class="col col-6">
                    <label class="label">Cliente</label>
                    <label class="input">
                        <input type="text" id="nome_cliente" name="nome_cliente" onfocus="jQueryCliente.autocomplete($(this))" value="">
                    </label>
                </section>
            @endif
            <section class="col col-2 pull-center">
                <label class="label text-align-center">Checklist</label>
                <label class="input">
                    <a class="btn btn-info btn-sm fa fa-calendar-check-o" onclick="jQueryOrdemServico.selecionaChecklist()"></a>
                </label>
            </section>
            <section class="col col-4">
                <label class="label">Situação</label>
                <label class="select">
                    <?php use Illuminate\Support\Facades\Session;\App\Utils\FormUtils::select(\App\Utils\SituacaoUtils::situacao(), 'situacao', 'situacao', 'form-control form-cascade-control', ($os['orse_situacao']) ?? null, false, '', '-- Selecionar --', true, 'situ_id', 'situ_titulo'); ?>
                    <i></i>
                </label>
            </section>
        </div>
    </fieldset>
    <fieldset>
        <div class="row">
            <section class="col col-6">
                <label class="label">Veículo</label>
                <label class="input">
                    <input type="text" id="nome_veiculo" name="nome_veiculo" onfocus="jQueryVeiculo.autocomplete($(this))" placeholder="Placa, Modelo ou Tecla Espaço" value="{{ (isset($os)) ? strtoupper($os['ordem_servico_veiculo']['veic_modelo']) . ' / ' . \App\Utils\Mask::placaVeiculo($os['ordem_servico_veiculo']['veic_placa']): '' }}">
                </label>
            </section>
            <section class="col col-3">
                <label class="label">Previsão de Entrega</label>
                <label class="input">
                    <div class='input-group date'>
                        <input name='dt_entrega' id='dt_entrega' type='text' class="form-control input-datepicker" value="{{ $os['orse_prev_entrega'] ?? '' }}" required/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </label>
            </section>


        </div>
    </fieldset>

    <fieldset>
        <div class="row">
            <section class="pull-center">
                <section>
                    <label class="label">Descrição do Cliente</label>
                    <label class="textarea">
                        <textarea rows="2" name="descricao_servico" id="descricao_servico">{{ $os['orse_descri_serv_cliente'] ?? '' }}</textarea>
                    </label>
                </section>
            </section>
        </div>
    </fieldset>
