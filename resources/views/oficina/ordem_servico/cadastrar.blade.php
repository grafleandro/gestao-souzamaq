@extends('layouts.page')

@push('css')
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/multiselect/css/bootstrap-multiselect.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.7/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('vendor/popupMultiSelect/css/multiselect.min.css') }}">
@endpush

@section('content')
    {{--@include('oficina.ordem_servico.acoes')--}}

    @include('oficina.ordem_servico.dados-os')

@endsection

@push('scripts')
    <script>
        let os = <?php echo  json_encode($os ?? null, JSON_FORCE_OBJECT) ; ?>;

        if(os){
            sessionStorage.setItem("produto_os", JSON.stringify(os.ordem_servico_produto));
            sessionStorage.setItem("servico_os", JSON.stringify(os.ordem_servico_servicos));
        }

    </script>
    <script src={{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}></script>
    <script src={{ asset('bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src={{ asset('vendor/multiselect/js/bootstrap-multiselect.js') }}></script>
    <script src={{ asset('js/custom/jquery-calendario.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.accounting.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-cliente.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-veiculo.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.key.js') }}></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-ordem-servico.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-ordem-servico-atalhos.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-os-finalizar.js') }}"></script>
{{--    <script src={{ assets('vendor/popupMultiSelect/js/multiselect.js') }}></script>--}}
    {{--<script>--}}
        {{--$(document).ready(function() {--}}
            {{--$('#mecanicos').multiselect({--}}
{{--//                includeSelectAllOption: true,--}}
{{--//                nonSelectedText: 'Nenhum Item Selecionado', /* Texto de Traduzido */--}}
{{--//                selectAllText: 'Selecionar Todos', /* Texto de Traduzido */--}}
{{--//                allSelectedText: 'Selecionar Todos', /* Texto de Traduzido */--}}
{{--//                nSelectedText: 'item', /* Texto de definição para itens selecionados */--}}
{{--//                filterPlaceholder: "Procurar", /* Texto de Traduzido */--}}
{{--//                minimumCountSelected: 1 /* Quantidade de itens a serem mostrado */--}}
                {{--title: "Selecionar Mecânicos",--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
@endpush
