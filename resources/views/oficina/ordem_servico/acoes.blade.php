
{{--/**--}}
 {{--* Created by PhpStorm.--}}
 {{--* User: lms--}}
 {{--* Date: 26/10/18--}}
 {{--* Time: 00:33--}}
 {{--*/--}}
<article class="col col-lg-12 padding-top-10">

        <div class="padding-bottom-10">
            <footer>
                <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                <button type="button" name="submit" class="btn btn-primary" data-title="Ordem de Serviço" data-loading-text="Salvando dados..." onclick="jQueryOrdemServico.salvarOS($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                <button type="button" name="submit" class="btn btn-success" data-title="Ordem de Serviço" data-loading-text="Salvando dados..." onclick="jQueryOrdemServico.finalizarOS($(this))"><i class="fa fa-check" ></i> Finalizar</button>
            </footer>

        </div>

</article>