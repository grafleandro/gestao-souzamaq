<!-- NEW COL START -->
<article class="col col-md-12 col-sm-12">
    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

    <!-- widget div-->
        <div>

            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->

            </div>
            <!-- end widget edit box -->

            <!-- widget content -->
            <div class="widget-body no-padding">
                <div class="row">
            <div class="text-center col-md-8 ">
                {{--<h2> <b>Total da Ordem de Serviço</b></h2>--}}
            </div>
                <div class="smart-form col-md-4 col-md-offset-8" >
                    <fieldset >
                        <div class="row">
                            <section class="col col-sm-6">
                                <label class="label">Total de Produtos:</label>
                                <label class="input">
                                    <input class='border-ra'type="text" id="total_produtos" name="total_produtos" class="form-control">
                                </label>
                            </section>

                            <section class="col col-sm-6">
                                <label class="label">Total de Serviços:</label>
                                <label class="input">
                                    <input type="text" name="total_servicos" id="total_servicos" class="form-control">
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-sm-6">
                                <label class="label">Desconto R$</label>
                                <label class="input">
                                    <input type="text" name="desconto_real" id="desconto_real" class="form-control">
                                </label>
                            </section>
                            <section class="col col-sm-6">
                                <label class="label">Desconto %</label>
                                <label class="input">
                                    <input type="text" name="desconto_pocentagem" id="desconto_pocentagem" class="form-control">
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-sm-12">
                                <label class="label">Adiantamento:</label>
                                <label class="input">
                                    <input type="text" name="adiantamento" id="adiantamento" class="form-control">
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-sm-12 pull-center text-align-center">
                                <h2 class="pull-center">Total a Pagar:<br><b id="valor_total" style="color:black"> R$ 50,00</b></h2>
                            </section>
                        </div>
                    </fieldset>

                </div>
            </div>
            </div>
        </div>
    </div>
</article>
