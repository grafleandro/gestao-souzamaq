<form role="form" id="form_ordem_servico_lista"  method=post action={{url('oficina/ordem_servico/') }} class="smart-form">
    <div class="row">
        <label class="label col col-2"><strong>Filtrar por Status:</strong></label>
        <section class="col col-2">
            <label class="select">
                <?php \App\Utils\FormUtils::select(\App\Utils\SituacaoUtils::situacao(), 'situacao', 'situacao', 'form-control form-cascade-control', ($interno['ckli_status']) ?? null, false, 'onchange="jQueryOrdemServico.init()"', '-- Selecionar --', true, 'situ_id', 'situ_titulo'); ?>
                <i></i>
            </label>
            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
        </section>
        <section>
            <div class="col col-8 text-right">
                <div>
                    <a class="btn btn-default btn-lg fa fa-print">  Imprimir</a>
                </div>
            </div>
        </section>
    </div>
</form>