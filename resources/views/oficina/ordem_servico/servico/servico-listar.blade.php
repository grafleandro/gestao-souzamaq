<!-- NEW COL START -->
<article class="col col-sm-12">

        <!-- widget div-->
        <div>

            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->

            </div>
            <!-- end widget edit box -->

            <!-- widget content -->
            <div class="widget-body">

                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive table-servico-listar">
                        <table id="tabela-ordem-servico" class="table table-bordered table-striped" width="100%">
                            <thead>
                            <tr>
                                <th data-hide="phone" class="text-center" style="width: 5%; text-align: center">#</th>
                                <th data-hide="phone" class="text-center">Descrição</th>
                                <th data-class="expand" class="text-center">Quantidade</th>
                                <th data-class="expand" class="text-center" style="width: 30%">Valor</th>
                                <th class="text-center">Ação</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <!-- end widget content -->

        </div>
        <!-- end widget div -->
    <!-- end widget -->
</article>

