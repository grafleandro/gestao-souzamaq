<fieldset>
    <div class="row">
        <section class="col col-sm-10">
            <label class="label">Serviço</label>
            <label class="input">
                <input type="text" id="servico" name="servico" placeholder="Selecionar Produto..." onfocus="jQueryOrdemServico.autocompleteServico($(this))">
            </label>
        </section>
        <section class="col col-2">
            <label class="label">Quantidade</label>
            <label class="input">
                <input type="text" class="mask-decimal sale-input-rigth" id="serv_qtd" name="serv_qtd">
            </label>
        </section>
    </div>

    <div class="row">
        <section class="col col-3">
            <label class="label">Valor</label>
            <label class="input">
                <input type="text" class="mask-money sale-input-rigth" id="serv_valor" name="serv_valor">
            </label>
        </section>
        <section class="col col-6">
            <label class="label">Macânico</label>
            <label class="input">
                <div class="form-group">
                    <?php \App\Utils\FormUtils::multiSelect(\App\Model\ColaboradorModel::where('empr_id', Session::get('empr_id'))->get()->toArray(), 'mecanicos', 'mecanicos', 'select2', false, array(), '', 'cola_id', 'cola_nome', $mecanicos ?? []); ?>
                </div>
            </label>
        </section>
        <section class="col col-2">
            <label class="label">Comissão</label>
            <label class="input">
                <input type="text" class="mask-percent sale-input-rigth" id="serv_comissao" name="serv_comissao">
            </label>
        </section>
        <section>
            <div class="col col-1">
                <button type="button" class="btn btn-primary btn-circle btn-margin-top" onclick="jQueryOrdemServico.addServicoTabela()"><i class="fa fa-plus"></i></button>
            </div>
        </section>
    </div>


    <div class="row">
        @include('oficina.ordem_servico.servico.servico-listar')
    </div>
</fieldset>