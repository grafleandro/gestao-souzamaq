@extends('layouts.page')

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Check-List Externo'])
        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">

                    <form role="form" id="form_checklist_externo"

                          @if(!isset($externo['chex_id'])) method="post"  action="{{url('oficina/checklist/externo')}}"

                          @else  method="put"  action="{{url('oficina/checklist/externo').'/'.$externo['chex_id']}}" @endif

                          class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section class="col col-4">
                                    <label class="label">Cliente</label>
                                    <label class="input">
                                        <input type="text" id="cliente" name="cliente" placeholder="Pesquisar pelo nome do Cliente" value="{{ $externo['chex_cliente'] ?? '' }}">
                                    </label>
                                </section>

                                <section class="col col-4">
                                    <label class="label">Responsável Pelo Serviço</label>
                                    <label class="input">
                                        <input type="text" id="responsavel" name="responsavel" placeholder="Pesquisar pelo nome do Colaborador" value="{{ $externo['chex_responsavel'] ?? '' }}">
                                    </label>
                                </section>

                                <section class="col col-2">
                                    <label class="label">Placa</label>
                                    <label class="input">
                                        <input type="text" class="mask-placa" id="placa" name="placa" placeholder="AAA-0000" value="{{ $externo['chex_placa'] ?? '' }}">
                                    </label>
                                </section>

                                <section class="col col-2">
                                    <label class="label">Nº O.S</label>
                                    <label class="input">
                                        <input type="text" id="numero_os" name="numero_os" placeholder="AS1235" value="{{ $externo['chex_num_os'] ?? '' }}">
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <section class="col col-4">
                                    <label class="label">Status</label>
                                    <label class="select">
                                        <?php \App\Utils\FormUtils::select(\App\Utils\SituacaoUtils::situacaoCheckList(), 'status', 'status', 'form-control form-cascade-control', ($externo['chex_status']) ?? null, false, array(), '-- Selecionar --', true, 'situ_id', 'situ_titulo'); ?>
                                        <i></i>
                                    </label>
                                </section>

                                <section class="col col-8">
                                    <label class="label">Descrição</label>
                                    <label class="input">
                                        <input type="text" id="descricao" name="descricao" placeholder="Informe o motivo da Check-List" value="{{ $externo['chex_descricao'] ?? '' }}">
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                        <footer>
                            <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                            <button type="button" name="submit" class="btn btn-primary" data-title="CheckList Externo" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </article>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/custom/jquery-mask-custom.js') }}"></script>
@endpush