@extends('layouts.page')

@push('css')
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/multiselect/css/bootstrap-multiselect.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/popupMultiSelect/css/multiselect.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/clockpicker/dist/bootstrap-clockpicker.min.css') }}">
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">
            @if(isset($interno['ckli_id']) && $interno['ckli_id'])
                @include('header.header-content', ['title' => 'Check-List Interno','action' => ['oficina.checklist.interno.header-menu-btn', 'oficina.checklist.interno.header-menu-btn-list']])
            @else
                @include('header.header-content', ['title' => 'Check-List Interno','action' => 'oficina.checklist.interno.header-menu-btn-list'])
            @endif
            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">

                    <form role="form" id="form_checklist_interno" {{ (!isset($interno['ckli_id'])) ? 'method=post action=' . url('oficina/checklist/interno') : 'method=put action=' . url('oficina/checklist/interno').'/'.$interno['ckli_id']}}

                          class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section class="col col-4">
                                    <label class="label">O.S Serviço</label>
                                    <label class="input">
                                        <input type="text" id="os_externo_servico" name="os_externo_servico" placeholder="Número da Ordem de Serviço        " value="{{$interno['ckli_os_externo_servico'] ?? ''}}">
                                    </label>
                                </section>

                                <section class="col col-4">
                                    <label class="label">O.S Peças</label>
                                    <label class="input">
                                        <input type="text" id="os_externo" name="os_externo" placeholder="Número da Ordem de Peças" value="{{$interno['ckli_os_externo'] ?? ''}}">
                                    </label>
                                </section>


                                <section class="col col-4">
                                    <label class="label">Status</label>
                                    <label class="select">
                                        <?php use Illuminate\Support\Facades\Session;\App\Utils\FormUtils::select(\App\Utils\SituacaoUtils::situacaoCheckList(), 'status', 'status', 'form-control form-cascade-control', ($interno['ckli_status']) ?? null, false, array(), '-- Selecionar --', true, 'situ_id', 'situ_titulo'); ?>
                                        <i></i>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <section class="col col-5">
                                    <label class="label">Cliente</label>
                                    <label class="input">
                                        <input type="text" id="nome_cliente" name="nome_cliente" onfocus="jQueryCliente.autocomplete($(this))" placeholder="Nome ou Razão Social" value="{{ $interno['checklist_interno_cliente']['clie_nome_razao_social'] ?? '' }}" required>
                                    </label>
                                </section>

                                <section class="col col-5">
                                    <label class="label">Veículo</label>
                                    <label class="input">
                                        <input type="text" id="nome_veiculo" name="nome_veiculo" onfocus="jQueryVeiculo.autocomplete($(this))" placeholder="Placa, Modelo ou Tecle 2x Espaço" value="{{ (isset($interno)) ? strtoupper($interno['checklist_interno_veiculo']['veic_modelo']) . ' / ' . \App\Utils\Mask::placaVeiculo($interno['checklist_interno_veiculo']['veic_placa']): '' }}" required>
                                    </label>
                                </section>
                                <section class="col col-2">
                                    <label class="label">KM</label>
                                    <label class="input">
                                        <input type="text" class="mask-integer" id="km_local" name="km_local" placeholder="km no pátio" value="{{ $interno['ckli_km'] ?? '' }}">
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <section class="col col-4">
                                    <section>
                                        <label class="label">Descrição do Cliente</label>
                                        <label class="textarea">
                                            <textarea rows="4" name="servico" id="servico" required>{{ $interno['ckli_servico'] ?? '' }}</textarea>
                                        </label>
                                    </section>
                                </section>

                                <section class="col col-4">
                                    <section>
                                        <label class="label">Observação</label>
                                        <label class="textarea">
                                            <textarea rows="4" id="obs" name="obs">{{ $interno['ckli_observacao'] ?? '' }}</textarea>
                                        </label>
                                    </section>
                                </section>

                                <section class="col col-4">
                                    <section>
                                        <label class="label">Dados do Orçamento</label>
                                        <label class="textarea">
                                            <textarea rows="4" id="objetos" name="objetos">{{ $interno['ckli_objetos_veiculo'] ?? '' }}</textarea>
                                        </label>
                                    </section>
                                </section>
                            </div>

                            <div class="row">

                                <section class="col col-6">
                                    <label class="label">Mecânico</label>
                                    <?php \App\Utils\FormUtils::multiSelect(\App\Model\ColaboradorModel::where('empr_id', Session::get('empr_id'))->get()->toArray(), 'mecanicos', 'mecanicos', 'custom-scroll', false, '', '-- Selecionar --', 'cola_id', 'cola_nome', $mecanicos ?? []); ?>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Previsão da Data de Entrega</label>
                                    <label class="input">
                                        <div class='input-group date'>
                                            <input name='dt_entrega' id='dt_entrega' type='text' class="form-control input-datepicker" value="{{ $interno['ckli_previsao_entrega'] ?? '' }}" required/>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Valor</label>
                                    <label class="input">
                                        <input type="text" id="valor" name="valor" class="mask-money" value="{{(isset($interno['ckli_valor'])) ? \App\Utils\Mask::dinheiro($interno['ckli_valor']) : ''}}">
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="id_cliente" name="id_cliente" value="{{ $interno['clie_id'] ?? '0' }}">
                        <input type="hidden" id="id_veiculo" name="id_veiculo" value="{{ $interno['veic_id'] ?? '0' }}">
                        <footer>
                            <button type="reset" name="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                            <button type="button" name="submit" id="submit" class="btn btn-primary" data-title="CheckList Interno" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </article>
@endsection

@push('scripts')
    <script type="text/javascript" src={{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}></script>
    <script type="text/javascript" src={{ asset('bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-calendario.js') }}></script>
    <script type="text/javascript" src={{ asset('vendor/multiselect/js/bootstrap-multiselect.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.key.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-cliente.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-veiculo.js') }}></script>
    <script type="text/javascript" src={{ asset('vendor/popupMultiSelect/js/multiselect.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-checklist-interno.js') }}></script>

    <script type="text/javascript" src="{{ asset('vendor/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/clockpicker/assets/highlight.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            $('.clockpicker').clockpicker()
                .find('input').change(function(){
                console.log(this.value);
            });

            hljs.configure({tabReplace: '    '});
            hljs.initHighlightingOnLoad();

            $('#mecanicos').multiselect({
                minimumCountSelected: 1, /* Quantidade de itens a serem mostrado */
                title: "Selecionar Mecânicos",
                riquired:true,
            });
        });
    </script>
@endpush
