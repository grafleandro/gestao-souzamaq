<form class="smart-form" id="form-almox-filtro" style="display: none">
    <fieldset>
        <legend>Busca Avaçada</legend>
        <div class="row">
            <section class="col col-4">
                <label class="label">Cliente</label>
                <label class="input">
                    <input type="text" id="cliente" name="cliente" onfocus="jQueryCliente.autocomplete($(this))">
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Veículo</label>
                <label class="input">
                    <input type="text" id="veiculo" name="veiculo" onfocus="jQueryVeiculo.autocomplete($(this))">
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Mecânico</label>
                <label class="input">
                    <input type="text" id="mecanico" name="mecanico" onfocus="jQueryColaborador.autocomplete($(this))">
                </label>
            </section>


        </div>
        <div class="row">

            <section class="col col-4">
                <label class="label">Data Inicio</label>
                <label class="input">
                    <div class='input-group date'>
                        <input name='data_inicio' id='data_inicio' type='text' class="form-control input-datepicker" value="{{ $interno['ckli_previsao_entrega'] ?? '' }}" required/>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Data Fim</label>
                <label class="input">
                    <div class='input-group date'>
                        <input name='data_fim' id='data_fim' type='text' class="form-control input-datepicker" value="{{ $interno['ckli_previsao_entrega'] ?? '' }}" required/>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Status</label>
                <label class="select">
                    <?php \App\Utils\FormUtils::select(\App\Utils\SituacaoUtils::situacaoCheckList(), 'status', 'status', 'form-control form-cascade-control', (isset($interno['ckli_status'])) ? $interno['ckli_status'] : \App\Utils\SituacaoUtils::_ORDEM_SERVICO, false, null, '-- Selecionar --', true, 'situ_id', 'situ_titulo'); ?>
                    <i></i>
                </label>
            </section>
        </div>
    </fieldset>

    <input type="hidden" id="id_cliente" name="id_cliente" value="">
    <input type="hidden" id="id_colaborador" name="id_colaborador" value="">
    <input type="hidden" id="id_veiculo" name="id_veiculo" value="">

    <footer>
        <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar Filtro</button>
        <button type="button" name="submit" class="btn btn-success" onclick="jQueryCheckListInterno.filtroListar($(this))"><i class="fa fa-filter"></i> Filtrar</button>
        <button type="button" name="submit" class="btn btn-warning" onclick="jQueryCheckListInterno.imprimirRelatorio($(this))"><i class="fa fa-print"></i> Imprimir</button>
    </footer>
</form>
