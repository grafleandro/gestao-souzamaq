@extends('layouts.page')

@push('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col col-md-12 col-sm-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false">
        @include('header.header-content', ['title' => 'CheckList Interno - Listagem', 'action' => ['oficina.checklist.interno.header-menu-btn', 'oficina.checklist.interno.header-menu-btn-filter']])

        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">



                    <div class="box">
                        <!-- /.box-header -->
                        @include('oficina.checklist.interno.filtros')

                        <div class="box-body table-responsive table-listar-checklist-interno pull-right">

                        </div>
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
        <!-- end widget -->
    </article>

@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-calendario.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery.key.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/custom/jquery-veiculo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-colaborador.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-cliente.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-checklist-interno.js') }}"></script>
@endpush
