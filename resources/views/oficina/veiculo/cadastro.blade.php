
<form id="form-veiculo" class="smart-form" action="{{ url('oficina/checklist/interno/create') }}" method="get">
    <div class="row">
        <section class="col col-4">
            <label class="label">Marca</label>
            <label class="input">
                <input type="text" name="cad_marca" id="cad_marca" value="{{ $veiculo['veic_marca'] ?? '' }}" required>
            </label>
        </section>

        <section class="col col-4">
            <label class="label">Modelo</label>
            <label class="input">
                <input type="text" name="cad_modelo" id="cad_modelo" value="{{ $veiculo['veic_modelo'] ?? '' }}" required>
            </label>
        </section>

        <section class="col col-4">
            <label class="label">Placa</label>
            <label class="input">
                <input type="text" name="cad_placa" id="cad_placa" value="{{ $veiculo['veic_placa'] ?? '' }}" >
            </label>
        </section>
    </div>


        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

        <input type="hidden" id="id_cliente" name="id_cliente" value="{{ $cliente }}">

        <footer>
            <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
            <button type="button" name="submit" class="btn btn-primary" data-title="Cadastrar Veículo" data-loading-text="Salvando dados..." onclick="jQueryCheckListInterno.cadastro($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
        </footer>
    </div>
</form>
<script type="text/javascript" src={{ asset('js/custom/jquery-app.js') }}></script>
<script type="text/javascript" src={{ asset('js/custom/jquery-form.js') }}></script>

