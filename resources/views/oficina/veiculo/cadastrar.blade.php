@extends('layouts.page')

@section('content')
    <!-- NEW COL START -->
    <article class="col col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false">
        @include('header.header-content', ['title' => 'Veículo - Novo ', 'action' => 'oficina.veiculo.header-menu-btn-list'])

        <!-- widget div-->
            <div>
                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">
                    @include('oficina.veiculo.formulario', ($veiculo) ?? [])
                </div>
            </div>
        </div>
    </article>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/custom/jquery-cliente.js') }}"></script>
@endpush
