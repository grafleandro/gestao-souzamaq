
@if(isset($veiculo['veic_id']))
    <form id="form-veiculo" class="smart-form" action="{{ url('oficina/veiculo/' . $veiculo['veic_id']) }}" method="put">
@else
    <form id="form-veiculo" class="smart-form" action="{{ url('oficina/veiculo') }}" method="post">
@endif
    <fieldset>
        <div class="row">
            <section class="col col-4">
                <label class="label">Cliente</label>
                <label class="input">
                    <input type="text" name="nome_cliente" onfocus="jQueryCliente.autocomplete($(this))" value="{{ $veiculo['veiculo_cliente']['clie_nome_razao_social'] ?? '' }}">
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Marca</label>
                <label class="input">
                    <input type="text" name="marca" value="{{ $veiculo['veic_marca'] ?? '' }}" required>
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Modelo</label>
                <label class="input">
                    <input type="text" name="modelo" value="{{ $veiculo['veic_modelo'] ?? '' }}" required>
                </label>
            </section>
        </div>

        <div class="row">
            <section class="col col-4">
                <label class="label">Placa</label>
                <label class="input">
                    <input type="text" name="placa" value="{{ $veiculo['veic_placa'] ?? '' }}" required>
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Cor</label>
                <label class="select">
                    {{ \App\Utils\FormUtils::select(\App\Model\VeiculoCorModel::orderBy('veco_titulo')->get()->toArray(), 'cor', 'cor', 'form-control form-cascade-control', ($veiculo['veic_cor']) ?? null, false, array(), '-- Selecionar --', true, 'veco_id', 'veco_titulo') }}
                    <i></i>
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Chassi</label>
                <label class="input">
                    <input type="text" name="chassi" value="{{ $veiculo['veic_chassi'] ?? '' }}">
                </label>
            </section>
        </div>

        <div class="row">
            <section class="col col-3">
                <label class="label">KM Inícial</label>
                <label class="input">
                    <input type="text" name="km_inicial" value="{{ $veiculo['veic_km_inicial'] ?? '' }}">
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Ano de Fabricação</label>
                <label class="input">
                    <input type="text" name="ano_fabricacao" value="{{ $veiculo['veic_ano_fabricacao'] ?? '' }}">
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Ano do Modelo</label>
                <label class="input">
                    <input type="text" name="ano_modelo" value="{{ $veiculo['veic_ano_modelo'] ?? '' }}">
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Tipo do Combustível</label>
                <label class="select">
                    {{ \App\Utils\FormUtils::select(\App\Utils\VeiculoUtils::combustivelVeiculo(), 'combustivel', 'combustivel', 'form-control form-cascade-control', ($veiculo['veic_combustivel']) ?? null, false, array(), '-- Selecionar --', true, 'id', 'titulo') }}
                    <i></i>
                </label>
            </section>
        </div>
    </fieldset>
    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="id_cliente" name="id_cliente" value="{{ $veiculo['veiculo_cliente']['clie_id'] ?? '0' }}">
    <footer>
        <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" name="submit" class="btn btn-primary" data-title="Veículo" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
    </footer>
</form>
