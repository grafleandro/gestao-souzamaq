@extends('layouts.page')

@push('css')

@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col col-md-12 col-sm-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false">
        @if(isset($servico['serv_id']))
            @include('header.header-content', ['title' => 'Serviço - Cadastrar', 'action' => ['oficina.servico.header-menu-btn', 'oficina.servico.header-menu-btn-list']])
        @else
            @include('header.header-content', ['title' => 'Serviço - Cadastrar', 'action' => 'oficina.servico.header-menu-btn-list'])
        @endif

        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">

                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            @if(isset($servico['serv_id']))
                                <form role="form" id="form_servico" method="put" action="{{url('/oficina/servico/' . $servico['serv_id'])}}" class="smart-form">
                            @else
                                <form role="form" id="form_servico" method="post" action="{{url('/oficina/servico')}}" class="smart-form">
                            @endif
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="label">Título</label>
                                            <label class="input">
                                                <input type="text" id="serv_titulo" name="serv_titulo" value="{{ $servico['serv_titulo'] ?? '' }}" required>
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Aliquota de ISS</label>
                                            <label class="input">
                                                <input class="mask-percent" type="text" id="serv_aliq_iss" name="serv_aliq_iss" value="{{ \App\Utils\Mask::porcentagem($servico['serv_aliq_iss'] ?? 0) }}" required>
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Valor</label>
                                            <label class="input">
                                                <input class="mask-money" type="text" id="serv_valor" name="serv_valor" value="{{ \App\Utils\Mask::dinheiro($servico['serv_valor'] ?? 0) }}" required>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="label">Cota Destinada a Comissão</label>
                                            <label class="input">
                                                <input type="text" class="mask-percent" id="cota_comissao" name="cota_comissao" onblur="jQueryComissao.cotaEmpresa($(this))" value="{{(isset($servico['serv_cota_comissao'])) ? \App\Utils\Mask::porcentagem($servico['serv_cota_comissao']) : ''}}" maxlength="7">
                                                <b class="tooltip tooltip-top-left">
                                                    <i class="fa fa-warning txt-color-teal"></i>
                                                    Porcentagem que será destinada as comissões sobre este Serviço.
                                                </b>
                                            </label>
                                        </section>

                                        <section class="col col-4">
                                            <label class="label">Cota Destinada a Empresa</label>
                                            <label class="input">
                                                <input type="text" class="mask-percent" id="cota_empresa" name="cota_empresa" value="{{(isset($servico['serv_cota_empresa'])) ? \App\Utils\Mask::porcentagem($servico['serv_cota_empresa']) : ''}}" maxlength="7" readonly>
                                                <b class="tooltip tooltip-top-left">
                                                    <i class="fa fa-warning txt-color-teal"></i>
                                                    Porcentagem que será destinada a Empresa sobre os valores deste Serviço.
                                                </b>
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <header>
                                    Produto
                                </header>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-3">
                                            <label class="label">Produto</label>
                                            <label class="input">
                                                <input type="text" id="serv_produto" name="serv_produto" placeholder="Selecionar Produto..." onfocus="jQueryServicos.autocompleteProduto($(this))">
                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <div class="col col-8">
                                                <label class="label">Quantidade</label>
                                                <label class="input">
                                                    <input type="text" id="serv_prod_qtd" name="serv_prod_qtd" placeholder="Quantidade">
                                                </label>
                                            </div>
                                            <div class="col col-4">
                                                <button type="button" class="btn btn-primary btn-circle btn-margin-top" onclick="jQueryServicos.addProdutoTabela($(this))"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </section>
                                        <section class="col col-6">
                                            <div class="table-responsive table-servico-produto">
                                                @if(isset($servico['servico_produto']) && count($servico['servico_produto']))
                                                    <table class="table table-hover tabela-produto">
                                                        <thead>
                                                            <tr>
                                                                <th class="pull-center">#</th>
                                                                <th>Produto</th>
                                                                <th class="pull-center">Quantidade</th>
                                                                <th class="pull-center">Valor</th>
                                                                <th>Ação</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($servico['servico_produto'] as $index => $produto)
                                                                <tr>
                                                                    <td>{{ $produto['sepr_id'] }}</td>
                                                                    <td>{{ $produto['produto']['prod_titulo'] }}</td>
                                                                    <td class="pull-center">{{ $produto['sepr_qtd'] }}</td>
                                                                    <td class="pull-center">{{ \App\Utils\Mask::dinheiro($produto['produto_valores']['prva_preco_venda']) }}</td>
                                                                    <td>
                                                                        <button class="btn btn-default btn-circle" data-index="{{ $index }}" onclick="jQueryServicos.removerProdutoTabela($(this))"><i class="fa fa-trash"></i></button>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                @endif
                                            </div>
                                        </section>
                                    </div>
                                </fieldset>
                                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                                <div id="dados_tabela">
                                    @if(isset($servico['servico_produto']) && !empty($servico['servico_produto']))
                                        @foreach($servico['servico_produto'] as $index => $produto)
                                            <input type="hidden" class="tabela-produto-{{ $index }}" id="tabela_prod_qtd" name="tabela_prod_qtd[{{ $index }}][prod_id]" value="{{ $produto['prod_id'] }}">
                                            <input type="hidden" class="tabela-produto-{{ $index }}" id="tabela_prod_qtd" name="tabela_prod_qtd[{{ $index }}][qtd]" value="{{ $produto['sepr_qtd'] }}">
                                        @endforeach
                                    @endif
                                </div>
                                <footer>
                                    <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                                    @if(isset($servico['serv_id']))
                                        <button type="button" name="submit" class="btn btn-primary" data-title="Atualizar Serviço" data-loading-text="Atualizando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                                    @else
                                        <button type="button" name="submit" class="btn btn-primary" data-title="Cadastrar Serviço" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                                    @endif
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
        <!-- end widget -->
    </article>

@endsection

@push('scripts')
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.accounting.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-servicos.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-comissao.js') }}></script>
@endpush
