<div class="table-responsive table-forma-pagamento">
    <table id="tabela-form-pgto" class="table table-bordered table-striped" width="100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Titulo</th>
                <th>Descricao</th>
                <th class="pull-center">Tipo Pagmento</th>
                <th class="pull-center">Parcelas</th>
                <th class="pull-center">Ação</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
