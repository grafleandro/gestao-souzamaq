<form role="form" class="smart-form" id="form-conta-bancaria" method="post" action="{{ url('/configuracao/conta_bancaria') }}">
    <fieldset>
        <div class="row">
            <section class="col col-3">
                <label class="label">Tipo da Conta</label>
                <div class="inline-group">
                    <label class="radio">
                        <input type="radio" id="cb-tipo-conta" name="cb-tipo-conta" value="{{ \App\Utils\ConfiguracaoUtils::CB_CONTA_CORRENTE }}" checked="checked" onclick="jQueryContaBancaria.tipoConta($(this))">
                        <i></i>Conta Corrente
                    </label>
                    <label class="radio">
                        <input type="radio" id="cb-tipo-conta" name="cb-tipo-conta" value="{{ \App\Utils\ConfiguracaoUtils::CB_CONTA_POUPANCA }}" onclick="jQueryContaBancaria.tipoConta($(this))">
                        <i></i>Conta Poupança
                    </label>
                </div>
            </section>

            <section class="col col-3">
                <label class="label">Banco</label>
                <label class="input">
                    <input type="text" id="cb-banco" name="cb-banco" value="" onfocus="jQueryContaBancaria.autocompleteBanco($(this))" required>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Agência</label>
                <label class="input">
                    <input type="text" id="cb-agencia" name="cb-agencia" value="" required>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Conta Corrente</label>
                <label class="input">
                    <input type="text" id="cb-conta-corrente" name="cb-conta-corrente" value="">
                </label>
            </section>

            <section class="col col-2 hidden">
                <label class="label">Conta Poupança</label>
                <label class="input">
                    <input type="text" id="cb-conta-poupanca" name="cb-conta-poupanca" value="">
                </label>
            </section>

            <section class="col col-2 hidden">
                <label class="label">Variação</label>
                <label class="input">
                    <input type="text" id="cb-variacao" name="cb-variacao" value="">
                </label>
            </section>
        </div>
    </fieldset>

    <input type="hidden" id="banc-id" name="banc-id" value="0">
    <footer>
        <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" name="submit" class="btn btn-primary" data-title="Conta Bancária" data-loading-text="Atualizando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Atualizar</button>
    </footer>
</form>

