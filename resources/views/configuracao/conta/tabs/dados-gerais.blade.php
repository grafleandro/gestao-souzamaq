<form role="form" id="form_empresa" method="put" action="{{url('configuracao/conta').'/'.$empresa['empr_id']}}" class="smart-form">
    <fieldset>
        <section class="col col-12">
            <label class="label">Situação</label>
            <div class="inline-group">
                <label class="radio">
                    <input type="radio" value="1" id="situacao" name="situacao" @if(!isset($empresa['empr_id'])) checked="checked" @endif  @if(isset($empresa['empr_id'])) @if($empresa['empr_status'] == 1)) checked="checked" @endif @endif disabled>
                    <i></i>Ativo
                </label>
                <label class="radio">
                    <input type="radio" value="0" id="situacao"  name="situacao"  @if(isset($empresa['empr_id'])) @if($empresa['empr_status'] == 0)) checked="checked" @endif @endif disabled>
                    <i></i>Inativo
                </label>
                <label class="radio">
                    <input type="radio" value="2" id="situacao"  name="situacao" @if(isset($empresa['empr_id'])) @if($empresa['empr_status'] == 2)) checked="checked" @endif @endif disabled>
                    <i></i>Negativado
                </label>
            </div>
        </section>
    </fieldset>

    <fieldset>
        <div class="row">
            <section class="col col-3">
                <label class="label">Nome</label>
                <label class="input">
                    <input type="text" id="nome" name="nome" placeholder="Nome da Empresa"  value="{{ $empresa['empr_nome'] ?? '' }}">
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Razão Social</label>
                <label class="input">
                    <input type="text" id="razao_social" name="razao_social" placeholder="Nome da Empresa" value="{{ $empresa['empr_razao_social'] ?? '' }}" readonly>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">CNPJ</label>
                <label class="input">
                    <input type="text" class="mask-cnpj" id="cnpj" name="cnpj" placeholder="CNPJ da empresa" value="{{ $empresa['empr_cnpj'] ?? '' }}" readonly>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Inscrição Estadual</label>
                <label class="input">
                    <input type="text" id="inscricao_estadual" name="inscricao_estadual" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="Registro Geral" readonly>
                </label>
            </section>
        </div>
    </fieldset>
    <input type="hidden" id="empresa" name="empresa" value="1">
    <footer>
        <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" name="submit" class="btn btn-primary" data-title="Atualizar Empresa" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-building" ></i> Atualizar Contato</button>
    </footer>
</form>
