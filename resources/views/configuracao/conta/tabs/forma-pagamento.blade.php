<form role="form" class="smart-form hidden" id="form-forma-pagamento" method="post" action="{{ url('/configuracao/forma_pagamento') }}">
    <fieldset>
        <div class="row">
            <section class="col col-3">
                <label class="label">Titulo</label>
                <label class="input">
                    <input type="text" id="form-titulo" name="form-titulo" value="" readonly>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Descrição</label>
                <label class="input">
                    <input type="text" id="form-descricao" name="form-descricao" value="">
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Quantidade de Parcelas</label>
                <label class="input">
                    <input type="text" id="form-parcelas" name="form-parcelas" value="">
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Tipo do Pagamento</label>
                <div class="inline-group">
                    <label class="radio">
                        <input type="radio" id="form-tipo-pgto-avista" name="form-tipo-pgto" value="1" checked="checked" onclick="jQueryConta.desabilitarCampo($(this))" readonly>
                        <i></i>à Vista
                    </label>
                    <label class="radio">
                        <input type="radio" id="form-tipo-pgto-aprazo" name="form-tipo-pgto" value="0" onclick="jQueryConta.desabilitarCampo($(this))" readonly>
                        <i></i>a Prazo
                    </label>
                </div>
            </section>
        </div>

        <div class="row">
            <section class="col col-3">
                <label class="label">Limite por Parcelas</label>
                <label class="input">
                    <input class="mask-money" type="text" id="form-limite-parcela" name="form-limite-parcela">
                    <b class="tooltip tooltip-top-right">
                        <i class="fa fa-warning txt-color-teal"></i>
                        Limite máximo que deverá ter por parcela.
                    </b>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Desconto Máximo a ser ofertado</label>
                <label class="input">
                    <input type="text" id="form-desconto-max" name="form-desconto-max" class="mask-percent" value="00,00%">
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Será aplicado juros ?</label>
                <div class="inline-group">
                    <label class="radio">
                        <input type="radio" id="form-possui-juros-sim" name="form-possui-juros" value="{{\App\Utils\SituacaoUtils::BOOL_SIM}}" onclick="jQueryFormaPgto.possuiJuros($(this))">
                        <i></i>Sim
                    </label>
                    <label class="radio">
                        <input type="radio" id="form-possui-juros-nao" name="form-possui-juros" value="{{\App\Utils\SituacaoUtils::BOOL_NAO}}" checked="checked" onclick="jQueryFormaPgto.possuiJuros($(this))">
                        <i></i>Não
                    </label>
                </div>
            </section>

            <section class="col col-3">
                <label class="label">Apartir de qual parcela ?</label>
                <label class="input">
                    <input type="text" class="mask-integer" id="form-juros-parcela" name="form-juros-parcela" value="" placeholder="Apenas número" disabled>
                </label>
            </section>
        </div>

        <div class="row">
            <section class="col col-3">
                <label class="label">Irá receber utilizando uma máquininha ?</label>
                <div class="inline-group">
                    <label class="radio">
                        <input type="radio" id="form-possui-maquininha-sim" name="form-possui-maquininha" value="{{\App\Utils\SituacaoUtils::BOOL_SIM}}" onclick="jQueryFormaPgto.possuiMarquininha($(this))">
                        <i></i>Sim
                    </label>
                    <label class="radio">
                        <input type="radio" id="form-possui-maquininha-nao" name="form-possui-maquininha" value="{{\App\Utils\SituacaoUtils::BOOL_NAO}}" checked="checked" onclick="jQueryFormaPgto.possuiMarquininha($(this))">
                        <i></i>Não
                    </label>
                </div>
            </section>

            <section class="col col-3">
                <label class="label">Selecionar Fornecedor</label>
                <label class="input">
                    <input type="text" id="form-fornecedor" name="form-fornecedor" value="" disabled>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Porcentagem cobrado por parcela</label>
                <label class="input">
                    <input type="text" class="mask-percent" id="form-porcentagem-parcela" name="form-porcentagem-parcela" value="" disabled>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Conta Bancária</label>
                <label class="input">
                    {{\App\Utils\FormUtils::select($contaBancaria, 'conta_bancaria', 'conta_bancaria', 'form-control form-cascade-control', null, false, array('required'), '-- Selecionar --', true, 'cocb_id', 'cocb_titulo')}}
                </label>
            </section>
        </div>
    </fieldset>
    <footer>
        <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" name="submit" class="btn btn-primary" data-title="Forma de Pagamento" data-loading-text="Salvando dados..." onclick="jQueryFormaPgto.salvarForm($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
    </footer>
</form>

