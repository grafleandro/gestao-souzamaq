<form role="form" id="form_empresa" method="put" action="{{url('configuracao/conta').'/'.$empresa['empr_id']}}" class="smart-form">
    <header>
        Endereço
    </header>
    <fieldset>
        <div class="row">
            <section class="col col-3">
                <label class="label">Tipo Logradouro</label>
                <label class="select">
                    <?php \App\Utils\FormUtils::select($tipoLogradouro, 'tipo_logradouro', 'tipo_logradouro', 'form-control form-cascade-control', ($empresa['empresa_endereco']['enlo_id']) ?? null, false, array(), '-- Selecionar --', true, 'enlo_id', 'enlo_titulo'); ?>
                    <i></i>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Logradouro</label>
                <label class="input">
                    <input type="text" id="rua" name="rua" placeholder="Nome do Tipo Logradouro" value="{{ $empresa['empresa_endereco']['ende_logradouro_titulo'] ?? '' }}" required>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Número</label>
                <label class="input">
                    <input type="text" id="numero" name="numero" placeholder="Número do Local" value="{{ $empresa['empresa_endereco']['ende_numero'] ?? '' }}" required>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">CEP</label>
                <label class="input">
                    <input type="text" id="cep" name="cep" placeholder="Código de Endereçamento Posta" value="{{ $empresa['empresa_endereco']['ende_cep'] ?? '' }}"  required>
                </label>
            </section>
        </div>

        <div class="row">
            <section class="col col-3">
                <label class="label">Bairro</label>
                <label class="input">
                    <input type="text" id="bairro" name="bairro" placeholder="Bairro" value="{{ $empresa['empresa_endereco']['ende_bairro'] ?? '' }}">
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Cidade</label>
                <label class="input">
                    <input type="text" id="cidade" name="cidade" placeholder="Cidade" value="{{ $empresa['empresa_endereco']['ende_cidade'] ?? '' }}">
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Estado</label>
                <label class="input">
                    <input type="text" id="estado" name="estado" placeholder="Cidade" value="{{ $empresa['empresa_endereco']['ende_estado'] ?? '' }}" required>
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Complemento</label>
                <label class="input">
                    <input type="text" id="complemento" name="complemento" placeholder="Complemento do Endereço" value="{{ $empresa['empresa_endereco']['ende_complemento'] ?? '' }}">
                </label>
            </section>
        </div>
    </fieldset>
    <input type="hidden" id="endereco" name="endereco" value="1">
    <footer>
        <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" name="submit" class="btn btn-primary" data-title="Cadastro de Empresa" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-map-marker" ></i> Atualizar Endereço</button>
    </footer>
</form>
