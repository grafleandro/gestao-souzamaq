<div class="table-responsive table-conta-bancaria">
    <table id="tabela-conta-bancaria" class="table table-bordered table-striped" width="100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Tipo da Conta</th>
                <th>Banco</th>
                <th>Agência</th>
                <th>Conta Corrente</th>
                <th>Conta Poupança</th>
                <th class="pull-center">Ação</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
