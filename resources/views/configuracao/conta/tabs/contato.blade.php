<form role="form" id="form_empresa" method="put" action="{{url('configuracao/conta').'/'.$empresa['empr_id']}}" class="smart-form">
    <header>
        Contato
    </header>
    <fieldset>
        <div class="row">
            <section class="col col-3">
                <label class="label">Telefone Fixo</label>
                <label class="input">
                    <input type="text" class="telephone" id="tel_fixo" name="tel_fixo" placeholder="(00) 0000-0000" value="{{ $empresa['empresa_contato']['cont_tel_fixo'] ?? '' }}">
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Telefone Celular 1</label>
                <label class="input">
                    <input type="text" class="cell_phone_1" id="celular_1" name="celular_1" placeholder="(00) 0 0000-0000" value="{{ $empresa['empresa_contato']['cont_cel_1'] ?? '' }}">
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Telefone Celular 2</label>
                <label class="input">
                    <input type="text" class="cell_phone_2" id="celular_2" name="celular_2" placeholder="(00) 0 0000-0000" value="{{ $empresa['empresa_contato']['cont_cel_2'] ?? '' }}" >
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Email</label>
                <label class="input">
                    <input type="email" id="email" name="email" value="{{ $empresa['empresa_contato']['cont_email'] ?? '' }}">
                </label>
            </section>
        </div>
    </fieldset>
    <input type="hidden" id="contato" name="contato" value="1">
    <footer>
        <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" name="submit" class="btn btn-primary" data-title="Atualizar Empresa" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-phone" ></i> Atualizar Contato</button>
    </footer>
</form>
