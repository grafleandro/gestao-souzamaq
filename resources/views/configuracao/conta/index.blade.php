@extends('layouts.page')

@push('css')
    <!-- CSS -->
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Configuração - Conta'])
        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">
                    <ul id="myTab1" class="nav nav-tabs bordered">
                        <li class="">
                            <a href="#empresa" data-toggle="tab"><i class="fa fa-fw fa-lg fa-building"></i> Empresa</a>
                        </li>
                        <li class="">
                            <a href="#empresa_endereco" data-toggle="tab"><i class="fa fa-fw fa-lg fa-map-marker"></i> Endereço Empresa</a>
                        </li>
                        <li class="">
                            <a href="#empresa_contato" data-toggle="tab"><i class="fa fa-fw fa-lg fa-phone"></i> Contato Empresa</a>
                        </li>
                        <li class="active">
                            <a href="#forma_pagamento" data-toggle="tab"><i class="fa fa-fw fa-lg fa-list-alt"></i> Formas de Pagamento</a>
                        </li>
                        <li class="">
                            <a href="#contaBancaria" data-toggle="tab"><i class="fa fa-fw fa-lg fa-bank"></i> Conta Bancária</a>
                        </li>
                    </ul>

                    <div id="myTabContent1" class="tab-content padding-10">
                        <div class="tab-pane fade" id="empresa">
                            @include('configuracao.conta.tabs.dados-gerais')
                        </div>
                        <div class="tab-pane fade" id="empresa_endereco">
                            @include('configuracao.conta.tabs.endereco')
                        </div>
                        <div class="tab-pane fade" id="empresa_contato">
                            @include('configuracao.conta.tabs.contato')
                        </div>
                        <div class="tab-pane fade in active" id="forma_pagamento">
                            @include('configuracao.conta.tabs.forma-pagamento')
                            @include('configuracao.conta.tabs.forma-pagamento-listar')
                        </div>
                        <div class="tab-pane fade" id="contaBancaria">
                            @include('configuracao.conta.tabs.conta-bancaria')
                            @include('configuracao.conta.tabs.conta-bancaria-listar')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
@endsection

@push('scripts')
    <script>
        let BOOL_SIM = "{{\App\Utils\SituacaoUtils::BOOL_SIM}}";
        let BOOL_NAO = "{{\App\Utils\SituacaoUtils::BOOL_NAO}}";
        let CB_CONTA_CORRENTE = "{{ \App\Utils\ConfiguracaoUtils::CB_CONTA_CORRENTE }}";
        let CB_CONTA_POUPANCA = "{{ \App\Utils\ConfiguracaoUtils::CB_CONTA_POUPANCA }}";
    </script>

    <script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.accounting.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-search-cep.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-conta.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-forma-pgto.js') }}></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-conta-bancaria.js') }}"></script>
@endpush
