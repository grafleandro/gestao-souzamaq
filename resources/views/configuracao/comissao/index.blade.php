@extends('layouts.page')

@push('css')
    <!-- CSS -->
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Configuração - Comissão', 'action' => 'configuracao.comissao.botao.header-menu-btn-list'])
        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">
                    @if(isset($comissao['coco_id']))
                        <form role="form" id="form_comissao" method="put" action="{{url('configuracao/comissao').'/'.$comissao['coco_id']}}" class="smart-form">
                    @else
                        <form role="form" id="form_comissao" method="post" action="{{url('configuracao/comissao')}}" class="smart-form">
                    @endif
                        <fieldset>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Módulo que haverá comissão</label>
                                    <div class="inline-group">
                                        <label class="checkbox">
                                            <input type="checkbox" id="modulo_comissao_produto" name="modulo_comissao_produto" value="{{\App\Utils\SituacaoUtils::CONF_COMI_VENDA_PROD}}" {{(isset($comissao['coco_modulo_venda']) && $comissao['coco_modulo_venda']) ? 'checked="checked"' : null}}>
                                            <i></i>Vendas de Produtos
                                        </label>
                                        <label class="checkbox">
                                            <input type="checkbox" id="modulo_comissao_os" name="modulo_comissao_os" value="{{\App\Utils\SituacaoUtils::CONF_COMI_OS}}" {{(isset($comissao['coco_modulo_os']) && $comissao['coco_modulo_os']) ? 'checked="checked"' : null}}>
                                            <i></i>Ordem de Serviços
                                        </label>
                                    </div>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Retirar comissão sobre:</label>
                                    <div class="inline-group">
                                        @if(isset($comissao['coco_liquido_bruto']) && $comissao['coco_liquido_bruto'] == \App\Utils\SituacaoUtils::CONF_VALOR_BRUTO)
                                            <label class="radio">
                                                <input type="radio" name="comissao_tipo" value="{{\App\Utils\SituacaoUtils::CONF_VALOR_BRUTO}}" checked>
                                                <i></i><strong>Valor Bruto</strong> do Item ou Compra
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="comissao_tipo" value="{{\App\Utils\SituacaoUtils::CONF_VALOR_LIQUIDO}}">
                                                <i></i><strong>Valor Líquido</strong> do Item ou Compra
                                            </label>
                                        @elseif(isset($comissao['coco_liquido_bruto']) && $comissao['coco_liquido_bruto'] == \App\Utils\SituacaoUtils::CONF_VALOR_LIQUIDO)
                                            <label class="radio">
                                                <input type="radio" name="comissao_tipo" value="{{\App\Utils\SituacaoUtils::CONF_VALOR_BRUTO}}">
                                                <i></i><strong>Valor Bruto</strong> do Item ou Compra
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="comissao_tipo" value="{{\App\Utils\SituacaoUtils::CONF_VALOR_LIQUIDO}}" checked>
                                                <i></i><strong>Valor Líquido</strong> do Item ou Compra
                                            </label>
                                        @else
                                            <label class="radio">
                                                <input type="radio" name="comissao_tipo" value="{{\App\Utils\SituacaoUtils::CONF_VALOR_BRUTO}}">
                                                <i></i><strong>Valor Bruto</strong> do Item ou Compra
                                            </label>
                                            <label class="radio">
                                                <input type="radio" name="comissao_tipo" value="{{\App\Utils\SituacaoUtils::CONF_VALOR_LIQUIDO}}">
                                                <i></i><strong>Valor Líquido</strong> do Item ou Compra
                                            </label>
                                        @endif
                                    </div>
                                </section>
                                <section class="col col-3">
                                    <label class="label">Cota destinada a Comissão</label>
                                    <label class="input">
                                        <input type="text" class="mask-percent" id="cota_comissao" name="cota_comissao" onblur="jQueryComissao.cotaEmpresa($(this))" value="{{(isset($comissao['coco_cota_comissao'])) ? \App\Utils\Mask::porcentagem($comissao['coco_cota_comissao']) : ''}}">
                                        <b class="tooltip tooltip-top-left">
                                            <i class="fa fa-warning txt-color-teal"></i>
                                            Porcentagem que será destinada as comissões sobre os produtos/serviços prestados.
                                        </b>
                                    </label>
                                </section>
                                <section class="col col-3">
                                    <label class="label">Cota destinada a Empresa</label>
                                    <label class="input">
                                        <input type="text" class="mask-percent" id="cota_empresa" name="cota_empresa" value="{{(isset($comissao['coco_cota_empresa'])) ? \App\Utils\Mask::porcentagem($comissao['coco_cota_empresa']) : ''}}" readonly>
                                        <b class="tooltip tooltip-top-left">
                                            <i class="fa fa-warning txt-color-teal"></i>
                                            Porcentagem que será destinada a Empresa sobre os valores arrecadados.
                                        </b>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                            <button type="button" name="submit" class="btn btn-primary" data-title="Configuração da Comissão" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </article>
@endsection

@push('scripts')
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-comissao.js') }}></script>
@endpush
