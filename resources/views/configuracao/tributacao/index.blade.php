@extends('layouts.page')

@push('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

            @include('header.header-content', ['title' => 'Tributação - NCM', 'action' => ['configuracao.tributacao.menu.header-menu-btn-insert']])
            <!-- widget div-->

            <!-- widget content -->
            <div class="widget-body">

                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive table-listar-ncm">

                    </div>
                </div>
            </div>
            <!-- end widget content -->
        </div>
    </article>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-tributacao.js') }}"></script>
@endpush
