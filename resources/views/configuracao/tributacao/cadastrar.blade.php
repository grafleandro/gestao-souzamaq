@extends('layouts.page')

@push('css')
    <!-- CSS -->
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Tributação - NCM', 'action' => ['configuracao.tributacao.menu.header-menu-btn-list', 'configuracao.tributacao.menu.header-menu-btn-details']])
        <!-- widget div-->

            @include('configuracao.tributacao.formulario')
        </div>
    </article>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/custom/jquery.maskedinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-mask-custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-tributacao.js') }}"></script>
@endpush
