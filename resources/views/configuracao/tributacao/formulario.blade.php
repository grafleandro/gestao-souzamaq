<div>
    <!-- widget edit box -->
    <div class="jarviswidget-editbox">
        <!-- This area used as dropdown edit box -->

    </div>
    <!-- end widget edit box -->

    <!-- widget content -->
    <div class="widget-body no-padding">

        @if(isset($tributo['fitr_id']))
            <form role="form" id="form-tributos" method="put" action="{{ url('configuracao/tributacao/' . $tributo['fitr_id'] )}}" class="smart-form">
        @else
            <form role="form" id="form-tributos" method="post" action="{{ url('configuracao/tributacao') }}" class="smart-form">
        @endif
            <fieldset>
                <div class="row">
                    <section class="col col-4">
                        <label class="label">NCM</label>
                        <label class="input"> <i class="icon-append fa fa-search"></i>
                            @if(isset($tributo['tributo_ncm']))
                                <input type="text" id="trib-ncm" name="trib-ncm" placeholder="Buscar NCM..." onfocus="jQueryTributacao.autocompleteNCM($(this))" value="{{ $tributo['tributo_ncm']['finc_id'] . ' - ' . $tributo['tributo_ncm']['finc_descricao'] }}" disabled="disabled">
                            @else
                                <input type="text" id="trib-ncm" name="trib-ncm" placeholder="Buscar NCM..." onfocus="jQueryTributacao.autocompleteNCM($(this))">
                            @endif
                            <b class="tooltip tooltip-top-right">
                                <i class="fa fa-warning txt-color-teal"></i>
                                Buscar NCM por Código ou Título
                            </b>
                        </label>
                    </section>

                    <section class="col col-8">
                        <label class="label">Descrição do N.C.M</label>
                        <label class="input">
                            <input type="text" id="trib-ncm-desc" name="trib-ncm-desc" value="{{($tributo['tributo_ncm']['finc_descricao']) ?? ''}}" disabled>
                        </label>
                    </section>
                </div>
            </fieldset>

            <fieldset>
                <div class="row">
                    <section class="col col-4">
                        <label class="label">CST B</label>
                        <label class="input">
                            <?php \App\Utils\FormUtils::select($fiscal_cst, 'trib-cst', 'trib-cst', 'form-control form-cascade-control', ($tributo['ficst_id']) ?? null, false, array(), '-- Selecionar --', true, 'fics_id', 'fics_titulo'); ?>
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">CST PIS Entrada</label>
                        <label class="input">
                            <?php \App\Utils\FormUtils::select($fiscal_pis_entrada, 'trib-pis-entrada', 'trib-pis-entrada', 'form-control form-cascade-control', ($tributo['fitr_pis_entrada']) ?? null, false, array(), '-- Selecionar --', true, 'fipc_id', 'fipc_titulo'); ?>
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">CST PIS Saída</label>
                        <label class="input">
                            <?php \App\Utils\FormUtils::select($fiscal_pis_saida, 'trib-pis-saida', 'trib-pis-saida', 'form-control form-cascade-control', ($tributo['fitr_pis_saida']) ?? null, false, array(), '-- Selecionar --', true, 'fipc_id', 'fipc_titulo'); ?>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section class="col col-4">
                        <label class="label">CST COFINS Entrada</label>
                        <label class="input">
                            <?php \App\Utils\FormUtils::select($fiscal_cofins_entrada, 'trib-cofins-entrada', 'trib-cofins-entrada', 'form-control form-cascade-control', ($tributo['fitr_cofins_entrada']) ?? null, false, array(), '-- Selecionar --', true, 'fipc_id', 'fipc_titulo'); ?>
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">CST COFINS Saída</label>
                        <label class="input">
                            <?php \App\Utils\FormUtils::select($fiscal_cofins_saida, 'trib-cofins-saida', 'trib-cofins-saida', 'form-control form-cascade-control', ($tributo['fitr_cofins_saida']) ?? null, false, array(), '-- Selecionar --', true, 'fipc_id', 'fipc_titulo'); ?>
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">CSONS</label>
                        <label class="input">
                            <?php \App\Utils\FormUtils::select($fiscal_csosn, 'trib-csosn', 'trib-csosn', 'form-control form-cascade-control', ($tributo['ficsosn_id']) ?? null, false, array(), '-- Selecionar --', true, 'fics_id', 'fics_titulo'); ?>
                        </label>
                    </section>
                </div>
            </fieldset>
            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="trib-ncm-codigo" name="trib-ncm-codigo" value="{{ ($tributo['tributo_ncm']['finc_id']) ?? 0 }}">
            <footer>
                <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                @if(isset($tributo['fitr_id']))
                    <button type="button" name="submit" class="btn btn-primary" data-title="Atualizar NCM" data-loading-text="Atualizando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                @else
                    <button type="button" name="submit" class="btn btn-primary" data-title="Cadastrar NCM" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                @endif
            </footer>
        </form>
    </div>
</div>
