<div>

    <!-- widget edit box -->
    <div class="jarviswidget-editbox">
        <!-- This area used as dropdown edit box -->

    </div>
    <!-- end widget edit box -->

    <!-- widget content -->
    <div class="widget-body no-padding">

        @if(!isset($centroCusto['cecu_id']))
            <form role="form" id="form-centro-custo" method="post" action="{{url('configuracao/centro/custo')}}" class="smart-form">
        @else
            <form role="form" id="form-centro-custo" method="put" action="{{url('configuracao/centro/custo').'/'.$centroCusto['cecu_id']}}" class="smart-form">
        @endif
            <fieldset>
                <div class="row">
                    <section class="col col-4">
                        <label class="label">Titulo</label>
                        <label class="input">
                            <input type="text" id="cc-titulo" name="cc-titulo"  required>
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">Descrição</label>
                        <label class="input">
                            <input type="text" id="cc-descricao" name="cc-descricao">
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">Situação</label>
                        <div class="inline-group">
                            <label class="radio">
                                <input type="radio" value="{{ \App\Utils\ConfiguracaoUtils::CC_SITUACAO_ATIVO }}" id="cc-situacao" name="cc-situacao" checked="checked" >
                                <i></i>Ativo
                            </label>
                            <label class="radio">
                                <input type="radio" value="{{ \App\Utils\ConfiguracaoUtils::CC_SITUACAO_INATIVO }}" id="cc-situacao"  name="cc-situacao">
                                <i></i>Inativo
                            </label>
                        </div>
                    </section>
                </div>
            </fieldset>
            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
            <footer>
                <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                <button type="button" name="submit" class="btn btn-primary" data-title="Cadastro de Empresa" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
            </footer>
        </form>
    </div>
</div>
