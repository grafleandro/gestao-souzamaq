@extends('layouts.page')

@push('css')
    <!-- CSS -->
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Centro de Custo - Cadastrar', 'action' => ['configuracao.centro-custo.header-menu-btn-list']])
        <!-- widget div-->
        @include('configuracao.centro-custo.formulario')
        </div>
    </article>
@endsection

@push('scripts')
    <script src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script src={{ asset('js/custom/jquery-search-cep.js') }}></script>
@endpush
