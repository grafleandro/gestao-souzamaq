@extends('layouts.page')

@section('content')
    <!-- NEW COL START -->
    <article class="col col-md-12 col-sm-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false">
        @include('header.header-content', ['title' => 'Centro de Custo', 'action' => ['configuracao.centro-custo.header-menu-btn']])

        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">

                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body table-responsive table-funcao-listar-empresa">

                        </div>
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
        <!-- end widget -->
    </article>

@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    {{--<script type="text/javascript" src="{{ assets('js/custom/jquery-empresa.js') }}"></script>--}}
@endpush
