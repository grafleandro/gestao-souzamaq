<div>

    <!-- widget edit box -->
    <div class="jarviswidget-editbox">
        <!-- This area used as dropdown edit box -->

    </div>
    <!-- end widget edit box -->

    <!-- widget content -->
    <div class="widget-body no-padding">

        @if(!isset($inventario['inve_id']))
            <form role="form" id="form_empresa" method="post" action="{{url('empresa/inventario')}}" class="smart-form">
        @else
            <form role="form" id="form_empresa" method="put" action="{{url('empresa/inventario').'/'.$inventario['inve_id']}}" class="smart-form">
        @endif
                <ul id="myTab1" class="nav nav-tabs bordered">
                    <li class="active">
                        <a href="#geral" data-toggle="tab"><i class="fa fa-fw fa-lg fa-user"></i> Geral</a>
                    </li>
                    <li>
                        <a href="#informacao" data-toggle="tab"><i class="fa fa-fw fa-lg fa-map-marker"></i> Informações</a>
                    </li>
                    <li>
                        <a href="#foto" data-toggle="tab"><i class="fa fa-fw fa-lg fa-image"></i> Foto</a>
                    </li>
                </ul>

                <div id="myTabContent1" class="tab-content">
                    <div class="tab-pane fade in active" id="geral">
                        @include('empresa.inventario.tabs.tab-geral')
                    </div>

                    <div class="tab-pane fade" id="informacao">
                        @include('empresa.inventario.tabs.tab-informacao')
                    </div>

                    <div class="tab-pane fade" id="foto">
                        @include('empresa.inventario.tabs.tab-arquivo')
                    </div>
                </div>
                <input type="hidden" name="perfil" id="perfil" value="1">
                <footer>
                    <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                    <button type="button" class="btn btn-success" data-title="Inventário" data-loading-text="Inventário" onclick="jQueryForm.send_form_file($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
                </footer>
            </form>
    </div>
</div>
