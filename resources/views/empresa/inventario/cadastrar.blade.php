@extends('layouts.page')

@push('css')
    <!-- CSS -->
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Inventário', 'action' => 'empresa.inventario.botao.header-menu-btn-list'])
        <!-- widget div-->
        @include('empresa.inventario.formulario')
        </div>
    </article>
@endsection

@push('scripts')
    <script>
        let BOOL_SIM = "{{\App\Utils\SituacaoUtils::BOOL_SIM}}";
        let BOOL_NAO = "{{\App\Utils\SituacaoUtils::BOOL_NAO}}";
    </script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-grupo.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-inventario.js') }}></script>
@endpush
