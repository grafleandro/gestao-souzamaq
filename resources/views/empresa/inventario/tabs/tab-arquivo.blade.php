<fieldset>
    @if(isset($inventario['inve_arquivo']) && !empty($inventario['inve_arquivo']))
        <div class="row">
            <section class="col col-4">
                <img src="{{asset('storage/inventario/' . $inventario['inve_arquivo'])}}">
            </section>
        </div>
    @endif

    @desktop
        <div class="row">
            <section class="col col-4">
                <div class="input input-file">
                    <input type="file" id="up-file" name="up-file" class="btn btn-default">
                </div>
            </section>
        </div>
    @elsedesktop
        <div class="row">
            <section class="col">
                <div class="superbox">
                    <div class="superbox-list">
                        <img id="up-file" class="superbox-img">
                    </div>
                </div>
            </section>
        </div>
        <div class="row">
            <section class="col">
                <button type="button" id="btn-camera" class="btn btn-primary btn-file">
                    <i class="fa fa-camera"></i> Registrar Item
                    <input type="file" accept="image/*" capture="camera" id="up-button">
                </button>
            </section>
        </div>
    @enddesktop
</fieldset>

@push('scripts')
    <script src="https://unpkg.com/exif-js@2.2.1"></script>
    <script type="text/javascript" src={{ asset('js/custom/rotate-image.jquery.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-detect-mobile.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-camera.js') }}></script>
@endpush
