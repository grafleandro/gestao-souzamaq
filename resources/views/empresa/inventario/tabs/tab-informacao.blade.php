<fieldset>
    <div class="row">
        <section class="col col-4">
            <label class="label">Valor Sucata</label>
            <label class="input">
                <input type="text" id="inv_valor_sucata" name="inv_valor_sucata" class="mask-money" value="{{\App\Utils\Mask::dinheiro($inventario['inve_valor_sucata'] ?? '')}}">
                <b class="tooltip tooltip-top-right">
                    <i class="fa fa-warning txt-color-teal"></i>
                    Se o valor da sucata não for possível calcular, NÃO PRECISE INFORMAR.
                </b>
            </label>
        </section>

        @if(isset($inventario['inve_intervalo_manutencao']) && !empty($inventario['inve_intervalo_manutencao']) || !empty($inventario['inve_ultima_manutencao']))
            <section class="col col-4">
                <label class="label">Possui Matenção Periódica?</label>
                <div class="inline-group">
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::BOOL_SIM}}" id="inv_manutencao" name="inv_manutencao" onclick="jQueryInventario.possuiManutencao($(this))" checked="checked">
                        <i></i>Sim
                    </label>
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::BOOL_NAO}}" id="inv_manutencao"  name="inv_manutencao" onclick="jQueryInventario.possuiManutencao($(this))">
                        <i></i>Não
                    </label>
                </div>
            </section>
        @else
            <section class="col col-4">
                <label class="label">Possui Matenção Periódica?</label>
                <div class="inline-group">
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::BOOL_SIM}}" id="inv_manutencao" name="inv_manutencao" onclick="jQueryInventario.possuiManutencao($(this))">
                        <i></i>Sim
                    </label>
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::BOOL_NAO}}" id="inv_manutencao"  name="inv_manutencao" onclick="jQueryInventario.possuiManutencao($(this))" checked="checked">
                        <i></i>Não
                    </label>
                </div>
            </section>
        @endif

        @if(isset($inventario['inve_periodo_garantia']) && !empty($inventario['inve_periodo_garantia']))
            <section class="col col-4">
                <label class="label">Possui Garantia?</label>
                <div class="inline-group">
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::BOOL_SIM}}" id="inv_garantia" name="inv_garantia" onclick="jQueryInventario.possuiGarantia($(this))" checked="checked">
                        <i></i>Sim
                    </label>
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::BOOL_NAO}}" id="inv_garantia"  name="inv_garantia" onclick="jQueryInventario.possuiGarantia($(this))">
                        <i></i>Não
                    </label>
                </div>
            </section>
        @else
            <section class="col col-4">
                <label class="label">Possui Garantia?</label>
                <div class="inline-group">
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::BOOL_SIM}}" id="inv_garantia" name="inv_garantia" onclick="jQueryInventario.possuiGarantia($(this))">
                        <i></i>Sim
                    </label>
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::BOOL_NAO}}" id="inv_garantia"  name="inv_garantia" onclick="jQueryInventario.possuiGarantia($(this))" checked="checked">
                        <i></i>Não
                    </label>
                </div>
            </section>
        @endif
    </div>
    <div class="row">
        @if(isset($inventario['inve_intervalo_manutencao']) && (!empty($inventario['inve_intervalo_manutencao']) || !empty($inventario['inve_ultima_manutencao'])))
            <section class="col col-4">
                <label class="label">Intervalo da Manutenção <small>(meses)</small></label>
                <label class="input">
                    <input type="text" class="mask-integer" id="inv_intevalo_manutencao" name="inv_intevalo_manutencao" value="{{ $inventario['inve_intervalo_manutencao'] ?? '' }}">
                    <b class="tooltip tooltip-top-right">
                        <i class="fa fa-warning txt-color-teal"></i>
                        Você deve informar de quanto em quanto tempo deverá ser realizado a Manutenção.
                    </b>
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Ultima Manutenção</label>
                <label class="input">
                    <div class='input-group date'>
                        <input type='text'  name='inv_ultima_manutencao' id='inv_ultima_manutencao' class="form-control input-datepicker mask-date" value="{{$inventario['inve_ultima_manutencao'] ?? ''}}"/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </label>
            </section>
        @else
            <section class="col col-4 hidden">
                <label class="label">Intervalo da Manutenção <small>(meses)</small></label>
                <label class="input">
                    <input type="text" class="mask-integer" id="inv_intevalo_manutencao" name="inv_intevalo_manutencao" value="">
                    <b class="tooltip tooltip-top-right">
                        <i class="fa fa-warning txt-color-teal"></i>
                        Você deve informar de quanto em quanto tempo deverá ser realizado a Manutenção.
                    </b>
                </label>
            </section>

            <section class="col col-4 hidden">
                <label class="label">Ultima Manutenção</label>
                <label class="input">
                    <div class='input-group date'>
                        <input type='text'  name='inv_ultima_manutencao' id='inv_ultima_manutencao' class="form-control input-datepicker mask-date"/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </label>
            </section>
        @endif

        @if(isset($inventario['inve_periodo_garantia']) && !empty($inventario['inve_periodo_garantia']))
            <section class="col col-4">
                <label class="label">Período da Garantia <small>(meses)</small></label>
                <label class="input">
                    <input type="text" class="mask-integer" id="inv_periodo_garantia" name="inv_periodo_garantia" placeholder="Quantidade de Meses" value="{{ $inventario['inve_periodo_garantia'] ?? '' }}">
                </label>
            </section>
        @else
            <section class="col col-4 hidden">
                <label class="label">Período da Garantia <small>(meses)</small></label>
                <label class="input">
                    <input type="text" class="mask-integer" id="inv_periodo_garantia" name="inv_periodo_garantia" placeholder="Quantidade de Meses">
                </label>
            </section>
        @endif
    </div>

    <section>
        <label class="label">Observação</label>
        <label class="textarea">
            <textarea id="inv_observacao" name="inv_observacao" rows="5">{{$inventario['inve_observacao'] ?? ''}}</textarea>
        </label>
    </section>
</fieldset>
