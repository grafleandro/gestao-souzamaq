<fieldset>
    <div class="row">
        <section class="col col-3">
            <label class="label">Código de Barra</label>
            <label class="input">
                <input type="text" id="inv_cod_barra" name="inv_cod_barra" placeholder="0000-0 0000-0 000"  value="{{ $inventario['inve_cod_barra'] ?? '' }}">
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Número de Série</label>
            <label class="input">
                <input type="text" id="inv_num_serie" name="inv_num_serie" placeholder="0000.000.000/000-00"  value="{{ $inventario['inve_num_serie'] ?? '' }}">
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Item</label>
            <label class="input">
                <input type="text" id="inv_item" name="inv_item" placeholder="Título do Item"  value="{{ $inventario['inve_item'] ?? '' }}" required>
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Grupo</label>
            <label class="input">
                <input type="text" id="inv_grupo" name="inv_grupo" placeholder="Selecionar Grupo" value="{{ $inventario['grupo']['grup_titulo'] ?? '' }}" onfocus="jQueryGrupo.autocomplete($(this))">
                <input type="hidden" id="grup_id" name="grup_id" value="{{ $inventario['grupo']['grup_id'] ?? '0' }}">
            </label>
        </section>
    </div>

    <div class="row">
        <section class="col col-3">
            <label class="label">Quantidade</label>
            <label class="input">
                <input type="text" class="mask-integer" id="inv_quantidade" name="inv_quantidade" value="{{ $inventario['inve_qtd'] ?? '1' }}" required>
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Data da Aquisição</label>
            <label class="input">
                <div class='input-group date'>
                    <input type='text'  name='inv_dt_aquisicao' id='inv_dt_aquisicao' class="form-control input-datepicker mask-date" value="{{$inventario['inve_dt_aquisicao'] ?? ''}}"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Vida Útil <small>(anos)</small></label>
            <label class="input">
                <input type="text" class="mask-integer" id="inv_vida_util" name="inv_vida_util" value="{{ $inventario['inve_vida_util'] ?? '1' }}">
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Valor Unitário</label>
            <label class="input">
                <input type="text" id="inv_valor_unit" name="inv_valor_unit" class="mask-money" value="{{\App\Utils\Mask::dinheiro($inventario['inve_valor_unit'] ?? '')}}">
            </label>
        </section>
    </div>
</fieldset>


@push('scripts')
    <script src={{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}></script>
    <script src={{ asset('bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}></script>
@endpush
