<fieldset>
    <div class="row">
        <section class="col col-4">
            <label class="label">Nome</label>
            <label class="input">
                <input type="text" name="cliente_nome" placeholder="Nome Completo" value="{{ ($cliente['clie_nome_razao_social']) ?? '' }}">
            </label>
        </section>

        <section class="col col-4">
            <label class="label">CPF</label>
            <label class="input">
                <input type="text" name="cliente_cpf" class="mask-cpf" placeholder="Cadastro de Pessoa Física" value="{{ (isset($cliente['clie_cpf_cnpj'])) ? \App\Utils\Mask::cpf(($cliente['clie_cpf_cnpj'])) : '' }}">
            </label>
        </section>

        <section class="col col-4">
            <label class="label">RG</label>
            <label class="input">
                <input type="text" name="cliente_rg" placeholder="Registro Geral" value="{{ ($cliente['clie_rg_insc_estadual']) ?? ''  }}">
            </label>
        </section>
    </div>
</fieldset>
