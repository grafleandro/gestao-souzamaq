<form id="smart-form-register" class="smart-form" action="{{ url('oficina/checklist/interno/create') }}" method="get">
    <fieldset>
        <div class="row">
            <section class="col col-5">
                <label class="label">Nome</label>
                <label class="input">
                    <input type="text"  id="cad_nome" name="cad_nome" >
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Telefone Celular 1</label>
                <label class="input">
                    <input type="text" class="cell_phone_1" id="cad_celular_1" name="cad_celular_1">
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Email</label>
                <label class="input">
                    <input type="email" id="cad_email" name="cad_email" value="{{ ($cliente['cliente_contato']['cont_email']) ?? '' }}" >
                </label>
            </section>

        </div>
    </fieldset>

    <fieldset>
        <div class="row">

            <section class="col col-5">
                <label class="label">Modelo do Veiculo</label>
                <label class="input">
                    <input type="email" id="vei_modelo" name="vei_modelo" value="{{ ($cliente['cliente_contato']['cont_email']) ?? '' }}" >
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Marca do Veiculo</label>
                <label class="input">
                    <input type="email" id="vei_marca" name="vei_marca" value="{{ ($cliente['cliente_contato']['cont_email']) ?? '' }}" >
                </label>
            </section>

            <section class="col col-3">
                <label class="label">Placa do Veiculo</label>
                <label class="input">
                    <input type="email" id="vei_placa" name="vei_placa" value="{{ ($cliente['cliente_contato']['cont_email']) ?? '' }}" >
                </label>
            </section>
        </div>
    </fieldset>
    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
    <footer>
        <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" name="submit" class="btn btn-primary" data-title="Cadastrar Cliente" data-loading-text="Salvando dados..." onclick="jQueryCheckListInterno.cadastro($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
    </footer>
</form>


<script src={{ asset('js/custom/jquery-app.js') }}></script>
<script src={{ asset('js/custom/jquery-form.js') }}></script>
<script src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
<script src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
