<fieldset>
    <div class="row">
        <section class="col col-3">
            <label class="label">Razão Social</label>
            <label class="input">
                <input type="text" name="cliente_razao_social" placeholder="Razão Social" value="{{ ($cliente['clie_nome_razao_social']) ?? '' }}">
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Nome Fantasia</label>
            <label class="input">
                <input type="text" name="cliente_nome_fantasia" placeholder="Nome Fantasia" value="{{ ($cliente['clie_nome_fantasia']) ?? '' }}">
            </label>
        </section>

        <section class="col col-3">
            <label class="label">CNPJ</label>
            <label class="input">
                <input type="text" name="cliente_cnpj" class="mask-cnpj" placeholder="Cadastro Nacioal de Pessoa Jurídica" value="{{ (isset($cliente['clie_cpf_cnpj'])) ? \App\Utils\Mask::cnpj(($cliente['clie_cpf_cnpj'])) : '' }}">
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Inscrição Estadual</label>
            <label class="input">
                <input type="text" name="cliente_insc_estadual" placeholder="Inscrição Estadual" value="{{ ($cliente['clie_rg_insc_estadual']) ?? ''  }}">
            </label>
        </section>
    </div>
</fieldset>
