@foreach($items as $item)
    <li>
        @if(!$item->node)
            <span><i class="fa fa-lg fa-minus-circle"></i>{{ $item->node }} - {{ ($item->title) ?? ''}}</span>
        @else
            <span>
                <label class="checkbox inline-block">
                    <input type="checkbox" name="checkbox-inline"><i></i>{{ ($item->title) ?? ''}}
                </label>
            </span>
        @endif
        @if($item->hasChildren())
            <ul>
                @include('empresa.usuario.menu-item', ['items' => $item->children()])
            </ul>
        @endif
    </li>
@endforeach
