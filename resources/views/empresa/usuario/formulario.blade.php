<div>

    <!-- widget edit box -->
    <div class="jarviswidget-editbox">
        <!-- This area used as dropdown edit box -->

    </div>
    <!-- end widget edit box -->

    <!-- widget content -->
    <div class="widget-body no-padding">
        <form role="form" id="form_empresa" method="post" action="{{url('empresa/usuario')}}" class="smart-form">
            <fieldset>
                <div class="row">
                    <section class="col col-6">
                        <div class="row">
                            <section class="col col-6">
                                <label class="label">Colaborador</label>
                                <label class="input">
                                    <input type="text" class="telephone" id="tel_fixo" name="tel_fixo" placeholder="Pesquisar..." value="" onfocus="jQueryCliente.autocomplete($(this))">
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="label">Nível de Permissão</label>
                                <label class="input">
                                    <select class="form-control">
                                        <option>Selecionar</option>
                                    </select>
                                </label>
                            </section>
                        </div>
                        <div class="row">
                            <section class="col col-6">
                                <label class="label">Padrão</label>
                                <div class="inline-group">
                                    <label class="radio">
                                        <input type="radio" value="1" id="padrao" name="padrao">
                                        <i></i>Sim</label>
                                    <label class="radio">
                                        <input type="radio" value="0" id="padrao"  name="padrao" checked="checked">
                                        <i></i>Não</label>
                                </div>
                            </section>
                        </div>
                    </section>
                    <section class="col col-6">
                        <div class="tree smart-form">
                            <ul>
                                @include('empresa.usuario.menu-item', ['items' => $MenuFull->roots()])

                                {{--<li>--}}
                                    {{--<span><i class="fa fa-lg fa-list"></i> Menus</span>--}}
                                    {{--<ul>--}}
                                        {{--<li>--}}
                                            {{--<span><i class="fa fa-lg fa-minus-circle"></i> Administrators</span>--}}
                                            {{--<ul>--}}
                                                {{--<li>--}}
                                                    {{--<span>--}}
                                                        {{--<label class="checkbox inline-block">--}}
                                                            {{--<input type="checkbox" name="checkbox-inline"><i></i>Michael.Jackson--}}
                                                        {{--</label>--}}
                                                    {{--</span>--}}
                                                {{--</li>--}}
                                                {{--<li>--}}
                                                    {{--<span>--}}
                                                        {{--<label class="checkbox inline-block">--}}
                                                            {{--<input type="checkbox" checked="checked" name="checkbox-inline"><i></i>Sunny.Ahmed--}}
                                                        {{--</label>--}}
                                                    {{--</span>--}}
                                                {{--</li>--}}
                                                {{--<li>--}}
                                                    {{--<span>--}}
                                                        {{--<label class="checkbox inline-block">--}}
                                                            {{--<input type="checkbox" checked="checked" name="checkbox-inline"><i></i>Jackie.Chan--}}
                                                        {{--</label>--}}
                                                    {{--</span>--}}
                                                {{--</li>--}}
                                            {{--</ul>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                            </ul>
                        </div>
                    </section>
                </div>
            </fieldset>

            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="id_cliente" name="id_cliente" value="{{ $cliente['clie_id'] ?? '0' }}">
            <footer>
                <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                <button type="button" name="submit" class="btn btn-primary" data-title="Cadastro de Empresa" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
            </footer>
        </form>
    </div>
</div>
