@extends('layouts.page')

@push('css')
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Colaborador Perfil'])
        <!-- widget div-->
            <div>
                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">
                    <form role="form" class="smart-form" method="put"  action="{{url('empresa/colaborador').'/'.$colaborador['cola_id']}}" enctype="multipart/form-data">
                        <ul id="myTab1" class="nav nav-tabs bordered">
                            <li class="active">
                                <a href="#conta" data-toggle="tab"><i class="fa fa-fw fa-lg fa-user"></i> Conta</a>
                            </li>
                            <li>
                                <a href="#endereco" data-toggle="tab"><i class="fa fa-fw fa-lg fa-map-marker"></i> Endereço</a>
                            </li>
                            <li>
                                <a href="#contato" data-toggle="tab"><i class="fa fa-fw fa-lg fa-phone"></i> Contato</a>
                            </li>
                            <li>
                                <a href="#foto" data-toggle="tab"><i class="fa fa-fw fa-lg fa-image"></i> Foto</a>
                            </li>
                        </ul>

                        <div id="myTabContent1" class="tab-content">
                            <div class="tab-pane fade in active" id="conta">
                                @include('empresa.colaborador.perfil.dados')
                            </div>

                            <div class="tab-pane fade" id="endereco">
                                @include('empresa.colaborador.perfil.endereco')
                            </div>

                            <div class="tab-pane fade" id="contato">
                                @include('empresa.colaborador.perfil.contato')
                            </div>

                            <div class="tab-pane fade" id="foto">
                                @include('empresa.colaborador.perfil.foto')
                            </div>
                        </div>
                        <input type="hidden" name="perfil" id="perfil" value="1">
                        <footer>
                            <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                            <button type="button" class="btn btn-success" data-title="Atualizar Colaborador" data-loading-text="Atualizando dados..." onclick="jQueryForm.send_form_file($(this))"><i class="fa fa-floppy-o"></i> Atualizar</button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </article>
@endsection
