@foreach($items as $item)
    <li class="parent" data-id="{{$item->attributes['id']}}" data-sequence="{{$menu->attributes['sequence']}}">
        @if($item->hasChildren())
            <span data-id="{{$item->attributes['id']}}"><i class="fa fa-lg fa-square-o"></i> {{ $item->title }}</span>
            <ul data-child="{{$item->children()->count()}}" data-fullchecked="false">
                @include('empresa.colaborador.permissao-menu', ['items' => $item->children()])
            </ul>
        @else
            <span>
                <label class="checkbox inline-block">
                    <input type="checkbox" id="permissao-item" name="permissao-item" value="{{$item->attributes['id']}}" data-status="{{ ($item->attributes['acesso']) ? 'true' : 'false' }}" data-sequence="{{$item->attributes['sequence']}}" onclick="jQueryPermissao.addMenu($(this))" {{ ($item->attributes['acesso']) ? 'checked="checked"' : '' }}>
                    <i></i>{{ $item->title }}
                </label>
            </span>
        @endif
    </li>
@endforeach
