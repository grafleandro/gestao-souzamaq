@push('css')
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

<form class="smart-form" id="form-extrato-filtro" style="display: none">
    <fieldset>
        <legend>Busca Avaçada</legend>

        <div class="row">
            <section class="col col-4">
                <label class="label">Colaborador</label>
                <label class="input">
                    <input type="text" id="filtro_colaborador" name="filtro_colaborador" onfocus="jQueryColaborador.autocomplete($(this))">
                </label>
            </section>

            <section class="col col-2 form-group">
                <label class="label">Data de Início</label>
                <label class="input">
                    <div class='input-group date'>
                        <input name='dt_inicio' id='dt_inicio' data-mask-placeholder="-" data-mask="99/99/9999" type='text' class="form-control input-datepicker" value="{{ $colaborador['cola_data_nascimento'] ?? '' }}" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </label>
            </section>

            <section class="col col-2 form-group">
                <label class="label">Data de Termino</label>
                <label class="input">
                    <div class='input-group date'>
                        <input name='dt_termino' id='dt_termino' data-mask-placeholder="-" data-mask="99/99/9999" type='text' class="form-control input-datepicker" value="{{ $colaborador['cola_data_nascimento'] ?? '' }}" />
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </label>
            </section>
        </div>
    </fieldset>

    <input type="hidden" id="id_colaborador" name="filtro_colaborador_id">
    <footer>
        <button type="button" name="submit" class="btn btn-success" onclick="jQueryCCcolaborador.filtroListar($(this))"><i class="fa fa-filter"></i> Filtrar</button>
    </footer>
</form>

@push('scripts')
    <script src={{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}></script>
    <script src={{ asset('bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}></script>
    <script src={{ asset('js/custom/jquery-calendario.js') }}></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-colaborador.js') }}"></script>
@endpush
