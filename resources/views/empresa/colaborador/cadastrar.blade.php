@extends('layouts.page')

@push('css')
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @if(isset($colaborador['cola_id']))
            @include('header.header-content', ['title' => 'Colaborador - Cadastro', 'action' => ['empresa.colaborador.botao.header-menu-btn-insert', 'empresa.colaborador.botao.header-menu-btn-list']])
        @else
            @include('header.header-content', ['title' => 'Colaborador - Cadastro', 'action' => 'empresa.colaborador.botao.header-menu-btn-list'])
        @endif
        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">

                    <form role="form" id="form_colaborador"

                          @if(!isset($colaborador['cola_id'])) method="post"  action="{{url('empresa/colaborador')}}"

                          @else  method="put"  action="{{url('empresa/colaborador').'/'.$colaborador['cola_id']}}" @endif

                          class="smart-form">

                        <fieldset>
                            <div class="row">

                                <section class="col col-3">
                                    <label class="label">Situação</label>
                                    <div class="inline-group">
                                        <label class="radio">
                                            <input type="radio" value="1" id="situacao" name="situacao" @if(!isset($colaborador['cola_id'])) checked="checked" @endif  @if(isset($colaborador['cola_id'])) @if($colaborador['cola_status'] == 1)) checked="checked" @endif @endif >
                                            <i></i>Ativo</label>
                                        <label class="radio">
                                            <input type="radio" value="0" id="situacao"  name="situacao"  @if(isset($colaborador['cola_id'])) @if($colaborador['cola_status'] == 0)) checked="checked" @endif @endif>
                                            <i></i>Inativo</label>
                                    </div>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Tornar Usuário</label>
                                    <div class="inline-group">
                                        <label class="radio"  @if(isset($colaborador['cola_id'])) class="state-disabled" @endif>
                                            <input type="radio" value="1" id="usuario" name="usuario" @if(isset($colaborador['cola_id'])) disabled="disabled" @endif>
                                            <i></i>Sim</label>
                                        <label class="radio" @if(isset($colaborador['cola_id'])) class="state-disabled" @endif>
                                            <input type="radio" value="0" id="usuario"  name="usuario" checked="checked"  @if(isset($colaborador['cola_id'])) disabled="disabled" @endif>
                                            <i></i>Não</label>
                                    </div>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Função</label>
                                    <label class="select">
                                        <?php \App\Utils\FormUtils::select($funcao, 'funcao', 'funcao', 'form-control form-cascade-control', ($colaborador['func_id']) ?? null, false, array(), '-- Selecionar --', true, 'func_id', 'func_nome'); ?>
                                        <i></i>
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Comissão</label>
                                    <label class="input">
                                        <input type="text" id="comissao" name="comissao" class="mask-percent" placeholder="00,00%" value="{{ (isset($colaborador['cola_comissao']) && $colaborador['cola_comissao']) ? \App\Utils\Mask::porcentagem($colaborador['cola_comissao']) : '' }}" maxlength="7">
                                    </label>
                                </section>
                            </div>
                        </fieldset>

                        <fieldset>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Nome</label>
                                    <label class="input">
                                        <input type="text" id="nome" name="nome" placeholder="Nome Completo" value="{{ $colaborador['cola_nome'] ?? '' }}">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">CPF</label>
                                    <label class="input">
                                        <input type="text" class="mask-cpf" id="cpf" name="cpf" placeholder="Cadastro de Pessoa Física" value="{{ $colaborador['cola_cpf'] ?? '' }}">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">RG</label>
                                    <label class="input">
                                        <input type="text" id="rg" name="rg" placeholder="Registro Geral" value="{{ $colaborador['cola_rg'] ?? '' }}">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Data de Nascimento</label>
                                    <label class="input">
                                        <div class='input-group date'>
                                            <input name='dt_nascimento' id='dt_nascimento' type='text' class="form-control input-datepicker" value="{{ $colaborador['cola_data_nascimento'] ?? '' }}" />
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <header>
                            Contato
                        </header>
                        <fieldset>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Telefone Fixo</label>
                                    <label class="input">
                                        <input type="text" class="telephone" id="tel_fixo" name="tel_fixo" placeholder="(00) 0000-0000" value="{{ $colaborador['pessoa_contato']['cont_tel_fixo'] ?? '' }}" >
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Telefone Celular 1</label>
                                    <label class="input">
                                        <input type="text" class="cell_phone_1" id="celular_1" name="celular_1" placeholder="(00) 0 0000-0000" value="{{ $colaborador['pessoa_contato']['cont_cel_1'] ?? '' }}" >
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Telefone Celular 2</label>
                                    <label class="input">
                                        <input type="text" class="cell_phone_2" id="celular_2" name="celular_2" placeholder="(00) 0 0000-0000" value="{{ $colaborador['pessoa_contato']['cont_cel_2'] ?? '' }}" >
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Email</label>
                                    <label class="input">
                                        <input type="email" id="email" name="email" value="{{ $colaborador['pessoa_contato']['cont_email'] ?? '' }}"  required>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <header>
                            Endereço
                        </header>
                        <fieldset>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Tipo Logradouro</label>
                                    <label class="select">
                                        <?php \App\Utils\FormUtils::select($tipoLogradouro, 'tipo_logradouro', 'tipo_logradouro', 'form-control form-cascade-control', ($colaborador['pessoa_endereco']['enlo_id']) ?? null, false, array(), '-- Selecionar --', true, 'enlo_id', 'enlo_titulo'); ?>
                                        <i></i>
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Logradouro</label>
                                    <label class="input">
                                        <input type="text" id="rua" name="rua" placeholder="Nome do Tipo Logradouro" value="{{ $colaborador['pessoa_endereco']['ende_logradouro_titulo'] ?? '' }}" >
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Número</label>
                                    <label class="input">
                                        <input type="text" id="numero" name="numero" placeholder="Número do Local" value="{{ $colaborador['pessoa_endereco']['ende_numero'] ?? '' }}" >
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">CEP</label>
                                    <label class="input">
                                        <input type="text" id="cep" name="cep" placeholder="Código de Endereçamento Posta" value="{{$colaborador['pessoa_endereco']['ende_cep'] ?? '' }}"  >
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Bairro</label>
                                    <label class="input">
                                        <input type="text" id="bairro" name="bairro" placeholder="Bairro" value="{{ $colaborador['pessoa_endereco']['ende_bairro'] ?? '' }}">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Cidade</label>
                                    <label class="input">
                                        <input type="text" id="cidade" name="cidade" placeholder="Cidade" value="{{ $colaborador['pessoa_endereco']['ende_cidade'] ?? '' }}">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Estado</label>
                                    <label class="input">
                                        <input type="text" id="estado" name="estado" placeholder="Cidade" value="{{ $colaborador['pessoa_endereco']['ende_estado'] ?? '' }}">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Complemento</label>
                                    <label class="input">
                                        <input type="text" id="complemento" name="complemento" placeholder="Complemento do Endereço" value="{{ $colaborador['pessoa_endereco']['ende_complemento'] ?? '' }}">
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                        <footer>
                            <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                            <button type="button" name="submit" class="btn btn-primary" data-title="Cadastro de Colaborador" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </article>
@endsection

@push('scripts')
    <script src={{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}></script>
    <script src={{ asset('bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}></script>
    <script src={{ asset('js/custom/jquery-calendario.js') }}></script>
    <script src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script src={{ asset('js/custom/jquery-search-cep.js') }}></script>
@endpush
