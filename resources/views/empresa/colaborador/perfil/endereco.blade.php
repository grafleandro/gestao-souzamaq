<fieldset>
    <div class="row">
        <section class="col col-3">
            <label class="label">Tipo Logradouro</label>
            <label class="select">
                <?php \App\Utils\FormUtils::select($tipoLogradouro, 'tipo_logradouro', 'tipo_logradouro', 'form-control form-cascade-control', ($colaborador['pessoa_endereco']['enlo_id']) ?? null, false, array(), '-- Selecionar --', true, 'enlo_id', 'enlo_titulo'); ?>
                <i></i>
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Logradouro</label>
            <label class="input">
                <input type="text" id="rua" name="rua" placeholder="Nome do Tipo Logradouro" value="{{ $colaborador['pessoa_endereco']['ende_logradouro_titulo'] ?? '' }}" required>
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Número</label>
            <label class="input">
                <input type="text" id="numero" name="numero" placeholder="Número do Local" value="{{ $colaborador['pessoa_endereco']['ende_numero'] ?? '' }}" required>
            </label>
        </section>

        <section class="col col-3">
            <label class="label">CEP</label>
            <label class="input">
                <input type="text" id="cep" name="cep" placeholder="Código de Endereçamento Posta" value="{{$colaborador['pessoa_endereco']['ende_cep'] ?? '' }}"  required>
            </label>
        </section>
    </div>

    <div class="row">
        <section class="col col-3">
            <label class="label">Bairro</label>
            <label class="input">
                <input type="text" id="bairro" name="bairro" placeholder="Bairro" value="{{ $colaborador['pessoa_endereco']['ende_bairro'] ?? '' }}">
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Cidade</label>
            <label class="input">
                <input type="text" id="cidade" name="cidade" placeholder="Cidade" value="{{ $colaborador['pessoa_endereco']['ende_cidade'] ?? '' }}">
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Estado</label>
            <label class="input">
                <input type="text" id="estado" name="estado" placeholder="Cidade" value="{{ $colaborador['pessoa_endereco']['ende_estado'] ?? '' }}" required>
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Complemento</label>
            <label class="input">
                <input type="text" id="complemento" name="complemento" placeholder="Complemento do Endereço" value="{{ $colaborador['pessoa_endereco']['ende_complemento'] ?? '' }}">
            </label>
        </section>
    </div>
</fieldset>
