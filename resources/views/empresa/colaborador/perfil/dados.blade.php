
<fieldset>
    <div class="row">
        <section class="col col-4">
            <label class="label">Nome</label>
            <label class="input">
                <input type="text" id="nome" name="nome" placeholder="Nome Completo" value="{{ $colaborador['cola_nome'] ?? '' }}" required>
            </label>
        </section>

        <section class="col col-4">
            <label class="label">CPF</label>
            <label class="input">
                <input type="text" id="cpf" name="cpf" placeholder="Cadastro de Pessoa Fisíca" value="{{ \App\Utils\Mask::cpf($colaborador['cola_cpf']) ?? '' }}">
            </label>
        </section>

        <section class="col col-4">
            <label class="label">RG</label>
            <label class="input">
                <input type="text" id="rg" name="rg" placeholder="Registro Geral" value="{{ $colaborador['cola_rg'] ?? '' }}">
            </label>
        </section>
    </div>

    <div class="row">
        <section class="col col-4">
            <label class="label">Data de Nascimento</label>
            <label class="input">
                <input type="text" id="dt_nascimento" name="dt_nascimento" placeholder="Data de Nascimento" value="{{ \Carbon\Carbon::parse($colaborador['cola_data_nascimento'])->format('d/m/Y') ?? '' }}">
            </label>
        </section>

        <section class="col col-4">
            <label class="label">Nova Senha</label>
            <label class="input">
                <input type="password" id="nova_senha" name="nova_senha" placeholder="Nova Senha">
            </label>
        </section>

        <section class="col col-4">
            <label class="label">Confirmar Senha</label>
            <label class="input">
                <input type="password" id="cofirmar_senha" name="cofirmar_senha" placeholder="Confirmar Senha">
            </label>
        </section>
    </div>
</fieldset>
