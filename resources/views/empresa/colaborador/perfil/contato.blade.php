<fieldset>
    <div class="row">
        <section class="col col-3">
            <label class="label">Telefone Fixo</label>
            <label class="input">
                <input type="text" class="telephone" id="tel_fixo" name="tel_fixo" placeholder="(00) 0000-0000" value="{{ $colaborador['pessoa_contato']['cont_tel_fixo'] ? \App\Utils\Mask::telComercial($colaborador['pessoa_contato']['cont_tel_fixo']) : '' }}" required>
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Telefone Celular 1</label>
            <label class="input">
                <input type="text" class="cell_phone_1" id="celular_1" name="celular_1" placeholder="(00) 0 0000-0000" value="{{ $colaborador['pessoa_contato']['cont_cel_1'] ? \App\Utils\Mask::telCelular($colaborador['pessoa_contato']['cont_cel_1']) : '' }}" required>
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Telefone Celular 2</label>
            <label class="input">
                <input type="text" class="cell_phone_2" id="celular_2" name="celular_2" placeholder="(00) 0 0000-0000" value="{{ $colaborador['pessoa_contato']['cont_cel_2'] ? \App\Utils\Mask::telCelular($colaborador['pessoa_contato']['cont_cel_2']) : '' }}" >
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Email</label>
            <label class="input">
                <input type="email" id="email" name="email" value="{{ $colaborador['pessoa_contato']['cont_email'] ?? '' }}" readonly required>
            </label>
        </section>
    </div>
</fieldset>
