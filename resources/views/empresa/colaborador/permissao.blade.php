@extends('layouts.page')

@push('css')
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

            @include('header.header-content', ['title' => 'Colaborador - Permissão de Acesso'])
            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">
                    <strong>Usuário:</strong> {{ $colaborador['cola_nome'] }}<br>
                    <strong>CPF:</strong> {{ \App\Utils\Mask::cpf($colaborador['cola_cpf']) }}<br>
                    <strong>Função:</strong> {{ $colaborador['pessoa_funcao']['func_sigla'] }} - {{ $colaborador['pessoa_funcao']['func_nome'] }}
                    <div class="alert alert-warning fade in margin-top-10">
                        <i class="fa-fw fa fa-warning"></i>
                        <strong>Atenção:</strong> A seguir estão listados, todos os menus de acesso ao sistema. Selecionar apenas os MENUS que o usuário poderá acessar. Alguns itens estarão selecionados de acordo com a Função que foi concedida ao Colaborador.
                    </div>
                    <form role="form" class="smart-form">
                        <fieldset>
                            <div class="panel-group smart-accordion-default" id="accordion-2">
                                @foreach($MenuPermissao->roots() as $index => $menu)
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-2" href="#submenu-{{$menu->id}}-1" class="collapsed">
                                                    <i class="fa fa-fw fa-plus-circle txt-color-green"></i>
                                                    <i class="fa fa-fw fa-minus-circle txt-color-red"></i>
                                                    {{ $menu->title }} #{{$menu->id}}
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="submenu-{{$menu->id}}-1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                @if($menu->hasChildren())
                                                    <div class="tree">
                                                        <ul>
                                                            <li class="parent" data-id="{{$menu->attributes['id']}}" data-sequence="{{$menu->attributes['sequence']}}">
                                                                <span data-id="{{$menu->attributes['id']}}"><i class="fa fa-lg fa-folder-open"></i> {{$menu->title}}</span>
                                                                <ul>
                                                                    @include('empresa.colaborador.permissao-menu', ['items' => $menu->children()])
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                @else
                                                    <div class="tree">
                                                        <ul>
                                                            <li class="parent" data-id="{{$menu->attributes['id']}}" data-sequence="{{$menu->attributes['sequence']}}">
                                                                <span data-id="{{$menu->attributes['id']}}"><i class="fa fa-lg fa-folder-open"></i> {{$menu->title}}</span>
                                                                <ul>
                                                                    <span>
                                                                        <label class="checkbox inline-block">
                                                                            <input type="checkbox" id="permissao-item" name="permissao-item" value="{{$menu->attributes['id']}}" data-status="{{ ($menu->attributes['acesso']) ? 'true' : 'false' }}" data-sequence="{{$menu->attributes['sequence']}}" onclick="jQueryPermissao.addMenu($(this))" {{ ($menu->attributes['acesso']) ? 'checked="checked"' : '' }}>
                                                                            <i></i>{{ $menu->title }}
                                                                        </label>
                                                                    </span>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </fieldset>
                        <input type="hidden" id="cola_id" name="cola_id" value="{{ $colaborador['cola_id'] }}">
                        <footer>
                            <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                            <button type="button" name="submit" class="btn btn-primary" data-title="Cadastro de Colaborador" data-loading-text="Salvando dados..." onclick="jQueryPermissao.save($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </article>
@endsection

@push('scripts')
    <script>
        let colaboradorMenus = JSON.parse('{{ json_encode($colaboradorMenu) }}');
    </script>
    <script src={{asset('js/custom/jquery-permissao.js')}}></script>
@endpush
