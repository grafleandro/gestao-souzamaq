@extends('layouts.page')

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Fornecedor - Cadastro'])
        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">

                    <form id="smart-form-register" class="smart-form">
                        <fieldset>
                            <div class="form-group">
                                <legend>Informações Cadastral</legend>
                                <label class="col-sm-1 control-label"><strong>Tipo</strong></label>
                                <div class="col col-2">
                                    <label class="radio">
                                        <input type="radio" name="tipo">
                                        <i></i>Física</label>
                                    <label class="radio">
                                        <input type="radio" name="tipo">
                                        <i></i>Jurídica</label>
                                </div>
                                <label class="col-sm-2 control-label"><strong>Contribuição ICMS</strong></label>
                                <div class="col col-2">
                                    <label class="radio">
                                        <input type="radio" name="ContICMS">
                                        <i></i>Sim</label>
                                    <label class="radio">
                                        <input type="radio" name="ContICMS">
                                        <i></i>Isento</label>
                                    <label class="radio">
                                        <input type="radio" name="ContICMS">
                                        <i></i>Não</label>
                                </div>
                                <label class="col-sm-2 control-label"><strong>Destaque padrão na Nota Fiscal de Devolução</strong></label>
                                <div class="col col-3">
                                    <label class="radio">
                                        <input type="radio" name="destNF">
                                        <i></i>Sempre destacar IPI no campo próprio</label>
                                    <label class="radio">
                                        <input type="radio" name="destNF">
                                        <i></i>Sempre destacar ICMS ST no campo próprio</label>
                                </div>
                            </div>

                        </fieldset>

                        <fieldset>
                            <legend>Fornecedor</legend>
                            <div class="row">
                                <section class="col col-4">
                                    <label class="label">Nome</label>
                                    <label class="input">
                                        <input type="text" name="nome">
                                    </label>
                                </section>

                                <section class="col col-4">
                                    <label class="label">Razão Social</label>
                                    <label class="input">
                                        <input type="text" name="razao_social">
                                    </label>
                                </section>

                                <section class="col col-4">
                                    <label class="label">CPF/CNPJ</label>
                                    <label class="input">
                                        <input type="text" name="cpf_cnpj">
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <header>
                            Contato
                        </header>
                        <fieldset>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Telefone Fixo</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="(00) 0000-0000">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Telefone Celular 1</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="(00) 0 0000-0000">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Telefone Celular 2</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="(00) 0 0000-0000">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Email</label>
                                    <label class="input">
                                        <input type="text" name="lastname">
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <header>
                            Endereço
                        </header>
                        <fieldset>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Tipo Logradouro</label>
                                    <label class="select">
                                        <select name="gender">
                                            <option value="1">Rua</option>
                                            <option value="2">Alameda</option>
                                            <option value="3">Avenida</option>
                                            <option value="4">Beco</option>
                                        </select>
                                        <i></i>
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Logradouro</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="Nome do Tipo Logradouro">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Número</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="Número do Local">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">CEP</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="Código de Endereçamento Posta">
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Bairro</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="Bairro">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Cidade</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="Cidade">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Estado</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="Cidade">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Complemento</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="Complemento do Endereço">
                                    </label>
                                </section>
                            </div>
                        </fieldset>

                        <footer>
                            <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                            <button type="button" name="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Salvar</button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </article>
@endsection