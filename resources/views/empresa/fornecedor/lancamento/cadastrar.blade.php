@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Fornecedor - Cadastro'])
        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">

                    <form id="smart-form-register" class="smart-form">
                        <fieldset>
                            <legend>Fornecedor</legend>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Fornecedor</label>
                                    <label class="input">
                                        <input type="text" name="fornecedor">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Tipo</label>
                                    <label class="select">
                                        <select name="tipo">
                                            <option value="0"> </option>
                                            <option value="1">Crédito</option>
                                            <option value="2">Débito</option>
                                        </select>
                                        <i></i>
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Data</label>
                                    <label class="input">
                                        <input type="date" name="data">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Valor</label>
                                    <label class="input">
                                        <input type="text" name="valor">
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <section class="col col-md-12">
                                    <label class="label">Histórico</label>
                                    <label class="input">
                                        <input type="text" name="historico">
                                    </label>
                                </section>
                            </div>
                        </fieldset>

                        <footer>
                            <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                            <button type="button" name="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Salvar</button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </article>
@endsection