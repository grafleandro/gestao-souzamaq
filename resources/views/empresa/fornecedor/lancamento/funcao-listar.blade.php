<!-- NEW COL START -->
<article class="col col-md-12 col-sm-12">
    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false">
        @include('header.header-content', ['title' => 'Fornecedor - Lançamentos', 'action' => 'empresa.fornecedor.lancamento.header-menu-btn'])

        <!-- widget div-->
        <div>

            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->

            </div>
            <!-- end widget edit box -->

            <!-- widget content -->
            <div class="widget-body">

                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                <table id="dt_basic" class="table table-striped table-bordered table-hover dataTable no-footer has-columns-hidden " width="100%">
                    <thead>
                    <tr>
                        <th data-hide="phone">ID</th>
                        <th data-class="expand"> Fornecedor</th>
                        <th data-hide="phone"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Data</th>
                        <th data-hide="phone"><i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i> Valor</th>
                        <th>Ação</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Jennifer</td>
                        <td>25/06/2018</td>
                        <td>R$ 100,00</td>
                        <td>
                            <button class="btn btn-default"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-default"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Clark</td>
                        <td>25/06/2018</td>
                        <td>R$ 100,00</td>
                        <td>
                            <button class="btn btn-default"><i class="fa fa-pencil"></i></button>
                            <button class="btn btn-default"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    </tbody>
                </table>
                    </div>
                </div>
            </div>
            <!-- end widget content -->

        </div>
        <!-- end widget div -->

    </div>
    <!-- end widget -->
</article>

@push('scripts')
    <script src={{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}></script>
    <script src={{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}></script>

    <script>
        $('#dt_basic').dataTable({
            "autoWidth" : true,
        });
    </script>
@endpush
