<div>

    <!-- widget edit box -->
    <div class="jarviswidget-editbox">
        <!-- This area used as dropdown edit box -->

    </div>
    <!-- end widget edit box -->

    <!-- widget content -->
    <div class="widget-body no-padding">

        @if(!isset($empresa['empr_id']))
            <form role="form" id="form_empresa" method="post" action="{{url('empresa/empresa')}}" class="smart-form">
        @else
            <form role="form" id="form_empresa" method="put" action="{{url('empresa/empresa').'/'.$empresa['empr_id']}}" class="smart-form">
        @endif
            <fieldset>

                <section class="col col-12">
                    <label class="label">Situação</label>
                    <div class="inline-group">
                        <label class="radio">
                            <input type="radio" value="1" id="situacao" name="situacao" @if(!isset($empresa['empr_id'])) checked="checked" @endif  @if(isset($empresa['empr_id'])) @if($empresa['empr_status'] == 1)) checked="checked" @endif @endif >
                            <i></i>Ativo</label>
                        <label class="radio">
                            <input type="radio" value="0" id="situacao"  name="situacao"  @if(isset($empresa['empr_id'])) @if($empresa['empr_status'] == 0)) checked="checked" @endif @endif>
                            <i></i>Inativo</label>
                        <label class="radio">
                            <input type="radio" value="2" id="situacao"  name="situacao" @if(isset($empresa['empr_id'])) @if($empresa['empr_status'] == 2)) checked="checked" @endif @endif>
                            <i></i>Negativado</label>
                    </div>
                </section>
            </fieldset>

            <fieldset>
                <div class="row">
                    <section class="col col-3">
                        <label class="label">Nome</label>
                        <label class="input">
                            <input type="text" id="nome" name="nome" placeholder="Nome da Empresa"  value="{{ $empresa['empr_nome'] ?? '' }}" required>
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">Razão Social</label>
                        <label class="input">
                            <input type="text" id="razao_social" name="razao_social" placeholder="Nome da Empresa" value="{{ $empresa['empr_razao_social'] ?? '' }}"  required>
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">CNPJ</label>
                        <label class="input">
                            <input type="text" class="mask-cnpj" id="cnpj" name="cnpj" placeholder="CNPJ da empresa" value="{{ $empresa['empr_cnpj'] ?? '' }}"   required>
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">Inscrição Estadual</label>
                        <label class="input">
                            <input type="text" id="inscricao_estadual" name="inscricao_estadual" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="Registro Geral">
                        </label>
                    </section>
                </div>
            </fieldset>
            <header>
                Contato
            </header>
            <fieldset>
                <div class="row">
                    <section class="col col-3">
                        <label class="label">Telefone Fixo</label>
                        <label class="input">
                            <input type="text" class="telephone" id="tel_fixo" name="tel_fixo" placeholder="(00) 0000-0000" value="{{ $empresa['empresa_contato']['cont_tel_fixo'] ?? '' }}" required>
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">Telefone Celular 1</label>
                        <label class="input">
                            <input type="text" class="cell_phone_1" id="celular_1" name="celular_1" placeholder="(00) 0 0000-0000" value="{{ $empresa['empresa_contato']['cont_cel_1'] ?? '' }}" required>
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">Telefone Celular 2</label>
                        <label class="input">
                            <input type="text" class="cell_phone_2" id="celular_2" name="celular_2" placeholder="(00) 0 0000-0000" value="{{ $empresa['empresa_contato']['cont_cel_2'] ?? '' }}" >
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">Email</label>
                        <label class="input">
                            <input type="email" id="email" name="email" value="{{ $empresa['empresa_contato']['cont_email'] ?? '' }}"  required>
                        </label>
                    </section>
                </div>
            </fieldset>
            <header>
                Endereço
            </header>
            <fieldset>
                <div class="row">
                    <section class="col col-3">
                        <label class="label">Tipo Logradouro</label>
                        <label class="select">
                            <?php \App\Utils\FormUtils::select($tipoLogradouro, 'tipo_logradouro', 'tipo_logradouro', 'form-control form-cascade-control', ($empresa['empresa_endereco']['enlo_id']) ?? null, false, array(), '-- Selecionar --', true, 'enlo_id', 'enlo_titulo'); ?>
                            <i></i>
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">Logradouro</label>
                        <label class="input">
                            <input type="text" id="rua" name="rua" placeholder="Nome do Tipo Logradouro" value="{{ $empresa['empresa_endereco']['ende_logradouro_titulo'] ?? '' }}" required>
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">Número</label>
                        <label class="input">
                            <input type="text" id="numero" name="numero" placeholder="Número do Local" value="{{ $empresa['empresa_endereco']['ende_numero'] ?? '' }}" required>
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">CEP</label>
                        <label class="input">
                            <input type="text" id="cep" name="cep" placeholder="Código de Endereçamento Posta" value="{{ $empresa['empresa_endereco']['ende_cep'] ?? '' }}"  required>
                        </label>
                    </section>
                </div>

                <div class="row">
                    <section class="col col-3">
                        <label class="label">Bairro</label>
                        <label class="input">
                            <input type="text" id="bairro" name="bairro" placeholder="Bairro" value="{{ $empresa['empresa_endereco']['ende_bairro'] ?? '' }}">
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">Cidade</label>
                        <label class="input">
                            <input type="text" id="cidade" name="cidade" placeholder="Cidade" value="{{ $empresa['empresa_endereco']['ende_cidade'] ?? '' }}">
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">Estado</label>
                        <label class="input">
                            <input type="text" id="estado" name="estado" placeholder="Cidade" value="{{ $empresa['empresa_endereco']['ende_estado'] ?? '' }}" required>
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">Complemento</label>
                        <label class="input">
                            <input type="text" id="complemento" name="complemento" placeholder="Complemento do Endereço" value="{{ $empresa['empresa_endereco']['ende_complemento'] ?? '' }}">
                        </label>
                    </section>
                </div>
            </fieldset>
            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
            <footer>
                <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                <button type="button" name="submit" class="btn btn-primary" data-title="Cadastro de Empresa" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
            </footer>
        </form>
    </div>
</div>
