@extends('layouts.page')

@push('css')
    <!-- CSS -->
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @if(!isset($produto['prod_id']))
            @include('header.header-content', ['title' => 'Produto - Cadastrar', 'action' => ['empresa.produto.header-menu-btn-list', 'empresa.produto.header-menu-btn-markup']])
        @else
            @include('header.header-content', ['title' => 'Produto - Cadastrar', 'action' => ['empresa.produto.header-menu-btn-insert', 'empresa.produto.header-menu-btn-list', 'empresa.produto.header-menu-btn-markup']])
        @endif
        <!-- widget div-->
            @include('empresa.produto.formulario')
        </div>
    </article>
@endsection

@push('scripts')
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-produto.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-grupo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-fabricante.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-marca.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-comissao.js') }}"></script>
@endpush
