<div>

    <!-- widget edit box -->
    <div class="jarviswidget-editbox">
        <!-- This area used as dropdown edit box -->

    </div>
    <!-- end widget edit box -->

    <!-- widget content -->
    <div class="widget-body no-padding">

        @if(!isset($produto['prod_id']))
            <form role="form" id="form_produto" method="post" action="{{url('/empresa/produto')}}" class="smart-form">
        @else
            <form role="form" id="form_produto" method="put" action="{{url('/empresa/produto').'/'.$produto['prod_id']}}" class="smart-form">
        @endif
            <fieldset>
                <div class="row">
                    <section class="col col-4">
                        <label class="label">Produto</label>
                        <label class="input">
                            <input type="text" id="prod_titulo" name="prod_titulo" value="{{ $produto['prod_titulo'] ?? '' }}"  required>
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">Código de Barra</label>
                        <label class="input">
                            <input type="text" id="prod_cod_barra" name="prod_cod_barra" placeholder="Código de barra"  value="{{ $produto['prod_cod_barra'] ?? '' }}" required>
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">Grupo</label>
                        <label class="input">
                            <input type="text" id="prod_grupo" name="prod_grupo" placeholder="Selecionar Grupo..." value="{{ $produto['produto_grupo']['grup_titulo'] ?? '' }}" onfocus="jQueryGrupo.autocomplete($(this))" required>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section class="col col-4">
                        <label class="label">Fabricante</label>
                        <label class="input">
                            <input type="text" id="prod_fabricante" name="prod_fabricante" placeholder="Selecionar Fabricante..." value="{{ $produto['produto_fabricante']['fabr_nome_nome_fantazia'] ?? '' }}" onfocus="jQueryFabricante.autocomplete($(this))" required>
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">Marcas</label>
                        <label class="input">
                            <input type="text" id="prod_marca" name="prod_marca" placeholder="Selecionar Marca..." value="{{ $produto['produto_marca']['marc_titulo'] ?? '' }}" onfocus="jQueryMarca.autocomplete($(this))" required>
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">Unidade de Venda</label>
                        <label class="input">
                            {{ \App\Utils\FormUtils::select($unid_medida, 'prod_unid_venda', 'prod_unid_venda', 'form-control form-cascade-control', ($produto['unidade_medida']['unme_id']) ?? null, false, array(), '-- Selecionar --', true, 'unme_id', 'unme_titulo') }}
                        </label>
                    </section>
                </div>
            </fieldset>
                <fieldset>
                    <div class="row">
                        <section class="col col-4">
                            <label class="label">NCM</label>
                            <label class="input"> <i class="icon-append fa fa-search"></i>
                                @if(isset($produto['prod_id']))
                                    <input type="text" id="prod_trib_ncm" name="prod_trib_ncm" placeholder="Buscar NCM..." onfocus="jQueryProduto.autocompleteTribNCM($(this))" value="{{ $produto['produto_ncm']['finc_cod_ncm'] . ' - ' . $produto['produto_ncm']['finc_descricao'] }}">
                                @else
                                    <input type="text" id="prod_trib_ncm" name="prod_trib_ncm" placeholder="Buscar NCM..." onfocus="jQueryProduto.autocompleteTribNCM($(this))">
                                @endif
                                <b class="tooltip tooltip-top-right">
                                    <i class="fa fa-warning txt-color-teal"></i>
                                    Buscar NCM por Código ou Título
                                </b>
                            </label>
                        </section>

                        <section class="col col-8">
                            <label class="label">Descrição do N.C.M</label>
                            <label class="input">
                                <input type="text" id="trib_ncm_desc" name="trib_ncm_desc" value="{{($produto['produto_ncm']['finc_descricao']) ?? ''}}" disabled>
                            </label>
                        </section>
                    </div>
                </fieldset>
            <header>
                Valores
            </header>
            <fieldset>
                <div class="row">
                    <section class="col col-4">
                        <label class="label">Custos (R$)</label>
                        <label class="input">
                            <input class="mask-money" type="text" id="prod_custo" name="prod_custo" value="{{ \App\Utils\Mask::dinheiro($produto['produto_valores']['prva_custo'] ?? 0 )}}"  required>
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">Markup (%)</label>
                        <label class="input">
                            <input class="mask-percent" type="text" id="prod_markup" name="prod_markup" maxlength="7" onblur="jQueryProduto.precoSugerido($(this))" value="{{ \App\Utils\Mask::porcentagem($produto['produto_valores']['prva_markup'] ?? 0) }}">
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">Preço Sugerido (R$)</label>
                        <label class="input state-disabled">
                            <input class="mask-money" type="text" id="prod_preco_sugerido" name="prod_preco_sugerido" value="{{ \App\Utils\Mask::dinheiro($produto['produto_valores']['prva_preco_sugerido'] ?? 0) }}" readonly>
                        </label>
                    </section>
                </div>
                <div class="row">
                    <section class="col col-4">
                        <label class="label">Preço de Venda (R$)</label>
                        <label class="input">
                            <input class="mask-money" type="text" id="prod_preco_venda" name="prod_preco_venda" onblur="jQueryProduto.precoVenda($(this))" value="{{ \App\Utils\Mask::dinheiro($produto['produto_valores']['prva_preco_venda'] ?? 0) }}">
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">Cota Destinada a Comissão</label>
                        <label class="input">
                            <input type="text" class="mask-percent" id="cota_comissao" name="cota_comissao" onblur="jQueryComissao.cotaEmpresa($(this))" value="{{(isset($produto['produto_valores']['prva_cota_comissao'])) ? \App\Utils\Mask::porcentagem($produto['produto_valores']['prva_cota_comissao']) : ''}}" maxlength="7">
                            <b class="tooltip tooltip-top-left">
                                <i class="fa fa-warning txt-color-teal"></i>
                                Porcentagem que será destinada as comissões sobre este produto.
                            </b>
                        </label>
                    </section>

                    <section class="col col-4">
                        <label class="label">Cota Destinada a Empresa</label>
                        <label class="input">
                            <input type="text" class="mask-percent" id="cota_empresa" name="cota_empresa" value="{{(isset($produto['produto_valores']['prva_cota_empresa'])) ? \App\Utils\Mask::porcentagem($produto['produto_valores']['prva_cota_empresa']) : ''}}" maxlength="7" readonly>
                            <b class="tooltip tooltip-top-left">
                                <i class="fa fa-warning txt-color-teal"></i>
                                Porcentagem que será destinada a Empresa sobre os valores deste produto.
                            </b>
                        </label>
                    </section>
                </div>
            </fieldset>
            <header>
                Estoque
            </header>
            <fieldset>
                <div class="row">
                    <section class="col col-3">
                        <label class="label">Contrololar Estoque</label>
                        <div class="inline-group">
                            <label class="radio">
                                <input type="radio" value="1" id="prod_cont_estoque" name="prod_cont_estoque" checked="checked">
                                <i></i>Sim
                            </label>
                            <label class="radio">
                                <input type="radio" value="0" id="prod_cont_estoque"  name="prod_cont_estoque">
                                <i></i>Não
                            </label>
                        </div>
                    </section>

                    <section class="col col-3">
                        <label class="label">Estoque Mínimo</label>
                        <label class="input">
                            <input type="text" id="prod_estoq_minimo" name="prod_estoq_minimo" value="{{ $produto['produto_estoque']['pres_estoq_mini'] ?? 0 }}"  required>
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">Estoque Máximo</label>
                        <label class="input">
                            <input type="text" id="prod_estoq_maximo" name="prod_estoq_maximo" value="{{ $produto['produto_estoque']['pres_estoq_max'] ?? 0 }}"  required>
                        </label>
                    </section>

                    <section class="col col-3">
                        <label class="label">Estoque Atual</label>
                        <label class="input">
                            <input type="text" id="prod_estoq_atual" name="prod_estoq_atual" value="{{ $produto['produto_estoque']['prest_estoq_atual'] ?? 0 }}"  required>
                        </label>
                    </section>
                </div>
            </fieldset>
            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" id="trib_ncm_codigo" name="trib_ncm_codigo" value="{{ ($produto['produto_ncm']['finc_id']) ?? 0 }}">
            <input type="hidden" id="grup_id" name="grup_id" value="{{ ($produto['produto_grupo']['grup_id']) ?? 0 }}">
            <input type="hidden" id="fabr_id" name="fabr_id" value="{{ ($produto['produto_fabricante']['fabr_id']) ?? 0 }}">
            <input type="hidden" id="marc_id" name="marc_id" value="{{ ($produto['produto_marca']['marc_id']) ?? 0 }}">
            <footer>
                <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                @if(isset($produto['prod_id']))
                    <button type="button" name="submit" class="btn btn-primary" data-title="Atualizar Produto" data-loading-text="Atualizando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                @else
                    <button type="button" name="submit" class="btn btn-primary" data-title="Cadastrar Produto" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                @endif
            </footer>
        </form>
    </div>
</div>
