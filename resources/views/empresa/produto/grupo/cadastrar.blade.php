@extends('layouts.page')

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Cadastrar Grupo','action' => 'empresa.produto.grupo.header-menu-btn-list'])
        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">
                    @if(!isset($grupo['grup_id']))
                        <form role="form" id="form_grupo" method="post" action="{{url('empresa/grupo')}}" class="smart-form">
                    @else
                        <form role="form" id="form_grupo" method="put" action="{{url('empresa/grupo').'/'.$grupo['grup_id']}}" class="smart-form">
                    @endif
                    <fieldset>
                        <section class="col col-4">
                            <label class="label">Título</label>
                            <label class="input">
                                <input type="text" id="gp_titulo" name="gp_titulo" placeholder="Título do Grupo"  value="{{ $grupo['grup_titulo'] ?? '' }}" required>
                            </label>
                        </section>

                        <section class="col col-8">
                            <label class="label">Descrição</label>
                            <label class="input">
                                <input type="text" id="gp_descricao" name="gp_descricao" placeholder="Descrição do Grupo"  value="{{ $grupo['grup_descricao'] ?? '' }}" required>
                            </label>
                        </section>
                    </fieldset>
                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="grup_id" name="grup_id" value="{{ $grupo['grup_id'] ?? 0 }}">
                    <footer>
                        <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                        <button type="button" name="submit" class="btn btn-primary" data-title="Grupo" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                    </footer>
                </form>
                </div>
            </div>
        </div>
    </article>
@endsection
