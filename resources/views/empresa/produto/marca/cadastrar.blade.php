@extends('layouts.page')

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

                @include('header.header-content', ['title' => 'Cadastrar Marca','action' => 'empresa.produto.marca.header-menu-btn-list'])
                <!-- widget div-->
                    <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                    <!-- This area used as dropdown edit box -->

                                </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                @if(!isset($marca['marc_id']))
                                    <form role="form" id="form_marca" method="post" action="{{url('empresa/marca')}}" class="smart-form">
                                @else
                                    <form role="form" id="form_marca" method="put" action="{{url('empresa/marca').'/'.$marca['marc_id']}}" class="smart-form">
                                @endif
                                <fieldset>
                                    <section class="col col-4">
                                            <label class="label">Título</label>
                                            <label class="input">
                                            <input type="text" id="gp_titulo" name="mc_titulo" placeholder="Título do Marca"  value="{{ $marca['marc_titulo'] ?? '' }}" required>
                                        </label>
                                    </section>

                                    <section class="col col-8">
                                            <label class="label">Descrição</label>
                                            <label class="input">
                                            <input type="text" id="mc_descricao" name="mc_descricao" placeholder="Descrição da Marca"  value="{{ $marca['marc_descricao'] ?? '' }}" required>
                                        </label>
                                    </section>
                                </fieldset>
                                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                                <footer>
                                    <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                                    <button type="button" name="submit" class="btn btn-primary" data-title="Marca" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                                </footer>
                            </form>
                        </div>
                    </div>
                </div>
        </article>
@endsection
