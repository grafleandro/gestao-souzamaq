<fieldset>
    <div class="row">
        <section class="col col-4">
            <label class="label">Nome</label>
            <label class="input">
                <input type="text" name="fab_nome" placeholder="Nome Completo" value="{{ ($fabricante['fabr_nome_nome_fantazia']) ?? '' }}">
            </label>
        </section>

        <section class="col col-4">
            <label class="label">CPF</label>
            <label class="input">
                <input type="text" name="fab_cpf" class="mask-cpf" placeholder="Cadastro de Pessoa Física" value="{{ (isset($fabricante['fabr_cpf_cnpj'])) ? \App\Utils\Mask::cpf(($fabricante['fabr_cpf_cnpj'])) : '' }}">
            </label>
        </section>

        <section class="col col-4">
            <label class="label">RG</label>
            <label class="input">
                <input type="text" name="fab_rg" placeholder="Registro Geral" value="{{ ($fabricante['fabr_rg_insc_estadual']) ?? ''  }}">
            </label>
        </section>
    </div>
</fieldset>
