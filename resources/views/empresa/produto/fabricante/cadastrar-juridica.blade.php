<fieldset>
    <div class="row">
        <section class="col col-3">
            <label class="label">Razão Social</label>
            <label class="input">
                <input type="text" name="fab_razao_social" placeholder="Razão Social" value="{{ ($fabricante['fabr_razao_social']) ?? '' }}">
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Nome Fantasia</label>
            <label class="input">
                <input type="text" name="fab_nome_fantasia" placeholder="Nome Fantasia" value="{{ ($fabricante['fabr_nome_nome_fantazia']) ?? '' }}">
            </label>
        </section>

        <section class="col col-3">
            <label class="label">CNPJ</label>
            <label class="input">
                <input type="text" name="fab_cnpj" class="mask-cnpj" placeholder="Cadastro Nacioal de Pessoa Jurídica" value="{{ (isset($fabricante['fabr_cpf_cnpj'])) ? \App\Utils\Mask::cnpj(($fabricante['fabr_cpf_cnpj'])) : '' }}">
            </label>
        </section>

        <section class="col col-3">
            <label class="label">Inscrição Estadual</label>
            <label class="input">
                <input type="text" name="fab_insc_estadual" placeholder="Inscrição Estadual" value="{{ ($fabricante['fabr_rg_insc_estadual']) ?? ''  }}">
            </label>
        </section>
    </div>
</fieldset>
