@extends('layouts.page')

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Cadastrar Fabricante','action' => 'empresa.produto.fabricante.header-menu-btn-list'])
        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">
                    @if(!isset($fabricante['fabr_id']))
                        <form role="form" id="form_fabricante" method="post" action="{{url('empresa/fabricante')}}" class="smart-form">
                    @else
                        <form role="form" id="form_fabricante" method="put" action="{{url('empresa/fabricante').'/'.$fabricante['fabr_id']}}" class="smart-form">
                    @endif
                    <fieldset>
                        @if(isset($fabricante['fabr_tipo']))
                            <div class="inline-group">
                                <label class="radio">
                                    <input type="radio" name="fab_tipo_pessoa" onclick="jQueryFabricante.carregarTipoCliente($(this))" value="{{ \App\Utils\ClienteUtils::_P_FISICA }}" {{ ($fabricante['fabr_tipo'] == \App\Utils\ClienteUtils::_P_FISICA) ? 'checked="checked"' : '' }}>
                                    <i></i>Física
                                </label>
                                <label class="radio">
                                    <input type="radio" name="fab_tipo_pessoa" onclick="jQueryFabricante.carregarTipoCliente($(this))" value="{{ \App\Utils\ClienteUtils::_P_JURIDICA }}" {{ ($fabricante['fabr_tipo'] == \App\Utils\ClienteUtils::_P_JURIDICA) ? 'checked="checked"' : '' }}>
                                    <i></i>Júridica
                                </label>
                            </div>
                        @else
                            <div class="inline-group">
                                <label class="radio">
                                    <input type="radio" name="fab_tipo_pessoa" onclick="jQueryFabricante.carregarTipoCliente($(this))" value="{{ \App\Utils\ClienteUtils::_P_FISICA }}" checked="checked">
                                    <i></i>Física
                                </label>
                                <label class="radio">
                                    <input type="radio" name="fab_tipo_pessoa" onclick="jQueryFabricante.carregarTipoCliente($(this))"  value="{{ \App\Utils\ClienteUtils::_P_JURIDICA }}" >
                                    <i></i>Júridica
                                </label>
                            </div>
                        @endif
                    </fieldset>
                    <div class="form-fisica {{ (isset($fabricante['fabr_tipo']) && $fabricante['fabr_tipo'] == \App\Utils\ClienteUtils::_P_FISICA) ? '' : 'hidden' }}">
                        @include('empresa.produto.fabricante.cadastrar-fisica', ['fabricante' => (isset($fabricante['fabr_id'])) ? $fabricante : []])
                    </div>

                    <div class="form-juridica {{ (isset($fabricante['fabr_tipo']) && $fabricante['fabr_tipo'] == \App\Utils\ClienteUtils::_P_JURIDICA) ? '' : 'hidden' }}">
                        @include('empresa.produto.fabricante.cadastrar-juridica', ['fabricante' => (isset($fabricante['fabr_id'])) ? $fabricante : []])
                    </div>
                    <header>
                        Contato
                    </header>
                    <fieldset>
                        <div class="row">
                            <section class="col col-3">
                                <label class="label">Telefone Fixo</label>
                                <label class="input">
                                    <input type="text" class="telephone" id="tel_fixo" name="tel_fixo" placeholder="(00) 0000-0000" value="{{ (isset($fabricante['fabricante_contato']['cont_tel_fixo'])) ? \App\Utils\Mask::telCelular($fabricante['fabricante_contato']['cont_tel_fixo']) : '' }}">
                                </label>
                            </section>

                            <section class="col col-3">
                                <label class="label">Telefone Celular 1</label>
                                <label class="input">
                                    <input type="text" class="cell_phone_1" id="celular_1" name="celular_1" placeholder="(00) 0 0000-0000" value="{{ (isset($fabricante['fabricante_contato']['cont_cel_1'])) ? \App\Utils\Mask::telCelular($fabricante['fabricante_contato']['cont_cel_1']) : '' }}">
                                </label>
                            </section>

                            <section class="col col-3">
                                <label class="label">Telefone Celular 2</label>
                                <label class="input">
                                    <input type="text" class="cell_phone_2" id="celular_2" name="celular_2" placeholder="(00) 0 0000-0000" value="{{ (isset($fabricante['fabricante_contato']['cont_cel_2'])) ? \App\Utils\Mask::telCelular($fabricante['fabricante_contato']['cont_cel_2']) : '' }}">
                                </label>
                            </section>

                            <section class="col col-3">
                                <label class="label">Email</label>
                                <label class="input">
                                    <input type="email" id="email" name="email" value="{{ ($fabricante['fabricante_contato']['cont_email']) ?? '' }}">
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    <header>
                        Endereço
                    </header>
                    <fieldset>
                        <div class="row">
                            <section class="col col-3">
                                <label class="label">Tipo Logradouro</label>
                                <label class="select">
                                    <?php \App\Utils\FormUtils::select($tipoLogradouro, 'tipo_logradouro', 'tipo_logradouro', 'form-control form-cascade-control', ($fabricante['fabricante_endereco']['enlo_id']) ?? null, false, array(), '-- Selecionar --', true, 'enlo_id', 'enlo_titulo'); ?>
                                    <i></i>
                                </label>
                            </section>

                            <section class="col col-3">
                                <label class="label">Logradouro</label>
                                <label class="input">
                                    <input type="text" id="rua" name="rua" placeholder="Nome do Tipo Logradouro" value="{{ $fabricante['fabricante_endereco']['ende_logradouro_titulo'] ?? '' }}" required>
                                </label>
                            </section>

                            <section class="col col-3">
                                <label class="label">Número</label>
                                <label class="input">
                                    <input type="text" id="numero" name="numero" placeholder="Número do Local" value="{{ ($fabricante['fabricante_endereco']['ende_numero']) ?? '' }}" required>
                                </label>
                            </section>

                            <section class="col col-3">
                                <label class="label">CEP</label>
                                <label class="input">
                                    <input type="text" id="cep" name="cep" placeholder="Código de Endereçamento Posta" value="{{ ($fabricante['fabricante_endereco']['ende_cep']) ?? '' }}" required>
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-3">
                                <label class="label">Bairro</label>
                                <label class="input">
                                    <input type="text" id="bairro" name="bairro" placeholder="Bairro" value="{{ ($fabricante['fabricante_endereco']['ende_bairro']) ?? '' }}">
                                </label>
                            </section>

                            <section class="col col-3">
                                <label class="label">Cidade</label>
                                <label class="input">
                                    <input type="text" id="cidade" name="cidade" placeholder="Cidade" value="{{ ($fabricante['fabricante_endereco']['ende_cidade']) ?? '' }}">
                                </label>
                            </section>

                            <section class="col col-3">
                                <label class="label">Estado</label>
                                <label class="input">
                                    <input type="text" id="estado" name="estado" placeholder="Estado" value="{{ ($fabricante['fabricante_endereco']['ende_estado']) ?? '' }}" required>
                                </label>
                            </section>

                            <section class="col col-3">
                                <label class="label">Complemento</label>
                                <label class="input">
                                    <input type="text" id="complemento" name="complemento" placeholder="Complemento do Endereço" value="{{ ($fabricante['fabricante_endereco']['ende_complemento']) ?? '' }}">
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                    <footer>
                        <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                        <button type="button" name="submit" class="btn btn-primary" data-title="Fabricante" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o" ></i> Salvar</button>
                    </footer>
                </form>
                </div>
            </div>
        </div>
    </article>
@endsection

@push('scripts')
    <script type="text/javascript">
        let PESSOA_FISICA   = "{{ \App\Utils\ClienteUtils::_P_FISICA }}";
        let PESSOA_JURIDICA = "{{ \App\Utils\ClienteUtils::_P_JURIDICA }}";
    </script>

    <script src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script src={{ asset('js/custom/jquery-search-cep.js') }}></script>
    <script src={{ asset('js/custom/jquery-fabricante.js') }}></script>
@endpush
