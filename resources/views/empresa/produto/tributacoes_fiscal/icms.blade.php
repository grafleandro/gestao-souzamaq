<?php
$aliquota = [
    [
        'nome' => 'Teste 1',
        'id' => 1
    ],
    [
        'nome' => 'Teste 2',
        'id' => 2
    ],
    [
        'nome' => 'Teste 3',
        'id' => 3
    ]
];
?>

<header>
    ICMS
</header>
<fieldset>
    <div class="row">
        <label class="label col col-2">CSOSN</label>
        <section class="col col-3">
            <label class="input">
                <input type="text" id="prod_csosn_estadual" name="prod_csosn_estadual" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="Estadual">
            </label>
        </section>
        <section class="col col-3">
            <label class="input">
                <input type="text" id="prod_csosn_interestadual" name="prod_csosn_interestadual" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="Interestadual">
            </label>
        </section>
    </div>
    <div class="row">
        <label class="label col col-2">Redução B.C.</label>
        <section class="col col-3">
            <label class="input">
                <input type="text" id="prod_redubc_estadual" name="prod_redubc_estadual" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="Estadual">
            </label>
        </section>
        <section class="col col-3">
            <label class="input">
                <input type="text" id="prod_redubc_interestadual" name="prod_redubc_interestadual" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="Interestadual">
            </label>
        </section>
    </div>
    <div class="row">
        <label class="label col col-2">Aliquota ECF</label>
        <section class="col col-3">
            <label class="input">
                <?php \App\Utils\FormUtils::select($aliquota, 'prod_ipi_venda', 'prod_ipi_venda', 'form-control form-cascade-control', ($empresa['empresa_endereco']['enlo_id']) ?? null, false, array(), '-- Selecionar Aliquota --', true, 'id', 'nome'); ?>
            </label>
        </section>
        <section class="col col-3">
            <label class="input">
                <input type="text" id="prod_redubc_interestadual" name="prod_redubc_interestadual" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="% Deferimento">
            </label>
        </section>
    </div>
    <div class="row">
        <label class="label col col-2">Regras de ICMS</label>
        <section class="col col-3">
            <label class="input">
                <input type="text" id="prod_redubc_estadual" name="prod_redubc_estadual" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="Estadual">
            </label>
        </section>
        <section class="col col-3">
            <label class="input">
                <input type="text" id="prod_redubc_interestadual" name="prod_redubc_interestadual" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="Interestadual">
            </label>
        </section>
    </div>
</fieldset>
