<?php
    $aliquota = [
        [
            'nome' => 'Teste 1',
            'id' => 1
        ],
        [
            'nome' => 'Teste 2',
            'id' => 2
        ],
        [
            'nome' => 'Teste 3',
            'id' => 3
        ]
    ];
?>

<header>
    PIS/CONFINS
</header>
<br>
<label class="label col col-2"><strong>PIS</strong></label>
<section class="col col-3">
    <label class="input">
        <?php \App\Utils\FormUtils::select($aliquota, 'prod_pis_aliquota_entrada', 'prod_pis_aliquota_entrada', 'form-control form-cascade-control', ($empresa['empresa_endereco']['enlo_id']) ?? null, false, array(), '-- Aliquota de Entrada --', true, 'id', 'nome'); ?>
    </label>
</section>
<section class="col col-2">
    <label class="input">
        <input type="text" id="prod_pis_cst_entrada" name="prod_pis_cst_entrada" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="CST Entrada">
    </label>
</section>
<section class="col col-3">
    <label class="input">
        <?php \App\Utils\FormUtils::select($aliquota, 'prod_pis_aliquota_saida', 'prod_pis_aliquota_saida', 'form-control form-cascade-control', ($empresa['empresa_endereco']['enlo_id']) ?? null, false, array(), '-- Aliquota de Saída --', true, 'id', 'nome'); ?>
    </label>
</section>
<section class="col col-2">
    <label class="input">
        <input type="text" id="prod_pis_cst_saida" name="prod_pis_cst_saida" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="CST Saída">
    </label>
</section>
<header>
    IPI
</header>
<br>
<label class="label col col-2"><strong>IPI</strong></label>
<section class="col col-3">
    <label class="input">
        <?php \App\Utils\FormUtils::select($aliquota, 'prod_ipi_venda', 'prod_ipi_venda', 'form-control form-cascade-control', ($empresa['empresa_endereco']['enlo_id']) ?? null, false, array(), '-- Venda --', true, 'id', 'nome'); ?>
    </label>
</section>
<section class="col col-2">
    <label class="input">
        <input type="text" id="prod_ipi_cst_entrada" name="prod_ipi_cst_entrada" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="CST Entrada">
    </label>
</section>
<section class="col col-2">
    <label class="input">
        <input type="text" id="prod_ipi_cst_entrada" name="prod_ipi_cst_entrada" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="CST Saída">
    </label>
</section>
<section class="col col-3">
    <label class="input">
        <input type="text" id="prod_ipi_cst_entrada" name="prod_ipi_cst_entrada" value="{{ $empresa['empr_insc_estadual'] ?? '' }}" placeholder="Ex. TIPI">
    </label>
</section>
