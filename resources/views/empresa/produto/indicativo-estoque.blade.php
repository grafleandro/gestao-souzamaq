<div class="pull-center">
    @if($estoqueAtual >= $estoqueMedia)
        <span class="badge bg-color-greenLight"><strong>{{ $estoqueAtual . ' ' . $unidadeMedida}} </strong><i class="fa fa-arrow-up"></i></span>
    @elseif($estoqueAtual < $estoqueMedia && $estoqueAtual > $estoqueMinimo)
        <span class="badge bg-color-orange"><strong>{{ $estoqueAtual . ' ' . $unidadeMedida}} - </strong></span>
    @elseif($estoqueAtual <= $estoqueMinimo)
        <span class="badge bg-color-red"><strong>{{ $estoqueAtual . ' ' . $unidadeMedida}} </strong><i class="fa fa-arrow-down"></i></span>
    @endif
</div>
