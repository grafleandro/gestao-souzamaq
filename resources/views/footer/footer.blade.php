<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="{{ url('vendor/smartadmin/js/plugin/pace/pace.min.js') }}"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="{{ url('vendor/smartadmin/js/libs/jquery-2.1.1.min.js') }}"><\/script>');
    }
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="{{ url('vendor/smartadmin/js/libs/jquery-ui-1.10.3.min.js') }}"><\/script>');
    }
</script>

<!-- IMPORTANT: APP CONFIG -->
<script src="{{ url('vendor/smartadmin/js/app.config.js') }}"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="{{ url('vendor/smartadmin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js') }}"></script>

<!-- BOOTSTRAP JS -->
<script src="{{ url('vendor/smartadmin/js/bootstrap/bootstrap.min.js') }}"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="{{ url('vendor/smartadmin/js/notification/SmartNotification.min.js') }}"></script>

<!-- JARVIS WIDGETS -->
<script src="{{ url('vendor/smartadmin/js/smartwidgets/jarvis.widget.min.js') }}"></script>

<!-- EASY PIE CHARTS -->
<script src="{{ url('vendor/smartadmin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>

<!-- SPARKLINES -->
<script src="{{ url('vendor/smartadmin/js/plugin/sparkline/jquery.sparkline.min.js') }}"></script>

<!-- JQUERY VALIDATE -->
<script src="{{ url('vendor/smartadmin/js/plugin/jquery-validate/jquery.validate.min.js') }}"></script>

<!-- JQUERY MASKED INPUT -->
<script src="{{ url('vendor/smartadmin/js/plugin/masked-input/jquery.maskedinput.min.js') }}"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="{{ url('vendor/smartadmin/js/plugin/select2/select2.min.js') }}"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="{{ url('vendor/smartadmin/js/plugin/bootstrap-slider/bootstrap-slider.min.js') }}"></script>

<!-- browser msie issue fix -->
<script src="{{ url('vendor/smartadmin/js/plugin/msie-fix/jquery.mb.browser.min.js') }}"></script>

<!-- FastClick: For mobile devices -->
<script src="{{ url('vendor/smartadmin/js/plugin/fastclick/fastclick.min.js') }}"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- Demo purpose only -->
{{--<script src="{{ url('vendor/smartadmin/js/demo.min.js') }}"></script>--}}

<!-- MAIN APP JS FILE -->
<script src="{{ url('vendor/smartadmin/js/app.min.js') }}"></script>

@if(config('coordmec.plugins.voice-command'))
<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="{{ url('vendor/smartadmin/js/speech/voicecommand.min.js') }}"></script>
@endif

@if(config('coordmec.plugins.smartchat'))
<!-- SmartChat UI : plugin -->
<script src="{{ url('vendor/smartadmin/js/smart-chat-ui/smart.chat.ui.min.js') }}"></script>
<script src="{{ url('vendor/smartadmin/js/smart-chat-ui/smart.chat.manager.min.js') }}"></script>
@endif

<!-- PAGE RELATED PLUGIN(S) -->

@if(config('coordmec.plugins.flot-chart-plugin'))
<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="{{ url('vendor/smartadmin/js/plugin/flot/jquery.flot.cust.min.js') }}"></script>
<script src="{{ url('vendor/smartadmin/js/plugin/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ url('vendor/smartadmin/js/plugin/flot/jquery.flot.time.min.js') }}"></script>
<script src="{{ url('vendor/smartadmin/js/plugin/flot/jquery.flot.tooltip.min.js') }}"></script>
@endif

@if(config('coordmec.plugins.jvectormap'))
<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="{{ url('vendor/smartadmin/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ url('vendor/smartadmin/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
@endif

@if(config('coordmec.plugins.fullcalendar'))
<!-- Full Calendar -->
<script src="{{ url('vendor/smartadmin/js/plugin/moment/moment.min.js') }}"></script>
<script src="{{ url('vendor/smartadmin/js/plugin/fullcalendar/jquery.fullcalendar.min.js') }}"></script>
@endif

<!-- PAGE RELATED PLUGIN(S) -->
<script type="text/javascript" src="{{ url('vendor/smartadmin/js/plugin/jquery-nestable/jquery.nestable.min.js') }}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

<!-- CUSTOM JS -->
<script type="text/javascript" src="{{ asset('js/vendor/adblock-detection.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom/loader.jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom/jquery-app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom/jquery-form.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vendor/jquery.idle.min.js') }}"></script>

<script type="text/javascript">

    $(document).ready(function() {
        pageSetUp();

        $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');

        $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');

            if (children.is(':visible')) {
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-square-o');
            } else {
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-check-square-o');
            }

            e.stopPropagation();
        });
    });


    // $(document).idle({
    //     onIdle: function(){
    //         alert('It\'s been a long time since you don\'t see me');
    //     },
    //     events: 'mouseover mouseout',
    //     idle: 5000
    // });

</script>

@yield('smart_js')

@stack('scripts')
