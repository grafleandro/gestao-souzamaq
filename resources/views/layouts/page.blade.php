<?php
use Illuminate\Support\Facades\Request;
?>

@extends('layouts.master')

@section('smart_css')

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="{{ url('vendor/smartadmin/img/splash/ipad-landscape.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="{{ url('vendor/smartadmin/img/splash/ipad-portrait.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="{{ url('vendor/smartadmin/img/splash/iphone.png') }}" media="screen and (max-device-width: 320px)">

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="{{ url('vendor/smartadmin/css/demo.min.css') }}">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="{{ url('vendor/smartadmin/img/favicon/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ url('vendor/smartadmin/img/favicon/favicon.ico') }}" type="image/x-icon">

    @yield('css')
@stop
{{--' desktop-detected menu-on-top pace-done'--}}


@section('body_class', (config('coordmec.layout') ? config('coordmec.layout')  : '') . ' fixed-header fixed-navigation')

@section('body')

    @include('layouts.partial.header')

    <aside id="left-panel">
        <!-- NAVIGATION : This navigation is also responsive-->
        <nav>

            <!-- Sidebar Menu -->
            <ul>
                @if($MenuFull)
                    @include('layouts.partial.menu-item', ['items' => $MenuFull->roots()])
                @else
                    @php
                        return redirect()->route('login');
                    @endphp
                @endif
            </ul>
        </nav>
        <!-- /.sidebar -->
    </aside>

    <!-- MAIN PANEL -->
    <div id="main" role="main">

        <!-- RIBBON -->
        <div id="ribbon">

				<span class="ribbon-button-alignment">
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Atenção: Esta ação resetar todas as configuções personalizadas!    " data-html="true">
						<i class="fa fa-refresh"></i>
					</span>
				</span>

            @yield('caminho-pagina')

            <!-- breadcrumb -->
            <ol class="breadcrumb">
                @if(Request::segments())
                    @foreach(Request::segments() as $index => $value)
                        @if(config("breadcrumb.{$value}"))
                            <li>{{ config("breadcrumb.{$value}") }}</li>
                        @else
                            <li>{{ ucwords(str_replace(['-','_'], ' ', $value)) }}</li>
                        @endif
                    @endforeach
                @endif
            </ol>
            <!-- end breadcrumb -->

            <!-- You can also add more buttons to the
            ribbon for further usability

            Example below:

            <span class="ribbon-button-alignment pull-right">
            <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
            <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
            <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
            </span> -->

        </div>
        <!-- END RIBBON -->



        <!-- MAIN CONTENT -->
        <div id="content">

            @yield('content')

        </div>
        <div class="loader hidden">
            <div class="loader-gif">
                <i class="fa fa-gear fa-5x fa-spin"></i><br>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
        @include('layouts.partial.modal')

    </div>
    <!-- END MAIN PANEL -->
@endsection
