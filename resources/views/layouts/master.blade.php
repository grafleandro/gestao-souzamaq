<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="OutPut Web - Soluções em Tecnologia ">
    <meta name="description" content="O Sistema Gestão Automotiva, foi desenvolvido para auxiliar tanto na Oficiana quanto na AutoPeça. Desenvolvimento com as melhores tecnologia e pronto para ser utilizar por você, nosso futuro cliente.">
    <meta name="keywords" content="Autopeças,auto peças,oficina,mecanica,nfe,nfce,fluxo de caixa,venda,ordem de servico,os,">
    <title>@yield('title_prefix', config('adminlte.title_prefix', ''))
        @yield('title', config('adminlte.title', 'CoordMec'))
        @yield('title_postfix', config('adminlte.title_postfix', ''))</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('icon/favicon-32x32.png') }}">

    @include('head.head')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
</head>
<body class="hold-transition @yield('body_class')">

@yield('body')

@include('footer.footer')

</body>
</html>
