@foreach($items as $item)
    <li {{ (isset($item->attributes['class'])) ? 'class="'. $item->attributes['class'] .'"' : '' }}>
        <a href="{{ ( $item->link->path['url'] ) ?? 'javascript:void(0);' }}" title="{{ ($item->title) ?? '' }}" <?php echo $item->attributes['parameter'] ?? '' ?>>
            @if(isset($item->attributes['icon']))
                <i class="fa fa-lg fa-fw fa-{{ $item->attributes['icon'] }}"></i>
            @endif
            <span class="menu-item-parent">{{ ($item->title) ?? ''}}</span>
        </a>
        @if($item->hasChildren())
            <ul>
                @include('layouts.partial.menu-item', ['items' => $item->children()])
            </ul>
        @endif
    </li>
@endforeach
