@extends('layouts.master')

<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('vendor/smartadmin/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('vendor/smartadmin/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('vendor/smartadmin/css/smartadmin-production.min.css') }}">
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('vendor/smartadmin/css/smartadmin-skins.min.css') }}">
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('vendor/smartadmin/css/smartadmin-rtl.min.css') }}">

<!DOCTYPE html>
<html lang="pt-br" id="extr-page">

<body class="animated fadeInDown">

{{--@include('header.header', ['type' => 'login'])--}}

<div id="main" role="main">

    <!-- MAIN CONTENT -->
    <div id="content" class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
                <img src="{{ url('vendor/smartadmin/img/logo.svg') }}" class="pull-left" >
                <div class="hero">

                    <div class="pull-left login-desc-box-l">
                        <h4 class="paragraph-header">{{trans('translate.login.descricao')}}</h4>
                    </div>

                    <img src="{{ url('vendor/smartadmin/img/demo/iphoneview.png') }}" class="pull-right display-image" alt="" style="width:210px">

                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h5 class="about-heading">{{trans('translate.login.sobre')}}</h5>
                        <p>
                            {{trans('translate.login.descricao_sobre')}}
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h5 class="about-heading">{{trans('translate.login.comodidade')}}</h5>
                        <p>
                            {{trans('translate.login.descricao_comodidade')}}
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                <div class="well no-padding">
                    <form role="form" method="post" action="{{ route('login') }}" id="login-form" class="smart-form client-form">
                        {!! csrf_field() !!}
                        <header>{{trans('translate.login.login')}}</header>

                        <fieldset>
                            <section>
                                <label class="label">E-mail</label>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input type="email" name="email">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> {{trans("translate.login.email_mensagem")}}</b></label>
                            </section>

                            <section>
                                <label class="label">{{trans('translate.login.senha')}}</label>
                                <label class="input"> <i class="icon-append fa fa-lock"></i>
                                    <input type="password" name="password">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> {{trans("translate.login.senha_mensagem")}}</b> </label>
                                <div class="note">
                                    <a href="forgotpassword.html">{{trans('translate.login.esqueceu')}}</a>
                                </div>
                            </section>

                            {{--<section>--}}
                                {{--<label class="checkbox">--}}
                                    {{--<input type="checkbox" name="remember" checked="">--}}
                                    {{--<i></i>{{trans("translate.login.lembrar")}}</label>--}}
                            {{--</section>--}}
                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary">
                                {{trans('translate.login.acessar')}}
                            </button>
                        </footer>
                    </form>

                </div>

            </div>
        </div>
    </div>

</div>

@include('footer.footer')

</body>
</html>
