<!-- #MOBILE -->
<!-- Top menu profile link : this shows only when top menu is active -->
<ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
    <li class="">
        <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
            @if(empty(Auth::user()->colaborador->cola_foto))
                <img src="{{ url('vendor/smartadmin/img/avatars/sunny.png') }}" alt="John Doe" class="online" />
            @else
                <img src="{{ asset('storage/perfil/' . Auth::user()->colaborador->cola_foto) }}" alt="{{ Auth::user()->name }}" class="online" />
            @endif
        </a>
        <ul class="dropdown-menu pull-right">
            {{--<li>--}}
                {{--<a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Configuração</a>--}}
            {{--</li>--}}
            <li class="divider"></li>
            <li>
                <a href="{{url('/empresa/colaborador/perfil')}}" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> Perfil</a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="#" class="padding-10 padding-top-5 padding-bottom-5" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-lg"></i> <strong>Sair</strong></a>
                <form id="logout-form" action="{{ route('logout')}}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </li>
</ul>
