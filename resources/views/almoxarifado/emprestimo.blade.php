<form class="smart-form padding-10" id="form-almox-emprestimo" method="get" action="{{url('/almoxarifado/create')}}">
    <div class="row">
        <section class="col-md-6">
            <label class="label">Destino</label>
            <div class="inline-group">
                <label class="radio">
                    <input type="radio" value="{{\App\Utils\SituacaoUtils::DEST_INTERNO}}" id="almo_empre_destino" name="almo_empre_destino" checked="checked" {{(isset($almoxarifad['almo_id'])) ? 'disabled="disabled"' : ''}} onclick="jQueryAlmoxarifado.emprestimoDestino($(this))">
                    <i></i>Interno
                </label>
                <label class="radio">
                    <input type="radio" value="{{\App\Utils\SituacaoUtils::DEST_EXTERNO}}" id="almo_empre_destino"  name="almo_empre_destino"  {{(isset($almoxarifad['almo_id'])) ? 'disabled="disabled"' : ''}} onclick="jQueryAlmoxarifado.emprestimoDestino($(this))">
                    <i></i>Externo
                </label>
            </div>
        </section>

        <section class="col-md-6">
            <label class="label">Quantidade</label>
            <label class="input">
                <input type="text" id="almo_empre_quantidade" name="almo_empre_quantidade" value="1" data-item="{{$item}}" onblur="jQueryAlmoxarifado.emprestimoQtd($(this))">
            </label>
        </section>
    </div>

    <div class="row">
        <section class="col-md-12 hidden">
            <label class="label">Nome do Responsável</label>
            <label class="input">
                <input type="text" id="almo_empre_responsavel" name="almo_empre_responsavel" placeholder="Nome, proprietário do telefone XXXXX-XXXX, funcionario da empresa">
            </label>
        </section>

        <section class="col-md-12">
            <label class="label">Colaborador</label>
            <label class="input">
                {{\App\Utils\FormUtils::select($mecanico, 'almo_empre_colaborador', 'almo_empre_colaborador', 'form-control form-cascade-control', null, false, array(), '-- Selecionar --', true, 'cola_id', 'cola_nome')}}
            </label>
        </section>
    </div>
    <input type="hidden" id="almo_id" name="almo_id" value="{{$almoxarifado}}">
    <input type="hidden" id="almo_form_tipo" name="almo_form_tipo" value="{{\App\Utils\SituacaoUtils::ALMOX_FORM_EMPRESTIMO}}">
</form>

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/custom/jquery-almoxarifado.js') }}"></script>
@endpush
