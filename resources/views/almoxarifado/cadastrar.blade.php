@extends('layouts.page')

@push('css')
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Almoxarifado - Cadastrar Item', 'action' => 'almoxarifado.action.header-menu-btn-list'])
        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">
                    @if(isset($almoxarifado['almo_id']))
                        <form role="form" id="form_almoxarifado" method="put" action="{{url('/almoxarifado').'/'.$almoxarifado['almo_id']}}" class="smart-form">
                    @else
                        <form role="form" id="form_almoxarifado" method="post" action="{{url('/almoxarifado')}}" class="smart-form">
                    @endif
                        <fieldset>
                            <legend>Item</legend>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Código de Barra</label>
                                    <label class="input">
                                        <input type="text" id="almo_cod_barra" name="almo_cod_barra" value="{{ $almoxarifado['almo_cod_barra'] ?? '' }}">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Item</label>
                                    <label class="input">
                                        <input type="text" id="almo_titulo" name="almo_titulo" value="{{ $almoxarifado['almo_titulo'] ?? '' }}"  required>
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Valor Item</label>
                                    <label class="input">
                                        <input type="text" id="almo_valor" name="almo_valor" value="{{(isset($almoxarifado['almo_valor'])) ? \App\Utils\Mask::dinheiro($almoxarifado['almo_valor']) : 0}}" class="mask-money" onblur="jQueryAlmoxarifado.calcularTotal($(this))">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Grupo</label>
                                    <label class="input">
                                        <input type="text" id="almo_grupo" name="almo_grupo" value="{{ $almoxarifado['grupo']['grup_titulo'] ?? '' }}"  onfocus="jQueryGrupo.autocomplete($(this))">
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Informações</legend>
                            <div class="row">
                                <section class="col col-4">
                                    <label class="label">Unidade de Medida</label>
                                    <label class="input">
                                        {{\App\Utils\FormUtils::select($unid_medida, 'almo_unid_venda', 'almo_unid_venda', 'form-control form-cascade-control', ($almoxarifado['unme_id']) ?? null, false, array(), '-- Selecionar --', true, 'unme_id', 'unme_titulo')}}
                                    </label>
                                </section>

                                <section class="col col-4">
                                    <label class="label">Quantidade</label>
                                    <label class="input">
                                        <input type="text" id="almo_qtd" name="almo_qtd" value="{{ $almoxarifado['almo_qtd'] ?? '1' }}" onblur="jQueryAlmoxarifado.calcularTotal($(this))">
                                    </label>
                                </section>

                                <section class="col col-4">
                                    <label class="label">Valor Total</label>
                                    <label class="input">
                                        @if(!empty($almoxarifado['almo_valor']) && !empty($almoxarifado['almo_qtd']))
                                            <input type="text" id="almo_valor_total" name="almo_valor_total" value="{{\App\Utils\Mask::dinheiro($almoxarifado['almo_valor']*$almoxarifado['almo_qtd'])}}" class="mask-money" readonly>
                                        @else
                                            <input type="text" id="almo_valor_total" name="almo_valor_total" class="mask-money" readonly>
                                        @endif
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <section class="col col-4">
                                    <label class="label">Finalidade</label>
                                    <div class="inline-group">
                                        @if(isset($almoxarifado['almo_finalidade']) && $almoxarifado['almo_finalidade'] == \App\Utils\SituacaoUtils::FINA_EMPRESTIMO)
                                            <label class="radio" {{(isset($almoxarifad['almo_id'])) ? 'class="state-disabled"' : ''}}>
                                                <input type="radio" value="{{\App\Utils\SituacaoUtils::FINA_CONSUMO}}" id="almo_finalidade" name="almo_finalidade" onclick="jQueryAlmoxarifado.finalidade($(this))">
                                                <i></i>Consumo
                                            </label>
                                            <label class="radio" {{(isset($almoxarifad['almo_id'])) ? 'class="state-disabled"' : ''}}>
                                                <input type="radio" value="{{\App\Utils\SituacaoUtils::FINA_EMPRESTIMO}}" id="almo_finalidade"  name="almo_finalidade" checked="checked" onclick="jQueryAlmoxarifado.finalidade($(this))">
                                                <i></i>Empréstimo
                                            </label>
                                        @elseif(isset($almoxarifado['almo_finalidade']) && $almoxarifado['almo_finalidade'] == \App\Utils\SituacaoUtils::FINA_CONSUMO)
                                            <label class="radio" {{(isset($almoxarifad['almo_id'])) ? 'class="state-disabled"' : ''}}>
                                                <input type="radio" value="{{\App\Utils\SituacaoUtils::FINA_CONSUMO}}" id="almo_finalidade" name="almo_finalidade" checked="checked" onclick="jQueryAlmoxarifado.finalidade($(this))">
                                                <i></i>Consumo
                                            </label>
                                            <label class="radio" {{(isset($almoxarifad['almo_id'])) ? 'class="state-disabled"' : ''}}>
                                                <input type="radio" value="{{\App\Utils\SituacaoUtils::FINA_EMPRESTIMO}}" id="almo_finalidade"  name="almo_finalidade" onclick="jQueryAlmoxarifado.finalidade($(this))">
                                                <i></i>Empréstimo
                                            </label>
                                        @else
                                            <label class="radio" {{(isset($almoxarifad['almo_id'])) ? 'class="state-disabled"' : ''}}>
                                                <input type="radio" value="{{\App\Utils\SituacaoUtils::FINA_CONSUMO}}" id="almo_finalidade" name="almo_finalidade" onclick="jQueryAlmoxarifado.finalidade($(this))">
                                                <i></i>Consumo
                                            </label>
                                            <label class="radio" {{(isset($almoxarifad['almo_id'])) ? 'class="state-disabled"' : ''}}>
                                                <input type="radio" value="{{\App\Utils\SituacaoUtils::FINA_EMPRESTIMO}}" id="almo_finalidade"  name="almo_finalidade" checked="checked" onclick="jQueryAlmoxarifado.finalidade($(this))">
                                                <i></i>Empréstimo
                                            </label>
                                        @endif
                                    </div>
                                </section>

                                <div id="div-fina-consumo" class="{{(isset($almoxarifado['almo_finalidade']) && $almoxarifado['almo_finalidade'] == \App\Utils\SituacaoUtils::FINA_CONSUMO) ? '' : 'hidden'}}">
                                    <section class="col col-4">
                                        <label class="label">Data de Vecimento</label>
                                        <label class='input-group date'>
                                            <input type='text' name='almo_dt_venc' id='almo_dt_venc' class="form-control input-datepicker" value="{{(isset($almoxarifado['almo_finalidade']) && !empty($almoxarifado['almo_dt_vencimento'])) ? \Carbon\Carbon::parse( $almoxarifado['almo_dt_vencimento'])->format('d/m/Y') : '' }}" />
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </label>
                                    </section>
                                </div>

                                <div id="div-fina-emprestimo"  class="{{(isset($almoxarifado['almo_finalidade']) && $almoxarifado['almo_finalidade'] == \App\Utils\SituacaoUtils::FINA_EMPRESTIMO) ? '' : 'hidden'}}">
                                    <section class="col col-4">
                                        <label class="label">Multa <small>(caso não entregue no dia)</small></label>
                                        <label class="input">
                                            <input type="text" id="almo_multa" name="almo_multa" value="{{(isset($almoxarifado['almo_multa'])) ? \App\Utils\Mask::dinheiro($almoxarifado['almo_multa']) : 0}}" class="mask-money" onfocus="jQueryAlmoxarifado.multa($(this))">
                                        </label>
                                    </section>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Localização</legend>
                            <div class="row">
                                <section class="col col-4">
                                    <label class="label">Sessão</label>
                                    <label class="input">
                                        <input type="text" id="almo_sessao" name="almo_sessao" value="{{ $almoxarifado['almo_sessao'] ?? '' }}">
                                    </label>
                                </section>

                                <section class="col col-4">
                                    <label class="label">Categoria</label>
                                    <label class="input">
                                        <input type="text" id="almo_categoria" name="almo_categoria" value="{{ $almoxarifado['almo_categoria'] ?? '' }}">
                                    </label>
                                </section>

                                <section class="col col-4">
                                    <label class="label">Sub-categoria</label>
                                    <label class="input">
                                        <input type="text" id="almo_subcategoria" name="almo_subcategoria" value="{{ $almoxarifado['almo_subcategoria'] ?? '' }}">
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <input type="hidden" id="grup_id" name="grup_id" value="{{$almoxarifado['grupo']['grup_id'] ?? 0}}">
                        <footer>
                            <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                            <button type="button" name="submit" class="btn btn-primary" data-title="Almoxarifado" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </article>
@endsection

@push('scripts')
    <script type="text/javascript">
        let FINA_CONSUMO    = "{{\App\Utils\SituacaoUtils::FINA_CONSUMO}}";
        let FINA_EMPRESTIMO = "{{\App\Utils\SituacaoUtils::FINA_EMPRESTIMO}}";
    </script>

    <script type="text/javascript" src={{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}></script>
    <script type="text/javascript" src={{ asset('bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-calendario.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.accounting.min.js') }}></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-almoxarifado.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-colaborador.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-grupo.js') }}"></script>
@endpush
