<div class="alert alert-info fade in margin-top-10">
    <i class="fa-fw fa fa-warning"></i>
    <strong>COLUNA AÇÃO:</strong> A coluna <strong>AÇÃO</strong>, contém todas as funcionalidades necessárias para a manipulação dos dados de um determinado item.
    <hr>
    <p><i class="fa fa-edit"></i> - Este botão serve para editar as informações de um determinado elemento.</p>
    <p><i class="fa fa-dropbox"></i> - Dar baixa em um item quando este for apenas para Consumo Interno.</p>
    <p><i class="fa fa-trash-o"></i> - Excluir um determinado Item.</p>
    <p><i class="fa fa-male"></i> - Realizar Impréstimo a um determinado Colaborador ou Parceiro.</p>
    <p><i class="fa fa-eye"></i> - Visualizar informações referente ao item selecionado,  e posteriormente, podendo dar baixa caso necessite.</p>
</div>

