@extends('layouts.page')

@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.css') }}">
@endpush

@section('content')
<!-- NEW COL START -->
<article class="col col-md-12">
    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false">
        @include('header.header-content', ['title' => 'Almoxarifado', 'action' => ['almoxarifado.action.header-menu-btn', 'almoxarifado.action.header-menu-btn-filter', 'almoxarifado.action.header-menu-btn-info']])

        <!-- widget div-->
        <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->

            <!-- widget content -->
            <div class="widget-body">

                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        @include('almoxarifado.filtro')

                        <div class="box-body table-almoxarifado"></div>
                    </div>
                </div>
            </div>
            <!-- end widget content -->

        </div>
        <!-- end widget div -->
    </div>
    <!-- end widget -->
</article>
@stop

@push('scripts')
    <script type="text/javascript">
        let DEST_INTERNO = "{{\App\Utils\SituacaoUtils::DEST_INTERNO}}";
        let DEST_EXTERNO = "{{\App\Utils\SituacaoUtils::DEST_EXTERNO}}";
    </script>

    <script type="text/javascript" src={{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}></script>
    <script type="text/javascript" src={{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-almoxarifado.js') }}"></script>
@endpush
