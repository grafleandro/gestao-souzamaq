@foreach($almoxarifado['emprestimo'] as $index => $almox)
    <div class="detalhe-item">
        @if($index > 0)
            <hr>
        @endif
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <section class="col-md-4">
                        <label><strong>Quantidade de Item</strong>:</label>
                    </section>
                    <section class="col-md-8">
                        {{$almox['alem_qtd']}}
                    </section>
                </div>
                <div class="row">
                    <section class="col-md-4">
                        <label><strong>Data da Retirada</strong>:</label>
                    </section>
                    <section class="col-md-8">
                        {{ \Carbon\Carbon::parse($almox['alem_dt_ocorrencia'])->format('d/m/Y')}}
                    </section>
                </div>
                <div class="row">
                    <section class="col-md-4">
                        <label><strong>Colaborador</strong>:</label>
                    </section>
                    <section class="col-md-8">
                        {{--{{dd($almox['alem_responsavel'])}}--}}
                        {{(isset($almox['colaborador'])) ? $almox['colaborador']['cola_nome'] : $almox['alem_responsavel']}}
                    </section>
                </div>
                {{--<div class="row">--}}
                    {{--<section class="col-md-4">--}}
                        {{--<label><strong>Observação</strong>:</label>--}}
                    {{--</section>--}}
                    {{--<section class="col-md-8">--}}
                        {{--Vamos testar essa parada--}}
                    {{--</section>--}}
                {{--</div>--}}
                <div class="row">
                    <section class="col-md-4">
                        <label><strong>Origem</strong>:</label>
                    </section>
                    <section class="col-md-8">
                        @if($almox['alem_destino'] == \App\Utils\SituacaoUtils::DEST_EXTERNO)
                            Externo
                        @elseif($almox['alem_destino'] == \App\Utils\SituacaoUtils::DEST_INTERNO)
                            Interno
                        @endif
                    </section>
                </div>
            </div>
            <div class="col-md-4 margin-top-34">
                <button type="button" class="btn btn-primary pull-right" data-emprestimo="{{$almox['alem_id']}}" onclick="jQueryAlmoxarifado.emprestimoDarBaixa($(this))"><i class="fa fa-check"></i> Dar Baixa</button>
            </div>
        </div>
    </div>
@endforeach
