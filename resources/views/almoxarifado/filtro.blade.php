<form class="smart-form" id="form-almox-filtro" style="display: none">
    <fieldset>
        <legend>Busca Avaçada</legend>
        <div class="row">
            <section class="col col-4">
                <label class="label">Sessão</label>
                <label class="input">
                    <input type="text" id="filtro_sessao" name="filtro_sessao">
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Categoria</label>
                <label class="input">
                    <input type="text" id="filtro_categoria" name="filtro_categoria">
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Sub-Categoria</label>
                <label class="input">
                    <input type="text" id="filtro_sub_categoria" name="filtro_sub_categoria">
                </label>
            </section>
        </div>
        <div class="row">
            <section class="col col-4">
                <label class="label">Colaborador</label>
                <label class="input">
                    <input type="text" id="filtro_colaborador" name="filtro_colaborador" onfocus="jQueryColaborador.autocomplete($(this))">
                </label>
            </section>

            <section class="col col-4">
                <label class="label">Finalidade</label>
                <div class="inline-group">
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::FINA_CONSUMO}}" id="filtro_finalidade" name="filtro_finalidade">
                        <i></i>Consumo
                    </label>
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::FINA_EMPRESTIMO}}" id="filtro_finalidade"  name="filtro_finalidade">
                        <i></i>Empréstimo
                    </label>
                    <label class="radio">
                        <input type="radio" value="3" id="filtro_finalidade"  name="filtro_finalidade" checked="checked">
                        <i></i>Todos
                    </label>
                </div>
            </section>

            <section class="col col-4">
                <label class="label">Ferramentas</label>
                <div class="inline-group">
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::FILTRO_FERRAMENTA}}" id="filtro_ferramenta" name="filtro_ferramenta">
                        <i></i>Emprestadas
                    </label>
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::FILTRO_N_FERRAMENTA}}" id="filtro_ferramenta"  name="filtro_ferramenta">
                        <i></i>Não Empresatadas
                    </label>
                    <label class="radio">
                        <input type="radio" value="{{\App\Utils\SituacaoUtils::FILTRO_FERRAMENTA_TODAS}}" id="filtro_ferramenta"  name="filtro_ferramenta" checked="checked">
                        <i></i>Todas
                    </label>
                </div>
            </section>
        </div>
    </fieldset>

    <input type="hidden" id="id_colaborador" name="filtro_colaborador_id">
    <footer>
        <button type="button" name="submit" class="btn btn-success" onclick="jQueryAlmoxarifado.filtroListar($(this))"><i class="fa fa-filter"></i> Filtrar</button>
        <button type="button" name="submit" class="btn btn-default" onclick="jQueryAlmoxarifado.imprimirRelatorio($(this))"><i class="fa fa-print"></i> Imprimir</button>
    </footer>
</form>

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/custom/jquery-colaborador.js') }}"></script>
@endpush
