<form role="form" class="smart-form">
    <div class="row">
        <div class="col col-6">
            <fieldset>
                <legend>Formas de Pagamento</legend>

                <label class="label">Selecionar</label>
                <label class="input">
                    <select class="form-control" id="venda-form-pgto" name="venda-form-pgto" onchange="jQueryVendaFinalizar.selecionarFormaPgto($(this))">
                        <option value="">Selecione</option>
                        @foreach($forma_pgto as $index => $value)
                            <option data-parcelas="{{ $value['cofp_parcelas'] }}" data-troco="{{ $value['cofp_troco'] }}" value="{{ $value['cofp_id'] }}">{{ $value['cofp_titulo'] }}</option>
                        @endforeach
                    </select>
                </label>

                <div class="col col-12 no-padding-left margin-top-10 hidden" id="venda-parcelas">
                </div>

                <div class="col col-8 no-padding-left margin-top-10">
                    <label class="label">TOTAL À PAGAR</label>
                    <label class="input">
                        <input class="input-lg mask-money" type="text" id="venda-total-pagar" name="venda-total-pagar" placeholder="R$ 0,00">
                    </label>
                </div>

                <div class="col col-4 margin-top-34 no-padding-right">
                    <button type="button" class="btn btn-default btn-lg" id="venda-btn-confirmar" disabled="disabled" onclick="jQueryVendaFinalizar.calcularPgto($(this))"><i class="fa fa-check"></i> Confirmar</button>
                </div>

            </fieldset>

            <fieldset>
                <legend>Produtos da Venda</legend>
                <div class="table-responsive" style="height: 150px">
                    <table class="table table-hover" id="table-vend-finalizar-item">
                        <thead>
                        <tr>
                            <th>Qtd.</th>
                            <th style="width: 200px">Produto</th>
                            <th>Desc.</th>
                            <th class="pull-center">Valor</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <div class="col col-6">
            <fieldset>
                <legend>Descontos</legend>
                <section class="col col-6">
                    <label class="label">Porcentagem</label>
                    <label class="input">
                        <input class="mask-percent" type="text" id="venda-desconto-porc" name="venda-desconto-porc" onblur="jQueryVendaFinalizar.calcularDescPorcet($(this))" placeholder="%">
                    </label>
                </section>
                <section class="col col-6">
                    <label class="label">Monetário</label>
                    <label class="input">
                        <input class="mask-money" type="text" id="venda-desconto-din" name="venda-desconto-din" onblur="jQueryVendaFinalizar.calcularDescDinheiro($(this))" placeholder="R$">
                    </label>
                </section>
            </fieldset>

            <fieldset class="hidden">
                <legend>Pagamentos</legend>
                <div id="venda-pagamento">

                </div>
            </fieldset>

            <fieldset>
                <legend>Total</legend>
                <div class="row no-margin-left sale-panel-details">
                    <div class="col-md-6">
                        <span><strong>SUBTOTAL</strong></span>
                    </div>
                    <div class="col-md-6 pull-center">
                        <span id="venda-fina-subtotal">R$ 0,00</span>
                    </div>
                </div>
                <div class="row no-margin-left sale-panel-details">
                    <div class="col-md-6">
                        <span><strong>DESCONTOS</strong></span>
                    </div>
                    <div class="col-md-6 pull-center">
                        <span id="venda-fina-desconto">R$ 0,00</span>
                    </div>
                </div>
                <div class="row no-margin-left sale-panel-details">
                    <div class="col-md-6">
                        <span><strong>PAGAMENTOS</strong></span>
                    </div>
                    <div class="col-md-6 pull-center">
                        <span id="venda-fina-pagamentos">R$ 0,00</span>
                    </div>
                </div>
                <div class="row no-margin-left sale-panel-details">
                    <div class="col-md-6">
                        <span><strong>TROCO</strong></span>
                    </div>
                    <div class="col-md-6 pull-center">
                        <span id="venda-fina-troco">R$ 0,00</span>
                    </div>
                </div>
                <div class="row no-margin-left sale-panel-details">
                    <div class="col-md-6">
                        <span><strong>TOTAL A PAGAR</strong></span>
                    </div>
                    <div class="col-md-6 pull-center">
                        <span id="venda-fina-total">R$ 0,00</span>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <input type="hidden" id="venda-parcelas-qtd" name="venda-parcelas-qtd" value="">

    <footer class="pull-right no-margin">
        <button type="button" name="submit" class="btn btn-default btn-md" data-dismiss="modal" id="venda-btn-cancelar"><i class="fa fa-times"></i> Cancelar</button>
        <button type="button" name="submit" class="btn btn-success btn-md" id="venda-btn-finalizar" onclick="jQueryVendaFinalizar.finalizarVenda($(this))"><i class="fa fa-check"></i> Finalizar</button>
    </footer>
</form>

<div class="col-md-12 sale-shortcut-key margin-top-10">
    <strong>F2 - Buscar Produto | F3 - Definir Quantidade | F4 - Definir Desconto | F6 - Adicionar Item | F7 - Aguardar | F8 - Cancelar | F9 - Finalizar</strong>
</div>

<script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
<script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
<script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
<script type="text/javascript" src="{{ asset('js/custom/jquery-app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom/jquery-form.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom/jquery.key.js') }}"></script>
