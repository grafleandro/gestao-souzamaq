@extends('layouts.page')

@push('css')
    <!-- CSS -->
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Venda - Balcão'])
        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">
                    <div class="row">
                        <div class="col-md-2">
                            <img src="{{ asset('img/icon/box-product.svg') }}" alt="Imagem do Produto" title="Imagem do Produto" class="superbox-img sale-product">
                        </div>
                        <div class="col-md-6">
                            <form role="form" class="form-horizontal" id="form_produto" method="post" action="{{url('/empresa/produto')}}">
                                <fieldset>
                                    <div class="form-group">
                                        {{--<label class="control-label col-md-3">Buscar Produto...</label>--}}
                                        <div class="col-md-12">
                                            <input style="text-align: center;" class="form-control input-lg" type="text" id="vend_buscar" name="vend_buscar" value="" placeholder="SELECIONAR PRODUTO..." onfocus="jQueryVenda.autocompleteProduto($(this))">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">CÓDIGO</label>
                                        <div class="col-md-9">
                                            <input class="form-control sale-input-rigth input-lg" type="text" id="vend_prod_codigo" name="vend_prod_codigo" value="" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">QUANTIDADE</label>
                                        <div class="col-md-9">
                                            <input class="form-control sale-input-rigth mask-decimal input-lg" maxlength="6" type="text" id="vend_qtd" name="vend_qtd" value="" readonly onblur="jQueryVenda.calcularSubTotalQtd($(this))">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">VALOR UNITÁRIO</label>
                                        <div class="col-md-9">
                                            <input class="form-control sale-input-rigth mask-money input-lg" type="text" id="vend_valor_unit" name="vend_valor_unit" value="" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">DESCONTO</label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <input class="form-control sale-input-rigth input-lg" type="text" id="vend_desconto" name="vend_desconto" value="" readonly onblur="jQueryVenda.calcularSubTotalDesc($(this))">

                                                        <div class="input-group-btn">
                                                            <button type="button" class="btn btn-default dropdown-toggle btn-lg" data-toggle="dropdown" tabindex="-1">
                                                                <strong id="vend-tipo-desconto" data-tipo="R$">R$</strong> <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu pull-right" role="menu">
                                                                <li><a data-desconto="R$" href="javascript:void(0);" onclick="jQueryVenda.mudarTipoDesconto($(this))">R$</a></li>
                                                                <li><a data-desconto="%" href="javascript:void(0);" onclick="jQueryVenda.mudarTipoDesconto($(this))">%</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">TOTAL</label>
                                        <div class="col-md-9">
                                            <input class="form-control sale-input-rigth mask-money input-lg" type="text" id="vend_total" name="vend_total" value="" readonly>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-9">
                                            <button type="button" class="btn btn-primary btn-md col-md-12 col-sm-12" id="btn-vend-add" onclick="jQueryVenda.vendaAdicionar($(this))"><i class="fa fa-shopping-cart"></i> ADICIONAR PRODUTO</button>
                                            <button type="button" class="btn btn-success btn-md col-md-5 hidden" id="btn-vend-atualizar" onclick="jQueryVenda.atualizarProdutoTabela($(this))"><i class="fa fa-refresh"></i> ATUALIZAR PRODUTO</button>
                                            <button type="button" class="btn btn-danger btn-md col-md-5 col-md-offset-2 hidden" id="btn-vend-excluir" onclick="jQueryVenda.excluirProdutoTabela($(this))"><i class="fa fa-trash"></i> EXCLUIR PRODUTO</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <div class="alert alert-success no-margin fade in" style="display: grid;">
                                <span><strong>VENDEDOR</strong>: {{ \Illuminate\Support\Facades\Auth::user()->name }}</span>
                                <span><strong>CLIENTE</strong>: <a class="sale-txt-client" href="#" title="Trocar de Cliente">{{$clienteConsumidor['clie_nome_razao_social']}}</a></span>
                            </div>
                            <div class="table-responsive" style="height: 211px">
                                <table class="table table-hover" id="table-vend-item">
                                    <thead>
                                        <tr>
                                            <th>Qtd.</th>
                                            <th style="width: 180px">Produto</th>
                                            <th>Desc.</th>
                                            <th class="pull-center">Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12" id="vend-total-balcao">
                                    <label>TOTAL DA COMPRA</label>
                                    <p class="pull-center sale-total-price">R$ <value>00,00</value></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-warning btn-md col-md-5 col-sm-12"><i class="fa fa-hourglass"></i> AGUARDAR</button>
                                    <button type="button" class="btn btn-danger btn-md col-md-5 col-sm-12 col-md-offset-2" onclick="jQueryVenda.cancelarVenda($(this))"><i class="fa fa-times"></i> CANCELAR</button>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5px ">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-primary btn-md col-md-12" onclick="jQueryVenda.finalizarVenda($(this))"><i class="fa fa-check"></i> FINALIZAR</button>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="clie_consumidor" name="clie_consumidor" value="{{$clienteConsumidor['clie_id']}}">
                    </div>
                    <div class="row">
                        <section class="col col-12 sale-shortcut-key">
                            <strong>F2 - Buscar Produto | F3 - Definir Quantidade | F4 - Definir Desconto | F6 - Adicionar Item | F7 - Aguardar | F8 - Cancelar | F9 - Finalizar</strong>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </article>
@endsection

@push('scripts')
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.accounting.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.key.js') }}></script>
    <script type="text/javascript" src={{ asset('js/vendor/modernizr-custom.js') }}></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-venda-finalizar.js') }}"></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-venda-balcao.js') }}></script>
@endpush
