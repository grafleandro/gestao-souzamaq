@extends('layouts.page')

@push('css')
    <!-- CSS -->
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Venda'])
        <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">
                    <!-- /.box-header -->
                    <div class="box-body table-venda-listar w-100">

                    </div>
                </div>
            </div>
        </div>
    </article>
@endsection

@push('scripts')
    <script>
        let VEND_STATUS = {
            VEND_ANDAMENTO: '{{ \App\Utils\VendaUtils::VEND_ANDAMENTO }}',
            VEND_AGUARDANDO: '{{ \App\Utils\VendaUtils::VEND_AGUARDANDO }}',
            VEND_FINALIZADA: '{{ \App\Utils\VendaUtils::VEND_FINALIZADA }}',
            VEND_CANCELADA: '{{ \App\Utils\VendaUtils::VEND_CANCELADA }}',
            VEND_DEVOLUCAO: '{{ \App\Utils\VendaUtils::VEND_DEVOLUCAO }}',
            VEND_ATRASADA: '{{ \App\Utils\VendaUtils::VEND_ATRASADA }}',
        }
    </script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-venda.js') }}"></script>
@endpush
