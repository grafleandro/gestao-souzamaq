<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<header id="header">
    <div id="logo-group">
        <span id="logo">
            <img src="{{ url('vendor/smartadmin/img/logo.png') }}" alt="SmartAdmin">
        </span>
    </div>

    @if($type === 'register')
        <span id="extr-page-header-space">
            <span class="hidden-mobile hidden-xs">Already registered?</span>
            <a href="{{ route('login') }}" class="btn btn-danger">Sign In</a>
        </span>
    @elseif($type === 'login')
        <span id="extr-page-header-space">
            <span class="hidden-mobile hidden-xs">Need an account?</span>
            <a href="{{ route('register') }}" class="btn btn-danger">Create account</a>
        </span>
    @endif
</header>
