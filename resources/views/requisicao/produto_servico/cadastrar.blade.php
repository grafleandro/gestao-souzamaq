@extends('layouts.page')

@section('content')
    <!-- NEW COL START -->
    <article class="col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

        @include('header.header-content', ['title' => 'Requisição de Produtos ou Serviços'])
        <!-- widget div-->
            <div>
                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body no-padding">

                    <form id="smart-form-register" class="smart-form">
                        <fieldset>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Tipo</label>
                                    <label class="select">
                                        <select name="gender">
                                            <option value="1">Produto</option>
                                            <option value="4">Serviço</option>
                                        </select>
                                        <i></i>
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Selecionar Veículo</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="Marca, Modelo ou Placa">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Quantidade</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="Informa apenas números">
                                    </label>
                                </section>

                                <section class="col col-3">
                                    <label class="label">Valor</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="R$ 00,00">
                                    </label>
                                </section>
                            </div>

                            <div class="row">
                                <section class="col col-md-12">
                                    <label class="label">Descrição</label>
                                    <label class="input">
                                        <input type="text" name="lastname" placeholder="Informe uma breve descrição">
                                    </label>
                                </section>
                            </div>
                        </fieldset>

                        <footer>
                            <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                            <button type="button" name="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Salvar</button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </article>
@endsection
