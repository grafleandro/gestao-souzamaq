<!-- NEW COL START -->
<article class="col col-md-6 col-sm-12">
    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false" data-widget-custombutton="false" role="widget">

    @include('header.header-content', ['title' => 'Estoque - Cadastrar Categoria'])
    <!-- widget div-->
        <div>

            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->

            </div>
            <!-- end widget edit box -->

            <!-- widget content -->
            <div class="widget-body no-padding">
                <form id="form-categoria" class="smart-form" action="{{ url('estoque/categoria') }}" method="post">
                    <fieldset>
                        <div class="row">
                            <section class="col col-6">
                                <label class="label">Título</label>
                                <label class="input">
                                    <input type="text" name="categoria_titulo" class="form-control" required>
                                </label>
                            </section>

                            <section class="col col-6">
                                <label class="label">Descrição</label>
                                <label class="input">
                                    <input type="text" name="categoria_descricao" class="form-control">
                                </label>
                            </section>
                        </div>
                    </fieldset>
                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

                    <footer>
                        <button type="reset" name="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                        <button type="button" name="submit" class="btn btn-primary" data-title="Cadastrar Categoria" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
                    </footer>
                </form>
            </div>
        </div>
    </div>
</article>
