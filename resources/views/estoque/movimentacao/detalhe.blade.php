@extends('layouts.page')

@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.css') }}">
@endpush

@section('content')
    <!-- NEW COL START -->
    <article class="col col-md-12">
        <!-- Widget ID (each widget will need unique ID)-->
        <div class="jarviswidget" id="wid-id-{{ \App\Utils\ViewsUtils::getHash(\Illuminate\Support\Facades\Request::url()) }}" data-widget-editbutton="false">
        @include('header.header-content', ['title' => 'Detalhamento de Produto'])

        <!-- widget div-->
            <div>
                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">

                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <section class="col-md-4">
                                    <div class="alert alert-info alert-block pull-center">
                                        <h4 class="alert-heading">Valor Arrecado</h4>
                                        <span class="font-200"><strong>{{\App\Utils\Mask::dinheiro($produto['cabecalho']['total'])}}</strong></span>
                                    </div>
                                </section>
                                <section class="col-md-4">
                                    <div class="alert alert-info alert-block pull-center">
                                        <h4 class="alert-heading">Qtd. Vendida</h4>
                                        <span class="font-200"><strong>{{$produto['cabecalho']['qtdVendido']}}</strong></span>
                                    </div>
                                </section>
                                <section class="col-md-4">
                                    <div class="alert alert-info alert-block pull-center">
                                        <h4 class="alert-heading">Última Venda</h4>
                                        <span class="font-200"><strong>{{\Carbon\Carbon::parse($produto['cabecalho']['ultimaVenda'])->format('d/m/Y H:i:s')}}</strong></span>
                                    </div>
                                </section>
                            </div>
                            <div class="row">
                                <div class="well">
                                    <h1 class="pull-center">{{$produto['cabecalho']['titulo']}}</h1>

                                    <div class="row">

                                        <div class="col-sm-3">

                                            <img src="{{asset('img/produto-sem-imagem.jpg')}}" alt="" style="max-width:300px; width:100%;">

                                            <br>
                                            <strong>Cadastrado em:</strong> {{\Carbon\Carbon::parse($produto['cabecalho']['criadoEm'])->format('d/m/Y H:i:s')}}<br>
                                            <strong>Cadastrado Por:</strong> {{$produto['cabecalho']['cadastradoPor']}}<br>
                                            <strong>Quantidade Disponível:</strong> {{$produto['cabecalho']['estoqueAtual']}} {{$produto['cabecalho']['unidMedidaSigla']}}<br>

                                        </div>
                                        <div class="col-sm-9">

                                            <table id="tabela-movimentacao" class="table table-bordered table-striped align-vertical" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Movimentado Em</th>
                                                        <th class="pull-center">Qtd. Movimentado</th>
                                                        <th>Entidade</th>
                                                        <th class="pull-center">Valor de Custo</th>
                                                        <th class="pull-center">Valor de Venda</th>
                                                        <th class="pull-center">Valor Total</th>
                                                        <th class="pull-center">Status</th>
                                                        <th>Descrição</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($produto['produto'] as $index => $value)
                                                        <tr>
                                                            <td>{{$value['colaborador']}}</td>
                                                            <td class="pull-center">{{$value['quantidade']}}</td>
                                                            <td>{{$value['cliente']}}</td>
                                                            <td class="pull-center">{{\App\Utils\Mask::dinheiro($value['valorCusto'])}}</td>
                                                            <td class="pull-center">{{\App\Utils\Mask::dinheiro($value['valorVenda'])}}</td>
                                                            <td class="pull-center">{{\App\Utils\Mask::dinheiro(($value['valorVenda']*$value['quantidade']))}}</td>
                                                            @if($value['status'] == \App\Utils\SituacaoUtils::PROD_MOVI_ENTRADA)
                                                                <td class="pull-center"><span class="label label-success">Entrada</span></td>
                                                            @elseif($value['status'] == \App\Utils\SituacaoUtils::PROD_MOVI_SAIDA)
                                                                <td class="pull-center"><span class="label label-danger">Saída</span></td>
                                                            @endif
                                                            <td>{{$value['descricao']}}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->
        </div>
        <!-- end widget -->
    </article>
@stop

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/custom/jquery-movimentacao.js') }}"></script>
@endpush
