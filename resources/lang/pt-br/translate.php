<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 03/09/18
 * Time: 15:52
 */

return [
    'login' => [
    'title' => 'CoordMec',
    'lembrar' => ' Lembrar-me',
    'email_mensagem' => 'Por favor insira um e-mail cadastrado',
    'senha_mensagem' => 'Por favor informe sua senha',
    'senha' => 'Senha',
    'esqueceu' => 'Esqueci minha senha?',
    'login' => 'Entre para iniciar uma nova sessão.',
    'acessar' => 'Entrar',
    'redes_sociais' => 'Acesso via redes Sociais',
    'descricao' => 'Sistema para a gestão de Auto Peças e Oficinas Mecânicas',
    'sobre' => 'Sobre o CoordMec',
    'descricao_sobre' => 'É um sistema online disponivel 24 horas por dia, possibilitando ao nosso cliente acessa-lo através de qualquer plataforma.',
    'comodidade' => 'Comodidade',
    'descricao_comodidade' => 'É um sistema online, disponivel 24 horas por dia, portanto não é necessario instalação, bastando somente ao usuario entrar na pagina do sistema informa se login e senha e utilizar.',
        ],

    'register' => [

    ]
];
