<?php

namespace App\Http\Middleware;

use App\Model\MenuModel;
use App\Utils\Common;
use Closure;
use Illuminate\Support\Facades\Session;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!empty($request->user())) {
            \Menu::make('MenuFull', function ($objMenu) use ($request) {

                /** Verifica se o menu jah foi carregado na SESSION, minimizando uma consulta no Servidor */
                $menus = MenuModel::join('menu_permissao', 'menu_permissao.menu_id', '=', 'menu.menu_id')
                    ->where('cola_id', $request->user()->cola_id)
                    ->orderBy('menu_permissao.menu_id', 'asc')
                    ->get()
                    ->toArray();

                $request->session()->push('menu_permissao', array_column($menus, 'menu_id'));

                foreach ($menus as $index => $menu) {
                    $tituloMenu = $menu['menu_texto'];

                    $dadosMenu = [
                        'url' => $menu['menu_href'],
                        'parameter' => $menu['menu_parametros'],
                        'class' => $menu['menu_class'],
                        'id' => $menu['menu_id'],
                        'icon' => $menu['menu_icone'],
                        'node' => $menu['menu_pai']
                    ];

                    if (empty($menu['menu_pai']) && is_null($objMenu->find($menu['menu_pai']))) {
                        $objMenu->add($tituloMenu, $dadosMenu);
                    } else {
                        if ($objMenu->find($menu['menu_pai'])) {
                            $objMenu->find($menu['menu_pai'])->add($tituloMenu, $dadosMenu);
                        }
                    }
                }
            });
        }

        return $next($request);
    }
}
