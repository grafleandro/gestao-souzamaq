<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class InventarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'inv_cod_barra'     => 'nullable|string',
            'inv_num_serie'     => 'nullable|string',
            'inv_item'      => 'required|string',
            'inv_grupo'     => 'nullable|string',
            'grup_id'       => 'required_with:inv_grupo|string',
            'inv_quantidade' => 'nullable|numeric',
            'inv_dt_aquisicao'  => 'nullable|date_format:d/m/Y',
            'inv_vida_util'     => 'nullable|numeric',
            'inv_valor_unit'    => 'nullable',
            'inv_valor_sucata'  => 'nullable',
            'inv_manutencao'    => 'nullable|numeric',
            'inv_ultima_manutencao'     => 'nullable|date_format:d/m/Y',
            'inv_garantia'      => 'nullable|numeric',
            'inv_intevalo_manutencao'   => 'nullable|numeric',
            'inv_periodo_garantia'      => 'nullable|numeric',
            'inv_observacao'    => 'nullable|string',
        ];
    }

    /**
     * @param array $invetario
     * @return array
     */
    public function validarInventario(array $invetario){
        $validator = Validator::make($invetario, $this->rules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'inv_item.required' => 'Você precisa informar o Título do Item.',
        ];
    }
}
