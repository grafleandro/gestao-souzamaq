<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class OrdemServicoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @return array
     */
    public function ordemServicoRules(){
        return [
            'id_cliente'    => 'required|numeric',
            'id_veiculo'   => 'required|numeric',
            'situacao'   => 'required|numeric',
            'servicos'       => 'required|array|min:1',
//            'mecanico'     => 'required|array|min:1',
        ];
    }

    /**
     * @param array $vendaBalcao
     * @return array
     */
    public function validarOrdemServico(array $ordem_servico)
    {
        $validator = Validator::make($ordem_servico, $this->ordemServicoRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'id_cliente.required'   => 'É necessario selecionar um cliente !',
            'id_veiculo.required'  => 'É necessario selecionar um veículo !',
            'situacao.required'    => 'Você precisa informar a situação da ordem de serviço!',
            'mecanico.required'    => 'Você precisa selecionar ao menos um mecânico!',
            'servicos.required'      => 'Não foi selecionado nenhum serviço!',
        ];
    }
}
