<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ColaboradorFuncaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @return array
     */
    public function funcaoRules(){
        return [
            'funcao_titulo'      => 'required|string',
            'funcao_descricao'   => 'nullable|string',
        ];
    }

    /**
     * @param $funcao
     * @return array
     */
    public function validarFuncao($funcao){
        $validator = Validator::make($funcao, $this->funcaoRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
