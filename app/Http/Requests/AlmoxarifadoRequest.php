<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class AlmoxarifadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'almo_cod_barra'    => 'nullable|numeric',
            'almo_titulo'       => 'required|string',
            'almo_valor'        => 'nullable|string',
            'almo_grupo'        => 'nullable|string',
            'almo_unid_venda'   => 'nullable|numeric',
            'almo_qtd'          => 'nullable|numeric',
            'almo_valor_total'  => 'nullable|string',
            'almo_finalidade'   => 'nullable|numeric',
            'almo_dt_venc'      => 'nullable|date_format:d/m/Y',
            'almo_multa'        => 'nullable|string',
            'almo_sessao'       => 'nullable|string',
            'almo_categoria'    => 'nullable|string',
            'almo_subcategoria' => 'nullable|string',
            'grup_id'           => 'required_with:almo_grupo|numeric'
        ];
    }

    /**
     * @param array $dadosAlmox
     * @return array
     */
    public function validarAlmoxarifado(array $dadosAlmox){
        $validator = Validator::make($dadosAlmox, $this->rules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
