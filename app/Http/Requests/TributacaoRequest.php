<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class TributacaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @return array
     */
    public function tributacaoRules(){
        return [
            'trib-ncm-codigo'   => 'required|numeric',
            'trib-cst'          => 'required|numeric',
            'trib-pis-entrada'  => 'required|numeric',
            'trib-pis-saida'    => 'required|numeric',
            'trib-cofins-entrada'   => 'required|numeric',
            'trib-cofins-saida'     => 'required|numeric',
            'trib-csosn'        => 'required|numeric',
        ];
    }

    /**
     * @param array $tributos
     * @return array
     */
    public function validarTributos($tributos = []){
        $validator = Validator::make($tributos, $this->tributacaoRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
