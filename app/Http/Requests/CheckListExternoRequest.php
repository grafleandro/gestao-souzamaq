<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class CheckListExternoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function CheckListExternoRules(){
        return [
//            'cliente_id' => 'required|numeric',
            'responsavel' => 'required|string',
            'placa' => 'required|string|size:8',
            'numero_os' => 'required|numeric',
            'status' => 'required',
            'descricao' => 'nullable',
        ];
    }

    public function validarCheckListExterno($externo){
        $validator = Validator::make($externo, $this->CheckListExternoRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
