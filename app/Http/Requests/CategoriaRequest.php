<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class CategoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function categoriaRules(){
        return [
            'categoria_titulo'      => 'required|regex:/^[\pL\s\-]+$/u',
            'categoria_descricao'   => 'nullable|regex:/^[\pL\s\-]+$/u',
        ];
    }

    public function validarCategoria($categoria){
        $validator = Validator::make($categoria, $this->categoriaRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
