<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ColaboradorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function colaboradorRules(){
        return [
            'nome' => 'required|string',
            'funcao' => 'required|numeric',
            'comissao' => 'nullable|string|max:7',
            'cpf' => 'nullable|string',
            'dt_nascimento' => 'nullable|date_format:d/m/Y',
            'tel_fixo' => 'nullable|size:14',
            'celular_1' => 'nullable|size:16',
            'celular_2' => 'nullable|size:16',
            'email' => 'required|email',
            'tipo_logradouro' => 'nullable|numeric',
            'rua' => 'nullable|string',
            'numero' => 'nullable|numeric',
            'bairro' => 'nullable|string',
            'cep' => 'nullable|string',
            'cidade' => 'nullable|string',
            'estado' => 'nullable|string',
        ];
    }

    public function validarColaborador($colaborador){
        $validator = Validator::make($colaborador, $this->colaboradorRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'celular_1.size'    => 'O telefone celular 1 deve seguir o seguinte padrão (99) 9 9999-9999.',
            'celular_2.size'    => 'O telefone celular 2 deve seguir o seguinte padrão (99) 9 9999-9999.',
            'nome.required'     => 'É necessario preencher o campo Nome!',
            'email.required'    => 'É necessario preencher o campo Email!',
            'funcao.required'   => 'É necessario escolher uma função!',
        ];
    }
}
