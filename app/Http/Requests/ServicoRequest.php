<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ServicoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @return array
     */
    public function servicoRules(){
        return [
            'serv_titulo'   => 'required|regex:/^[\pL\s\-]+$/u',
            'serv_aliq_iss' => 'required',
            'serv_valor'    => 'required',
            'serv_produto'  => 'nullable',
            'cota_comissao'  => 'nullable|string|max:7',
            'cota_empresa'  => 'nullable|string|max:7',
            'serv_prod_qtd' => 'nullable',
            'tabela_prod_qtd'   => 'nullable|array'
        ];
    }

    /**
     * @param $servico
     * @return array
     */
    public function validarServico($servico){
        $validator = Validator::make($servico, $this->servicoRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }

    public function messages()
    {
        return [
            'serv_titulo.required'      => 'Você precisa informar um Título para este Serviço!',
            'serv_aliq_iss.required'    => 'Você precisa informar a Aliquota ISS deste Serviço!',
            'serv_valor.required'       => 'É preciso informar o valor deste Serviço!'
        ];
    }
}
