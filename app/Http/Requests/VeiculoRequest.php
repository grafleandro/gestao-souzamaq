<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class VeiculoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function veiculoRules(){
        return [
            'nome_cliente'  => 'required|regex:/^[\pL\s\-]+$/u',
            'marca'         => 'required|regex:/^[\pL\s\-]+$/u',
            'modelo'        => 'required|string',
            'placa'         => 'nullable|string',
            'cor'           => 'nullable|numeric',
            'chassi'        => 'nullable|string',
            'ano_fabricacao'    => 'nullable|numeric',
            'ano_modelo'        => 'nullable|numeric',
            'km_inicial'        => 'nullable|numeric',
            'combustivel'       => 'nullable|numeric',
            'id_cliente'        => 'required|required_with:nome_cliente|numeric',
        ];
    }

    /**
     * @param $veiculo
     * @return array
     */
    public function validarVeiculo($veiculo){
        $validator = Validator::make($veiculo, $this->veiculoRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
