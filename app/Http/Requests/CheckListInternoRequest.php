<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class CheckListInternoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function CheckListInternoRules(){
        return [
            'nome_veiculo'  => 'required|string',
            'dt_entrega'    => 'required|string',
            'servico'       => 'required|string',
            'mecanicos'       => 'required',
            'status'        => 'required|numeric| min:1',
        ];
    }

    public function validarCheckListInterno($externo){
        $validator = Validator::make($externo, $this->CheckListInternoRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
