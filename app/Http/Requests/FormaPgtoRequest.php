<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class FormaPgtoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @return array
     */
    public function formPgtoRules(){
        return [
            'form-titulo'   => 'required|regex:/^[\pL\s\-]+$/u',
            'form-descricao'=> 'nullable',
            'form-tipo-pgto'=> 'required|numeric',
            'form-parcelas' => 'required_if:form-tipo-pgto, ==, 0|numeric',
            'form-desconto' => 'nullable',
            'conta_bancaria' => 'required|integer|min:1',
        ];
    }

    /**
     * @param array $formaPgto
     * @return array
     */
    public function validarFormaPgto(array $formaPgto){
        $validator = Validator::make($formaPgto, $this->formPgtoRules(), $this->customMessages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }

    public function customMessages()
    {
        return [
            'form-titulo.required'   => 'Você precisa informar um título para esta Forma de Pagamento!',
            'form-tipo-pgto.required'=> 'Você precisa selecionar o Tipo de Pagamento: À VISTA ou A PRAZO!',
            'form-parcelas.required_if' => 'Informe o número de parcelas que terá nesta Forma de Pagamento!',
            'conta_bancaria.min' => 'Você precisa vincular um Conta Bancária.'
        ];
    }
}
