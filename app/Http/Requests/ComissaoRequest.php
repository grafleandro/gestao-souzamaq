<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ComissaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'modulo_comissao_produto'   => 'nullable|integer',
            'modulo_comissao_os'    => 'nullable|integer',
            'comissao_tipo'     => 'required|integer',
            'cota_comissao'     => 'nullable|string|max:7',
            'cota_empresa'      => 'nullable|string|max:7',
        ];
    }

    /**
     * @param array $comissao
     * @return array
     */
    public function validarComissao(array $comissao){
        $validator = Validator::make($comissao, $this->rules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
