<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ContasAPagarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function contasPagarRules(){
        return [
            'cp_descricao'  => 'required|string',
            'cp_banco'      => 'required|numeric',
            'cp_centro_custo'   => 'nullable|numeric',
            'cp_dt_competencia' => 'nullable|date_format:d/m/Y',
            'cp_dt_vencimento'  => 'required|date_format:d/m/Y',
            'cp_valor_total'    => 'required',
            'cp_parcelas'       => 'nullable|numeric',
            'cp_observacao'     => 'nullable',
            'parcelas'          => 'required_if:cp_parcelas, ==, 1|array|min:1',
            'copc_id'           => 'required|numeric'
        ];
    }

    public function validarContasPagar(array $contasPagar){
        $validator = Validator::make($contasPagar, $this->contasPagarRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }

    public function messages()
    {
        return [
            'cp_descricao.request' => 'Você precisa informar uma Descrição para está Conta!',
            'cp_banco.required'  => 'É preciso selecionar uma Conta Bancária, para que se possa faturar os valores correspondentes!',
            'cp_dt_competencia.required' => 'Você precisa selecionar uma Data de Competência!',
            'cp_dt_vencimento.required' => 'Você precisa informar à Data de Vencimento!',
            'cp_valor_total'    => 'O Valor Total da Conta, precisa ser informado!',
            'parcelas.required_if' => 'Você não gerou as parcelas!'
        ];
    }
}
