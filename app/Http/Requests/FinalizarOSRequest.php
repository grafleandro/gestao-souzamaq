<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class FinalizarOSRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @return array
     */
    public function finalizarOSRules(){
        return [
            'total_pgto'    => 'required|numeric',
            'total_os'   => 'required|numeric',
            'servicos'       => 'required|array|min:1',
            'pagamento'     => 'required|array|min:1',
        ];
    }

    /**
     * @param array $vendaBalcao
     * @return array
     */
    public function validarFinalizarOS(array $finalizarOS)
    {
        $validator = Validator::make($finalizarOS, $this->finalizarOSRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'total_pgto.required'   => 'Não foi seleciona nenhuma forma de Pagamento!',
            'total_os.required'  => 'Houve um erro, e não foi possível adquirir o total da Ordem de Serviço!',
            'servicos.required'      => 'Não foi selecionado nenhum serviço!',
            'pagamento.required'    => 'Você precisa informar pelo menos um Tipo de Pagamento!'
        ];
    }
}
