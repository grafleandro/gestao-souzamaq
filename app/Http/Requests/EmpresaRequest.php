<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class EmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function empresaRules(){
        return [
            'nome' => 'required|regex:/^[\pL\s\-]+$/u',
            'razao_social' => 'required|regex:/^[\pL\s\-]+$/u',
            'tel_fixo' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|size:14',
            'celular_1' => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|size:15',
            'email' => 'nullable|email',
            'tipo_logradouro' => 'nullable|numeric',
            'rua' => 'nullable|string',
            'numero' => 'nullable|numeric',
            'bairro' => 'nullable|string',
            'cep' => 'nullable|numeric',
            'cidade' => 'nullable|string',
            'estado' => 'nullable|string',
        ];
    }

    public function validarEmpresa($empresa){
        $validator = Validator::make($empresa, $this->empresaRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
