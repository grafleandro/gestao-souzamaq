<?php

namespace App\Http\Requests;

use App\Utils\Mask;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProdutoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function produtoRules(){
        $rule = [
            'prod_titulo'       => 'required|string',
            'prod_cod_barra'    => 'required|numeric',
            'prod_grupo'        => 'required|regex:/^[\pL\s\-]+$/u',
            'prod_fabricante'   => 'required|regex:/^[\pL\s\-]+$/u',
            'prod_marca'        => 'required|regex:/^[\pL\s\-]+$/u',
            'prod_unid_venda'   => 'required|numeric',
            'prod_trib_ncm'     => 'required|string',
            'prod_custo'        => 'required',
            'prod_margem_lucro' => 'nullable',
            'prod_markup'       => 'nullable',
            'prod_preco_sugerido'  => 'nullable',
            'prod_preco_venda'  => 'nullable',
            'cota_comissao'  => 'nullable|string|max:7',
            'cota_empresa'  => 'nullable|string|max:7',
            'prod_cont_estoque' => 'required|numeric',
            'prod_estoq_minimo' => 'required|numeric',
            'prod_estoq_maximo' => 'required|numeric',
            'prod_estoq_atual'  => 'required|numeric',
            'trib_ncm_codigo'   => 'required_with:prod_trib_ncm|numeric',
            'grup_id'           => 'required_with:prod_grupo|numeric',
            'fabr_id'           => 'required_with:prod_fabricante|numeric',
            'marc_id'           => 'required_with:prod_marca|numeric'
        ];

        return $rule;
    }

    public function validarProduto($produto){
        $validator = Validator::make($produto, $this->produtoRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
