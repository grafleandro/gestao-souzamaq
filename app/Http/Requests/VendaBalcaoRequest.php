<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class VendaBalcaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @return array
     */
    public function balcaoVendaRules(){
        return [
            'total_pgto'    => 'required|numeric',
            'total_venda'   => 'required|numeric',
            'produto'       => 'required|array|min:1',
            'pagamento'     => 'required|array|min:1',
            'clie_consumidor'   => 'required|numeric'
        ];
    }

    /**
     * @param array $vendaBalcao
     * @return array
     */
    public function validarVendaBalcao(array $vendaBalcao)
    {
        $validator = Validator::make($vendaBalcao, $this->balcaoVendaRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'total_pgto.required'   => 'Não foi seleciona nenhuma forma de Pagamento!',
            'total_venda.required'  => 'Houve um erro, e não foi possível adquirir o total da venda!',
            'produto.required'      => 'Não foi selecionado nenhum produto!',
            'pagamento.required'    => 'Você precisa informar pelo menos um Tipo de Pagamento!',
            'clie_consumidor.required'  => 'Houve um erro, e não foi possível informar o nome do Cliente!'
        ];
    }
}
