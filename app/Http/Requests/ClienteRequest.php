<?php

namespace App\Http\Requests;

use App\Utils\ClienteUtils;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @return array
     */
    public function clienteRule(){
        $rules = [
            'cliente_tipo_pessoa'   => 'required|' . Rule::in(ClienteUtils::tiposPessoaCliente()),
            'cliente_situacao'      => 'required|' . Rule::in(ClienteUtils::tipoStatusCliente()),
            'tel_fixo'      => 'nullable|string',
            'celular_1'     => 'nullable|string',
            'celular_2'     => 'nullable|string',
            'tipo_logradouro'     => 'nullable|numeric',
            'rua'           => 'nullable|string',
            'numero'        => 'nullable|string',
            'cep'           => 'nullable|string|max:10',
            'bairro'        => 'nullable|regex:/^[\pL\s\-]+$/u',
            'cidade'        => 'nullable|regex:/^[\pL\s\-]+$/u',
            'estado'        => 'nullable|string|max:2',
            'complemento'   => 'nullable|regex:/^[\pL\s\-]+$/u',
        ];

        if ($this->all()['cliente_tipo_pessoa'] == ClienteUtils::_P_FISICA) {
            $rules['cliente_nome']  = 'required|string';
            $rules['cliente_rg']    = 'nullable|string';
            $rules['cliente_cpf']   = 'required|string|max:14';
        } else if ($this->all()['cliente_tipo_pessoa'] == ClienteUtils::_P_JURIDICA) {
            $rules['cliente_razao_social']  = 'nullable|string';
            $rules['cliente_nome_fantasia']  = 'required|string';
            $rules['cliente_cnpj']    = 'required|string|max:18';
            $rules['cliente_insc_estadual']   = 'nullable|numeric';
        }


        return $rules;
    }

    /**
     * @param $cliente
     * @return array
     */
    public function validarCliente($cliente){
        $validator = Validator::make($cliente, $this->clienteRule(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
