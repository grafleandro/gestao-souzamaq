<?php

namespace App\Http\Requests;

use App\Utils\ConfiguracaoUtils;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ContaBancariaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @return array
     */
    public function contaBancariaRules(){
        $rules = [
            'cb-tipo-conta'     => 'required|numeric',
            'cb-banco'          => 'required|string',
            'banc-id'           => 'required_with:cb_banco|numeric',
            'cb-agencia'        => 'required|numeric',
        ];

        if ($this->all()['cb-tipo-conta'] == ConfiguracaoUtils::CB_CONTA_CORRENTE) {
            $rules['cb-conta-corrente'] = 'required|numeric';
        } else if ($this->all()['cb-tipo-conta'] == ConfiguracaoUtils::CB_CONTA_POUPANCA) {
            $rules['cb-conta-polpanca'] = 'required|numeric';
            $rules['cb-variacao'] = 'required|numeric';
        }

        return $rules;
    }

    /**
     * @param array $dados
     * @return array
     */
    public function validarContaBancaria(array $dados){
        $validator = Validator::make($dados, $this->contaBancariaRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'cb-tipo-conta.required'    => 'Você precisa informar o Tipo da Conta!',
            'cb-banco.required'    => 'Você precisa selecionar o Banco!',
            'cb-agencia.required'    => 'Você deve informar a Agência Bancária!',
            'cb-conta-corrente.required'    => 'O número da Conta Corrente é obrigatório!',
        ];
    }
}
