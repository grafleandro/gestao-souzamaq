<?php

namespace App\Http\Controllers\Estoque;

use App\Repository\MovimentacaoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MovimentacaoController extends Controller
{
    private $movimentacaoRepository;

    public function __construct(MovimentacaoRepository $movimentacaoRepository)
    {
        $this->movimentacaoRepository = $movimentacaoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('estoque/movimentacao/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return array
     */
    public function show($resource)
    {
        try{
            switch ($resource){
                case 'produtos':
                    return $this->movimentacaoRepository->listarProdutos();
                    break;
                case (preg_match( '/\d*/', $resource ) ? true : false ):
                    if(is_numeric($resource)) {
                        return view('estoque/movimentacao/detalhe', ['produto' => $this->movimentacaoRepository->detalhamento($resource)]);
                    }

                    return redirect('estoque/movimentacao');
                    break;
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
