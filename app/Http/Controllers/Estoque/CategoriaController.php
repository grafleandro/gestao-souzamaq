<?php

namespace App\Http\Controllers\Estoque;

use App\Http\Requests\CategoriaRequest;
use App\Repository\CategoriaRepository;
use App\Utils\Common;
use App\Http\Controllers\Controller;

class CategoriaController extends Controller
{
    private $categoriaRepository;

    public function __construct(CategoriaRepository $categoriaRepository)
    {
        $this->middleware('auth');
        $this->categoriaRepository = $categoriaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('estoque/categoria/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoriaRequest $categoriaRequest
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriaRequest $categoriaRequest)
    {
        try{
            $errors = $categoriaRequest->validarCategoria($categoriaRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->categoriaRepository->salvarDados($categoriaRequest->all());

            if(!$resposta){
                Common::setError('Houve um erro ao salvar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($resource)
    {
        try{
            switch ($resource){
                case 'tabela':
                    return $this->categoriaRepository->tabela();
                    break;
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoriaRequest $categoriaRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriaRequest $categoriaRequest, $id)
    {
        try{

            $errors = $categoriaRequest->validarCategoria($categoriaRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->categoriaRepository->atualizarDados($categoriaRequest->all(), $id);

            if(!$resposta){
                Common::setError('Houve um erro ao atualizar os dados!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->categoriaRepository->deletarCategoria($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
