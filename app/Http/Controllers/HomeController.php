<?php

namespace App\Http\Controllers;

use App\Repository\HomeRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $homeRepository;

    /**
     * Create a new controller instance.
     *
     * @param HomeRepository $homeRepository
     */
    public function __construct(HomeRepository $homeRepository)
    {
        $this->middleware('auth');

        $this->homeRepository = $homeRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $informativo = $this->homeRepository->informativo();

        $lembretes = $this->homeRepository->lembretes($request);

        return view('home/index', $informativo);
    }
}
