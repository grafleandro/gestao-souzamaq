<?php

namespace App\Http\Controllers\Venda;

use App\Http\Requests\VendaBalcaoRequest;
use App\Model\FormaPgtoModel;
use App\Repository\ClienteRepository;
use App\Repository\VendaBalcaoRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class BalcaoController extends Controller
{
    private $balcaoRepository;
    private $clienteRepository;

    public function __construct(VendaBalcaoRepository $balcaoRepository, ClienteRepository $clienteRepository)
    {
        $this->balcaoRepository     = $balcaoRepository;
        $this->clienteRepository    = $clienteRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('/venda/balcao/index', [
            'clienteConsumidor' => $this->clienteRepository->clienteConsumidor()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VendaBalcaoRequest $balcaoRequest
     * @return \Illuminate\Http\Response
     */
    public function store(VendaBalcaoRequest $balcaoRequest)
    {
        try{
            $errors = $balcaoRequest->validarVendaBalcao($balcaoRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->balcaoRepository->salvarDados($balcaoRequest);

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function show($resource)
    {
        switch ($resource){
            case 'finalizar_venda':
                $formaPgto = FormaPgtoModel::where('empr_id', Session::get('empr_id'))->get()->toArray();

                return response()->json(['success' => 1, 'view' => view('venda/balcao/finalizar_venda', ['forma_pgto' => $formaPgto])->render()]);
                break;
            case 'tabela':
                return $this->balcaoRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $resposta = $this->balcaoRepository->atualizarDados($request->all(), $id);

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->balcaoRepository->deletarVenda($id);

            return response()->json(['success' => $resposta]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
