<?php

namespace App\Http\Controllers\Configuracao;

use App\Http\Requests\FormaPgtoRequest;
use App\Repository\FormaPgtoRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormaPgtoController extends Controller
{
    private $formaPgtoRepository;

    public function __construct(FormaPgtoRepository $formaPgtoRepository)
    {
        $this->formaPgtoRepository = $formaPgtoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FormaPgtoRequest $formaPgtoRequest
     * @return \Illuminate\Http\Response
     */
    public function store(FormaPgtoRequest $formaPgtoRequest)
    {
        try{
            $errors = $formaPgtoRequest->validarFormaPgto($formaPgtoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->formaPgtoRepository->salvarDados($formaPgtoRequest->all());

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($resource)
    {
        switch ($resource){
            case 'tabela':
                return $this->formaPgtoRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FormaPgtoRequest $formaPgtoRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormaPgtoRequest $formaPgtoRequest, $id)
    {
        try{
            $errors = $formaPgtoRequest->validarFormaPgto($formaPgtoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->formaPgtoRepository->atualizarDados($formaPgtoRequest->all(), $id);

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->formaPgtoRepository->deletarFormaPgto($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
