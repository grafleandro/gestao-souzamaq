<?php

namespace App\Http\Controllers\Configuracao;

use App\Http\Requests\ComissaoRequest;
use App\Repository\ComissaoRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComissaoController extends Controller
{
    private $comissaoRepository;

    public function __construct(ComissaoRepository $comissaoRepository)
    {
        $this->comissaoRepository = $comissaoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comissao = $this->comissaoRepository->findById();

        return view('configuracao/comissao/index', ['comissao' => ($comissao) ? $comissao->toArray() : []]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ComissaoRequest $comissaoRequest
     * @return \Illuminate\Http\Response
     */
    public function store(ComissaoRequest $comissaoRequest)
    {
        try{
            $errors = $comissaoRequest->validarComissao($comissaoRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->comissaoRepository->salvarDados($comissaoRequest->all());

            return response()->json(['success' => 1, 'reload' => true]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param ComissaoRequest $comissaoRequest
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ComissaoRequest $comissaoRequest, $id)
    {
        try{
            $errors = $comissaoRequest->validarComissao($comissaoRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->comissaoRepository->atualizarDados($comissaoRequest->all(), $id);

            return response()->json(['success' => $resposta['success'], 'reload' => true]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
