<?php

namespace App\Http\Controllers\Configuracao;

use App\Http\Requests\EmpresaRequest;
use App\Model\ContaBancariaModel;
use App\Model\TipoLogradouroModel;
use App\Repository\ContaBancariaRepository;
use App\Repository\ContaRepository;
use App\Repository\EmpresaRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ContaController extends Controller
{
    private $empresaRepository;
    private $contaRepository;
    private $contaBancariaRepository;

    public function __construct(EmpresaRepository $empresaRepository, ContaRepository $contaRepository, ContaBancariaRepository $contaBancariaRepository)
    {
        $this->empresaRepository = $empresaRepository;
        $this->contaRepository  = $contaRepository;
        $this->contaBancariaRepository = $contaBancariaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('configuracao/conta/index', [
            'contaBancaria' => $this->contaBancariaRepository->contaBancaria(),
            'empresa' => $this->empresaRepository->findById(Session::get('empr_id'))->toArray(),
            'tipoLogradouro' => TipoLogradouroModel::get()->toArray()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EmpresaRequest $empresaRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmpresaRequest $empresaRequest, $id)
    {
        try{
            $resposta = $this->contaRepository->atualizarDados($empresaRequest->all(), $id);

            return response()->json(['success' => $resposta]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
