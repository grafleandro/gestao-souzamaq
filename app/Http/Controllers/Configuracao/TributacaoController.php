<?php

namespace App\Http\Controllers\Configuracao;

use App\Http\Requests\TributacaoRequest;
use App\Repository\TributacaoRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TributacaoController extends Controller
{
    private $tributacaoRepository;

    public function __construct(TributacaoRepository $tributacaoRepository)
    {
        $this->tributacaoRepository = $tributacaoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dados = $request->all();

        switch (isset($dados['type'])){
            case 'ncm':
                return response()->json(['success' => 1, 'ncm' => $this->tributacaoRepository->autocompleteNCM($dados['search_value'])]);
                break;
            default:
                return view('configuracao/tributacao/index');
                break;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TributacaoRequest $tributacaoRequest
     * @return \Illuminate\Http\Response
     */
    public function store(TributacaoRequest $tributacaoRequest)
    {
        try{
            $errors = $tributacaoRequest->validarTributos($tributacaoRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->tributacaoRepository->salvarDados($tributacaoRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view('/configuracao/tributacao/cadastrar', [
                    'fiscal_cst'    => $this->tributacaoRepository->tributacaoCST(),
                    'fiscal_csosn'  => $this->tributacaoRepository->tributacaoCSOSN(),
                    'fiscal_pis_entrada'    => $this->tributacaoRepository->tributacaoPisCofinsEntrada(),
                    'fiscal_pis_saida'    => $this->tributacaoRepository->tributacaoPisCofinsSaida(),
                    'fiscal_cofins_entrada'    => $this->tributacaoRepository->tributacaoPisCofinsEntrada(),
                    'fiscal_cofins_saida'      => $this->tributacaoRepository->tributacaoPisCofinsSaida(),
                ]);
                break;
            case 'tabela':
                return $this->tributacaoRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            return view('/configuracao/tributacao/cadastrar', [
                'fiscal_cst'    => $this->tributacaoRepository->tributacaoCST(),
                'fiscal_csosn'  => $this->tributacaoRepository->tributacaoCSOSN(),
                'fiscal_pis_entrada'    => $this->tributacaoRepository->tributacaoPisCofinsEntrada(),
                'fiscal_pis_saida'    => $this->tributacaoRepository->tributacaoPisCofinsSaida(),
                'fiscal_cofins_entrada'    => $this->tributacaoRepository->tributacaoPisCofinsEntrada(),
                'fiscal_cofins_saida'      => $this->tributacaoRepository->tributacaoPisCofinsSaida(),
                'tributo' => $this->tributacaoRepository->findById($id)->toArray()
            ]);

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TributacaoRequest $tributacaoRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(TributacaoRequest $tributacaoRequest, $id)
    {
        try{
            $errors = $tributacaoRequest->validarTributos($tributacaoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->tributacaoRepository->atualizarDados($tributacaoRequest->all(), $id);

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->tributacaoRepository->deletarTributo($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
