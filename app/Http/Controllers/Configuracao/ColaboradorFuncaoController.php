<?php

namespace App\Http\Controllers\Configuracao;

use App\Http\Requests\ColaboradorFuncaoRequest;
use App\Repository\FuncaoRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ColaboradorFuncaoController extends Controller
{
    private $funcaoRepository;

    public function __construct(FuncaoRepository $funcaoRepository)
    {
        $this->middleware('auth');
        $this->funcaoRepository = $funcaoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('configuracao/funcao/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ColaboradorFuncaoRequest $funcaoRequest
     * @return \Illuminate\Http\Response
     */
    public function store(ColaboradorFuncaoRequest $funcaoRequest)
    {
        try{
            $errors = $funcaoRequest->validarFuncao($funcaoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->funcaoRepository->salvarDados($funcaoRequest->all());

            if(!$resposta){
                Common::setError('Houve um erro ao salvar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     */
    public function show($resource)
    {
        try{
            switch ($resource){
                case 'tabela':
                    return $this->funcaoRepository->tabela();
                    break;
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ColaboradorFuncaoRequest $funcaoRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ColaboradorFuncaoRequest $funcaoRequest, $id)
    {
        try{

            $errors = $funcaoRequest->validarFuncao($funcaoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->funcaoRepository->atualizarDados($funcaoRequest->all(), $id);

            if(!$resposta){
                Common::setError('Houve um erro ao atualizar os dados!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->funcaoRepository->deletarFuncao($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
