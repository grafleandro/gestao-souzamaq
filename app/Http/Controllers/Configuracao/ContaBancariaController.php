<?php

namespace App\Http\Controllers\Configuracao;

use App\Http\Requests\ContaBancariaRequest;
use App\Repository\ContaBancariaRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContaBancariaController extends Controller
{
    private $contaBancariaRepository;

    public function __construct(ContaBancariaRepository $contaBancariaRepository)
    {
        $this->contaBancariaRepository = $contaBancariaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dados = $request->all();

        if(isset($dados['autocomplete'])){
            switch ($dados['autocomplete']){
                case 'banco':
                    return response()->json(['success' => 1, 'bancos' => $this->contaBancariaRepository->autocompleteBanco($dados['search_value'])]);
                    break;
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ContaBancariaRequest $contaBancariaRequest
     * @return \Illuminate\Http\Response
     */
    public function store(ContaBancariaRequest $contaBancariaRequest)
    {
        try{
            $errors = $contaBancariaRequest->validarContaBancaria($contaBancariaRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->contaBancariaRepository->salvarDados($contaBancariaRequest->all());

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show($resource)
    {
        switch ($resource){
            case 'tabela':
                return $this->contaBancariaRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
