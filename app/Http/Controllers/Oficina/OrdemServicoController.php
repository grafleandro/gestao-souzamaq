<?php

namespace App\Http\Controllers\Oficina;

use App\Http\Requests\FinalizarOSRequest;
use App\Http\Requests\OrdemServicoRequest;
use App\Model\CheckListMecanicoModel;
use App\Model\FormaPgtoModel;
use App\Model\OrdemServicoModel;
use App\Repository\OrdemServicoRepository;
use App\Utils\Common;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class OrdemServicoController extends Controller
{
    private $OrdemServicoRepository;

    public function __construct(OrdemServicoRepository $ordemServicoRepository)
    {
        $this->middleware('auth');
        $this->OrdemServicoRepository = $ordemServicoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(FinalizarOSRequest $finalizarOSRequest)
    {
        try{
            $errors = $finalizarOSRequest->validarFinalizarOS($finalizarOSRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->OrdemServicoRepository->finalizarOS($finalizarOSRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrdemServicoRequest $ordemServicoRequest)
    {
        try{
            $errors = $ordemServicoRequest->validarOrdemServico($ordemServicoRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->OrdemServicoRepository->salvarDados($ordemServicoRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1,'impressao' => \url('oficina/ordem_servico/imprimir_os?id='.$resposta)]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $resource
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function show(Request $request, $resource)
    {
        switch ($resource) {
            case 'listar':
                return view('oficina/ordem_servico/listar');
                break;
            case 'cadastrar':
                return view('oficina/ordem_servico/cadastrar');
                break;
            case 'checklist':
                return $this->OrdemServicoRepository->selecionaChecklist();
                break;
            case 'finalizar_os':
                $formaPgto = FormaPgtoModel::where('empr_id', Session::get('empr_id'))->get()->toArray();

                return response()->json(['success' => 1, 'view' => view('oficina/ordem_servico/finalizar_os', ['forma_pgto' => $formaPgto])->render()]);
                break;
            case 'tabela':
                return $this->OrdemServicoRepository->tabela($request->all());
                break;
            case 'imprimir_os':
                return $this->OrdemServicoRepository->imprimir_os($request->id);
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $dadosOrdemServico = OrdemServicoModel::with(['ordemServicoVeiculo', 'ordemServicoCliente', 'ordemServicoServicos'  => function($query){
                return $query->with(['servico'])->get();
            }, 'ordemServicoProduto' => function($query){
                return $query->with(['produto'])->get();
            }, 'ordemServicoMecanico'])->where('orse_id', $id)->first();

            $dadosOrdemServico->orse_prev_entrega = Carbon::createFromFormat('Y-m-d', $dadosOrdemServico->orse_prev_entrega)->format('d/m/Y');

            $colaborador = [];

            foreach ($dadosOrdemServico->ordemServicoMecanico->toArray() as $item){
                array_push($colaborador, $item['cola_id']);
            }

            return view('oficina/ordem_servico/cadastrar', ['os' => $dadosOrdemServico->toArray(), 'mecanicos' => $colaborador]);

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OrdemServicoRequest $ordemServicoRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrdemServicoRequest $ordemServicoRequest, $id)
    {
        try{
            $errors = $ordemServicoRequest->validarOrdemServico($ordemServicoRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->OrdemServicoRepository->atualizarDados($ordemServicoRequest->all(), $id);

            if($resposta > 0){
                return response()->json(['success' => 1,'impressao' => \url('oficina/ordem_servico/imprimir_os?id='.$resposta)]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resp =  $this->OrdemServicoRepository->deletarOrdemServico($id);

            if($resp){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
