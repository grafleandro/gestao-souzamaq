<?php

namespace App\Http\Controllers\Oficina;

use App\Http\Requests\ServicoRequest;
use App\Repository\ServicoRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicoController extends Controller
{
    private $servicoRepository;

    /**
     * Create a new controller instance.
     *
     * @param ServicoRepository $servicoRepository
     */
    public function __construct(ServicoRepository $servicoRepository)
    {
        $this->middleware('auth');

        $this->servicoRepository = $servicoRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->all()['type'])){

            $autocomplete = $this->servicoRepository->autocomplete($request->all()['search_value']);

            return response()->json(['success' => 1, 'servico' => $autocomplete]);
        }

        return view('oficina/servico/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ServicoRequest $servicoRequest
     * @return \Illuminate\Http\Response
     */
    public function store(ServicoRequest $servicoRequest)
    {
        try{
            $errors = $servicoRequest->validarServico($servicoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->servicoRepository->salvarDados($servicoRequest->all());

            if(!$resposta){
                Common::setError('Houve um erro ao salvar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resouce
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function show($resouce)
    {
        try{
            switch ($resouce){
                case 'cadastrar':
                    return view('oficina/servico/cadastrar');
                    break;
                case 'tabela':
                    return $this->servicoRepository->tabela();
                    break;
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        try{
            return view('oficina/servico/cadastrar', [
                'servico'   => $this->servicoRepository->findById($id)->toArray(),
            ]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ServicoRequest $servicoRequest
     * @param $idServico
     * @return \Illuminate\Http\Response
     */
    public function update(ServicoRequest $servicoRequest, $idServico)
    {
        try{
            $errors = $servicoRequest->validarServico($servicoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->servicoRepository->atualizarDados($servicoRequest->all(), $idServico);

            if(!$resposta){
                Common::setError('Houve um erro ao atualizar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->servicoRepository->deletarServico($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
