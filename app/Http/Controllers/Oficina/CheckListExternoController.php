<?php

namespace App\Http\Controllers\Oficina;

use App\Http\Requests\CheckListExternoRequest;
use App\Repository\CheckListExternoRepository;
use App\Utils\Common;
use App\Utils\Mask;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckListExternoController extends Controller
{
    private $CheckListExternoRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CheckListExternoRepository $checkListExternoRepository)
    {
        $this->middleware('auth');

        $this->CheckListExternoRepository = $checkListExternoRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CheckListExternoRequest $checkListExternoRequest)
    {
        try{
            $errors = $checkListExternoRequest->validarCheckListExterno($checkListExternoRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->CheckListExternoRepository->salvarDados($checkListExternoRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view('oficina/checklist/externo/cadastro');
                break;
            case 'listar':
                return view('oficina/checklist/externo/listar');
                break;
            case 'tabela':
                return $this->CheckListExternoRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $dadosCheckListExterno = $this->CheckListExternoRepository->findById($id);

            return view('oficina/checklist/externo/cadastro', ['externo' => $dadosCheckListExterno->toArray()]);

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CheckListExternoRequest $checkListExternoRequest, $id)
    {
        try{
            $errors = $checkListExternoRequest->validarCheckListExterno($checkListExternoRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->CheckListExternoRepository->atualizarDados($checkListExternoRequest->all(), $id);

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resp = $this->CheckListExternoRepository->deletarCheckListExterno($id);

            if($resp > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
