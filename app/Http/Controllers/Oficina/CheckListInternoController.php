<?php

namespace App\Http\Controllers\Oficina;

use App\Http\Requests\CheckListInternoRequest;
use App\Model\CheckListInternoModel;
use App\Model\CheckListMecanicoModel;
use App\Model\TipoLogradouroModel;
use App\Repository\CheckListInternoRepository;
use App\Utils\ClienteUtils;
use App\Utils\Common;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckListInternoController extends Controller
{
    private $CheckListInternoRepository;

    /**
     * Create a new controller instance.
     *
     * @param CheckListInternoRepository $checkListInternoRepository
     */
    public function __construct(CheckListInternoRepository $checkListInternoRepository)
    {
        $this->middleware('auth');
        $this->CheckListInternoRepository = $checkListInternoRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        if(isset($request->all()['search_value'])){
            $autocomplete = $this->CheckListInternoRepository->autocomplete($request->all()['search_value']);

            return response()->json(['success' => 1, 'veiculo' => $autocomplete]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try{
            if($request->cad_nome){
                $cliente = $this->CheckListInternoRepository->cadastroCliente($request->all());

                if($cliente['clie_id'] > 0){
                    return response()->json(['success' => 1, 'servico' => $cliente]);
                }else{
                    return response()->json(['success' => 0]);
                }
            }
            if ($request->id_cliente){
                $veiculo = $this->CheckListInternoRepository->cadastroVeiculo($request->all());

                if($veiculo->veic_id > 0){
                    return response()->json(['success' => 1, 'veiculo' => $veiculo]);
                }else{
                    return response()->json(['success' => 0]);
                }
            }
            Common::setError('Cadastro errado!!!');

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CheckListInternoRequest $checkListInternoRequest
     * @return \Illuminate\Http\Response
     */
    public function store(CheckListInternoRequest $checkListInternoRequest)
    {
        try{
            $errors = $checkListInternoRequest->validarCheckListInterno($checkListInternoRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->CheckListInternoRepository->salvarDados($checkListInternoRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1, 'url' => url('oficina/checklist/interno/listar')]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $resource
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function show(Request $request, $resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view('oficina/checklist/interno/cadastrar');
                break;
            case 'listar':
                return view('oficina/checklist/interno/listar');
                break;
            case 'tabela':
                return $this->CheckListInternoRepository->tabela($request->all());
                break;
            case 'cadastrar_cliente':
                $resposta = [
                    'success' => 1,
                    'view' => view('empresa/cliente/cadastro')->render()
                ];
                return response()->json($resposta);
                break;
            case 'cadastrar_veiculo':
                $resposta = [
                    'success' => 1,
                    'view' => view('oficina/veiculo/cadastro', ['cliente' => $request->id_cliente])->render()
                ];
                return response()->json($resposta);
                break;
            case 'imprimir':
                return $this->CheckListInternoRepository->imprimirCheckList($request->id);
                break;
            case 'imprimir_relatorio':
                return $this->CheckListInternoRepository->relatorio($request->all());
                break;

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $dadosCheckListInterno = CheckListInternoModel::with(['checklistInternoVeiculo', 'checklistInternoCliente'])->where('ckli_id', $id)->first();

            $dadosCheckListInterno->ckli_previsao_entrega = Carbon::createFromFormat('Y-m-d', $dadosCheckListInterno->ckli_previsao_entrega)->format('d/m/Y');

            $mecanicos = CheckListMecanicoModel::where('ckli_id', $id)->get();

            $colaborador = [];

            foreach ($mecanicos as $item){
                array_push($colaborador, $item['cola_id']);
            }

            return view('oficina/checklist/interno/cadastrar', ['interno' => $dadosCheckListInterno->toArray(), 'mecanicos' => $colaborador]);

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CheckListInternoRequest $checkListInternoRequest, $id)
    {
        try{
            $errors = $checkListInternoRequest->validarCheckListInterno($checkListInternoRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->CheckListInternoRepository->atualizarDados($checkListInternoRequest->all(), $id);

            if($resposta > 0){
                return response()->json(['success' => 1, 'url' => url('oficina/checklist/interno/listar')]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resp =  $this->CheckListInternoRepository->deletarCheckListInterno($id);

            if($resp){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
