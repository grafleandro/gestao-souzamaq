<?php

namespace App\Http\Controllers\Oficina;

use App\Http\Requests\VeiculoRequest;
use App\Repository\VeiculoRepository;
use App\Utils\ClienteUtils;
use App\Utils\Common;
use App\Utils\VeiculoUtils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VeiculoController extends Controller
{
    private $veiculoRepository;

    /**
     * Create a new controller instance.
     *
     * @param VeiculoRepository $veiculoRepository
     */
    public function __construct(VeiculoRepository $veiculoRepository)
    {
        $this->middleware('auth');

        $this->veiculoRepository = $veiculoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->all()['search_value'])){
            $autocomplete = $this->veiculoRepository->autocomplete($request->all()['search_value'], $request->all()['id_cliente']);

            return response()->json(['success' => 1, 'veiculo' => $autocomplete]);
        }

        return view('oficina/veiculo/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VeiculoRequest $veiculoRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VeiculoRequest $veiculoRequest)
    {
        try{
            $errors = $veiculoRequest->validarVeiculo($veiculoRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->veiculoRepository->salvarDados($veiculoRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resouce
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function show($resouce)
    {
        try{
            switch ($resouce){
                case 'cadastrar':
                    return view('oficina/veiculo/cadastrar');
                    break;
                case 'listar':
                    return view('oficina/veiculo/index');
                    break;
                case 'tabela':
                    return $this->veiculoRepository->tabela();
                    break;
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        try{
            $dadosVeiculo = $this->veiculoRepository->findById($id);

            $params = [
                'veiculo_combustivel' => VeiculoUtils::combustivelVeiculo(),
                'veiculo' => $dadosVeiculo->toArray()
            ];

            return view('oficina/veiculo/cadastrar', $params);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param VeiculoRequest $veiculoRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(VeiculoRequest $veiculoRequest, $id)
    {
        try{
            $errors = $veiculoRequest->validarVeiculo($veiculoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->veiculoRepository->atualizarVeiculo($veiculoRequest->all(), $id);

            if(!$resposta){
                Common::setError('Houve um erro ao atualizar os dados!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->veiculoRepository->deletarVeiculo($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
