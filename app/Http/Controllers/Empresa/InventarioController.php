<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Requests\InventarioRequest;
use App\Repository\InventarioRepository;
use App\Utils\Common;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InventarioController extends Controller
{
    private $inventarioRepository;

    public function __construct(InventarioRepository $inventarioRepository)
    {
        $this->inventarioRepository = $inventarioRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**e
     * Store a newly created resource in storage.
     *
     * @param InventarioRequest $inventarioRequest
     * @return \Illuminate\Http\Response
     */
    public function store(InventarioRequest $inventarioRequest)
    {
        try{
            $errors = $inventarioRequest->validarInventario($inventarioRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->inventarioRepository->salvarDados($inventarioRequest);

            return response()->json(['success' => $resposta]);

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view('empresa/inventario/cadastrar');
                break;
            case 'listar':
                return view('empresa/inventario/listar');
                break;
            case 'tabela':
                return $this->inventarioRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $dadosInventario = $this->inventarioRepository->findById($id);
            $dadosInventario->inve_dt_aquisicao = Carbon::parse($dadosInventario->inve_dt_aquisicao)->format('d/m/Y');
            $dadosInventario->inve_ultima_manutencao = Carbon::parse($dadosInventario->inve_ultima_manutencao)->format('d/m/Y');

            return view('empresa/inventario/cadastrar', ['inventario' => $dadosInventario->toArray()]);

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param InventarioRequest $inventarioRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(InventarioRequest $inventarioRequest, $id)
    {
        try{
            $errors = $inventarioRequest->validarInventario($inventarioRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->inventarioRepository->atualizarDados($inventarioRequest, $id);

            return response()->json(['success' => $resposta]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->inventarioRepository->deletarInventario($id);

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
