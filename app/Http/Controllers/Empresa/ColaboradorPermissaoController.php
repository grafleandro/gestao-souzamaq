<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Controllers\Controller;
use App\Policies\ColaboradorPermissaoPolicy;
use App\Repository\ColaboradorPermissaoRepository;
use App\Repository\ColaboradorRepository;
use Illuminate\Http\Request;

class ColaboradorPermissaoController extends Controller
{
    private $colaboradorRepository;
    private $permissaoRepository;

    public function __construct(ColaboradorRepository $colaboradorRepository, ColaboradorPermissaoRepository $permissaoRepository)
    {
        $this->middleware('auth');
//        $this->authorizeResource(ColaboradorPermissaoPolicy::class);

        $this->colaboradorRepository = $colaboradorRepository;
        $this->permissaoRepository  = $permissaoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'index';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $resposta = $this->permissaoRepository->salvarDados($request->all());

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return 'show';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $menuIdPermissao = $this->permissaoRepository->permissaoMenu($request);

            $dadosColaborador = $this->colaboradorRepository->findById($id);

            return view('empresa/colaborador/permissao', [
                'colaborador' => $dadosColaborador->toArray(),
                'colaboradorMenu' => $menuIdPermissao
            ]);

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
