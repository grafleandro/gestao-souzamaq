<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Requests\ProdutoFabricanteRequest;
use App\Model\TipoLogradouroModel;
use App\Repository\ProdutoFabricanteRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProdutoFabricanteController extends Controller
{
    private $fabricanteRepository;

    public function __construct(ProdutoFabricanteRepository $fabricanteRepository)
    {
        $this->middleware('auth');

        $this->fabricanteRepository = $fabricanteRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dados = $request->all();

        if(isset($dados['search_value'])){
            return response()->json(['success' => 1, 'fabricante' => $this->fabricanteRepository->autocomplete($dados['search_value'])]);
        }

        return view('empresa/produto/fabricante/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProdutoFabricanteRequest $fabricanteRequest
     * @return \Illuminate\Http\Response
     */
    public function store(ProdutoFabricanteRequest $fabricanteRequest)
    {
        try{
            $errors = $fabricanteRequest->validarFabricante($fabricanteRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->fabricanteRepository->salvarDados($fabricanteRequest->all());

            if(!$resposta){
                Common::setError('Houve um erro ao salvar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($resource)
    {
        switch ($resource){
            case 'cadastrar':
                $tipoLogradouro = TipoLogradouroModel::get()->toArray();

                return view('empresa/produto/fabricante/cadastrar', ['tipoLogradouro' => $tipoLogradouro]);
                break;
            case 'tabela':
                return $this->fabricanteRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $tipoLogradouro = TipoLogradouroModel::get()->toArray();
            $dadosFabricante = $this->fabricanteRepository->findById($id)->toArray();

            return view('empresa/produto/fabricante/cadastrar', ['tipoLogradouro' => $tipoLogradouro, 'fabricante' => $dadosFabricante]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProdutoFabricanteRequest $fabricanteRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProdutoFabricanteRequest $fabricanteRequest, $id)
    {
        try{
            $errors = $fabricanteRequest->validarFabricante($fabricanteRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->fabricanteRepository->atualizarDados($fabricanteRequest->all(), $id);

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->fabricanteRepository->deletarFabricante($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
