<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClienteRequest;
use App\Model\TipoLogradouroModel;
use App\Repository\ClienteRepository;
use App\Utils\ClienteUtils;
use App\Utils\Common;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    private $clienteRepository;

    public function __construct(ClienteRepository $clienteRepository)
    {
        $this->middleware('auth');
        $this->clienteRepository = $clienteRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->all()['search_value'])){
            $autocomplete = $this->clienteRepository->autocomplete($request->all()['search_value']);

            return response()->json(['success' => 1, 'cliente' => $autocomplete]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClienteRequest $clienteRequest
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteRequest $clienteRequest)
    {
        try{
            $errors = $clienteRequest->validarCliente($clienteRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->clienteRepository->salvarDados($clienteRequest->all());

            if(!$resposta){
                Common::setError('Houve um erro ao salvar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($resource)
    {
        switch ($resource){
            case 'cadastrar':
                $tipoLogradouro = TipoLogradouroModel::get()->toArray();

                return view('empresa/cliente/cadastrar', [
                    'tipoLogradouro' => $tipoLogradouro,
                    'cliente' => [
                        'cliente_pessoa_fisica' => ClienteUtils::_P_FISICA
                    ]]);
                break;
            case 'listar':
                return view('empresa/cliente/listar');
                break;
            case 'tabela':
                return $this->clienteRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $tipoLogradouro = TipoLogradouroModel::get()->toArray();
            $dadosCliente = $this->clienteRepository->findById($id)->toArray();

            return view('empresa/cliente/cadastrar', ['tipoLogradouro' => $tipoLogradouro, 'cliente' => $dadosCliente]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ClienteRequest $clienteRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClienteRequest $clienteRequest, $id)
    {
        try{
            $errors = $clienteRequest->validarCliente($clienteRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->clienteRepository->atualizarDados($clienteRequest->all(), $id);

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->clienteRepository->deletarCliente($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
