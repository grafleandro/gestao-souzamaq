<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Requests\EmpresaRequest;
use App\Model\TipoLogradouroModel;
use App\Repository\EmpresaRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmpresaController extends Controller
{

    private $EmpresaRepository;


    public function __construct(EmpresaRepository $empresaRepository)
    {
        $this->middleware('auth');
        $this->EmpresaRepository = $empresaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EmpresaRequest $empresaRequest
     * @return \Illuminate\Http\Response
     */
    public function store(EmpresaRequest $empresaRequest)
    {
        try{
            $errors = $empresaRequest->validarEmpresa($empresaRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->EmpresaRepository->salvarDados($empresaRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($resource)
    {
        switch ($resource){
            case 'cadastrar':
                $tipoLogradouro = TipoLogradouroModel::get()->toArray();
                return view('empresa/cadastrar', ['tipoLogradouro' => $tipoLogradouro]);
                break;
            case 'listar':
                return view('empresa/listar');
                break;
            case 'tabela':
                return $this->EmpresaRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $dadosEmpresa = $this->EmpresaRepository->findById($id);

            $tipoLogradouro = TipoLogradouroModel::get()->toArray();

            return view('empresa/cadastrar', ['tipoLogradouro' => $tipoLogradouro, 'empresa' => $dadosEmpresa->toArray()]);

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EmpresaRequest $empresaRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmpresaRequest $empresaRequest, $id)
    {
        try{
            $errors = $empresaRequest->validarEmpresa($empresaRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->EmpresaRepository->atualizarDados($empresaRequest->all(), $id);

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resp = $this->EmpresaRepository->deletarEmpresa($id);

            if($resp > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
