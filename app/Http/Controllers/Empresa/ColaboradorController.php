<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Requests\ColaboradorRequest;
use App\Model\FuncaoModel;
use App\Model\TipoLogradouroModel;
use App\Repository\ColaboradorRepository;
use App\Utils\Common;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ColaboradorController extends Controller
{

    private $ColaboradorRepository;


    public function __construct(ColaboradorRepository $colaboradorRepository)
    {
        $this->middleware('auth');
        $this->ColaboradorRepository = $colaboradorRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->all()['search_value'])){
            $autocomplete = $this->ColaboradorRepository->autocomplete($request->all()['search_value']);

            return response()->json(['success' => 1, 'colaborador' => $autocomplete]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ColaboradorRequest $colaboradorRequest
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(ColaboradorRequest $colaboradorRequest)
    {
        try{
            $errors = $colaboradorRequest->validarColaborador($colaboradorRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->ColaboradorRepository->salvarDados($colaboradorRequest);

            return response()->json(['success' => $resposta, 'url' => url('empresa/colaborador/listar')]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $resource
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show(Request $request, $resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view('empresa/colaborador/cadastrar', [
                    'tipoLogradouro' => TipoLogradouroModel::get()->toArray(),
                    'funcao' => FuncaoModel::where('empr_id', Session::get('empr_id'))->get()->toArray()
                ]);
                break;
            case 'listar':
                return view('empresa/colaborador/listar');
                break;
            case 'tabela':
                return $this->ColaboradorRepository->tabela();
                break;
            case 'perfil':
                return view('empresa/colaborador/perfil', [
                    'colaborador' => $this->ColaboradorRepository->findById($request->user()->cola_id)->toArray(),
                    'tipoLogradouro' => TipoLogradouroModel::get()->toArray()

                ]);
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $dadosColaborador = $this->ColaboradorRepository->findById($id);

            $dadosColaborador->cola_data_nascimento = ($dadosColaborador->cola_data_nascimento) ? Carbon::createFromFormat('Y-m-d', $dadosColaborador->cola_data_nascimento)->format('d/m/Y') : null;

            $tipoLogradouro = TipoLogradouroModel::get()->toArray();

            $funcao = FuncaoModel::get()->toArray();

            return view('empresa/colaborador/cadastrar', ['tipoLogradouro' => $tipoLogradouro,
                            'funcao' => $funcao, 'colaborador' => $dadosColaborador->toArray()]);

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ColaboradorRequest $colaboradorRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ColaboradorRequest $colaboradorRequest, $id)
    {
        try{
            $errors = $colaboradorRequest->validarColaborador($colaboradorRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->ColaboradorRepository->atualizarDados($colaboradorRequest, $id);

            return response()->json(['success' => 1, 'url' => url('empresa/colaborador/listar')]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resp = $this->ColaboradorRepository->deletarColaborador($id);

            if($resp > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
