<?php

namespace App\Http\Controllers\Empresa;

use App\Model\ProdutoGrupoModel;
use App\Repository\ProdutoGrupoRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProdutoGrupoController extends Controller
{
    private $grupoRepository;

    public function __construct(ProdutoGrupoRepository $grupoRepository)
    {
        $this->middleware('auth');

        $this->grupoRepository = $grupoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dados = $request->all();

        if(isset($dados['search_value'])){
            return response()->json(['success' => 1, 'grupo' => $this->grupoRepository->autocomplete($dados['search_value'])]);
        }

        return view('empresa/produto/grupo/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $resposta = $this->grupoRepository->salvarDados($request->all());

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view('empresa/produto/grupo/cadastrar');
                break;
            case 'tabela':
                return $this->grupoRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            return view('empresa/produto/grupo/cadastrar', ['grupo' => $this->grupoRepository->findById($id)->toArray()]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $resposta = $this->grupoRepository->atualizarDados($request->all(), $id);

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->grupoRepository->deletarGrupo($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
