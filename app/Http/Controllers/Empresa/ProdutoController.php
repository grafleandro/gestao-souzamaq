<?php

namespace App\Http\Controllers\Empresa;

use App\Http\Requests\ProdutoRequest;
use App\Repository\ProdutoRepository;
use App\Repository\UnidadeMedidaRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProdutoController extends Controller
{
    private $unidadeMedidaRepository;
    private $produtoRepository;

    public function __construct(UnidadeMedidaRepository $unidadeMedidaRepository, ProdutoRepository $produtoRepository)
    {
        $this->middleware('auth');

        $this->unidadeMedidaRepository  = $unidadeMedidaRepository;
        $this->produtoRepository        = $produtoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dados = $request->all();

        if(isset($dados['type'])) {
            switch ($dados['type']) {
                case 'ncm':
                    return response()->json(['success' => 1, 'ncm' => $this->produtoRepository->autocompleteTribNCM($dados['search_value'])]);
                    break;
                case 'prod':
                    return response()->json(['success' => 1, 'produto' => $this->produtoRepository->autocompleteProduto($dados['search_value'])]);
                    break;
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProdutoRequest $produtoRequest
     * @return \Illuminate\Http\Response
     */
    public function store(ProdutoRequest $produtoRequest)
    {
        try{
            if(isset($produtoRequest->all()['atualizar_estoque'])){
                $resposta = $this->produtoRepository->atualizarEstoque($produtoRequest->all()['prod_estoq_atual'], $produtoRequest->all()['prod_id']);

                if(!$resposta){
                    Common::setError('Houve um erro ao salvar os dados!');
                }

                return response()->json($resposta);
            }

            $errors = $produtoRequest->validarProduto($produtoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->produtoRepository->salvarDados($produtoRequest->all());

            if(!$resposta){
                Common::setError('Houve um erro ao salvar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view(
                    'empresa/produto/cadastrar',
                    [
                        'unid_medida' => $this->unidadeMedidaRepository->unidadeMedida()
                    ]
                );
                break;
            case 'listar':
                return view('empresa/produto/listar');
                break;
            case 'tabela':
                return $this->produtoRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            return view('empresa/produto/cadastrar', [
                'produto' => $this->produtoRepository->findById($id)->toArray(),
                'unid_medida' => $this->unidadeMedidaRepository->unidadeMedida()
            ]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProdutoRequest $produtoRequest
     * @param $idProduto
     * @return \Illuminate\Http\Response
     */
    public function update(ProdutoRequest $produtoRequest, $idProduto)
    {
        try{
            $errors = $produtoRequest->validarProduto($produtoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->produtoRepository->atualizarDados($produtoRequest->all(), $idProduto);

            if(!$resposta){
                Common::setError('Houve um erro ao salvar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->produtoRepository->deletarProduto($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
