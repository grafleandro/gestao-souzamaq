<?php

namespace App\Http\Controllers\Almoxarifado;

use App\Http\Controllers\Controller;
use App\Http\Requests\AlmoxarifadoRequest;
use App\Model\ProdutoGrupoModel;
use App\Repository\AlmoxarifadoRepository;
use App\Repository\ColaboradorRepository;
use App\Repository\UnidadeMedidaRepository;
use App\Utils\Common;
use App\Utils\SituacaoUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AlmoxarifadoController extends Controller
{
    private $unidadeMedidaRepository;
    private $almoxarifadoRepository;
    private $colaboradorRepository;

    public function __construct(UnidadeMedidaRepository $unidadeMedidaRepository, AlmoxarifadoRepository $almoxarifadoRepository, ColaboradorRepository $colaboradorRepository)
    {
        $this->unidadeMedidaRepository  = $unidadeMedidaRepository;
        $this->almoxarifadoRepository   = $almoxarifadoRepository;
        $this->colaboradorRepository    = $colaboradorRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('almoxarifado/index', [
            'grupo' => ProdutoGrupoModel::where('empr_id', Session::get('empr_id'))->get()->toArray()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        try{
            $resposta = 0;

            if($request->has('almo_form_tipo')){
                switch ($request->get('almo_form_tipo'))
                {
                    case SituacaoUtils::ALMOX_FORM_EMPRESTIMO:
                        $resposta = $this->almoxarifadoRepository->salvarEmprestimo($request->all());
                        break;
                    case SituacaoUtils::ALMOX_FORM:
                        break;
                }
            }

            if(!$resposta){
                Common::setError('Houve um erro ao salvar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AlmoxarifadoRequest $almoxarifadoRequest
     * @return \Illuminate\Http\Response
     */
    public function store(AlmoxarifadoRequest $almoxarifadoRequest)
    {
        try{
            $errors = $almoxarifadoRequest->validarAlmoxarifado($almoxarifadoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->almoxarifadoRepository->salvarDados($almoxarifadoRequest->all());

            if(!$resposta){
                Common::setError('Houve um erro ao salvar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     * @throws \Throwable
     */
    public function show($resource, Request $request)
    {
        switch ($resource){
            case 'cadastrar':
                return view('almoxarifado/cadastrar',
                    [
                        'unid_medida' => $this->unidadeMedidaRepository->unidadeMedida()
                    ]
                );
                break;
            case 'tabela':
                return $this->almoxarifadoRepository->tabela($request);
                break;
            case 'emprestimo':
                return response()->json(['success' => 1, 'view' => view('almoxarifado/emprestimo', [
                    'mecanico' => $this->colaboradorRepository->mecanico(),
                    'almoxarifado' => $request->get('almo'),
                    'item' => $request->get('item')
                ])->render()]);
                break;
            case 'detalhe':
                return response()->json(['success' => 1, 'view' => view('almoxarifado/detalhe', [
                    'almoxarifado' => $this->almoxarifadoRepository->findById($request->get('almo'))->toArray()
                ])->render()]);
                break;
            case 'informacao':
                return response()->json(['success' => 1, 'view' => view('almoxarifado/informacao')->render()]);
                break;
            case 'imprimir_termo_emprestimo':
                return $this->almoxarifadoRepository->imprimir_termo_emprestimo($request->id);
                break;
            case 'imprimir_relatorio':
                return $this->almoxarifadoRepository->imprimir_relatorio($request);
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            return view('almoxarifado/cadastrar',
                [
                    'almoxarifado'=> $this->almoxarifadoRepository->findById($id)->toArray(),
                    'unid_medida' => $this->unidadeMedidaRepository->unidadeMedida()
                ]
            );

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AlmoxarifadoRequest $almoxarifadoRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AlmoxarifadoRequest $almoxarifadoRequest, $id)
    {
        try{
            $errors = $almoxarifadoRequest->validarAlmoxarifado($almoxarifadoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->almoxarifadoRepository->atualizarDados($almoxarifadoRequest->all(), $id);

            if(!$resposta){
                Common::setError('Houve um erro ao salvar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->almoxarifadoRepository->deletarAlmoxarifado($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
