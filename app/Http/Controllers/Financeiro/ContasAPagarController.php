<?php

namespace App\Http\Controllers\Financeiro;

use App\Http\Requests\ContasAPagarRequest;
use App\Model\ContaBancariaModel;
use App\Model\ContasAPagarCategoriaModel;
use App\Repository\ContasAPagarRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ContasAPagarController extends Controller
{
    private $contasAPagarRepository;

    public function __construct(ContasAPagarRepository $contasAPagarRepository)
    {
        $this->contasAPagarRepository = $contasAPagarRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function index(Request $request)
    {
        $dados = $request->all();

        if(isset($dados['visualizar'])){
            $view =  view('financeiro/contas-pagar/visualizar', [
                'conta' => $this->contasAPagarRepository->findById($dados['visualizar'])->toArray()
            ])->render();

            return response()->json(['success' => 1, 'view' => $view]);
        }

        return view('financeiro/contas-pagar/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ContasAPagarRequest $contasAPagarRequest
     * @return \Illuminate\Http\Response
     */
    public function store(ContasAPagarRequest $contasAPagarRequest)
    {
        try{
            $errors = $contasAPagarRequest->validarContasPagar($contasAPagarRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->contasAPagarRepository->salvarDados($contasAPagarRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1, 'url' => url('financeiro/contas-pagar')]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view('financeiro/contas-pagar/cadastrar', [
                    'conta'         => $this->contasAPagarRepository->contasBancarias()->toArray(),
                    'forma_pgto'    => $this->contasAPagarRepository->formaPgto()->toArray(),
                    'centro_custo'  => $this->contasAPagarRepository->centroCusto()->toArray(),
                    'categoria'     => $this->contasAPagarRepository->categoria()->toArray(),
                ]);
                break;
            case 'tabela':
                return $this->contasAPagarRepository->tabela();
                break;
            case 'visualizar':

                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            return view('financeiro/contas-pagar/cadastrar', [
                'conta'         => $this->contasAPagarRepository->contasBancarias()->toArray(),
                'forma_pgto'    => $this->contasAPagarRepository->formaPgto()->toArray(),
                'centro_custo'  => $this->contasAPagarRepository->centroCusto()->toArray(),
                'categoria'     => $this->contasAPagarRepository->categoria()->toArray(),
                'contasPagar'  => $this->contasAPagarRepository->findById($id)->toArray()
            ]);

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ContasAPagarRequest $contasAPagarRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ContasAPagarRequest $contasAPagarRequest, $id)
    {
        try{
            $errors = $contasAPagarRequest->validarContasPagar($contasAPagarRequest->all());

            if(sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->contasAPagarRepository->atualizarDados($contasAPagarRequest->all(), $id);

            if($resposta > 0){
                return response()->json(['success' => 1, 'url' => url('financeiro/contas-pagar')]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->contasAPagarRepository->deletarContaPagar($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
