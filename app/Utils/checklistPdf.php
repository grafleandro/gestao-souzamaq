<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 01/11/18
 * Time: 15:33
 */

namespace App\Utils;

use App\Model\ColaboradorModel;
use App\Model\EmpresaModel;
use App\Model\TipoLogradouroModel;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Auth;

class checklistPdf extends Fpdf
{
    public function Header()
    {
        $user = Auth::user();

        $pessoa = ColaboradorModel::where('cola_id', $user->cola_id)->firstOrFail();

        $empresa = EmpresaModel::with(['empresaEndereco','empresaContato'])
            ->where('empr_id', '=', $pessoa->empr_id)
            ->firstOrFail()->toArray();

        if(isset($empresa['empr_logo']) && $empresa['empr_logo']){
            $this->Image(storage_path('app/public/empresa/logo/') . $empresa['empr_logo'],10,10,-300);
        }

        $this->SetY(5);
        $this->SetFont('Arial','B',12);
        $this->Cell(190,10,utf8_decode($empresa['empr_nome']),0,0,'C');$this->Ln(5);
        $this->SetFont('Arial','B',7);
        $this->Cell(190,10,(Mask::telComercial($empresa['empresa_contato']['cont_tel_fixo'])) ?? '',0,0,'C');$this->Ln(5);
        $this->Cell(190,10,utf8_decode(TipoLogradouroModel::find($empresa['empresa_endereco']['enlo_id'])->firstOrFail()->toArray()['enlo_titulo'].', '.
                $empresa['empresa_endereco']['ende_logradouro_titulo'].', nº'.$empresa['empresa_endereco']['ende_numero'].', Bairro '. $empresa['empresa_endereco']['ende_bairro'].' - '.
                $empresa['empresa_endereco']['ende_cidade'].' / '. $empresa['empresa_endereco']['ende_estado'])
            ,0,0,'C');$this->ln(10);
        $this->SetLineWidth(0.5);
        $this->rect(8,     6, 190,18);
        $this->SetLineWidth(0,8);
        $this->Line(64,10, 64, 21,5);
        $this->Line(146,10, 146, 21,5);

    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        $this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'C');
    }
}
