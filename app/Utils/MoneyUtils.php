<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 25/11/18
 * Time: 16:54
 */

namespace App\Utils;


class MoneyUtils
{
    /**
     * @param int $valor1
     * @param int $valor2
     * @return float|int
     */
    public static function subtrair($valor1 = 0, $valor2 = 0){
        if($valor1 < $valor2){
            return 0;
        }

        $total = ((($valor1*100)-($valor2*100))/100);

        if(is_int($total)){
            return $total;
        }

        return (float) number_format(($total/100), 2, '.', '');
    }

    /**
     * @param int $valor
     * @param int $qtd
     * @return float|int
     */
    public static function dividir($valor = 0, $qtd = 0){
        $total = ($valor/$qtd);

        if(is_int($total)){
            return $total;
        }

        return (float) number_format($total, 2, '.', '');
    }
}
