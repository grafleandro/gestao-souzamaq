<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 26/07/18
 * Time: 01:14
 */

namespace App\Utils;


class FormUtils
{
    private function __construct() {}

    /**
     * @param $parametros
     * @param $name
     * @param string $id
     * @param string $class
     * @param null $default
     * @param bool $return
     * @param array $outros
     * @param string $nome_padrao
     * @param bool $selectpicker
     * @param string $coluna_id
     * @param string $coluna_nome
     * @return string
     */
    public static function select($parametros, $name, $id = '', $class = '', $default = null, $return = true, $outros = array(), $nome_padrao = "Escolha uma opção", $selectpicker = true, $coluna_id = 'id', $coluna_nome = 'nome') {

        $tipo_select = ($selectpicker === true) ? 'selectpicker ' : 'form-control ';
        $outros = (is_array($outros)) ? implode(' ', $outros) : implode(' ', array($outros));

        // montar o parametro class
        $class = (is_array($class)) ? implode(' ', $class) : $class;

        $html = '<div class="select">';
        $html .= '<select name="' . $name . '" data-live-search="true" id="' . $id . '" class="' . $tipo_select . $class . '" ' . $outros . '>';
        $html .= '<option selected="selected" value="0">' . $nome_padrao . '</option>';

        foreach ($parametros as $id_option => $option) {

            $id_option = (!isset($option[$coluna_id])) ? $id_option : $option[$coluna_id];
            $selected = ( !is_null($default) && $default != '' && $id_option == $default ) ? ' selected="selected"' : '';

            $html .= '<option value="' . $id_option . '"' . $selected . '>' . $option[$coluna_nome] . '</option>';
        }

        $html .= "</select>";
        $html .= "<i></i>";
        $html .= "</div>";

        if ($return === true) {
            return $html;
        }

        echo $html;
    }

    /**
     * @param $parametros
     * @param $name
     * @param string $id
     * @param string $class
     * @param bool $return
     * @param array $outros
     * @param bool $selectpicker
     * @param string $coluna_id
     * @param string $coluna_nome
     * @param array $default
     * @return string
     */
    public static function multiSelect($parametros, $name, $id = '', $class = '', $return = true, $outros = array(), $selectpicker = true, $coluna_id = 'id', $coluna_nome = 'nome', $default = []) {

        $tipo_select = ($selectpicker === true) ? 'selectpicker ' : 'form-control ';
        $outros = (is_array($outros)) ? implode(' ', $outros) : implode(' ', array($outros));

        // montar o parametro class
        $class = (is_array($class)) ? implode(' ', $class) : $class;

        $html = "";
        $html .= '<select name="' . $name . '" data-live-search="true" id="' . $id . '" class="' . $tipo_select . $class . '" ' . $outros . ' multiple>';

        foreach ($parametros as $id_option => $option) {

            $id_option = (!isset($option[$coluna_id])) ? $id_option : $option[$coluna_id];

            $selected = ( !empty($default) && in_array($id_option, $default)) ? ' selected="selected"' : '';

            $html .= '<option value="' . $id_option . '"' . $selected . '>' . $option[$coluna_nome] . '</option>';
        }

        $html .= "</select>";

        if ($return === true)

            return $html;

        echo $html;
    }
}
