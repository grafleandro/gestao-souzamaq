<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 26/07/18
 * Time: 01:52
 */

namespace App\Utils;

class VeiculoUtils
{
    /**
     * VARIAVEIS DE CONTROLE
     */
    const _VEICULO  = 1;
    const _FROTA    = 2;
    const _ORDEM_SERVICO    = 3;

    /**
    * COMBUSTIVEL DE VEICULO
    */
    const _CB_GASOLINA  = 1;
    const _CB_ETANOL    = 2;
    const _CB_DIESEL    = 3;
    const _CB_DIESEL_S10= 4;
    const _CB_GNV       = 5;
    const _CB_FLEX      = 6;


    /**
     * @return array
     */
    public static function combustivelVeiculo(){
        return [
            self::_CB_GASOLINA => [
                'titulo' => 'Gasolina',
                'id' => self::_CB_GASOLINA
            ],
            self::_CB_ETANOL => [
                'titulo' => 'Etanol',
                'id' => self::_CB_ETANOL
            ],
            self::_CB_DIESEL => [
                'titulo' => 'Diesel',
                'id' => self::_CB_DIESEL
            ],
            self::_CB_DIESEL_S10 => [
                'titulo' => 'Diesel S10',
                'id' => self::_CB_DIESEL_S10
            ],
            self::_CB_GNV => [
                'titulo' => 'GNV',
                'id' => self::_CB_GNV
            ],
            self::_CB_FLEX => [
                'titulo' => 'Flex',
                'id' => self::_CB_FLEX
            ],
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function tituloCombustivel($id){
        return self::combustivelVeiculo()[$id]['titulo'];
    }
}
