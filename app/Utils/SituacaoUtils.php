<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 26/09/18
 * Time: 23:54
 */

namespace App\Utils;


class SituacaoUtils
{
    const BOOL_SIM  = 1;
    const BOOL_NAO  = 0;

    /** Tipos de Status */
    const OS_ORCAMENTO = 1;
    const _INICIALIZADA = 2;
    const _SOLICITACAO  = 3;
    const _CANCELADA    = 4;
    const _FINALIZADA   = 5;
    const _AGUARDANDO   = 6;
    const _EXECUTANDO   = 7;

    /*Situação do tanque de combustivel*/
    const _VAZIO   = 1;
    const _UM_QUARTO   = 2;
    const _MEIO   = 3;
    const _TRES_QUARTO   = 4;
    const _CHEIO = 5;

    /*Situação do Filtro*/
    const _NOVO   = 1;
    const _VELHO   = 2;

    /** Tipos de Status Checklist*/
    const _ORCAMENTO = 1;
    const _ORDEM_SERVICO  = 2;
    const _ARQUIVO  = 3;

    /** Tipos de Status da Venda */
    const VEND_ANDAMENTO    = 1;
    const VEND_AGUARDANDO   = 2;
    const VEND_FINALIZADA   = 3;
    const VEND_CANCELADA    = 4;
    const VEND_DEVOLUCAO    = 5;
    const VEND_ATRASADA     = 6;

    /** Almoxarifado - Finalidade do Item */
    const ALMOX_FORM            = 1;
    const ALMOX_FORM_EMPRESTIMO = 2;

    const FINA_CONSUMO      = 1;
    const FINA_EMPRESTIMO   = 2;

    const FILTRO_FERRAMENTA         = 1;
    const FILTRO_N_FERRAMENTA       = 2;
    const FILTRO_FERRAMENTA_TODAS   = 3;

    const DEST_INTERNO      = 1;
    const DEST_EXTERNO      = 2;

    const ALMOX_EMPRESTIMO_ENTREGUE     = 0;
    const ALMOX_EMPRESTIMO_N_ENTREGUE   = 1;

    /** CONFIGURACAO - COMISSAO */
    const CONF_COMI_VENDA_PROD  = 1;
    const CONF_COMI_OS          = 2;
    const CONF_VALOR_LIQUIDO    = 3;
    const CONF_VALOR_BRUTO      = 4;

    /** PRODUTO MOVIMENTACAO */
    const PROD_MOVI_ENTRADA = 1;
    const PROD_MOVI_SAIDA   = 2;


    /**
     * @param int $idFinalidade
     * @return string
     */
    public static function getTituloFinalidade(int $idFinalidade)
    {
        switch ($idFinalidade)
        {
            case self::FINA_EMPRESTIMO:
                return 'Empréstimo';
                break;
            case self::FINA_CONSUMO:
                return 'Consumo';
                break;
        }

        return null;
    }

    /**
     * @return array
     */
    public static function situacaoCheckList(){
        return [
            self::_ORCAMENTO => [
                'situ_titulo' => 'Orçamento',
                'situ_id' => self::_ORCAMENTO
            ],
            self::_ORDEM_SERVICO => [
                'situ_titulo' => 'Ordem de Serviço',
                'situ_id' => self::_ORDEM_SERVICO
            ],
            self::_ARQUIVO => [
                'situ_titulo' => 'Arquivo',
                'situ_id' => self::_ARQUIVO
            ],
        ];
    }

    public static function getTituloCheckList($index){
        return self::situacaoCheckList()[$index]['situ_titulo'];
    }

    /**
     * @return array
     */
    public static function situacao(){
        return [
            self::_INICIALIZADA => [
                'situ_titulo' => 'Inicializada',
                'situ_id' => self::_INICIALIZADA,
                'situ_class' => 'bg-color-greenLight'
            ],
            self::_AGUARDANDO => [
                'situ_titulo' => 'Aguardando',
                'situ_id' => self::_AGUARDANDO,
                'situ_class' => 'bg-color-red'
            ],
            self::_EXECUTANDO => [
                'situ_titulo' => 'Executando',
                'situ_id' => self::_EXECUTANDO,
                'situ_class' => 'bg-color-greenDark'
            ],
            self::_FINALIZADA => [
                'situ_titulo' => 'Finalizada',
                'situ_id' => self::_FINALIZADA,
                'situ_class' => 'bg-color-green'
            ],
            self::OS_ORCAMENTO => [
                'situ_titulo' => 'Orçamento',
                'situ_id' => self::OS_ORCAMENTO,
                'situ_class' => 'bg-color-orange'
            ],
        ];
    }

    /**
     * @param $index
     * @return mixed
     */
    public static function getTitulo($index){
        return self::situacao()[$index]['situ_titulo'];
    }

    public static function getClassSituacao($index){
        return self::situacao()[$index]['situ_class'];
    }

    public static function situacaoCombustivel(){
        return [
            self::_VAZIO => [
                'situ_titulo' => 'Vazio',
                'situ_id' => self::_VAZIO
            ],
            self::_UM_QUARTO => [
                'situ_titulo' => '1/4',
                'situ_id' => self::_UM_QUARTO
            ],
            self::_MEIO => [
                'situ_titulo' => '1/2',
                'situ_id' => self::_MEIO
            ],
            self::_TRES_QUARTO => [
                'situ_titulo' => '3/4',
                'situ_id' => self::_TRES_QUARTO
            ],
            self::_CHEIO => [
                'situ_titulo' => 'Cheio',
                'situ_id' => self::_CHEIO
            ],
        ];
    }

    /**
     * @param $index
     * @return mixed
     */
    public static function getTituloCombustivel($index){
        return self::situacaoCombustivel()[$index]['situ_titulo'];
    }

    public static function situacaoFiltro(){
        return [
            self::_NOVO => [
                'situ_titulo' => 'Novo',
                'situ_id' => self::_NOVO
            ],
            self::_VELHO => [
                'situ_titulo' => 'Velho',
                'situ_id' => self::_VELHO
            ],
        ];
    }

    /**
     * @param $index
     * @return mixed
     */
    public static function getTituloFiltro($index){
        return self::situacaoFiltro()[$index]['situ_titulo'];
    }
}
