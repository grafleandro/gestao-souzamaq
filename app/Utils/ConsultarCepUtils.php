<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 17/01/19
 * Time: 10:58
 */

namespace App\Utils;


class ConsultarCepUtils
{
    public static function getCep($cep){
        try {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://apps.widenet.com.br/busca-cep/api/cep/{$cep}.json",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/javascript",
                    "postman-token: 86418522-649f-3571-f711-9bdbe3577359"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                Common::setError($err);
            } else {
                $endereco =(array) json_decode($response);

                return ['success' => 1, 'endereco' => $endereco];
            }
        } catch (\Exception $e){
            return ['success' => 0, 'alert' => $e->getMessage()];
        }
    }
}
