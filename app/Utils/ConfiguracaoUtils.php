<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 04/12/18
 * Time: 23:03
 */

namespace App\Utils;


use App\Model\ConfiguracaoNumeracaoModel;
use Illuminate\Support\Facades\Session;

class ConfiguracaoUtils
{
    /** CONTAS BANCARIAS */
    const CB_CONTA_CORRENTE = 1;
    const CB_CONTA_POUPANCA = 2;

    /** CENTRO DE CUSTO */
    const CC_SITUACAO_ATIVO     = 1;
    const CC_SITUACAO_INATIVO   = 2;

    public static function atualizaNumero($documento){
        $config_numero = ConfiguracaoNumeracaoModel::where('empr_id', Session::get('empr_id'))->first();

        if($config_numero) {
            $config_numero->increment($documento, 1);

            return $config_numero->conu_ordem_servico;
        }else{
            /*Criando a Tabela de Numeração caso não exista*/
            $config_numero = new ConfiguracaoNumeracaoModel();

            $config_numero->conu_checklist_interno = '0';
            $config_numero->conu_produto = '0';
            $config_numero->conu_servicos = '0';
            $config_numero->conu_venda = '0';
            $config_numero->conu_ordem_servico = '0';
            $config_numero->conu_orcamento = '0';
            $config_numero->conu_nfe = '0';
            $config_numero->conu_nfce = '0';
            $config_numero->empr_id =(int) Session::get('empr_id');

            $config_numero->save();

            /*E ja incrementando o primeiro numero*/
            $config_numero->increment($documento, 1);

            return $config_numero->$documento;
        }
    }
}
