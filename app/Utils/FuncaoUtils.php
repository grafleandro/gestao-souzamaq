<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 14/09/18
 * Time: 23:28
 */

namespace App\Utils;


class FuncaoUtils
{
    /** FUNCOES PADROES */
    const FN_GERENTE    = 6;
    const FN_MECANICO   = 7;
    const FN_CAIXA      = 8;
    const FN_VENDEDOR   = 9;
    const FN_FINANCEIRO = 10;

    /**
     * @param string $string
     * @return string
     */
    public static function prefixoSigla(string $string){
        $str = explode(' ', self::removerAcentos($string));

        switch (count($str)){
            case 1:
                return strtoupper(substr($str[0], 0, 4));
                break;
            case 2:
                $part1 = strtoupper(substr($str[0], 0, 2));
                $part2 = strtoupper(substr($str[1], 0, 2));

                return $part1 . $part2;
                break;
            case 3:
                $part1 = strtoupper(substr($str[0], 0, 2));
                $part2 = strtoupper(substr($str[1], 0, 1));
                $part3 = strtoupper(substr($str[2], 0, 1));

                return $part1 . $part2 . $part3;
                break;
            default:
                $part1 = strtoupper(substr($str[0], 0, 1));
                $part2 = strtoupper(substr($str[1], 0, 1));
                $part3 = strtoupper(substr($str[2], 0, 1));
                $part4 = strtoupper(substr($str[3], 0, 1));

                return $part1 . $part2 . $part3 . $part4;
                break;
        }
    }

    /**
     * @param string $string
     * @return null|string|string[]
     */
    public static function removerAcentos($string = ''){
        return preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );
    }
}
