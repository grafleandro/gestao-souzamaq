<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 01/12/18
 * Time: 09:20
 */

namespace App\Utils;


class VendaUtils
{
    const VEND_ANDAMENTO    = 1;
    const VEND_AGUARDANDO   = 2;
    const VEND_FINALIZADA   = 3;
    const VEND_CANCELADA    = 4;
    const VEND_DEVOLUCAO    = 5;
    const VEND_ATRASADA     = 6;

    /**
     * @return array
     */
    public static function statusVendaJs(){
        return [
            'VEND_ANDAMENTO'    => self::VEND_ANDAMENTO,
            'VEND_AGUARDANDO'   => self::VEND_AGUARDANDO,
            'VEND_FINALIZADA'   => self::VEND_FINALIZADA,
            'VEND_CANCELADA'    => self::VEND_CANCELADA,
            'VEND_DEVOLUCAO'    => self::VEND_DEVOLUCAO,
            'VEND_ATRASADA'     => self::VEND_ATRASADA,
        ];
    }
}
