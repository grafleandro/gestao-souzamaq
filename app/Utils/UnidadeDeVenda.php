<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 14/06/18
 * Time: 12:31
 */

namespace App\Utils;

class UnidadeDeVenda
{
    const metro = [
        'titulo' => 'Metro',
        'tipo' => 'int',
        'id' => 1
    ];

    const litro = [
        'titulo' => 'Litro',
        'tipo' => 'float',
        'id' => 2
    ];

    const caixa = [
        'titulo' => 'Caixa',
        'tipo' => 'int',
        'id' => 3
    ];

    const duzia = [
        'titulo' => 'Duzia',
        'tipo' => 'int',
        'id' => 4
    ];

    const kg = [
        'titulo' => 'Quilograma',
        'tipo' => 'float',
        'id' => 5
    ];

    const metro_quadrado = [
        'titulo' => 'Metro Quadrado',
        'tipo' => 'float',
        'id' => 6
    ];

    const pacote = [
        'titulo' => 'Pacote',
        'tipo' => 'int',
        'id' => 7
    ];

    const peca = [
        'titulo' => 'Peça',
        'tipo' => 'int',
        'id' => 8
    ];

    const unidade = [
        'titulo' => 'Unidade',
        'tipo' => 'int',
        'id' => 9
    ];

    const tonelada = [
        'titulo' => 'Tonelada',
        'tipo' => 'int',
        'id' => 10
    ];

    /**
     * @return array
     */
    public static function getTitulos(){
        $types =  [
            self::metro,
            self::litro,
            self::caixa,
            self::duzia,
            self::kg,
            self::metro_quadrado,
            self::pacote,
            self::peca,
            self::unidade,
            self::tonelada,
        ];

        /* Ordena o Vetor pelo titulo */
        uasort($types, function($a, $b){
            if ($a['titulo'] == $b['titulo']){
                return 0;
            }

            return ($a['titulo'] < $b['titulo']) ? -1 : 1;
        });

        return $types;
    }

    /**
     * @param $id int
     * @return null
     */
    public static function getTituloPorId($id){
        $type = self::getTypes();

        foreach ($type as $index => $value){
            if($value['id'] == $id){
                return $value['titulo'];
            }
        }

        return null;
    }
}
