<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 13/06/18
 * Time: 18:34
 */

namespace App\Utils;

use Carbon\Carbon;

class Mask
{
    /**
     * @param $string
     * @return mixed
     */
    public static function removarMascaraPorcent($string){
        if(empty($string)){
            return null;
        }

        return (float) trim(str_replace(',', '.', str_replace('%', '', $string)));
    }

    /**
     * @param $string
     * @return mixed
     */
    public static function removerMascara($string){
        return (empty($string)) ? '' : trim(str_replace(['_', '.', '/', '-', ',', '(', ')', ' '], "", $string));
    }

    /**
     * @param string $valor
     * @return float
     */
    public static function removerMascaraDinheiro(string $valor){

        if(empty($valor)){
            return 0;
        } else {
            $valor = str_replace('R$', '', $valor); // remover cifrao
            $valor = str_replace('.', '', $valor); // remover caracter de milhar

            return (float) trim(str_replace(',', '.', $valor));
        }
    }

    /**
     * @param $string
     * @return string
     */
    public static function dinheiro($string){
        if(empty($string)){
            return null;
        } else {
            return 'R$ ' . number_format($string, 2, ',', '.');
        }
    }

    /**
     * @param $string
     * @return string
     */
    public static function porcentagem($string){
        if(empty($string)){
            return '00,00%';
        } else {
            return number_format($string, 2, ',', '.') . '%';
        }
    }

    /**
     * @param string $val
     * @param string $mask
     * @return string
     */
    public static function cnpj($val = "", $mask = '##.###.###/####-##'){
        return self::setMask($val, $mask);
    }

    /**
     * @param string $val
     * @param string $mask
     * @return string
     */
    public static function cpf($val = "", $mask = '###.###.###-##'){
        return self::setMask($val, $mask);
    }

    /**
     * @param string $val
     * @param string $mask
     * @return string
     */
    public static function telComercial($val = "", $mask = '(##) ####-####'){
        return self::setMask($val, $mask);
    }

    /**
     * @param string $val
     * @param string $mask
     * @return string
     */
    public static function telCelular($val = null, $mask = '(##) # ####-####'){
        return (is_null($val)) ? '' : self::setMask($val, $mask);
    }

    /**
     * @param string $val
     * @param string $mask
     * @return string
     */
    public static function placaVeiculo($val = "", $mask = '###-####'){
        return strtoupper(self::setMask($val, $mask));
    }

    /**
     * @param $val
     * @param $mask
     * @return string
     */
    private static function setMask($val, $mask){
        $maskared = '';
        $k = 0;

        if(empty($val)){
            return "";
        }

        for($i = 0; $i<=strlen($mask)-1; $i++)
        {
            if($mask[$i] == '#')
            {
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else
            {
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    public static function dataPtBr($data,$minuto = true){
        if($minuto) {
            return ($data) ? Carbon::createFromFormat('Y-m-d H:i:s', $data)->format('d/m/Y H:i:s') : '';
        }
        else{
            return ($data) ? Carbon::createFromFormat('Y-m-d', $data)->format('d/m/Y') : '';
        }
    }
}
