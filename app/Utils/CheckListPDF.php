<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 04/01/19
 * Time: 14:02
 */

namespace App\Utils;

use App\Model\ColaboradorModel;
use App\Model\EmpresaModel;
use App\Model\TipoLogradouroModel;
use Carbon\Carbon;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Auth;

class CheckListPDF extends Fpdf
{
    var $widths;
    var $aligns;

    public function Header()
    {
        $user = Auth::user();

        $pessoa = ColaboradorModel::where('cola_id', $user->cola_id)->firstOrFail();

        $empresa = EmpresaModel::with(['empresaEndereco','empresaContato'])
            ->where('empr_id', '=', $pessoa->empr_id)
            ->firstOrFail()->toArray();

        if(isset($empresa['empr_logo']) && $empresa['empr_logo']) {
            $this->Image(storage_path('app/public/empresa/logo/') . $empresa['empr_logo'], 10, 10, -630, -630);
        }
        if($this->CurOrientation == "P") {
            $this->SetY(5);
            $this->SetFont('Arial', 'B', 12);
            $this->Cell(190, 10, utf8_decode($empresa['empr_nome']), 0, 0, 'C');
            $this->Ln(5);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(190, 10, (Mask::telComercial($empresa['empresa_contato']['cont_tel_fixo'])) ?? '', 0, 0, 'C');
            $this->Ln(5);
            $this->Cell(190, 10, utf8_decode(TipoLogradouroModel::find($empresa['empresa_endereco']['enlo_id'])->firstOrFail()->toArray()['enlo_titulo'] . ', ' .
                    $empresa['empresa_endereco']['ende_logradouro_titulo'] . ', nº' . $empresa['empresa_endereco']['ende_numero'] . ', Bairro ' . $empresa['empresa_endereco']['ende_bairro'] . ' - ' .
                    $empresa['empresa_endereco']['ende_cidade'] . ' / ' . $empresa['empresa_endereco']['ende_estado'])
                , 0, 0, 'C');
            $this->ln(10);
            $this->SetLineWidth(0.5);
            $this->rect(8, 6, 190, 18);
            $this->SetLineWidth(0, 8);
            $this->Line(64, 10, 64, 21, 5);
            $this->Line(146, 10, 146, 21, 5);
        }elseif($this->CurOrientation == "L"){

            $this->SetY(5);
            $this->SetX(80);
            $this->SetFont('Arial', 'B', 12);
            $this->Cell(118, 10, utf8_decode($empresa['empr_nome']), 0, 0, 'C');
            $this->Ln(5);
            $this->SetFont('Arial', 'B', 7);
            $this->SetX(80);
            $this->Cell(118, 10, (Mask::telComercial($empresa['empresa_contato']['cont_tel_fixo'])) ?? '', 0, 0, 'C');
            $this->Ln(5);
            $this->SetX(80);
            $this->Cell(118, 10, utf8_decode(TipoLogradouroModel::find($empresa['empresa_endereco']['enlo_id'])->firstOrFail()->toArray()['enlo_titulo'] . ', ' .
                    $empresa['empresa_endereco']['ende_logradouro_titulo'] . ', nº' . $empresa['empresa_endereco']['ende_numero'] . ', Bairro ' . $empresa['empresa_endereco']['ende_bairro'] . ' - ' .
                    $empresa['empresa_endereco']['ende_cidade'] . ' / ' . $empresa['empresa_endereco']['ende_estado'])
                , 0, 0, 'C');
            $this->ln(10);
            $this->SetLineWidth(0.5);
            $this->rect(10, 6, 278, 18);
            $this->SetLineWidth(0, 8);
            $this->rect(80, 10, 0, 11.5);
            $this->rect(198, 10, 0, 11.5);
        }
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        $this->Cell(95,10,utf8_decode('Página '.$this->PageNo()),0,0,'L');
        $this->Cell(95,10,utf8_decode('Gerado por Gestão Automotiva em '. Carbon::now()->format('d/m/Y H:i:s')),0,0,'R');
    }

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
