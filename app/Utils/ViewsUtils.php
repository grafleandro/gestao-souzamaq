<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 03/09/18
 * Time: 00:04
 */

namespace App\Utils;

class ViewsUtils
{
    /** GERAR HASH PARA CADA FORMULARIO */
    public static function getHash($view){
        return substr(\hash('md5', $view), -5);
    }
}
