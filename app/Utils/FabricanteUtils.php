<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 30/10/18
 * Time: 16:00
 */

namespace App\Utils;


class FabricanteUtils
{
    /**
     * @param array $dadosFabricante
     * @return mixed|null
     */
    public static function getNomeOuRazaoSocial($dadosFabricante = [])
    {

        if (isset($dadosFabricante['fab_nome']) && $dadosFabricante['fab_nome']) {
            return $dadosFabricante['fab_nome'];
        } else if (isset($dadosFabricante['fab_razao_social']) && $dadosFabricante['fab_razao_social']) {
            return $dadosFabricante['fab_razao_social'];
        }

        return null;
    }

    /**
     * @param array $dadosFabricante
     * @return mixed|null
     */
    public static function getCpfOuCnpj($dadosFabricante = []){

        if(isset($dadosFabricante['fab_cpf']) && $dadosFabricante['fab_cpf']){
            return $dadosFabricante['fab_cpf'];
        } else if(isset($dadosFabricante['fab_cnpj']) && $dadosFabricante['fab_cnpj']){
            return $dadosFabricante['fab_cnpj'];
        }

        return null;
    }

    /**
     * @param array $dadosFabricante
     * @return mixed|null
     */
    public static function getRgOuInscEstadual($dadosFabricante = []){

        if (isset($dadosFabricante['fab_rg']) && $dadosFabricante['fab_rg']) {
            return $dadosFabricante['fab_rg'];
        } else if (isset($dadosFabricante['fab_insc_estadual']) && $dadosFabricante['fab_insc_estadual']) {
            return $dadosFabricante['fab_insc_estadual'];
        }

        return null;
    }
}
