<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 15/09/18
 * Time: 01:31
 */

namespace App\Utils;


use App\Model\VeiculoModel;

class ClienteUtils
{
    const _P_FISICA     = 3;
    const _P_JURIDICA   = 4;

    const _SITUACAO_INATIVO = 0;
    const _SITUACAO_ATIVO   = 1;
    const _SITUACAO_NEGATIVADO  = 2;

    /**
     * @return array
     */
    public static function tiposPessoaCliente(){
        return [
            self::_P_FISICA,
            self::_P_JURIDICA
        ];
    }

    /**
     * @return array
     */
    public static function tipoStatusCliente(){
        return [
            self::_SITUACAO_ATIVO,
            self::_SITUACAO_INATIVO,
            self::_SITUACAO_NEGATIVADO
        ];
    }

    /**
     * @param null $idTipo
     * @return array
     */
    public static function tituloTiposPessoaCliente($idTipo = null){
        $tipo = [
            self::_P_FISICA => [
                'id' => self::_P_FISICA,
                'titulo' => 'Pessoa Física',
            ],
            self::_P_JURIDICA => [
                'id' => self::_P_JURIDICA,
                'titulo' => 'Pessoa Jurídica',
            ]
        ];

        return (is_null($idTipo)) ? $tipo : $tipo[$idTipo]['titulo'];
    }

    /**
     * @param null $idStatus
     * @return array
     */
    public static function tituloTipoStatusCliente($idStatus = null){
        $status = [
            self::_SITUACAO_ATIVO => [
                'id' => self::_SITUACAO_ATIVO,
                'titulo' => 'Ativo',
            ],
            self::_SITUACAO_INATIVO => [
                'id' => self::_SITUACAO_INATIVO,
                'titulo' => 'Inativo',
            ],
            self::_SITUACAO_NEGATIVADO => [
                'id' => self::_SITUACAO_NEGATIVADO,
                'titulo' => 'Negativado',
            ],
        ];

        return (is_null($status)) ? $status : $status[$idStatus]['titulo'];
    }

    /**
     * @param array $dadosCliente
     * @return mixed|null
     */
    public static function getCpfOuCnpj($dadosCliente = []){

        if(isset($dadosCliente['cliente_cpf']) && $dadosCliente['cliente_cpf']){
            return $dadosCliente['cliente_cpf'];
        } else if(isset($dadosCliente['cliente_cnpj']) && $dadosCliente['cliente_cnpj']){
            return $dadosCliente['cliente_cnpj'];
        }

        return null;
    }

    /**
     * @param array $dadosCliente
     * @return mixed|null
     */
    public static function getNomeOuRazaoSocial($dadosCliente = [])
    {
        if (isset($dadosCliente['cliente_nome']) && $dadosCliente['cliente_nome']) {
            return $dadosCliente['cliente_nome'];
        } else if (isset($dadosCliente['cliente_razao_social']) && $dadosCliente['cliente_razao_social']) {
            return $dadosCliente['cliente_razao_social'];
        }

        return null;
    }

    /**
     * @param array $dadosCliente
     * @return mixed|null
     */
    public static function getRgOuInscEstadual($dadosCliente = []){

        if (isset($dadosCliente['cliente_rg']) && $dadosCliente['cliente_rg']) {
            return $dadosCliente['cliente_rg'];
        } else if (isset($dadosCliente['cliente_insc_estadual']) && $dadosCliente['cliente_insc_estadual']) {
            return $dadosCliente['cliente_insc_estadual'];
        }

        return null;
    }
}
