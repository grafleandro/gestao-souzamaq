<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 09/10/18
 * Time: 14:11
 */

namespace App\Utils;


class HomeUtils
{
    /** ID1s Widgets */
    const ULTIMAS_TAREFAS   = 1;
    const LEMBRETES         = 2;
    const CALENDARIO        = 3;
}
