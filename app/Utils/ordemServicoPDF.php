<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 21/12/18
 * Time: 23:57
 */

namespace App\Utils;


use App\Model\ColaboradorModel;
use App\Model\EmpresaModel;
use App\Model\TipoLogradouroModel;
use Carbon\Carbon;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Auth;

class ordemServicoPDF extends Fpdf
{
    public function Header()
    {
        $user = Auth::user();

        $pessoa = ColaboradorModel::where('cola_id', $user->cola_id)->firstOrFail();

        $empresa = EmpresaModel::with(['empresaEndereco','empresaContato'])
            ->where('empr_id', '=', $pessoa->empr_id)
            ->firstOrFail()->toArray();

        if(isset($empresa['empr_logo']) && $empresa['empr_logo']) {
            $this->Image(storage_path('app/public/empresa/logo/') . $empresa['empr_logo'], 12, 10, 50, 13);
        }

        $this->SetY(5);
        $this->SetFont('Arial','B',16);
        $this->Cell(190,10,utf8_decode($empresa['empr_nome']),0,0,'C');$this->Ln(9);
        $this->SetFont('Arial','B',9);
        $this->Cell(190,10,(Mask::telComercial($empresa['empresa_contato']['cont_tel_fixo'])) ?? '',0,0,'C');$this->Ln(9);
        $this->SetFont('Arial','B',7);
        $this->Cell(190,10,utf8_decode(TipoLogradouroModel::find($empresa['empresa_endereco']['enlo_id'])->firstOrFail()->toArray()['enlo_titulo'].', '.
                $empresa['empresa_endereco']['ende_logradouro_titulo'].', nº'.$empresa['empresa_endereco']['ende_numero'].', Bairro '. $empresa['empresa_endereco']['ende_bairro'].' - '.
                $empresa['empresa_endereco']['ende_cidade'].' / '. $empresa['empresa_endereco']['ende_estado'])
            ,0,0,'C');$this->ln(10);
        $this->SetLineWidth(0.5);
        $this->rect(8,     6, 192,25);
        $this->SetLineWidth(0,8);
//        $this->Line(64,6, 64, 31);
        $this->Line(65,6, 65, 31);
        $this->Line(146,6, 146, 31);
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        $this->Cell(95,10,utf8_decode('Página '.$this->PageNo()),0,0,'L');
        $this->Cell(95,10,utf8_decode('Gerado por Gestão Automotiva em '. Carbon::now()->format('d/m/Y H:i:s')),0,0,'R');
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
}
