<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 28/01/19
 * Time: 21:06
 */

namespace App\Repository;


use App\DAO\MovimentacaoDAO;
use App\DAO\MovimentacaoProdutoDAO;
use App\Model\OrdemServicoModel;
use App\Model\VendaBalcaoModel;
use App\Model\VendaBalcaoProdutoModel;
use App\Utils\Mask;
use App\Utils\VendaUtils;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;

class MovimentacaoRepository
{
    private $movimentacaoDAO;

    public function __construct(MovimentacaoDAO $movimentacaoDAO)
    {
        $this->movimentacaoDAO = $movimentacaoDAO;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function listarProdutos()
    {
        $this->vendaPoduto();

        $this->ordemServicoProduto();

        return DataTables::of($this->movimentacaoDAO->getProduto())
            ->editColumn('codigo', function($produto){
                return $produto['prod_id'];
            })
            ->editColumn('produto', function($produto){
                return $produto['titulo'];
            })
            ->editColumn('valor_venda', function($produto){
                return Mask::dinheiro($produto['valorVenda']);
            })
            ->editColumn('dt_cadastro', function($produto){
                return Carbon::parse($produto['created'])->format('d/m/Y');
            })
            ->editColumn('estoque', function($produto){
                return $produto['estoqueAtual'];
            })
            ->addColumn('action', function ($produto){
                $url = url('estoque/movimentacao/' . $produto['prod_id']);

                return  '<a href="'.$url.'" class="btn btn-default btn-circle" data-produto="'. $produto['prod_id'] .'""><i class="fa fa-search"></i></a>';
            })
            ->make(true);
    }

    /**
     * @param int $idProduto
     * @return array|mixed
     */
    public function detalhamento(int $idProduto){
        $this->vendaPoduto($idProduto);
        $this->ordemServicoProduto($idProduto);

        return $this->movimentacaoDAO->getProduto();
    }

    /** Produtos vinculado as Vendas realizadas
     * @param null $prodId
     * @return void
     */
    private function vendaPoduto($prodId = null){
        $vendasProduto =  VendaBalcaoModel::query()->with(['colaborador', 'status', 'cliente', 'produto' => function($query) use ($prodId){
                if($prodId){
                    $query->with(['produto' => function($subQuery){
                        $subQuery->with(['produtoEstoque', 'colaborador', 'unidadeMedida', 'produtoValores']);
                    }])->where('prod_id', $prodId);
                } else {
                    $query->with(['produto' => function($subQuery){
                        $subQuery->with(['produtoEstoque', 'colaborador', 'unidadeMedida', 'produtoValores']);
                    }]);
                }
            }])
            ->where('vest_id', VendaUtils::VEND_FINALIZADA)
            ->where('empr_id', Session::get('empr_id'))
            ->get()->toArray();

        foreach ($vendasProduto as $i => $produtos)
        {
            $produtoDAO = new MovimentacaoProdutoDAO();

            foreach ($produtos['produto'] as $j => $produto)
            {
                $produtoDAO->setId($produto['prmo_id']);
                $produtoDAO->setProdId($produto['prod_id']);
                $produtoDAO->setTitulo($produto['produto']['prod_titulo']);
                $produtoDAO->setUtimaVenda($produtos['created_at']);
                $produtoDAO->setDescricao("Venda Balcão Nº: " . $produto['prmo_origem_id']);
                $produtoDAO->setEstoqueAtual($produto['produto']['produto_estoque']['prest_estoq_atual']);
                $produtoDAO->setValor($produto['prmo_valor']);
                $produtoDAO->setValorDeCusto($produto['produto']['produto_valores']['prva_custo']);
                $produtoDAO->setOrigemId(VendaBalcaoRepository::OPERACAO_ORIGEM_VB);
                $produtoDAO->setQuantidade($produto['prmo_qtd']);
                $produtoDAO->setCreated($produto['produto']['created_at']);
                $produtoDAO->setColaNome($produtos['colaborador']['cola_nome']);
                $produtoDAO->setColaId($produtos['colaborador']['cola_id']);
                $produtoDAO->setClieId($produtos['cliente']['clie_id']);
                $produtoDAO->setClieNomeRazaoSocial($produtos['cliente']['clie_nome_razao_social']);
                $produtoDAO->setStatus($produto['prmo_status']);
                $produtoDAO->setCadastradoPor($produto['produto']['colaborador']['cola_nome']);
                $produtoDAO->setCadastradoPorId($produto['produto']['colaborador']['cola_id']);
                $produtoDAO->setUnidMedidaTitulo($produto['produto']['unidade_medida']['unme_titulo']);
                $produtoDAO->setUnidMedidaSigla($produto['produto']['unidade_medida']['unme_sigla']);
                $produtoDAO->setValorDeCusto($produto['produto']['produto_valores']['prva_custo']);

                if($prodId){
                    $this->movimentacaoDAO->addProdutoDetalhe($produtoDAO);
                } else {
                    $this->movimentacaoDAO->addProduto($produtoDAO);
                }
            }
        }
    }

    /**
     * @param null $prodId
     */
    private function ordemServicoProduto($prodId = null){
        $ordemServProd = OrdemServicoModel::with(['ordemServicoProduto' => function($query) use($prodId){
                if($prodId){
                    $query->with(['produto' => function($subQuery){
                        $subQuery->with(['colaborador', 'unidadeMedida']);
                    }, 'produtoEstoque'])->where('prod_id', $prodId);
                } else {
                    $query->with(['produto' => function($subQuery){
                        $subQuery->with(['colaborador', 'unidadeMedida']);
                    }, 'produtoEstoque']);
                }
            }])
            ->where('empr_id', Session::get('empr_id'))
            ->get()->toArray();

        foreach ($ordemServProd as $i => $produtos)
        {
            $produtoDAO = new MovimentacaoProdutoDAO();

            foreach ($produtos['ordem_servico_produto'] as $j => $produto)
            {
                $produtoDAO->setId($produto['osrp_id']);
                $produtoDAO->setProdId($produto['prod_id']);
                $produtoDAO->setTitulo($produto['produto']['prod_titulo']);
                $produtoDAO->setUtimaVenda($produtos['created_at']);
                $produtoDAO->setDescricao("Ordem de Serviço Nº: " . $produto['orse_id']);
                $produtoDAO->setEstoqueAtual($produto['produto_estoque']['prest_estoq_atual']);
                $produtoDAO->setValor($produto['osrp_prod_valor']);
                $produtoDAO->setOrigemId(OrdemServicoRepository::OPERACAO_ORIGEM_OS);
                $produtoDAO->setQuantidade($produto['osrp_prod_qtd']);
                $produtoDAO->setCreated($produto['produto']['created_at']);
                $produtoDAO->setStatus($produto['prmo_status']);
                $produtoDAO->setCadastradoPor($produto['produto']['colaborador']['cola_nome']);
                $produtoDAO->setCadastradoPorId($produto['produto']['colaborador']['cola_id']);

                if($prodId){
                    $this->movimentacaoDAO->addProdutoDetalhe($produtoDAO);
                } else {
                    $this->movimentacaoDAO->addProduto($produtoDAO);
                }
            }
        }
    }
}
