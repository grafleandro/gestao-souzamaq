<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 14/01/19
 * Time: 13:26
 */

namespace App\Repository;


use App\Http\Requests\InventarioRequest;
use App\Model\InventarioModel;
use App\Utils\Common;
use App\Utils\Mask;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Yajra\DataTables\Facades\DataTables;

class InventarioRepository
{
    /**
     * @param InventarioRequest $inventarioRequest
     * @return array
     * @throws \Exception
     */
    public function salvarDados(InventarioRequest $inventarioRequest)
    {
        return $this->salvar(new InventarioModel(), $inventarioRequest);
    }

    /**
     * @param InventarioRequest $inventarioRequest
     * @param int $idInventario
     * @return array
     * @throws \Exception
     */
    public function atualizarDados(InventarioRequest $inventarioRequest, int $idInventario)
    {
        $investarioModel = $this->findById($idInventario);

        return $this->salvar($investarioModel, $inventarioRequest);
    }

    /**
     * @param InventarioModel $inventarioModel
     * @param InventarioRequest $inventarioRequest
     * @return array
     * @throws \Exception
     */
    public function salvar(InventarioModel $inventarioModel, InventarioRequest $inventarioRequest)
    {
        $dadosInventario = $inventarioRequest->all();

        $inventarioModel->inve_item = $dadosInventario['inv_item'];
        $inventarioModel->inve_cod_barra    = $dadosInventario['inv_cod_barra'];
        $inventarioModel->inve_num_serie    = $dadosInventario['inv_num_serie'];
        $inventarioModel->inve_observacao   = $dadosInventario['inv_observacao'];
        $inventarioModel->inve_qtd = $dadosInventario['inv_quantidade'];
        $inventarioModel->inve_dt_aquisicao = Carbon::createFromFormat('d/m/Y', $dadosInventario['inv_dt_aquisicao'])->format('Y-m-d');
        $inventarioModel->inve_vida_util    = $dadosInventario['inv_vida_util'];
        $inventarioModel->inve_valor_unit   = Mask::removerMascaraDinheiro($dadosInventario['inv_valor_unit'] ?? 0);
        $inventarioModel->inve_valor_sucata = Mask::removerMascaraDinheiro($dadosInventario['inv_valor_sucata'] ?? 0);
        $inventarioModel->inve_intervalo_manutencao = $dadosInventario['inv_intevalo_manutencao'];
        $inventarioModel->inve_ultima_manutencao    = Carbon::createFromFormat('d/m/Y', $dadosInventario['inv_ultima_manutencao'])->format('Y-m-d');
        $inventarioModel->inve_periodo_garantia = $dadosInventario['inv_periodo_garantia'];
        $inventarioModel->grup_id =(int) $dadosInventario['grup_id'];
        $inventarioModel->empr_id =(int) Session::get('empr_id');


        if($inventarioRequest->hasFile('up-file')){
            $file       = $inventarioRequest->file('up-file');
            $fileName   = md5(microtime()) . '.' . $file->getClientOriginalExtension();

            if(Storage::exists('public/inventario/' . $inventarioModel->inve_arquivo)){
                Storage::delete('public/inventario/' . $inventarioModel->inve_arquivo);
            }

            $pathPerfil = storage_path('app/public/inventario/');

            Image::make($file->getPathname())->resize(320, 240)->save($pathPerfil . $fileName);

            $inventarioModel->inve_arquivo = $fileName;
        }

        if(!$inventarioModel->save())
        {
            Common::setError('Houve um erro ao salvar o Inventário!');
        }

        return ['success' => $inventarioModel->inve_id];
    }

    /**
     * @param $idInventario
     * @return mixed
     */
    public function findById($idInventario)
    {
        return InventarioModel::with(['grupo'])->where('inve_id', $idInventario)->firstOrFail();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function tabela()
    {
        $queryInventario = InventarioModel::query()->with(['grupo'])->orderBy('inve_id');

        return DataTables::eloquent($queryInventario)
            ->editColumn('arquivo', function($inventario){
                if($inventario->inve_arquivo){
                    $image = '<img src="'. asset('storage/inventario/' . $inventario->inve_arquivo) .'" width="80px">';
                } else {
                    $image = '<img src="'. asset('img/produto-sem-imagem.jpg') .'" width="80px">';
                }

                return $image;
            })
            ->editColumn('inve_valor_unit', function($invetario){
                return Mask::dinheiro($invetario->inve_valor_unit);
            })
            ->editColumn('action', function($invetario){
                $urlEdit = "/empresa/inventario/". $invetario->inve_id."/edit";

                $button = '<a  href="'. url($urlEdit) .'" title="Editar Inventário" class="btn btn-default btn-circle margin-right-5"><i class="fa fa-edit"></i></a>';
                $button .= '<button class="btn btn-default btn-circle" title="Excluir Inventário" data-inventario="'.$invetario->inve_id.'" onclick="jQueryInventario.deletarInventario($(this))"><i class="fa fa-trash-o"></i></button>';

                return $button;
            })
            ->rawColumns(['action', 'arquivo'])
            ->make(true);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deletarInventario($id)
    {
        $inventario = $this->findById($id);

        return $inventario->delete();
    }
}
