<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 16/09/18
 * Time: 23:25
 */

namespace App\Repository;


use App\Http\Controllers\Auth\RegisterController;
use App\Http\Requests\ColaboradorRequest;
use App\Mail\emailCadastro;
use App\Mail\EnviarNotificacaoMail;
use App\Model\ColaboradorModel;
use App\Model\ContatoModel;
use App\Model\EnderecoModel;
use App\User;
use App\Utils\ClienteUtils;
use App\Utils\Common;
use App\Utils\Mask;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\ImageManagerStatic as Image;

class ColaboradorRepository
{
    private $ColaboradorModel;
    private $RegisterController;

    public function __construct(ColaboradorModel $colaboradorModel, RegisterController $registerController)
    {
        $this->ColaboradorModel = $colaboradorModel;
        $this->RegisterController = $registerController;
    }

    /**
     * @param ColaboradorRequest $colaboradorRequest
     * @return ColaboradorModel
     * @throws \PHPMailer\PHPMailer\Exception
     * @throws \Throwable
     */
    public function salvarDados(ColaboradorRequest $colaboradorRequest){
        $dadosColaborador = $colaboradorRequest->all();

        if(isset($dadosColaborador['cpf'])) {
            $respostaColaborador = ColaboradorModel::where('cola_cpf', Mask::removerMascara($dadosColaborador['cpf']))->get();

            if ($respostaColaborador->count()) {
                Common::setError('Já possui um Colaborador cadastrado com esse CPF: ' . $respostaColaborador->cola_cpf);
            }
        }

        $colaborador = $this->salvar(new ColaboradorModel(),
            new EnderecoModel(),
            new ContatoModel(),
            $colaboradorRequest
        );

        if($dadosColaborador['usuario'] == 1){

            $senha = str_random(6);

            $respostaUser = User::where('email', $dadosColaborador['email'])->get();

            if($respostaUser->count()){
                Common::setError('Já possui um Usuario cadastrado com esse Email: ' . $respostaUser->email);
            }

            User::create([
                'name' => $dadosColaborador['nome'],
                'email' => $dadosColaborador['email'],
                'password' => Hash::make($senha),
                'cola_id' => $colaborador,
            ]);

            $dadosEmail['name'] = $dadosColaborador['nome'];
            $dadosEmail['email'] = $dadosColaborador['email'];
            $dadosEmail['password'] = $senha;

            (new EnviarNotificacaoMail())->enviarNotificacao(
                'CoordMec',
                'Cadastro de Usuário',
                $dadosColaborador['email'],
                'empresa.colaborador.email',
                ['colaborador' => $dadosEmail]
            );
        }

        return $colaborador;
    }

    /**
     * @param ColaboradorRequest $dadosColaborador
     * @param int $id
     * @return ColaboradorModel
     * @throws \Exception
     */
    public function atualizarDados(ColaboradorRequest $dadosColaborador, int $id){

        $colaborador = $this->findById($id);

        return $this->salvar($colaborador,
            $colaborador->pessoaEndereco()->getResults() ?? new EnderecoModel(),
            $colaborador->pessoaContato()->getResults() ?? new ContatoModel(),
            $dadosColaborador
        );
    }

    /**
     * @param int $idColaborador
     * @return ClienteModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function findById($idColaborador = 0){
        return ColaboradorModel::with(['pessoaEndereco', 'pessoaContato', 'pessoaFuncao', 'pessoaLogin'])
            ->where('cola_id', $idColaborador)
            ->firstOrFail();
    }

    /**
     * @param ColaboradorModel $colaboradorModel
     * @param EnderecoModel $enderecoModel
     * @param ContatoModel $contatoModel
     * @param ColaboradorRequest $colaboradorRequest
     * @return ColaboradorModel
     * @throws \Exception
     * @internal param ClienteModel $clienteModel
     * @internal param ClientePessoaFisicaModel $fisicaModel
     */
    public function salvar(ColaboradorModel $colaboradorModel, EnderecoModel $enderecoModel, ContatoModel $contatoModel,  ColaboradorRequest $colaboradorRequest){
        $dadosColaborador = $colaboradorRequest->all();

        /* Salvando os dados de Endereco */
        $this->salvarEndereco($enderecoModel, $dadosColaborador);

        /* Salvando os dados de Contato */
        $this->salvarContato($contatoModel, $dadosColaborador);

        $comissao = Mask::removarMascaraPorcent($dadosColaborador['comissao']);

        if($comissao > 100){
            Common::setError('O valor da comissão não pode ser maior que 100%');
        }

        /* Salvando os dados do Cliente */
        $colaboradorModel->cola_nome = $dadosColaborador['nome'];
        $colaboradorModel->cola_cpf = Mask::removerMascara($dadosColaborador['cpf']);
        $colaboradorModel->cola_rg = $dadosColaborador['rg'];
        $colaboradorModel->cola_data_nascimento = (isset($dadosColaborador['dt_nascimento'])) ? Carbon::createFromFormat('d/m/Y', $dadosColaborador['dt_nascimento'])->format('Y-m-d H:i:s') : null;
        $colaboradorModel->cola_comissao = $comissao;
        $colaboradorModel->ende_id = $enderecoModel->ende_id;
        $colaboradorModel->cont_id = $contatoModel->cont_id;
        $colaboradorModel->empr_id = Session::get('empr_id');

        if(isset($dadosColaborador['funcao'])){
            $colaboradorModel->func_id = $dadosColaborador['funcao'];
        }

        if(isset($dadosColaborador['situacao'])) {
            $colaboradorModel->cola_status = $dadosColaborador['situacao'];
        }

        if($colaboradorRequest->hasFile('foto')){
            $file       = $colaboradorRequest->file('foto');
            $fileName   = md5(microtime()) . '.' . $file->getClientOriginalExtension();

            if(Storage::exists('public/perfil/' . $colaboradorModel->cola_foto)){
                Storage::delete('public/perfil/' . $colaboradorModel->cola_foto);
            }

            $pathPerfil = storage_path('app/public/perfil/');

            Image::make($file->getPathname())->resize(320, 240)->save($pathPerfil . $fileName);

            $colaboradorModel->cola_foto = $fileName;
        }

        if(!$colaboradorModel->save()){
            Common::setError('Erro ao salvar os dados do Cliente Pessoa Física!');
        }

        return $colaboradorModel->cola_id;
    }

    /**
     * @param EnderecoModel $enderecoModel
     * @param array $dadosColaborador
     * @return EnderecoModel
     * @throws \Exception
     */
    public function salvarEndereco(EnderecoModel $enderecoModel, $dadosColaborador = []){
        $enderecoModel->enlo_id = $dadosColaborador['tipo_logradouro' ];
        $enderecoModel->ende_logradouro_titulo = $dadosColaborador['rua'] ;
        $enderecoModel->ende_bairro = $dadosColaborador['bairro'];
        $enderecoModel->ende_cidade = $dadosColaborador['cidade'];
        $enderecoModel->ende_estado = $dadosColaborador['estado'];
        $enderecoModel->ende_complemento = ($dadosColaborador['complemento']) ? $dadosColaborador['complemento'] : null;
        $enderecoModel->ende_numero = $dadosColaborador['numero'];
        $enderecoModel->ende_cep = Mask::removerMascara($dadosColaborador['cep']);

        if(!$enderecoModel->save()){
            Common::setError('Erro ao salvar os dados do Endereço!');
        }

        return $enderecoModel;
    }

    /**
     * @param ContatoModel $contatoModel
     * @param array $dadosColaborador
     * @return ContatoModel
     * @throws \Exception
     */
    public function salvarContato(ContatoModel $contatoModel, $dadosColaborador = []){
        $contatoModel->cont_cel_1 = (isset($dadosColaborador['celular_1'])) ? Mask::removerMascara($dadosColaborador['celular_1']) : null;
        $contatoModel->cont_cel_2 = (isset($dadosColaborador['celular_2'])) ? Mask::removerMascara($dadosColaborador['celular_2']) : null;
        $contatoModel->cont_tel_fixo = (isset($dadosColaborador['tel_fixo'])) ? Mask::removerMascara($dadosColaborador['tel_fixo']) : null;
        $contatoModel->cont_email = (isset($dadosColaborador['email']) && !isset($dadosColaborador['perfil'])) ? $dadosColaborador['email'] : null;

        if(!$contatoModel->save()){
            Common::setError('Erro ao salvar os dados do Contato!');
        }

        return $contatoModel;
    }

    /**
     * @return array
     */
    public function mecanico()
    {
        /** TODO Trocar o valor MAGICO por uma CONSTANTE - FUNC_ID */

        return ColaboradorModel::with(['pessoaEndereco', 'pessoaContato', 'pessoaFuncao'])
            ->where('empr_id', Session::get('empr_id'))
            ->where('func_id', 4)
            ->get()
            ->toArray();
    }

    /**
     * @param $valor
     * @return mixed
     */
    public function autocomplete($valor){

        if(strlen($valor) == 2 && $valor == '**'){
            return ColaboradorModel::where('empr_id', Session::get('empr_id'))
                ->get()
                ->toArray();
        }

        return ColaboradorModel::where('cola_nome', 'like', '%'.$valor.'%')
            ->where('empr_id', Session::get('empr_id'))
            ->get()
            ->toArray();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $dadosColaborador = ColaboradorModel::with(['pessoaEndereco', 'pessoaContato'])->where('empr_id', '=', Session::get('empr_id'))->get()->toArray();


        return Datatables::of($dadosColaborador)

            ->editColumn('cola_status', function($colaborador){
                return ClienteUtils::tituloTipoStatusCliente($colaborador['cola_status']);
            })
            ->editColumn('cola_cpf', function($colaborador){
                    $cpf = Mask::cpf($colaborador['cola_cpf']);
                return $cpf;
            })
            ->editColumn('cont_id', function($colaborador){

                if(!empty($colaborador['pessoa_contato']['cont_tel_fixo'])){
                    $contato[] =  Mask::telComercial($colaborador['pessoa_contato']['cont_tel_fixo']);
                }

                if(!empty($colaborador['pessoa_contato']['cont_cel_1'])){
                    $contato[] =  Mask::telCelular($colaborador['pessoa_contato']['cont_cel_1']);
                }

                if(!empty($colaborador['pessoa_contato']['cont_cel_2'])){
                    $contato[] =  Mask::telCelular($colaborador['pessoa_contato']['cont_cel_1']);
                }

                return (empty($contato)) ? '' : implode(' / ', $contato);
            })
            ->addColumn('action', function ($colaborador){
                $urlEdit = "/empresa/colaborador/". $colaborador['cola_id'] ."/edit";
                $urlPermissao = "/empresa/colaborador/permissao/" . $colaborador['cola_id'] . "/edit";

                return '<div style="text-align: center">
                                <a href="'. url($urlEdit) .'" title="Editar Colaborador" data-colaborador="'.$colaborador['cola_id'].'" style="margin: 4px" class="btn btn-default btn-circle"><i class="fa fa-edit"></i></a>
                                <button title="Excluir Colaborador" data-colaborador="'.$colaborador['cola_id'].'" style="margin: 4px" class="btn btn-default btn-circle" onclick="jQueryColaborador.deletarColaborador($(this))"><i class="fa fa-trash-o"></i></button>
                                <a href="'. url($urlPermissao) .'" title="Permissão de Acesso" data-colaborador="'.$colaborador['cola_id'].'" style="margin: 4px" class="btn btn-default btn-circle"><i class="fa fa-key"></i></a>
                        </div>';
            })
            ->make(true);
    }

    /**
     * @param $id
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function deletarColaborador($id){

        $colaborador = $this->findById($id);

        $colaborador->pessoaEndereco->delete();

        $colaborador->pessoaContato->delete();

        return $colaborador->delete();
    }
}
