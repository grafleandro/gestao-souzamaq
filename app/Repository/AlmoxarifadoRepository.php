<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 20/12/18
 * Time: 23:16
 */

namespace App\Repository;


use App\Model\AlmoxarifadoEmprestimoModel;
use App\Model\AlmoxarifadoModel;
use App\Model\ColaboradorModel;
use App\Utils\AlmoxarifadoUtil;
use App\Utils\Common;
use App\Utils\Mask;
use App\Utils\ordemServicoPDF;
use App\Utils\SituacaoUtils;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Request;
use Yajra\DataTables\Facades\DataTables;

class AlmoxarifadoRepository
{
    /**
     * @param array $dadosAlmox
     * @return array
     * @throws \Exception
     */
    public function salvarDados(array $dadosAlmox)
    {
        return $this->salvar(new AlmoxarifadoModel(), $dadosAlmox);
    }

    /**
     * @param array $dadosAlmox
     * @param int $idAlmox
     * @return array
     * @throws \Exception
     */
    public function atualizarDados(array $dadosAlmox, int $idAlmox)
    {
        $almox = $this->findById($idAlmox);

        return $this->salvar($almox, $dadosAlmox);
    }

    /**
     * @param AlmoxarifadoModel $almoxarifadoModel
     * @param array $dadosAlmox
     * @return array
     * @throws \Exception
     */
    public function salvar(AlmoxarifadoModel $almoxarifadoModel, array $dadosAlmox)
    {
        $almoxarifadoModel->almo_cod_barra = $dadosAlmox['almo_cod_barra'] ?? null;
        $almoxarifadoModel->almo_titulo =  $dadosAlmox['almo_titulo'] ?? null;
        $almoxarifadoModel->almo_valor = Mask::removerMascaraDinheiro($dadosAlmox['almo_valor'] ?? 0);
        $almoxarifadoModel->almo_qtd = $dadosAlmox['almo_qtd'] ?? null;
        $almoxarifadoModel->almo_finalidade = $dadosAlmox['almo_finalidade'] ?? null;
        $almoxarifadoModel->almo_dt_vencimento = (isset($dadosAlmox['almo_dt_venc']) ?? $dadosAlmox['almo_dt_venc']) ? Carbon::createFromFormat('d/m/Y', $dadosAlmox['almo_dt_venc'])->format('Y-m-d') : null;
        $almoxarifadoModel->almo_multa = Mask::removerMascaraDinheiro($dadosAlmox['almo_multa'] ?? 0);
        $almoxarifadoModel->almo_sessao = $dadosAlmox['almo_sessao'] ?? null;
        $almoxarifadoModel->almo_categoria = $dadosAlmox['almo_categoria'] ?? null;
        $almoxarifadoModel->almo_subcategoria = $dadosAlmox['almo_subcategoria'] ?? null;
        $almoxarifadoModel->grup_id =(int) $dadosAlmox['grup_id'] ?? null;
        $almoxarifadoModel->unme_id =(int) $dadosAlmox['almo_unid_venda'] ?? null;
        $almoxarifadoModel->empr_id = Session::get('empr_id');

        if(!$almoxarifadoModel->save())
        {
            Common::setError('Erro ao salvar os dados!');
        }

        return ['success' => $almoxarifadoModel->almo_id];
    }

    public function imprimir_termo_emprestimo($id){

        $emprestimo = AlmoxarifadoEmprestimoModel::with(['almoxarifado'])->where('alem_id', $id)->firstOrFail();

        if($emprestimo && $emprestimo['alem_destino'] == SituacaoUtils::DEST_EXTERNO) {

            $file = new ordemServicoPDF('P', 'mm', 'A4');
            $file->SetCreator('LMS', true);
            $file->SetFont('Arial', 'B', 12);
            $file->AddPage();
            $file->SetY(8);
            $file->SetX(145);
            $file->Cell(50, 9, utf8_decode('Concessão'), 0, 0, 'C');
            $file->ln();
            $file->SetX(145);
            $file->Cell(50, 9, utf8_decode('Nº '.$emprestimo['alem_id']), 0, 0, 'C');
            $file->ln(15);
            $file->SetFont('Times', 'B', 10);
            $file->MultiCell(190, 9,utf8_decode('    Eu,  ' . $emprestimo['alem_responsavel'] . ', declaro ter pego sob concessão a ferramenta descrita abaixo:'),0, 'J');
            $file->MultiCell(190, 9, strtoupper(utf8_decode($emprestimo['almoxarifado']['almo_titulo'])));
            $file->ln(7);
            $file->Cell(95, 6, utf8_decode('____________________________________________________'), 0,1, 'L');
            $file->Cell(95, 6, utf8_decode('Assinatura do Responsável pela retirada'), 0, 1, 'C');
            $file->ln(10);
            $file->SetFont('Arial','I',8);
            $file->Cell(95,10,utf8_decode('Página '.$file->PageNo()),0,0,'L');
            $file->Cell(95,10,utf8_decode('Gerado por Gestão Automotiva em '. Carbon::now()->format('d/m/Y H:i:s')),0,0,'R');
            $file->Output();
        }
    }

    public function imprimir_relatorio(Request $request){

        $ferramenta = AlmoxarifadoModel::with(['emprestimo' => function($query) use ($request){
            if($request->get('filtro_ferramenta') != SituacaoUtils::FILTRO_FERRAMENTA_TODAS) {
                $query->where('alem_status', $request->get('filtro_ferramenta'));
            }
            if ($request->has('filtro_colaborador_id') && !empty($request->get('filtro_colaborador_id')) && !empty($request->get('filtro_colaborador'))) {
                if ($request->get('filtro_ferramenta') != SituacaoUtils::FILTRO_FERRAMENTA_TODAS) {
                    $query->where('cola_id', $request->get('filtro_colaborador_id'))->where('alem_status', $request->get('filtro_ferramenta'));
                }
            }
        }])->where($this->montarFiltro($request->all()))->get()->toArray();

        $colaborador = [];
        $consumo = [];
        $Nemprestadas = [];

        foreach ($ferramenta as $index => $item){
            if($item['almo_finalidade'] == SituacaoUtils::FINA_EMPRESTIMO) {
                if(!empty($item['emprestimo'])) {
                    foreach ($item['emprestimo'] as $emprestimo) {
                        $colaborador[$emprestimo['cola_id']][] = array_merge($emprestimo, $item);
                    }
                }else{
                    $Nemprestadas[] = $item;
                }

            }else{
                $consumo[] = $item;
            }

        }

        $file = new ordemServicoPDF('P', 'mm', 'A4');
        $file->SetCreator('Gestão Automotiva', true);
        $file->SetFont('Arial', 'B', 12);
        $file->AddPage();
        $file->SetLineWidth(0,5);
        $file->SetY(10);
        $file->SetX(150);
        $file->SetFont('Times','B',10);
        $file->Cell(3,3,'Data: ' . Carbon::now()->format('d/m/Y H:i:s') );
        $file->ln(5);
        $file->SetX(150);
        $file->SetFont('Times','B',10);
        $file->SetX($file->GetX()+10);
        $file->ln(18);
        $file->SetX(8);
        $file->SetFont('Times','B',12);
        $file->Cell(0, 7,utf8_decode('RELATÓRIO ALMOXERIFADO'),1,1, 'C');
        $file->ln(2);
        $file->SetX(8);
        $file->SetFillColor(192);
        $file->SetFont('Times','B',10);
        if(!empty($colaborador)) {
            foreach ($colaborador as $funcionario) {

                $nome = ColaboradorModel::with(['pessoaEndereco', 'pessoaContato', 'pessoaFuncao', 'pessoaLogin'])
                    ->where('cola_id', '=', $funcionario[0]['cola_id'])
                    ->first();
                $file->SetX(8);

                $file->Cell(0, 5, utf8_decode(strtoupper($nome['cola_nome'] ?? 'Emprestimos Externos')), 1, 1, 'C', 1);

                foreach ($funcionario as $item) {
                    $file->SetX(8);
                    if ($item['cola_id']) {
                        $file->Cell(193 * 0.05, 5, utf8_decode($item['alem_qtd']), 1, 0, 'C', 0);
                        $file->Cell(193 * 0.75, 5, utf8_decode($item['almo_titulo']), 1, 0, 'L', 0);
                        $file->Cell(0, 5, Carbon::createFromFormat('Y-m-d H:i:s', $item['alem_dt_ocorrencia'])->format('d/m/Y'), 1, 1, "C");
                    } else {
                        $file->Cell(193 * 0.05, 5, utf8_decode($item['alem_qtd']), 1, 0, 'C', 0);
                        $file->Cell(193 * 0.75, 5, utf8_decode($item['almo_titulo'] . ' / ' . substr($item['alem_responsavel'], 0, 48)), 1, 0, 'L', 0);
                        $file->Cell(0, 5, Carbon::createFromFormat('Y-m-d H:i:s', $item['alem_dt_ocorrencia'])->format('d/m/Y'), 1, 1, "C");
                    }
                }
            }
        }
        if(!empty($Nemprestadas)){
            $file->SetX(8);
            $file->Cell(0, 5, utf8_decode(strtoupper('FERRAMENTAS NÃO EMPRESTADAS')), 1, 1, 'C', 1);
            foreach ($Nemprestadas as $item) {
                $file->SetX(8);
                $file->Cell(193 * 0.05, 5, utf8_decode($item['almo_qtd']), 1, 0, 'C', 0);
                $file->Cell(193 * 0.75, 5, utf8_decode($item['almo_titulo']), 1, 0, 'L', 0);
                $file->Cell(0, 5, '', 1, 1, "C");
            }
        }
        if(!empty($consumo)){

            $file->SetX(8);
            $file->Cell(0, 5, utf8_decode(strtoupper('ITENS DE CONSUMO')), 1, 1, 'C', 1);
            $file->SetX(8);
            $file->Cell(193 * 0.05, 5, "Qtd.", 1, 0, 'C', 0);
            $file->Cell(193 * 0.75, 5, utf8_decode("Descrição"), 1, 0, 'C', 0);
            $file->Cell(0, 5, utf8_decode("Ultima compra"), 1, 1, "C");
            $file->SetX(8);
            foreach ($consumo as $item) {
                $file->Cell(193 * 0.05, 5, utf8_decode($item['almo_qtd']), 1, 0, 'C', 0);
                $file->Cell(193 * 0.75, 5, utf8_decode($item['almo_titulo']), 1, 0, 'L', 0);
                $file->Cell(0, 5, Carbon::createFromFormat('Y-m-d H:i:s', $item['updated_at'])->format('d/m/Y'), 1, 1, "C");
            }
        }
        $file->Output();
    }

    /**
     * @param $dados
     * @return array
     */
    private function montarFiltro($dados){
        $filtro = [
            ['empr_id', '=', Session::get('empr_id')]
        ];

        if($dados['filtro_sessao'] != 0 && !empty($dados['filtro_sessao'])){
            $filtro[] = ['almo_sessao', '=', $dados['filtro_sessao'] ];
        }
        if($dados['filtro_categoria'] != 0 && !empty($dados['filtro_categoria'])){
            $filtro[] = ['almo_categoria', '=', $dados['filtro_categoria'] ];
        }
        if($dados['filtro_sub_categoria'] != 0 && !empty($dados['filtro_sub_categoria'])){
            $filtro[] = ['almo_subcategoria', '=', $dados['filtro_sub_categoria'] ];
        }
        if($dados['filtro_finalidade'] == SituacaoUtils::FINA_EMPRESTIMO || $dados['filtro_finalidade'] == SituacaoUtils::FINA_CONSUMO){
            $filtro[] = ['almo_finalidade', '=', $dados['filtro_finalidade'] ];
        }
        if ($dados['filtro_ferramenta'] && !empty($dados['filtro_ferramenta']))
        {
            if($dados['filtro_ferramenta'] == SituacaoUtils::FILTRO_FERRAMENTA)
            {
                $filtro[] = ['almo_qtd_emprestimo', '>', 0];
            } else if($dados['filtro_ferramenta'] == SituacaoUtils::FILTRO_N_FERRAMENTA) {
                $filtro[] = ['almo_qtd_emprestimo',0];
            }
        }

        return $filtro;
    }

    /**
     * @param array $dadosEmprestimoAlmox
     * @return array
     * @throws \Exception
     */
    public function salvarEmprestimo(array $dadosEmprestimoAlmox)
    {
        $emprestimo = new AlmoxarifadoEmprestimoModel();

        $emprestimo->alem_responsavel = ($dadosEmprestimoAlmox['almo_empre_destino'] == SituacaoUtils::DEST_INTERNO) ? null : $dadosEmprestimoAlmox['almo_empre_responsavel'];
        $emprestimo->alem_destino = $dadosEmprestimoAlmox['almo_empre_destino'] ?? null;
        $emprestimo->alem_qtd = $dadosEmprestimoAlmox['almo_empre_quantidade'];
        $emprestimo->alem_dt_ocorrencia = Carbon::now();
        $emprestimo->cola_id = ($dadosEmprestimoAlmox['almo_empre_destino'] == SituacaoUtils::DEST_EXTERNO) ? null : $dadosEmprestimoAlmox['almo_empre_colaborador'];
        $emprestimo->almo_id = $dadosEmprestimoAlmox['almo_id'];
        $emprestimo->empr_id = Session::get('empr_id');

        if(!$emprestimo->save()){
            Common::setError('Houve um erro ao salvar os dados do Empréstimo!');
        }

        $almoxarifado = $this->findById($dadosEmprestimoAlmox['almo_id']);

        $almoxarifado->increment('almo_qtd_emprestimo', $dadosEmprestimoAlmox['almo_empre_quantidade']);

        $impressao = ($dadosEmprestimoAlmox['almo_empre_destino'] == SituacaoUtils::DEST_EXTERNO) ? url('almoxarifado/imprimir_termo_emprestimo?id='.$emprestimo->alem_id) : null;

        return ['success' => 1, 'impressao' => $impressao];
    }

    /**
     * @param int $idAlmox
     * @return AlmoxarifadoModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function findById(int $idAlmox)
    {
        return AlmoxarifadoModel::with(['grupo', 'unidadeMedida', 'emprestimo' => function($query){
            return $query->with(['colaborador'])->where('alem_status', 1);
        }])->where('almo_id', $idAlmox)->firstOrFail();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function tabela(Request $request)
    {
        $queryAlmox = AlmoxarifadoModel::query()->with(['unidadeMedida', 'emprestimo' => function($query){
            return $query->with(['colaborador'])->where('alem_status', SituacaoUtils::ALMOX_EMPRESTIMO_N_ENTREGUE);
        }])->where('empr_id', Session::get('empr_id'));

        return DataTables::eloquent($queryAlmox)
            ->filter(function($instance) use ($request){

                /* Filtrar registro da Respectiva Empresa logada */
                $instance->where('empr_id', Session::get('empr_id'));

                /* Filtrar por SESSAO */
                if ($request->has('filtro_sessao') && !empty($request->get('filtro_sessao')))
                {
                    $instance->where('almo_sessao', 'like', '%'. $request->get('filtro_sessao') .'%');
                }

                /* Filtrar por CATEGORIA */
                if ($request->has('filtro_categoria') && !empty($request->get('filtro_categoria')))
                {
                    $instance->where('almo_categoria', 'like', '%'. $request->get('filtro_categoria') .'%');
                }

                /* Filtrar por SUB-CATEGORIA */
                if ($request->has('filtro_sub_categoria') && !empty($request->get('filtro_sub_categoria')))
                {
                    $instance->where('almo_subcategoria', 'like', '%'. $request->get('filtro_sub_categoria') .'%');
                }

                /* Filtrar por finalidade - Consumo ou Emprestimo */
                if ($request->has('filtro_finalidade') && !empty($request->get('filtro_finalidade')))
                {
                    if($request->get('filtro_finalidade') == SituacaoUtils::FINA_EMPRESTIMO || $request->get('filtro_finalidade') == SituacaoUtils::FINA_CONSUMO)
                    {
                        $instance->where('almo_finalidade', $request->get('filtro_finalidade'));
                    }
                }

                /* Filtrar por Colaborador */
                if ($request->has('filtro_colaborador_id') && !empty($request->get('filtro_colaborador_id')) && !empty($request->get('filtro_colaborador')))
                {
                    $instance->whereHas('emprestimo', function($query) use ($request){
                        $query->where('cola_id', $request->get('filtro_colaborador_id'))
                            ->where('alem_status', SituacaoUtils::ALMOX_EMPRESTIMO_N_ENTREGUE);
                    });
                }

                /* Filtrar por FERRAMENTAS - Emprestadas e Nao Emprestadas */
                if ($request->has('filtro_ferramenta') && !empty($request->get('filtro_ferramenta')))
                {
                    if($request->get('filtro_ferramenta') == SituacaoUtils::FILTRO_FERRAMENTA)
                    {
                        $instance->where('almo_qtd_emprestimo', '>', 0);
                    } else if($request->get('filtro_ferramenta') == SituacaoUtils::FILTRO_N_FERRAMENTA) {
                        $instance->where('almo_qtd_emprestimo', 0);
                    }
                }

                /* Filtrar utilizando o Procurar */
                if($request->has('search') && !empty($request->get('search')['value']))
                {
                    $instance->where('almo_titulo', 'like', '%'.$request->get('search')['value'].'%')
                        ->orWhere('almo_cod_barra', 'like', '%'.$request->get('search')['value'].'%')
                        ->orWhere('almo_id', 'like', '%'.$request->get('search')['value'].'%');
                }

                return $instance;
            })
            ->editColumn('almo_qtd', function($almox){
                return ($almox->almo_qtd-$almox->almo_qtd_emprestimo);
            })
            ->editColumn('grup_titulo', function($almox){
                if($almox->grupo()->first())
                {
                    $grupo = $almox->grupo()->first();

                    return $grupo->grup_titulo;
                }
            })
            ->editColumn('unme_titulo', function($almox){
                if($almox->unidadeMedida()->first())
                {
                    $unidMedida = $almox->unidadeMedida()->first();

                    return $unidMedida->unme_sigla;
                }

                return '';
            })
            ->editColumn('almo_finalidade', function($almox){
                return SituacaoUtils::getTituloFinalidade($almox->almo_finalidade);
            })
            ->editColumn('action', function($almox){
                if($almox->almo_finalidade == SituacaoUtils::FINA_CONSUMO)
                {
                    return $this->btnTabelaConsumo($almox->almo_id, ($almox->almo_qtd-$almox->almo_qtd_emprestimo));
                }

                if($almox->almo_finalidade == SituacaoUtils::FINA_EMPRESTIMO)
                {
                    return $this->btnTabelaEmprestimo($almox->almo_id, $almox->almo_qtd, $almox->almo_qtd_emprestimo);
                }

                return "";
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * @param $id
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function deletarAlmoxarifado($id){
        $almox = $this->findById($id);

        return $almox->delete();
    }

    /**
     * @param $idAlmox
     * @param $almoxQtd
     * @param $emprQtd
     * @return string
     */
    private function btnTabelaEmprestimo($idAlmox, $almoxQtd, $emprQtd)
    {
        $qtdItem = ($almoxQtd-$emprQtd);

        $urlEdit = "/almoxarifado/". $idAlmox ."/edit";

        $botao = '<button type="button" data-item="'. $qtdItem .'" data-almox="'. $idAlmox .'" class="btn btn-default btn-circle margin-right-5" title="Realizar Empréstimo" onclick="jQueryAlmoxarifado.emprestimo($(this))"><i class="fa fa-male"></i></button>';

        if($emprQtd)
        {
            $botao .= '<button type="button" data-almox="'. $idAlmox .'" class="btn btn-default btn-circle margin-right-5" title="Detalhamento" onclick="jQueryAlmoxarifado.detalhe($(this))"><i class="fa fa-eye"></i></button>';
        }

        $botao .= '<a href="'. url($urlEdit) .'" class="btn btn-default btn-circle margin-right-5" title="Editar Item"><i class="fa fa-edit"></i></a>';


        $botao .= '<button type="button" data-almox="'. $idAlmox .'" class="btn btn-default btn-circle" title="Excluir Item" onclick="jQueryAlmoxarifado.deletarAlmoxItem($(this))"><i class="fa fa-trash-o"></i></button>';

        return $botao;
    }

    /**
     * @param $idAlmox
     * @param $qtdItem
     * @return string
     */
    private function btnTabelaConsumo($idAlmox, $qtdItem)
    {
        $urlEdit = "/almoxarifado/". $idAlmox ."/edit";

        $botao = '<a href="'. url($urlEdit) .'" class="btn btn-default btn-circle margin-right-5" title="Editar Item"><i class="fa fa-edit"></i></a>';
//        $botao .= '<button type="button" class="btn btn-default btn-circle" title="Consumo Interno"><i class="fa fa-dropbox"></i></button>';
        $botao .= '<button type="button" data-almox="'. $idAlmox .'" class="btn btn-default btn-circle" title="Excluir Item" onclick="jQueryAlmoxarifado.deletarAlmoxItem($(this))"><i class="fa fa-trash-o"></i></button>';

        return $botao;
    }
}
