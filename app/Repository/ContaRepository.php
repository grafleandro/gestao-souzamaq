<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 15/01/19
 * Time: 18:15
 */

namespace App\Repository;


use App\Model\EmpresaModel;
use App\Utils\Common;
use App\Utils\Mask;
use App\Utils\SituacaoUtils;

class ContaRepository
{
    /**
     * @param $dadosEmpresa
     * @param $idEmpresa
     * @return array
     * @throws \Exception
     */
    public function atualizarDados($dadosEmpresa, $idEmpresa)
    {
        $empresa = EmpresaModel::with(['empresaEndereco', 'empresaContato'])->find($idEmpresa);

        if(isset($dadosEmpresa['empresa']) && $dadosEmpresa['empresa']) {
            $empresa->empr_nome = $dadosEmpresa['nome'];
            $empresa->empr_razao_social = $dadosEmpresa['razao_social'];
            $empresa->empr_cnpj = Mask::removerMascara($dadosEmpresa['cnpj']);
            $empresa->empr_insc_estadual = $dadosEmpresa['inscricao_estadual'] ?? null;
            $empresa->empr_status = $dadosEmpresa['situacao'] ?? null;
        }

        if(isset($dadosEmpresa['endereco']) && $dadosEmpresa['endereco']) {
            $empresa->empresaEndereco->enlo_id = $dadosEmpresa['tipo_logradouro'] ?? null;
            $empresa->empresaEndereco->ende_logradouro_titulo = $dadosEmpresa['rua'] ?? null;
            $empresa->empresaEndereco->ende_bairro = $dadosEmpresa['bairro'] ?? null;
            $empresa->empresaEndereco->ende_cidade = $dadosEmpresa['cidade'] ?? null;
            $empresa->empresaEndereco->ende_estado = $dadosEmpresa['estado'] ?? null;
            $empresa->empresaEndereco->ende_complemento = $dadosEmpresa['complemento'] ?? null;
            $empresa->empresaEndereco->ende_numero = $dadosEmpresa['numero'] ?? null;
            $empresa->empresaEndereco->ende_cep = $dadosEmpresa['cep'] ?? null;

            $empresa->empresaEndereco->save();
        }

        if(isset($dadosEmpresa['contato']) && $dadosEmpresa['contato']) {
            $empresa->empresaContato->cont_cel_1 = Mask::removerMascara($dadosEmpresa['celular_1'] ?? '');
            $empresa->empresaContato->cont_cel_2 = Mask::removerMascara($dadosEmpresa['celular_2'] ?? '');
            $empresa->empresaContato->cont_tel_fixo = Mask::removerMascara($dadosEmpresa['tel_fixo'] ?? '');
            $empresa->empresaContato->cont_email = $dadosEmpresa['email'] ?? null;
            $empresa->empresaContato->save();
        }

        if(!$empresa->save()){
            Common::setError('Houve um erro ao atualizar os dados da Empresa!');
        }

        return ['success' => $empresa->empr_id];
    }
}
