<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 25/01/19
 * Time: 09:20
 */

namespace App\Repository;


use App\Model\ComissaoModel;
use App\Utils\Common;
use App\Utils\Mask;
use Illuminate\Support\Facades\Session;

class ComissaoRepository
{

    /**
     * @param array $dadosComissao
     * @return array
     * @throws \Exception
     */
    public function salvarDados(array $dadosComissao){
        return $this->salvar(new ComissaoModel(), $dadosComissao);
    }

    /**
     * @param array $dadosComissao
     * @param int $idComissao
     * @return array
     * @throws \Exception
     */
    public function atualizarDados(array $dadosComissao, int $idComissao){
        $comissao = $this->findById($idComissao);

        return $this->salvar($comissao, $dadosComissao);
    }

    /**
     * @param ComissaoModel $comissaoModel
     * @param array $dadosComissao
     * @return array
     * @throws \Exception
     */
    public function salvar(ComissaoModel $comissaoModel, array $dadosComissao){

        $comissaoModel->coco_liquido_bruto = $dadosComissao['comissao_tipo'] ?? null;
        $comissaoModel->coco_cota_comissao = Mask::removarMascaraPorcent($dadosComissao['cota_comissao']);
        $comissaoModel->coco_cota_empresa = Mask::removarMascaraPorcent($dadosComissao['cota_empresa']);
        $comissaoModel->coco_modulo_venda = $dadosComissao['modulo_comissao_produto'] ?? null;
        $comissaoModel->coco_modulo_os = $dadosComissao['modulo_comissao_os'] ?? null;
        $comissaoModel->empr_id = Session::get('empr_id');

        if(!$comissaoModel->save()){
            Common::setError('Houve um erro ao salvar os dados da Comissão!');
        }

        return ['success' => $comissaoModel->coco_id];
    }

    /**
     * @param int|null $idComissao
     * @return mixed
     */
    public function findById(int $idComissao = null){
        return ComissaoModel::where('empr_id', Session::get('empr_id'))->orWhere('coco_id', $idComissao)->first();
    }
}
