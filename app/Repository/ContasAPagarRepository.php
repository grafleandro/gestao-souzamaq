<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 04/12/18
 * Time: 23:41
 */

namespace App\Repository;


use App\Model\CentroCustoModel;
use App\Model\ContaBancariaModel;
use App\Model\ContasAPagarCategoriaModel;
use App\Model\ContasAPagarModel;
use App\Model\ContasAPagarParcelasModel;
use App\Model\FormaPgtoModel;
use App\Utils\Common;
use App\Utils\Mask;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class ContasAPagarRepository
{

    /**
     * @param array $contasPagar
     * @return array
     * @throws \Exception
     */
    public function salvarDados(array $contasPagar){
        return $this->salvar(new ContasAPagarModel(), $contasPagar);
    }

    /**
     * @param array $dadosContaPagar
     * @param int $idContaPagar
     * @return array
     * @throws \Exception
     */
    public function atualizarDados(array $dadosContaPagar, int $idContaPagar){
        $contaPagar = $this->findById($idContaPagar);

        return $this->salvar($contaPagar, $dadosContaPagar);
    }

    /**
     * @param ContasAPagarModel $contasAPagarModel
     * @param array $contasPagar
     * @return array
     * @throws \Exception
     */
    public function salvar(ContasAPagarModel $contasAPagarModel, array $contasPagar){

        $contasAPagarModel->copa_descricao = $contasPagar['cp_descricao'];
        $contasAPagarModel->copa_dt_competencia = (isset($contasPagar['cp_dt_competencia']) && $contasPagar['cp_dt_competencia']) ? Carbon::createFromFormat('d/m/Y', $contasPagar['cp_dt_competencia'])->format('Y-m-d') : null;
        $contasAPagarModel->copa_dt_vencimento = (isset($contasPagar['cp_dt_vencimento']) && $contasPagar['cp_dt_vencimento']) ? Carbon::createFromFormat('d/m/Y', $contasPagar['cp_dt_vencimento'])->format('Y-m-d') : null;
        $contasAPagarModel->copa_valor = Mask::removerMascaraDinheiro($contasPagar['cp_valor_total']);
        $contasAPagarModel->copa_parcelas =(int) $contasPagar['cp_parcelas'];
        $contasAPagarModel->copa_parcelas_qtd = ($contasPagar['cp_qtd_parcelas']) ?? 0;
        $contasAPagarModel->copa_observacao = $contasPagar['cp_observacao'] ?? null;
        $contasAPagarModel->copc_id =(int) $contasPagar['copc_id'];
        $contasAPagarModel->cocb_id =(int) $contasPagar['cp_banco'];
        $contasAPagarModel->cocc_id =(int) $contasPagar['cp_centro_custo'];

        if(!$contasAPagarModel->save()){
            Common::setError('Houve um erro ao salvar os dados de Contas à Pagar!');
        }

        if($contasPagar['cp_parcelas']){
            ContasAPagarParcelasModel::where('copa_id', $contasAPagarModel->copa_id)->delete();

            foreach ($contasPagar['parcelas'] as $index => $parcela){
                ContasAPagarParcelasModel::create([
                    'copp_valor' =>(float) $parcela['valor'],
                    'copp_dt_vencimento' => Carbon::createFromFormat('d/m/Y', $parcela['data_vencimento'])->format('Y-m-d'),
                    'copa_id' => $contasAPagarModel->copa_id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }

        return ['success' => 1];
    }

    /**
     * @param int $idConta
     * @return ContasAPagarModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function findById(int $idConta){
        return ContasAPagarModel::with(['categoria', 'contaBancaria', 'centroCusto', 'parcelas'])
            ->where('copa_id', $idConta)
            ->firstOrFail();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function tabela(){
        $queryContasPagar = ContasAPagarModel::query()->with(['categoria', 'contaBancaria', 'centroCusto', 'parcelas']);

        return DataTables::eloquent($queryContasPagar)
            ->editColumn('copc_titulo', function($contasPagar){
                return $contasPagar->categoria->copc_titulo;
            })
            ->editColumn('cocc_titulo', function($contasPagar){
                return $contasPagar->centroCusto->cocc_titulo;
            })
            ->editColumn('copa_dt_vencimento', function($contasPagar){
                return Carbon::parse($contasPagar->copa_dt_vencimento)->format('d/m/Y');
            })
            ->editColumn('copa_valor', function($contasPagar){
                return Mask::dinheiro($contasPagar->copa_valor);
            })
            ->editColumn('action', function($contasPagar){
                $urlEdit = url('financeiro/contas-pagar/' . $contasPagar->copa_id . '/edit');

                $return = '<a href="'. $urlEdit .'" class="btn btn-default btn-circle margin-right-5" title="Editar "><i class="fa fa-edit"></i></a>';
                $return .= '<button type="button" data-conta="'. $contasPagar->copa_id .'" class="btn btn-default btn-circle margin-right-5" title="Excluir" onclick="jQueryContasPagar.deletarConta($(this))"><i class="fa fa-trash-o"></i></button>';
                $return .= '<button type="button" data-conta="'. $contasPagar->copa_id .'" class="btn btn-default btn-circle margin-right-5" title="Visualizar" onclick="jQueryContasPagar.visualizarConta($(this))"><i class="fa fa-eye"></i></a>';
//                $return .= '<button class="btn btn-default btn-circle" title="Imprimir"><i class="fa fa-print"></i></button>';

                return $return;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * @param $id
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function deletarContaPagar($id){
        $contaPagar = $this->findById($id);

        return $contaPagar->delete();
    }

    /**
     * @return mixed
     */
    public function categoria(){
        return ContasAPagarCategoriaModel::orderBy('copc_titulo', 'asc')->get();
    }

    /**
     * @return CentroCustoModel[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function centroCusto(){
        return CentroCustoModel::with(['empresa'])
            ->where('empr_id', Session::get('empr_id'))
            ->get();
    }

    /**
     * @return FormaPgtoModel[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function formaPgto(){
        return FormaPgtoModel::with(['formaPgtoEmpresa'])
            ->where('empr_id', Session::get('empr_id'))
            ->get();
    }

    /**
     * @return ContaBancariaModel[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function contasBancarias(){
        return ContaBancariaModel::with(['banco', 'empresa'])
            ->where('empr_id', Session::get('empr_id'))
            ->get();
    }
}
