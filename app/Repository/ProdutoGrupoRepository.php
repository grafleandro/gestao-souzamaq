<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 29/10/18
 * Time: 23:26
 */

namespace App\Repository;


use App\Model\ProdutoGrupoModel;
use App\Utils\Common;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class ProdutoGrupoRepository
{
    /**
     * @param array $dadosGrupo
     * @return array
     * @throws \Exception
     */
    public function salvarDados($dadosGrupo = []){
        $resposta = ProdutoGrupoModel::create([
            'grup_titulo' => $dadosGrupo['gp_titulo'],
            'grup_descricao' => $dadosGrupo['gp_descricao'],
            'empr_id' => Session::get('empr_id')
        ]);

        if(!$resposta->save()){
            Common::setError('Não foi possível adicionar o novo Grupo!');
        }

        return ['success' => $resposta->grup_id];
    }

    /**
     * @param array $dadosGrupo
     * @param int $idGrupo
     * @return array
     * @throws \Exception
     */
    public function atualizarDados($dadosGrupo = [], int $idGrupo){
        $grupo = $this->findById($idGrupo);

        $grupo->grup_titulo     = $dadosGrupo['gp_titulo'];
        $grupo->grup_descricao  = $dadosGrupo['gp_descricao'];

        if(!$grupo->save()){
            Common::setError('Erro ao atualizar os dados do Grupo!');
        }

        return ['success' => 1];
    }

    /**
     * @param int $idGrupo
     * @return mixed
     */
    public function findById(int $idGrupo){
        return $dadosGrupo = ProdutoGrupoModel::where('grup_id', '=', $idGrupo)
            ->where('empr_id', '=', Session::get('empr_id'))
            ->first();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $dadosGrupo = ProdutoGrupoModel::where('empr_id', '=', Session::get('empr_id'))
            ->get()
            ->toArray();

        return Datatables::of($dadosGrupo)
            ->editColumn('qtd_produto', function($grupo){
                return 0;
            })
            ->editColumn('action', function($grupo){
                $urlEdit = "/empresa/grupo/". $grupo['grup_id'] ."/edit";

                return '<div style="text-align: center">
                                <a href="'. $urlEdit .'" title="Editar Grupo" data-grupo="'.$grupo['grup_id'].'" style="margin: 4px" class="btn btn-default btn-circle"><i class="fa fa-edit"></i></a>
                                <button title="Excluir Grupo" data-grupo="'.$grupo['grup_id'].'" style="margin: 4px" class="btn btn-default btn-circle" onclick="jQueryGrupo.deletarGrupo($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->make(true);
    }

    /**
     * @param int $idGrupo
     * @return mixed
     */
    public function deletarGrupo(int $idGrupo){
        $grupo = $this->findById($idGrupo);

        return $grupo->delete();
    }

    /**
     * @param $search
     * @return mixed
     */
    public function autocomplete($search){

        if(strlen($search) == 2 && $search == '**'){
            $dadosGrupo = ProdutoGrupoModel::where('empr_id', '=', Session::get('empr_id'))
                ->get()
                ->toArray();
        } else {
            $dadosGrupo = ProdutoGrupoModel::where('empr_id', '=', Session::get('empr_id'))
                ->where('grup_titulo', 'like', "{$search}%")
                ->get()
                ->toArray();
        }

        return $dadosGrupo;
    }
}
