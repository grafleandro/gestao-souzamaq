<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 27/12/18
 * Time: 09:34
 */

namespace App\Repository;


use App\Model\AlmoxarifadoEmprestimoModel;
use App\Utils\Common;

class AlmoxarifadoEmprestimoRepository
{
    private $almoxarifadoRepository;

    public function __construct(AlmoxarifadoRepository $almoxarifadoRepository)
    {
        $this->almoxarifadoRepository = $almoxarifadoRepository;
    }

    /**
     * @param array $dadosEmpr
     * @param int $idEmpr
     * @return array
     * @throws \Exception
     */
    public function alterarStatus(array $dadosEmpr, int $idEmpr)
    {
        $emprestimo = $this->findById($idEmpr);

        $emprestimo->alem_status =(int) $dadosEmpr['status'];

        if(!$emprestimo->save()){
            Common::setError('Houve um erro ao atualizar o Status!');
        }

        $almoxarifado = $this->almoxarifadoRepository->findById($emprestimo->almo_id);

        $almoxarifado->decrement('almo_qtd_emprestimo', $emprestimo->alem_qtd);

        return ['success' => 1];
    }

    /**
     * @param int $idEmpr
     * @return mixed
     */
    public function findById(int $idEmpr)
    {
        return AlmoxarifadoEmprestimoModel::where('alem_id', $idEmpr)->firstOrFail();
    }
}
