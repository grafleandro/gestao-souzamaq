<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 27/09/18
 * Time: 22:58
 */

namespace App\Repository;


use App\Model\CheckListExternoModel;
use App\Utils\Common;
use App\Utils\Mask;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class CheckListExternoRepository
{
    private $CheckListExternoModel;

    public function __construct(CheckListExternoModel $checkListExternoModel)
    {
        $this->CheckListExternoModel = $checkListExternoModel;
    }

    /**
     * @param array $dadosColaborador
     * @return array
     * @throws \Exception
     */
    public function salvarDados($dadosCheckListExterno = []){

        $checkListExterno = $this->salvar(new CheckListExternoModel(), $dadosCheckListExterno);

        return $checkListExterno->chex_id;
    }

    /**
     * @param array $dadosColaborador
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function atualizarDados($dadosCheckListExterno = [], int $id){

        $externo = $this->findById($id);

        return $this->salvar($externo, $dadosCheckListExterno)->chex_id;

    }

    /**
     * @param int $idCliente
     * @return ClienteModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function findById($idExterno = 0){
        return CheckListExternoModel::where('chex_id', $idExterno)->first();
    }


    /**
     * @param CheckListExternoModel $checkListExternoModel
     * @param array $dadosCheckListExterno
     * @return CheckListExternoModel
     */
    public function salvar(CheckListExternoModel $checkListExternoModel, $dadosCheckListExterno = []){

        /* Salvando os dados do CheckList Externo */
        $checkListExternoModel->chex_cliente = $dadosCheckListExterno['cliente'];
        $checkListExternoModel->chex_responsavel = $dadosCheckListExterno['responsavel'];
        $checkListExternoModel->chex_placa = Mask::removerMascara($dadosCheckListExterno['placa']);
        $checkListExternoModel->chex_num_os = $dadosCheckListExterno['numero_os'];
        $checkListExternoModel->chex_status = $dadosCheckListExterno['status'];
        $checkListExternoModel->chex_descricao = $dadosCheckListExterno['descricao'];
        $checkListExternoModel->empr_id = Session::get('empr_id');

        if(!$checkListExternoModel->save()){
            Common::setError('Erro ao salvar os dados do Cliente Pessoa Física!');
        }

        return $checkListExternoModel;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){

        $dadosChecklistExterno = CheckListExternoModel::where('empr_id', '=', Session::get('empr_id'))->get()->toArray();

        return Datatables::of($dadosChecklistExterno)

            ->editColumn('chex_placa', function($externo){
                return strtoupper(Mask::placaVeiculo($externo['chex_placa']));
            })
            ->addColumn('action', function ($externo){
                $urlEdit = "oficina/checklist/externo/". $externo['chex_id'] ."/edit";

                return '<div style="text-align: center">
                                <a href="'. url($urlEdit) .'" title="Editar CheckList Externo" data-externo="'.$externo['chex_id'].'" style="margin: 4px" class="btn btn-default btn-circle"><i class="fa fa-edit"></i></a>
                                <button title="Excluir CheckList Externo" data-externo="'.$externo['chex_id'].'" style="margin: 4px" class="btn btn-default btn-circle" onclick="jQueryCheckListExterno.deletarCheckListExterno($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->make(true);
    }

    /**
     * @param $id
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function deletarCheckListExterno($id){

        $externo = $this->findById($id);

        return $externo->delete();
    }
}