<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 25/11/18
 * Time: 14:30
 */

namespace App\Repository;


use App\Http\Requests\VendaBalcaoRequest;
use App\Model\ContasReceberModel;
use App\Model\OperacaoPagamentoModel;
use App\Model\OperacaoParcelasModel;
use App\Model\VendaBalcaoModel;
use App\Model\VendaBalcaoPagamentoModel;
use App\Model\VendaBalcaoParcelasModel;
use App\Model\VendaBalcaoProdutoModel;
use App\Utils\Common;
use App\Utils\Mask;
use App\Utils\MoneyUtils;
use App\Utils\SituacaoUtils;
use App\Utils\VendaUtils;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class VendaBalcaoRepository
{
    /* Identificar a origem da Contas a Receber Lancada
     * VB: Venda Balcao
     * */
    const OPERACAO_ORIGEM_VB = 2;

    private $balcaoModel;
    private $produtoRepository;

    public function __construct(VendaBalcaoModel $balcaoModel, ProdutoRepository $produtoRepository)
    {
        $this->balcaoModel      = $balcaoModel;
        $this->produtoRepository= $produtoRepository;
    }

    /**
     * @param VendaBalcaoRequest $balcaoRequest
     * @return array
     * @throws \Exception
     */
    public function salvarDados(VendaBalcaoRequest $balcaoRequest){
        $dadosVenda = $balcaoRequest->all();

        /** Verificar a disponibilidade dos produtos em Estoque */
        $this->consultarProdutoEstoque($dadosVenda['produto']);

        $this->balcaoModel->veba_venda_total =(float) $dadosVenda['total_venda'];
        $this->balcaoModel->veba_venda_troco = MoneyUtils::subtrair($dadosVenda['total_pgto'], $dadosVenda['total_venda']);
        $this->balcaoModel->veba_total_pago = (float) $dadosVenda['total_pgto'];
        $this->balcaoModel->veba_qtd_produto = count($dadosVenda['produto']);
        $this->balcaoModel->cola_id =(int) $balcaoRequest->user()->cola_id;
        $this->balcaoModel->empr_id =(int) Session::get('empr_id');
        $this->balcaoModel->vest_id =(int) $this->vericarPossuiParcelas($dadosVenda);
        $this->balcaoModel->clie_id =(int) $dadosVenda['clie_consumidor'];
        $this->balcaoModel->created_at = Carbon::now();
        $this->balcaoModel->updated_at = Carbon::now();

        if(!$this->balcaoModel->save()){
            Common::setError('Houve um erro ao salvar os dados da Venda!');
        }

        /** Salvando os dados do Pagamento */
        foreach ($dadosVenda['pagamento'] as $index => $pagamento){

            /** Salvando os dados das Parcelas, caso tenha */
            if(!empty($pagamento['parcela']))
            {
                /** Criar lancamento em Contas a Receber */
                $contasReceber = ContasReceberModel::create([
                    'core_titulo' => 'Contas a Receber - Venda Balcão',
                    'core_valor' => $pagamento['valor'],
                    'core_dt_ocorrencia' => Carbon::now(),
                    'core_origem_id' => $this->balcaoModel->veba_id,
                    'opor_id' => self::OPERACAO_ORIGEM_VB,
                    'core_juros' => 0,
                    'vest_id' => VendaUtils::VEND_ANDAMENTO,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);

                $opPagamento = OperacaoPagamentoModel::create([
                    'oppa_valor' => $pagamento['valor'],
                    'oppa_parcelas' => $pagamento['parcela'] ?? 0,
                    'oppa_desconto' => $pagamento['desconto'] ?? 0,
                    'cofp_id' => (int) $pagamento['tipo_pgto'],
                    'core_id' => $contasReceber->core_id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);

                $valorParcela = MoneyUtils::dividir($pagamento['valor'], $pagamento['parcela']);

                for($i = 1; $i <= $pagamento['parcela']; $i++){
                    $opPgtoParcelas = OperacaoParcelasModel::create([
                        'oppr_valor' => $valorParcela,
                        'oppr_dt_vencimento' => Carbon::now()->addMonths($i),
                        'oppa_id' => $opPagamento->oppa_id,
                    ]);
                }
            } else {
                /* Salvando as formas de pagamento Avista */
                $pagamentoModel = VendaBalcaoPagamentoModel::create([
                    'vbpa_valor' => (float) $pagamento['valor'],
                    'vbpa_parcelas' => (int) $pagamento['parcela'],
                    'cofp_id' => (int) $pagamento['tipo_pgto'],
                    'veba_id' => $this->balcaoModel->veba_id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }

        /** Salvando os dados do Produto da Venda */
        if(isset($dadosVenda['produto']) && !empty($dadosVenda['produto'])){
            $this->produtoRepository->salvarMovimentacao($dadosVenda['produto'], $this->balcaoModel->veba_id, self::OPERACAO_ORIGEM_VB);
        }

//        foreach ($dadosVenda['produto'] as $index => $produto){
//
//            VendaBalcaoProdutoModel::create([
//                'vebp_valor' =>(float) $produto['prod_valor'],
//                'vebp_quantidade' =>(float) $produto['prod_qtd'],
//                'prod_id' =>(int) $produto['prod_id'],
//                'veba_id' => $this->balcaoModel->veba_id,
//                'created_at' => Carbon::now(),
//                'updated_at' => Carbon::now(),
//            ]);
//
//            if(!$this->produtoRepository->atualizarEstoqueAtual($produto)){
//                Common::setError('Houve um erro ao atualizar o valor do Estoque!');
//            }
//        }


        return ['success' => 1];
    }

    /**
     * @param $dadosVenda
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function atualizarDados($dadosVenda, $id){

        switch ($dadosVenda['venda_status']){
            case VendaUtils::VEND_ANDAMENTO:
                break;
            case VendaUtils::VEND_AGUARDANDO:
                break;
            case VendaUtils::VEND_FINALIZADA:
                break;
            case VendaUtils::VEND_CANCELADA:
                return $this->cancelarVenda($id);
                break;
            case VendaUtils::VEND_DEVOLUCAO:
                break;
            case VendaUtils::VEND_ATRASADA:
                break;
        }
    }

    /**
     * @param $idVenda
     * @return VendaBalcaoModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function findById($idVenda){
        return VendaBalcaoModel::with(['colaborador', 'empresa', 'pagamento', 'status', 'contasReceber'])
            ->where('veba_id', $idVenda)
            ->firstOrFail();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $queryVenda = VendaBalcaoModel::query()
            ->with(['colaborador', 'empresa', 'pagamento', 'contasReceber', 'produto', 'status'])
            ->where('empr_id', Session::get('empr_id'));

        return DataTables::eloquent($queryVenda)
            ->editColumn('veba_venda_total', function ($venda){
                return Mask::dinheiro($venda->veba_venda_total);
            })
            ->editColumn('parcelas', function ($venda){

                if($venda->contasReceber()->first()){
                    $idContasReceber  = $venda->contasReceber()->first()->core_id;
                    $paracelasPagas   = $venda->parcelas()->where('oppr_status', 1)->where('core_id', $idContasReceber)->count();
                    $paracelas    = $venda->parcelas()->where('core_id', $idContasReceber)->count();

                    return "{$paracelasPagas}/{$paracelas}";
                }

                return '0';
            })
            ->editColumn('forma_pgto', function($venda){
                if($venda->contasReceber()->first()){
                    $tituloFromPgto = OperacaoPagamentoModel::with(['formaPgto'])->where('core_id', $venda->contasReceber()->first()->core_id)->first()->toArray();

                    $tituloFromPgto = $tituloFromPgto['forma_pgto']['cofp_titulo'];
                } else {
                    $tituloFromPgto =  $venda->pagamento()->first()->with(['formaPagamento'])->first()->formaPagamento()->first()->cofp_titulo;
                }

                return $tituloFromPgto;
            })
            ->editColumn('produto', function($venda){
                $produtoTitulo = [];

                foreach ($venda->produto()->get() as $index => $produto){
                    $produtoTitulo[] = (new ProdutoRepository())->findById($produto->prod_id)->prod_titulo;
                }

                return implode('; ', $produtoTitulo);
            })
            ->editColumn('vest_id', function($venda){
                return '<span class="badge '. $venda->status()->first()->vest_class .'">'. $venda->status()->first()->vest_titulo .'</span>';
            })
            ->addColumn('action', function ($venda) {
                return  '
                        
                        <div class="btn-group">
                            <button class="btn btn-default btn-circle">
                                <i class="glyphicon glyphicon-list"></i>
                            </button>
                            <button class="btn btn-default btn-circle dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="javascript:void(0);" data-venda="'. $venda->veba_id .'" onclick="jQueryVenda.deletarVenda($(this))"><i class="fa fa-trash-o"></i> Deletar Venda</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" data-venda="'. $venda->veba_id .'" onclick="jQueryVenda.cancelarVenda($(this))"><i class="fa fa-times"></i> Cancelar Venda</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);"><i class="fa fa-check"></i> Finalizar</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);"><i class="fa fa-reply-all"></i> Devolução</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="javascript:void(0);"><i class="fa fa-exchange"></i> Trocar</a>
                                </li>
                            </ul>
                        </div>';
            })
            ->rawColumns(['vest_id', 'action'])
            ->make(true);
    }

    private function vericarPossuiParcelas(array $dadosVenda){

        foreach ($dadosVenda['pagamento'] as $index => $pagamento){

            if(!empty($pagamento['parcela'])){
                return VendaUtils::VEND_ANDAMENTO;
            }
        }

        return VendaUtils::VEND_FINALIZADA;
    }

    /**
     * @param array $dadosProduto
     * @return void
     * @throws \Exception
     */
    private function consultarProdutoEstoque(array $dadosProduto){
        foreach ($dadosProduto as $index => $produto){
            $this->produtoRepository->consultarEstoque($produto);
        }
    }

    /**
     * @param $idVenda
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function deletarVenda($idVenda){
        $venda = $this->findById($idVenda);

        $venda->contasReceber()->where('core_origem_id', $idVenda)->where('opor_id', self::OPERACAO_ORIGEM_VB)->delete();

        return $venda->delete();
    }

    /**
     * @param $id
     * @return array
     * @throws \Exception
     */
    private function cancelarVenda($id){
        $venda = $this->findById($id);

        foreach ($venda->produto()->get() as $vendaProduto){
            $produto = (new ProdutoRepository())->findById($vendaProduto->prod_id);

            $produto->produtoEstoque()->updated_at = Carbon::now();
            $produto->produtoEstoque()->increment('prest_estoq_atual', $vendaProduto->vebp_quantidade);

            if(!$produto->save()){
                Common::setError('Houve um erro ao reajustar o estoque deste produto!');
            }
        }

        if($venda->contasReceber()->first()){
            $contasReceber = $venda->contasReceber()->first();

            $contasReceber->updated_at = Carbon::now();
            $contasReceber->vest_id = SituacaoUtils::VEND_CANCELADA;

            if(!$contasReceber->save()){
                Common::outPut('Houve um erro ao atualizar o Status da Venda!');
            }
        }

        /* Atualizar Status da Venda */
        $venda->vest_id = SituacaoUtils::VEND_CANCELADA;

        if(!$venda->save()){
            Common::setError('Houve um erro ao Cancelar esta venda!');
        }

        return ['success' => 1];
    }
}
