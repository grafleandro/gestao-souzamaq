<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 27/11/18
 * Time: 15:27
 */

namespace App\Repository;


use App\Model\MenuModel;
use App\Model\MenuPermissaoModel;
use App\Utils\Common;
use Illuminate\Http\Request;

class ColaboradorPermissaoRepository
{
    /**
     * @param array $dadosPermissao
     * @return array
     * @throws \Exception
     */
    public function salvarDados(array $dadosPermissao){

        /* Remove todos os Menus referente ao usuario para ser inserido novamente */
        MenuPermissaoModel::where('cola_id', $dadosPermissao['cola_id'])->delete();

        if(!empty($dadosPermissao['permissao'])){
            foreach ($dadosPermissao['permissao'] as $value){
                MenuPermissaoModel::create([
                    'cola_id' => $dadosPermissao['cola_id'],
                    'menu_id' => $value,
                ]);
            }

            return ['success' => 1];
        }

        Common::setError('Não foi selecionado nenhum menu!');
    }

    public function permissaoMenu(Request $request){
        $menuIdPermissao    = [];

        \Menu::make('MenuPermissao', function ($objMenu) use ($request, &$menuIdPermissao){

            /** Verifica se o menu jah foi carregado na SESSION, minimizando uma consulta no Servidor */
            $menus = MenuModel::get()->toArray();
            $menusPermissao     = MenuPermissaoModel::where('cola_id', $request->user()->cola_id)->get()->toArray();
            $menuIdPermissao    = array_column($menusPermissao, 'menu_id');

            foreach ($menus as $index => $menu){
                $tituloMenu = $menu['menu_texto'];

                $dadosMenu  = [
                    'url'       => $menu['menu_href'],
                    'parameter' => $menu['menu_parametros'],
                    'sequence'  => $menu['menu_sequencia'],
                    'acesso'    => in_array($menu['menu_id'], $menuIdPermissao),
                    'class' => $menu['menu_class'],
                    'id'    => $menu['menu_id'],
                    'icon'  => $menu['menu_icone'],
                    'node'  => $menu['menu_pai']
                ];

                if(empty($menu['menu_pai']) && is_null($objMenu->find($menu['menu_pai']))){
                    $objMenu->add($tituloMenu, $dadosMenu);
                }else{
                    if($objMenu->find($menu['menu_pai'])){
                        $objMenu->find($menu['menu_pai'])->add($tituloMenu, $dadosMenu);
                    }
                }
            }
        });

        return $menuIdPermissao;
    }
}
