<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 04/12/18
 * Time: 22:25
 */

namespace App\Repository;


use App\Model\BancosModel;
use App\Model\ContaBancariaModel;
use App\Utils\ConfiguracaoUtils;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class ContaBancariaRepository
{
    public function salvarDados(array $dadosContaBancaria){
        ContaBancariaModel::create([
            'cocb_tipo_conta' => $dadosContaBancaria['cb-tipo-conta'],
            'cocb_agencia' => $dadosContaBancaria['cb-agencia'],
            'cocb_conta_corrente' => $dadosContaBancaria['cb-conta-corrente'] ?? null,
            'cocb_conta_poupanca' => $dadosContaBancaria['cb-conta-poupanca'] ?? null,
            'cocb_variacao' => $dadosContaBancaria['cb-variacao'] ?? null,
            'banc_id' =>(int) $dadosContaBancaria['banc-id'],
            'empr_id' =>(int) Session::get('empr_id'),
        ]);

        return ['success' => 1];
    }

    /**
     * @param $valor
     * @return mixed
     */
    public function autocompleteBanco($valor){
        if(strlen($valor) == 2 && $valor == '**'){
            return BancosModel::get()->toArray();
        }

        if(is_numeric($valor)){
            return BancosModel::where('banc_codigo', 'like', "%{$valor}%")->get()->toArray();
        }

        return BancosModel::where('banc_titulo', 'like', "%{$valor}%")->get()->toArray();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function tabela(){
        $contaBancariaQuery = ContaBancariaModel::query()->where('empr_id', '=', Session::get('empr_id'));

        return Datatables::eloquent($contaBancariaQuery)
            ->editColumn('cocb_tipo_conta', function($contaBancaria){
                if($contaBancaria->cocb_tipo_conta == ConfiguracaoUtils::CB_CONTA_CORRENTE){
                    return 'Conta Corrente';
                } else if($contaBancaria->cocb_tipo_conta == ConfiguracaoUtils::CB_CONTA_POUPANCA){
                    return 'Conta Polpança';
                }
            })
            ->editColumn('banc_id', function($contaBancaria){
                return $contaBancaria->banco->banc_titulo;
            })
            ->addColumn('action', function ($pagamento){
                return '<button title="Excluir Conta Bancaria" data-form-pgto="'. $pagamento['cofp_id'] .'" class="btn btn-default btn-circle" onclick="jQueryFormaPgto.deletarFormaPgto($(this))"><i class="fa fa-trash-o"></i></button>';
            })
            ->make(true);
    }

    /**
     * @return array
     */
    public function contaBancaria(){
        $contaBancaria = ContaBancariaModel::with('banco')->where('empr_id', Session::get('empr_id'))->get()->toArray();

        $dados = [];

        foreach ($contaBancaria as $index => $value){
            $conta  = '';

            if($value['cocb_tipo_conta'] == ConfiguracaoUtils::CB_CONTA_CORRENTE){
                $conta = "AG: {$value['cocb_agencia']} - CC: {$value['cocb_conta_corrente']}";
            } else if($value['cocb_tipo_conta'] == ConfiguracaoUtils::CB_CONTA_POUPANCA){
                $conta = "AG: {$value['cocb_agencia']} - CP: {$value['cocb_conta_poupanca']} - VA: {$value['cocb_variacao']}";
            }

            $dados[] = [
                'cocb_id' => $value['cocb_id'],
                'cocb_tipo_conta' => $value['cocb_tipo_conta'],
                'cocb_titulo' => "{$value['banco']['banc_titulo']} - {$conta}",
                'cocb_agencia' => $value['cocb_agencia'],
                'cocb_conta_corrente' => $value['cocb_conta_corrente'],
                'cocb_conta_poupanca' => $value['cocb_conta_poupanca'],
                'cocb_variacao' => $value['cocb_variacao'],
                'banco' => [
                    'banc_id' => $value['banco']['banc_id'],
                    'banc_titulo' => $value['banco']['banc_titulo'],
                    'banc_codigo' => $value['banco']['banc_codigo'],
                ]
            ];
        }

        return $dados;
    }
}
