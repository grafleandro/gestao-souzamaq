<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 14/09/18
 * Time: 23:29
 */

namespace App\Repository;


use App\Model\ContatoModel;
use App\Model\EmpresaModel;
use App\Model\EnderecoModel;
use App\Utils\Common;
use App\Utils\Mask;
use Yajra\DataTables\Facades\DataTables;

class EmpresaRepository
{
    /**
     * @param $dadosEmpresa
     * @return mixed
     * @throws \Exception
     */
    public function salvarDados($dadosEmpresa){

        $respostaEmpresa = EmpresaModel::where('empr_cnpj', Mask::removerMascara($dadosEmpresa['cnpj']))->get();

        if($respostaEmpresa->count()){
            Common::setError('Já possui uma Empresa cadastrada com esse CNPJ: ' . $dadosEmpresa->cnpj);
        }

        $endereco = new EnderecoModel();

        $endereco->enlo_id = $dadosEmpresa['tipo_logradouro'];
        $endereco->ende_logradouro_titulo = $dadosEmpresa['rua'] ;
        $endereco->ende_bairro = $dadosEmpresa['bairro'];
        $endereco->ende_cidade = $dadosEmpresa['cidade'];
        $endereco->ende_estado = $dadosEmpresa['estado'];
        $endereco->ende_complemento = ($dadosEmpresa['complemento']) ? $dadosEmpresa['complemento'] : null;
        $endereco->ende_numero = $dadosEmpresa['numero'];
        $endereco->ende_cep = $dadosEmpresa['cep'];

        if(!$endereco->save()){
            Common::setError('Erro ao salvar os dados o Endereço!');
        }

        $contato = new ContatoModel();

        $contato->cont_cel_1 = (isset($dadosEmpresa['celular_1'])) ? str_replace(array('-', ' ', '(', ')'), "", $dadosEmpresa['celular_1']) : null;
        $contato->cont_cel_2 = (isset($dadosEmpresa['celular_2'])) ? str_replace(array('-', ' ', '(', ')'), "", $dadosEmpresa['celular_2']) : null;
        $contato->cont_tel_fixo = (isset($dadosEmpresa['tel_fixo'])) ? str_replace(array('-', ' ', '(', ')'), "", $dadosEmpresa['tel_fixo']) : null;
        $contato->cont_email = (isset($dadosEmpresa['email'])) ? str_replace(array('-', ' ', '(', ')'), "", $dadosEmpresa['email']) : null;

        if(!$contato->save()){
            Common::setError('Erro ao salvar os dados o Contato!');
        }

        $empresa = new EmpresaModel();

        $empresa->empr_nome = $dadosEmpresa['nome'];
        $empresa->empr_razao_social = $dadosEmpresa['razao_social'];
        $empresa->empr_cnpj = Mask::removerMascara($dadosEmpresa['cnpj']);
        $empresa->empr_insc_estadual = $dadosEmpresa['inscricao_estadual'];
        $empresa->empr_status = $dadosEmpresa['situacao'];
        $empresa->ende_id = $endereco->ende_id;
        $empresa->cont_id = $contato->cont_id;


        if(!$empresa->save()){
            Common::setError('Erro ao salvar os dados da Empresa!');
        }

        /** Ativando as Configuracoes Padroes */
        $this->configuracaoPadrao($empresa);

        return $empresa->empr_id;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $dadosEmpresa = EmpresaModel::with(['empresaEndereco', 'empresaContato'])->get()->toArray();

        foreach ($dadosEmpresa as $empresa){
            $empresa['empr_cnpj'] = Mask::cnpj($empresa['empr_cnpj']);
            $empresa['empresa_contato']['cont_tel_fixo'] = Mask::telComercial($empresa['empresa_contato']['cont_tel_fixo']);

            $tableEmpresa[] = $empresa;
        }

        return Datatables::of($tableEmpresa)
            ->addColumn('action', function ($empresa){
                return '<div style="text-align: center">
                                <button title="Editar Empresa" data-empresa="'.$empresa['empr_id'].'" style="margin: 4px" class="btn btn-primary" onclick="jQueryEmpresa.editarEmpresa($(this))"><i class="fa fa-edit"></i></button>
                                <button title="Excluir Empresa" data-empresa="'.$empresa['empr_id'].'" style="margin: 4px" class="btn btn-danger" onclick="jQueryEmpresa.deletarEmpresa($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->editColumn('cont_id', '{{ $empresa_contato["cont_tel_fixo"] }}')
            ->make(true);
    }

    /**
     * @param $dadosEmpresa
     * @param $idEmpresa
     * @return bool
     */
    public function atualizarDados($dadosEmpresa, $idEmpresa){

        $empresa = EmpresaModel::with(['empresaEndereco', 'empresaContato'])->find($idEmpresa);

        $empresa->empr_nome = $dadosEmpresa['nome'];
        $empresa->empr_razao_social = $dadosEmpresa['razao_social'];
        $empresa->empr_cnpj = Mask::removerMascara($dadosEmpresa['cnpj']);
        $empresa->empr_insc_estadual = $dadosEmpresa['inscricao_estadual'] ?? null;
        $empresa->empr_status = $dadosEmpresa['situacao'] ?? null;

        $empresa->empresaEndereco->enlo_id = $dadosEmpresa['tipo_logradouro'] ?? null;
        $empresa->empresaEndereco->ende_logradouro_titulo = $dadosEmpresa['rua'] ?? null;
        $empresa->empresaEndereco->ende_bairro = $dadosEmpresa['bairro'] ?? null;
        $empresa->empresaEndereco->ende_cidade = $dadosEmpresa['cidade'] ?? null;
        $empresa->empresaEndereco->ende_estado = $dadosEmpresa['estado'] ?? null;
        $empresa->empresaEndereco->ende_complemento = $dadosEmpresa['complemento'] ?? null;
        $empresa->empresaEndereco->ende_numero = $dadosEmpresa['numero'] ?? null;
        $empresa->empresaEndereco->ende_cep = $dadosEmpresa['cep'] ?? null;
        $empresa->empresaEndereco->save();

        $empresa->empresaContato->cont_cel_1 = Mask::removerMascara($dadosEmpresa['celular_1'] ?? '');
        $empresa->empresaContato->cont_cel_2 = Mask::removerMascara($dadosEmpresa['celular_2'] ?? '');
        $empresa->empresaContato->cont_tel_fixo = Mask::removerMascara($dadosEmpresa['tel_fixo'] ?? '');
        $empresa->empresaContato->cont_email = $dadosEmpresa['email'] ?? null;
        $empresa->empresaContato->save();

        return $empresa->save();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id){

        return EmpresaModel::with(['empresaEndereco', 'empresaContato'])->get()->find($id);

    }

    public function deletarEmpresa($id){

        $empresa = $this->findById($id);

        $empresa->empresaContato->delete();

        $empresa->empresaEndereco->delete();

        $resp = $empresa->delete();

        return $resp;
    }

    /**
     * @param EmpresaModel $empresaModel
     */
    private function configuracaoPadrao(EmpresaModel $empresaModel){
        /* Formas de Pagamento */
        (new \FormaPagamentoSeeder())->run($empresaModel->empr_id);
    }
}
