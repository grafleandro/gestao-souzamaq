<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 14/11/18
 * Time: 19:14
 */

namespace App\Repository;


use App\Model\ConfiguracaoNumeracaoModel;
use App\Model\FiscalNCMModel;
use App\Model\ProdutoEstoqueModel;
use App\Model\ProdutoModel;
use App\Model\ProdutoMovimentacaoModel;
use App\Model\ProdutoValoresModel;
use App\Utils\Common;
use App\Utils\Mask;
use App\Utils\SituacaoUtils;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class ProdutoRepository
{

    /**
     * @param $dadosProduto
     * @return array
     * @throws \Exception
     */
    public function salvarDados($dadosProduto){
        $return = $this->salvar(new ProdutoModel(), new ProdutoEstoqueModel(), new ProdutoValoresModel(), $dadosProduto);

        (new ConfiguracaoNumeracaoModel())->where('empr_id', Session::get('empr_id'))->increment('conu_produto');

        return $return;
    }

    /**
     * @param $dadosProduto
     * @param $idProduto
     * @return array
     * @throws \Exception
     */
    public function atualizarDados($dadosProduto, $idProduto){
        $produto = $this->findById($idProduto);

        return $this->salvar(
            $produto,
            $produto->produtoEstoque()->getResults() ?? new ProdutoEstoqueModel(),
            $produto->produtoValores()->getResults() ?? new ProdutoValoresModel(),
            $dadosProduto
        );
    }

    /**
     * @param ProdutoModel $produtoModel
     * @param ProdutoEstoqueModel $estoqueModel
     * @param ProdutoValoresModel $valoresModel
     * @param array $dadosProduto
     * @return array
     * @throws \Exception
     */
    public function salvar(ProdutoModel $produtoModel, ProdutoEstoqueModel $estoqueModel, ProdutoValoresModel $valoresModel, array $dadosProduto){

        if(!Mask::removerMascaraDinheiro($dadosProduto['prod_preco_sugerido'] ?? 0) && !Mask::removerMascaraDinheiro($dadosProduto['prod_preco_venda'] ?? 0)){
            Common::setError('Você precisa informar o Preço de Venda ou informar o Markup!');
        }

        /** Salvando os dados do Produto */
        $produtoModel->prod_titulo = $dadosProduto['prod_titulo'];
        $produtoModel->prod_cod_barra = $dadosProduto['prod_cod_barra'];
        $produtoModel->prod_unid_venda =(int) $dadosProduto['prod_unid_venda'];
        $produtoModel->grup_id =(int) $dadosProduto['grup_id'];
        $produtoModel->fabr_id =(int) $dadosProduto['fabr_id'];
        $produtoModel->marc_id =(int) $dadosProduto['marc_id'];
        $produtoModel->fincm_id =(int) $dadosProduto['trib_ncm_codigo'];
        $produtoModel->empr_id =(int) Session::get('empr_id');

        if(!$produtoModel->save()){
            Common::setError('Erro ao salvar os dados do Produto!');
        }

        /** Salvando as informacoes no Estoque do respectivo produto */
        $estoqueModel->pres_controlar_estoque =(int) $dadosProduto['prod_cont_estoque'];
        $estoqueModel->pres_estoq_mini =(int) $dadosProduto['prod_estoq_minimo'];
        $estoqueModel->pres_estoq_max =(int) $dadosProduto['prod_estoq_maximo'];
        $estoqueModel->prest_estoq_atual =(int) $dadosProduto['prod_estoq_atual'];
        $estoqueModel->prod_id =(int) $produtoModel->prod_id;

        if(!$estoqueModel->save()){
            Common::setError('Erro ao salvar os dados do Produto no Estoque!');
        }

        /** Salvando os valores do respectivo produto */
        $precoSugerido  = Mask::removerMascaraDinheiro($dadosProduto['prod_preco_sugerido'] ?? 0);
        $precoVenda     = Mask::removerMascaraDinheiro($dadosProduto['prod_preco_venda'] ?? 0);

        $valoresModel->prva_custo = Mask::removerMascaraDinheiro($dadosProduto['prod_custo'] ?? 0);
        $valoresModel->prva_markup = Mask::removarMascaraPorcent($dadosProduto['prod_markup'] ?? 0);
        $valoresModel->prva_preco_sugerido = $precoSugerido;
        $valoresModel->prva_preco_venda = (empty($precoSugerido)) ? $precoVenda: $precoSugerido ;
        $valoresModel->prva_cota_comissao = Mask::removarMascaraPorcent($dadosProduto['cota_comissao']);
        $valoresModel->prva_cota_empresa = Mask::removarMascaraPorcent($dadosProduto['cota_empresa']);
        $valoresModel->prod_id =(int) $produtoModel->prod_id;

        if(!$valoresModel->save()){
            Common::setError('Erro ao salvar os valores do Produto!');
        }

        return ['success' => 1];
    }

    /**
     * @param array $produto
     * @param int $idOrigem
     * @param int $origem
     * @throws \Exception
     */
    public function salvarMovimentacao(array $produto, int $idOrigem, int $origem){
        foreach ($produto as $index => $item){
            ProdutoMovimentacaoModel::create([
                'prmo_valor' =>(float) $item['prod_valor'],
                'prmo_qtd' => $item['prod_qtd'],
                'prmo_status' => SituacaoUtils::PROD_MOVI_SAIDA,
                'prmo_origem' => $origem,
                'prmo_origem_id' => $idOrigem,
                'prod_id' => $item['prod_id'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if(!$this->atualizarEstoqueAtual($item)){
                Common::setError('Houve um erro ao atualizar o valor do Estoque!');
            };
        }
    }

    /**
     * @param $dadosProduto
     * @return bool
     * @throws \Exception
     */
    public function consultarEstoque(array $dadosProduto){
        $produto = $this->findById($dadosProduto['prod_id'])->toArray();

        if($produto['produto_estoque']['prest_estoq_atual'] < $dadosProduto['prod_qtd']){
            Common::setError("O produto {$produto['prod_titulo']}, não possui a quantidade informada em Estoque!\nEstoque Atual: {$produto['produto_estoque']['prest_estoq_atual']}\nQuantidade Solicitada: {$dadosProduto['prod_qtd']}");
        }

        return true;
    }

    /**
     * @param array $dadosProduto
     * @return int
     * @throws \Exception
     */
    public function atualizarEstoqueAtual(array $dadosProduto){
        $produto = $this->findById($dadosProduto['prod_id']);

        return $produto->produtoEstoque()->decrement('prest_estoq_atual', $dadosProduto['prod_qtd']);
    }

    /**
     * @param int $idProduto
     * @return ProdutoModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function findById(int $idProduto){
        return ProdutoModel::with(['unidadeMedida', 'produtoNcm', 'produtoValores', 'produtoEstoque', 'produtoGrupo', 'produtoMarca', 'produtoFabricante'])
            ->where('prod_id', '=', $idProduto)
            ->first();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $dadosProduto = ProdutoModel::with(['unidadeMedida', 'produtoNcm', 'produtoValores', 'produtoEstoque'])
            ->where('empr_id', '=', Session::get('empr_id'))
            ->get()
            ->toArray();

        return Datatables::of($dadosProduto)
            ->editColumn('finc_cod_ncm', function($produto){
                return $produto['produto_ncm']['finc_cod_ncm'];
            })
            ->editColumn('prva_preco_venda', function($produto){
                $valorProduto = ($produto['produto_valores']['prva_preco_sugerido']) ? $produto['produto_valores']['prva_preco_sugerido'] :  $produto['produto_valores']['prva_preco_venda'];

                return Mask::dinheiro($valorProduto);
            })
            ->editColumn('prest_estoq_atual', function($produto){
                $mediaEstoque   = ceil($produto['produto_estoque']['pres_estoq_max']/2);

                return view('empresa/produto/indicativo-estoque', [
                    'estoqueAtual'  => $produto['produto_estoque']['prest_estoq_atual'],
                    'estoqueMedia'  => $mediaEstoque,
                    'estoqueMinimo' => $produto['produto_estoque']['pres_estoq_mini'],
                    'unidadeMedida' => $produto['unidade_medida']['unme_sigla']
                ]);
            })
            ->editColumn('action', function($produto){
                $urlEdit = "/empresa/produto/". $produto['prod_id'] ."/edit";

                return '<div style="text-align: center">
                                <a href="'. $urlEdit .'" title="Editar Produto" data-produto="'.$produto['prod_id'].'" style="margin: 4px" class="btn btn-default btn-circle"><i class="fa fa-edit"></i></a>
                                <button title="Excluir Produto" data-produto="'.$produto['prod_id'].'" style="margin: 4px" class="btn btn-default btn-circle" onclick="jQueryProduto.deletarProduto($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->rawColumns(['prest_estoq_atual', 'action'])
            ->make(true);
    }

    /**
     * @param $qtdEstoque
     * @param $idProduto
     * @return array
     * @throws \Exception
     */
    public function atualizarEstoque($qtdEstoque, $idProduto){
        $produtoEstoque = ProdutoEstoqueModel::where('prod_id', $idProduto)->first();

        $produtoEstoque->prest_estoq_atual =(int) $qtdEstoque;

        if(!$produtoEstoque->save()){
            Common::setError('Erro ao atualizar o Estoque do Produto!');
        }

        return ['success' => 1];
    }

    /**
     * @param $idProduto
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function deletarProduto($idProduto){
        $produto = $this->findById($idProduto);

        return $produto->delete();
    }

    /**
     * @param $search
     * @return mixed
     */
    public function autocompleteTribNCM($search){
        return FiscalNCMModel::join('fiscal_tributos', 'finc_id', '=', 'fincm_id')
            ->orWhere('finc_cod_ncm', 'like', "{$search}%")
            ->orWhere('finc_descricao', 'like', "%{$search}%")
            ->get()
            ->toArray();
    }

    /**
     * @param $search
     * @return array
     */
    public function autocompleteProduto($search){
        return ProdutoModel::with(['unidadeMedida', 'produtoNcm', 'produtoValores', 'produtoEstoque', 'produtoGrupo', 'produtoMarca', 'produtoFabricante'])
            ->where('empr_id', '=', Session::get('empr_id'))
            ->orWhere('prod_titulo', 'like', "%{$search}%")
            ->get()
            ->toArray();
    }
}
