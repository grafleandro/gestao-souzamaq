<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 30/10/18
 * Time: 09:18
 */

namespace App\Repository;


use App\Model\ProdutoMarcaModel;
use App\Utils\Common;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class ProdutoMarcaRepository
{
    /**
     * @param array $dadosMarca
     * @return array
     * @throws \Exception
     */
    public function salvarDados($dadosMarca = []){
        $resposta = ProdutoMarcaModel::create([
            'marc_titulo' => $dadosMarca['mc_titulo'],
            'marc_descricao' => $dadosMarca['mc_descricao'],
            'empr_id' => Session::get('empr_id')
        ]);

        if(!$resposta->save()){
            Common::setError('Houve um erro ao salvar os dados da Marca!');
        }

        return ['success' => 1];
    }

    /**
     * @param array $dadosMarca
     * @param int $idMarca
     * @return array
     * @throws \Exception
     */
    public function atualizarDados($dadosMarca = [], int $idMarca){
        $marca = $this->findById($idMarca);

        $marca->marc_titulo     = $dadosMarca['mc_titulo'];
        $marca->marc_descricao  = $dadosMarca['mc_descricao'];

        if(!$marca->save()){
            Common::setError('Erro ao atualizar os dados da Marca!');
        }

        return ['success' => 1];
    }

    /**
     * @param int $idMarca
     * @return mixed
     */
    public function findById(int $idMarca){
        return $dadosMarca = ProdutoMarcaModel::where('marc_id', '=', $idMarca)
            ->where('empr_id', '=', Session::get('empr_id'))
            ->first();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $dadosMarca = ProdutoMarcaModel::where('empr_id', '=', Session::get('empr_id'))
            ->get()
            ->toArray();

        return Datatables::of($dadosMarca)
            ->editColumn('qtd_produto', function($marca){
                return 0;
            })
            ->editColumn('action', function($marca){
                $urlEdit = "/empresa/marca/". $marca['marc_id'] ."/edit";

                return '<div style="text-align: center">
                                <a href="'. $urlEdit .'" title="Editar Marca" data-marca="'.$marca['marc_id'].'" style="margin: 4px" class="btn btn-default btn-circle"><i class="fa fa-edit"></i></a>
                                <button title="Excluir Marca" data-marca="'.$marca['marc_id'].'" style="margin: 4px" class="btn btn-default btn-circle" onclick="jQueryMarca.deletarMarca($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->make(true);
    }

    /**
     * @param int $idMarca
     * @return mixed
     */
    public function deletarMarca(int $idMarca){
        $marca = $this->findById($idMarca);

        return $marca->delete();
    }

    /**
     * @param $search
     * @return mixed
     */
    public function autocomplete($search){

        if(strlen($search) == 2 && $search == '**'){
            $dadosProduto = ProdutoMarcaModel::where('empr_id', '=', Session::get('empr_id'))
                ->get()
                ->toArray();
        } else {
            $dadosProduto = ProdutoMarcaModel::where('empr_id', '=', Session::get('empr_id'))
                ->where('marc_titulo', 'like', "{$search}%")
                ->get()
                ->toArray();
        }

        return $dadosProduto;
    }
}
