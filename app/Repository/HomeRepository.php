<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 18/12/18
 * Time: 10:34
 */

namespace App\Repository;


use App\Model\OrdemServicoModel;
use App\Model\VendaBalcaoModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HomeRepository
{
    public function informativo()
    {
        $mesAnteriorInicio  = Carbon::now()->subMonth()->format('Y-m-01 00:00:00');
        $mesAnteriorFim     = Carbon::now()->subMonth()->format('Y-m-d H:i:s');

        $mesAtualInicio  = Carbon::now()->format('Y-m-01 00:00:00');
        $mesAtualFim     = Carbon::now()->format('Y-m-d H:i:s');

        $mesAnterior= VendaBalcaoModel::where('empr_id', Session::get('empr_id'))->whereBetween('created_at', [$mesAnteriorInicio, $mesAnteriorFim])->get()->sum('veba_venda_total');
        $mesAtual   = VendaBalcaoModel::where('empr_id', Session::get('empr_id'))->whereBetween('created_at', [$mesAtualInicio, $mesAtualFim])->get()->sum('veba_venda_total');

        /* Crescimento Percentual - Comparando o mesmo periodo com o anterior M-1 */
        $crescimento = ($mesAnterior) ? number_format(((($mesAtual-$mesAnterior)/$mesAnterior)*100), 2) : 100.00;

        /* Ordem de Servicos geradas */
        $osQtd = OrdemServicoModel::where('empr_id', Session::get('empr_id'))->count();

        return [
            'qtdOs'        => $osQtd,
            'crescimento'   => $crescimento,
            'qtdVendas'     => VendaBalcaoModel::where('empr_id', Session::get('empr_id'))->whereBetween('created_at', [$mesAtualInicio, $mesAtualFim])->get()->count() ,
            'periodo'       => "de ". Carbon::parse($mesAnteriorInicio)->format('d/m/Y') ." à ". Carbon::parse($mesAnteriorFim)->format('d/m/Y') ." e de ". Carbon::parse($mesAtualInicio)->format('d/m/Y') ." à " . Carbon::parse($mesAtualInicio)->format('d/m/Y'),
        ];
    }

    public function lembretes(Request $request)
    {

        return [];
    }
}
