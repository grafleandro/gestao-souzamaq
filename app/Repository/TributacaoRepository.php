<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 09/11/18
 * Time: 17:33
 */

namespace App\Repository;


use App\Model\FiscalCSOSNModel;
use App\Model\FiscalCSTModel;
use App\Model\FiscalNCMModel;
use App\Model\FiscalPisCofinsModel;
use App\Model\FiscalTributoModel;
use App\Utils\Common;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class TributacaoRepository
{
    /**
     * @param array $dados
     * @return array
     * @throws \Exception
     */
    public function salvarDados(array $dados){
        return $this->salvar(new FiscalTributoModel(), $dados);
    }

    /**
     * @param array $dados
     * @param int $idTributo
     * @return array
     * @throws \Exception
     */
    public function atualizarDados(array $dados, int $idTributo){
        $dadosTributo = $this->findById($idTributo);

        return $this->salvar($dadosTributo, $dados);
    }

    /**
     * @param FiscalTributoModel $tributoModel
     * @param array $dadosTributo
     * @return array
     * @throws \Exception
     */
    public function salvar(FiscalTributoModel $tributoModel, array $dadosTributo){

        $tributoModel->fitr_cofins_entrada = $dadosTributo['trib-cofins-entrada'];
        $tributoModel->fitr_cofins_saida = $dadosTributo['trib-cofins-saida'];
        $tributoModel->fitr_pis_entrada = $dadosTributo['trib-pis-entrada'];
        $tributoModel->fitr_pis_saida = $dadosTributo['trib-pis-saida'];
        $tributoModel->fincm_id = $dadosTributo['trib-ncm-codigo'];
        $tributoModel->ficst_id = $dadosTributo['trib-cst'];
        $tributoModel->ficsosn_id = $dadosTributo['trib-csosn'];
        $tributoModel->empr_id  = Session::get('empr_id');

        if(!$tributoModel->save()){
            Common::setError('Erro ao salvar os dados do Tributo!');
        }

        return ['success' => 1];
    }

    /**
     * @param int $idTributo
     * @return FiscalTributoModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function findById(int $idTributo){
        return FiscalTributoModel::with('tributoNcm')->where('fitr_id', $idTributo)->first();
    }

    /**
     * @return array
     */
    public function tributacaoCST(){
        $cst     = FiscalCSTModel::get()->toArray();
        $dataCST = [];

        foreach ($cst as $index => $value){
            $dataCST[] = [
                'fics_id' => $value['fics_id'],
                'fics_titulo' => $value['fics_codigo'] . ' - ' . $value['fics_titulo'],
            ];
        }

        return $dataCST;
    }

    /**
     * @return array
     */
    public function tributacaoCSOSN(){
        $cst     = FiscalCSOSNModel::get()->toArray();
        $dataCSOSN = [];

        foreach ($cst as $index => $value){
            $dataCSOSN[] = [
                'fics_id' => $value['fics_id'],
                'fics_titulo' => $value['fics_codigo'] . ' - ' . $value['fics_titulo'],
            ];
        }

        return $dataCSOSN;
    }

    /** CST PIS ENTRADA
     * @return array
     */
    public function tributacaoPisCofinsEntrada(){
        $cst     = FiscalPisCofinsModel::where('fipc_entrada', '1')->get()->toArray();
        $dataCSOSN = [];

        foreach ($cst as $index => $value){
            $descricao = (!empty($value['fipc_descricao'])) ? ' - ' . $value['fipc_descricao'] : '';

            $dataCSOSN[] = [
                'fipc_id' => $value['fipc_id'],
                'fipc_titulo' => $value['fipc_codigo'] . ' - ' . $value['fipc_titulo'] . $descricao,
            ];
        }

        return $dataCSOSN;
    }

    /** CST PIS SAIDA
     * @return array
     */
    public function tributacaoPisCofinsSaida(){
        $cst     = FiscalPisCofinsModel::where('fipc_entrada', '0')->get()->toArray();
        $dataCSOSN = [];

        foreach ($cst as $index => $value){
            $descricao = (!empty($value['fipc_descricao'])) ? ' - ' . $value['fipc_descricao'] : '';

            $dataCSOSN[] = [
                'fipc_id' => $value['fipc_id'],
                'fipc_titulo' => $value['fipc_codigo'] . ' - ' . $value['fipc_titulo'] . $descricao,
            ];
        }

        return $dataCSOSN;
    }

    /**
     * @param string $search
     * @return mixed
     */
    public function autocompleteNCM(string $search){

        if(strlen($search) == 2 && $search == '**'){
            return FiscalNCMModel::limit(50)->get()->toArray();
        }

        return FiscalNCMModel::orWhere('finc_cod_ncm', 'like', "{$search}%")
            ->orWhere('finc_descricao', 'like', "%{$search}%")
            ->get()
            ->toArray();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $dadosNCM = FiscalTributoModel::with('tributoNcm')
            ->where('empr_id', '=', Session::get('empr_id'))
            ->get()
            ->toArray();

        return Datatables::of($dadosNCM)
            ->addColumn('action', function ($ncm){
                $urlEdit = "/configuracao/tributacao/". $ncm['fitr_id'] ."/edit";

                return '<div style="text-align: center">
                                <a href="'. url($urlEdit) .'" title="Editar N.C.M" data-ncm="'. $ncm['fitr_id'] .'" style="margin: 4px" class="btn btn-default btn-circle" onclick="jQueryTributacao.editarTributo($(this))"><i class="fa fa-edit"></i></a>
                                <button title="Excluir N.C.M" data-ncm="'. $ncm['fitr_id'] .'" style="margin: 4px" class="btn btn-default btn-circle" onclick="jQueryTributacao.deletarTributo($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->editColumn('qtd_produto', '0')
            ->editColumn('finc_cod_ncm', function($ncm){
                return $ncm['tributo_ncm']['finc_cod_ncm'];
            })
            ->editColumn('finc_descricao', function($ncm){
                return $ncm['tributo_ncm']['finc_descricao'];
            })
            ->editColumn('finc_inicio_vigencia', function($ncm){
                return Carbon::parse($ncm['tributo_ncm']['finc_inicio_vigencia'])->format('d/m/Y');
            })
            ->make(true);
    }

    /**
     * @param $idTributo
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function deletarTributo($idTributo){
        $tributo = $this->findById($idTributo);

        return $tributo->delete();
    }
}
