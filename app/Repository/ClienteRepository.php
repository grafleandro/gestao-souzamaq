<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 15/09/18
 * Time: 10:17
 */

namespace App\Repository;


use App\Model\ClienteModel;
use App\Model\ContatoModel;
use App\Model\EnderecoModel;
use App\Utils\ClienteUtils;
use App\Utils\Common;
use App\Utils\Mask;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class ClienteRepository
{
    /**
     * @param array $dadosCliente
     * @return array
     * @throws \Exception
     */
    public function salvarDados($dadosCliente = []){

        $respostaCliente = ClienteModel::orWhere('clie_cpf_cnpj', Mask::removerMascara($dadosCliente['cliente_cpf']))
            ->orWhere('clie_cpf_cnpj', Mask::removerMascara($dadosCliente['cliente_cnpj']))
            ->where('empr_id', '=', Session::get('empr_id'))
            ->get();

        if($respostaCliente->count()){
            $sigla = ($dadosCliente['cliente_tipo_pessoa'] == ClienteUtils::_P_FISICA) ? 'CPF' : 'CNPJ';

            Common::setError('Já possui um Cliente cadastrado com esse '. $sigla .': ' . $respostaCliente->clpf_cpf);
        }

        return $this->salvarDadosCliente(new ClienteModel(),
            new EnderecoModel(),
            new ContatoModel(),
            $dadosCliente
        );
    }

    /**
     * @param array $dadosCliente
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function atualizarDados($dadosCliente = [], int $id){

        $cliente = $this->findById($id);

        return $this->salvarDadosCliente($cliente,
            $cliente->clienteEndereco()->getResults() ?? new EnderecoModel(),
            $cliente->clienteContato()->getResults() ?? new ContatoModel(),
            $dadosCliente
        );
    }

    /**
     * @param int $idCliente
     * @return ClienteModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function findById($idCliente = 0){
        return ClienteModel::with(['clienteEndereco', 'clienteContato', 'clienteEmpresa'])
            ->where('clie_id', $idCliente)
            ->first();
    }

    /**
     * @param ClienteModel $clienteModel
     * @param EnderecoModel $enderecoModel
     * @param ContatoModel $contatoModel
     * @param array $dadosCliente
     * @return array
     * @throws \Exception
     */
    public function salvarDadosCliente(ClienteModel $clienteModel, EnderecoModel $enderecoModel, ContatoModel $contatoModel, $dadosCliente = []){
        /* Salvando os dados de Endereco */
        $respostaEndereco = $this->salvarEndereco($enderecoModel, $dadosCliente);

        /* Salvando os dados de Contato */
        $respostaContato = $this->salvarContato($contatoModel, $dadosCliente);

        /* Salvando os dados do Cliente */
        $clienteModel->clie_nome_razao_social =  ClienteUtils::getNomeOuRazaoSocial($dadosCliente);
        $clienteModel->clie_nome_fantasia = ($dadosCliente['cliente_nome_fantasia']) ?? null;
        $clienteModel->clie_cpf_cnpj = Mask::removerMascara(ClienteUtils::getCpfOuCnpj($dadosCliente));
        $clienteModel->clie_rg_insc_estadual = ClienteUtils::getRgOuInscEstadual($dadosCliente);
        $clienteModel->clie_tipo = $dadosCliente['cliente_tipo_pessoa'];
        $clienteModel->clie_status = $dadosCliente['cliente_situacao'];
        $clienteModel->ende_id = $respostaEndereco->ende_id;
        $clienteModel->cont_id = $respostaContato->cont_id;
        $clienteModel->empr_id = Session::get('empr_id');

        if(!$clienteModel->save()){
            Common::setError('Erro ao salvar os dados do Cliente!');
        }

        return ['success' => 1];
    }

    /**
     * @param EnderecoModel $enderecoModel
     * @param array $dadosCliente
     * @return EnderecoModel
     * @throws \Exception
     */
    public function salvarEndereco(EnderecoModel $enderecoModel, $dadosCliente = []){
        $enderecoModel->enlo_id = $dadosCliente['tipo_logradouro'];
        $enderecoModel->ende_logradouro_titulo = $dadosCliente['rua'] ;
        $enderecoModel->ende_bairro = $dadosCliente['bairro'];
        $enderecoModel->ende_cidade = $dadosCliente['cidade'];
        $enderecoModel->ende_estado = $dadosCliente['estado'];
        $enderecoModel->ende_complemento = ($dadosCliente['complemento']) ? $dadosCliente['complemento'] : null;
        $enderecoModel->ende_numero = $dadosCliente['numero'];
        $enderecoModel->ende_cep = Mask::removerMascara($dadosCliente['cep']);

        if(!$enderecoModel->save()){
            Common::setError('Erro ao salvar os dados do Endereço!');
        }

        return $enderecoModel;
    }

    /**
     * @param ContatoModel $contatoModel
     * @param array $dadosCliente
     * @return ContatoModel
     * @throws \Exception
     */
    public function salvarContato(ContatoModel $contatoModel, $dadosCliente = []){
        $contatoModel->cont_cel_1 = (isset($dadosCliente['celular_1'])) ? Mask::removerMascara($dadosCliente['celular_1']) : null;
        $contatoModel->cont_cel_2 = (isset($dadosCliente['celular_2'])) ? Mask::removerMascara($dadosCliente['celular_2']) : null;
        $contatoModel->cont_tel_fixo = (isset($dadosCliente['tel_fixo'])) ? Mask::removerMascara($dadosCliente['tel_fixo']) : null;
        $contatoModel->cont_email = (isset($dadosCliente['email'])) ? $dadosCliente['email'] : null;

        if(!$contatoModel->save()){
            Common::setError('Erro ao salvar os dados do Contato!');
        }

        return $contatoModel;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $dadosCliente = ClienteModel::with(['clienteEndereco', 'clienteContato', 'clienteEmpresa'])
            ->where('empr_id', '=', Session::get('empr_id'))
            ->get()
            ->toArray();

        return Datatables::of($dadosCliente)
            ->editColumn('clie_tipo', function($cliente){
                return ClienteUtils::tituloTiposPessoaCliente($cliente['clie_tipo']);
            })
            ->editColumn('clie_situacao', function($cliente){
                return ClienteUtils::tituloTipoStatusCliente($cliente['clie_status']);
            })
            ->editColumn('clie_nome_razao', function($cliente){
                $nomeRazao = '';

                if($cliente['clie_nome_razao_social']){
                    $nomeRazao = $cliente['clie_nome_razao_social'];
                } else if($cliente['clie_nome_fantasia']){
                    $nomeRazao = $cliente['clie_nome_fantasia'];
                }

                return $nomeRazao;
            })
            ->editColumn('clie_cpf_cnpj', function($cliente){
                $cpfCnpj = '';

                if($cliente['clie_tipo'] == ClienteUtils::_P_FISICA){
                    $cpfCnpj = Mask::cpf($cliente['clie_cpf_cnpj']);
                } else if($cliente['clie_tipo'] == ClienteUtils::_P_JURIDICA){
                    $cpfCnpj = Mask::cnpj($cliente['clie_cpf_cnpj']);
                }

                return $cpfCnpj;
            })
            ->editColumn('clie_contato', function($cliente){
                $contato = [];

                if($cliente['cliente_contato']['cont_cel_1']){
                    $contato[] = Mask::telCelular($cliente['cliente_contato']['cont_cel_1']);
                }

                if($cliente['cliente_contato']['cont_cel_2']){
                    $contato[] = Mask::telCelular($cliente['cliente_contato']['cont_cel_2']);
                }

                if($cliente['cliente_contato']['cont_tel_fixo']){
                    $contato[] = Mask::telCelular($cliente['cliente_contato']['cont_tel_fixo']);
                }

                return implode(' / ', $contato);
            })
            ->addColumn('action', function ($cliente){
                $urlEdit = "empresa/cliente/". $cliente['clie_id'] ."/edit";

                return '<div style="text-align: center">
                                <a href="'. url($urlEdit) .'" title="Editar Cliente" data-cliente="'.$cliente['clie_id'].'" style="margin: 4px" class="btn btn-default btn-circle"><i class="fa fa-edit"></i></a>
                                <button title="Excluir Cliente" data-cliente="'.$cliente['clie_id'].'" style="margin: 4px" class="btn btn-default btn-circle" onclick="jQueryCliente.deletarCliente($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->make(true);
    }

    /**
     * @param $id
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function deletarCliente($id){
        $cliente = $this->findById($id);

        $cliente->clienteEndereco()->delete();
        $cliente->clienteContato()->delete();

        return $cliente->delete();
    }

    /**
     * @param $valor
     * @return mixed
     */
    public function autocomplete($valor){

        if(strlen($valor) == 2 && $valor == '**'){
            return ClienteModel::where('empr_id', Session::get('empr_id'))
                ->get()
                ->toArray();
        }

        return ClienteModel::where(function ($query) use ($valor){
            $query->where('clie_nome_razao_social', 'like', '%'.$valor.'%')
                ->orWhere('clie_nome_fantasia', 'like', '%'.$valor.'%');
            })
            ->where('empr_id', Session::get('empr_id'))
            ->get()
            ->toArray();
    }

    /**
     * @return array
     */
    public function clienteConsumidor()
    {
        $consumidor = ClienteModel::with(['clienteEndereco', 'clienteContato', 'clienteEmpresa'])
            ->where('empr_id', Session::get('empr_id'))
            ->where('clie_cpf_cnpj', '00000000000')
            ->firstOrFail()
            ->toArray();

        return $consumidor;
    }
}
