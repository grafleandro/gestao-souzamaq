<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 03/10/18
 * Time: 14:53
 */

namespace App\Repository;


use App\Model\CheckListInternoModel;
use App\Model\CheckListMecanicoModel;
use App\Model\ClienteModel;
use App\Model\ColaboradorModel;
use App\Model\ContatoModel;
use App\Model\EmpresaModel;
use App\Model\EnderecoModel;
use App\Model\TipoLogradouroModel;
use App\Model\VeiculoCorModel;
use App\Model\VeiculoModel;
use App\Utils\CheckListPDF;
use App\Utils\ClienteUtils;
use App\Utils\Common;
use App\Utils\FormUtils;
use App\Utils\Mask;
use App\Utils\SituacaoUtils;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class CheckListInternoRepository
{
    private $CheckListInternoModel;

    public function __construct(CheckListInternoModel $checkListInternoModel)
    {
        $this->CheckListInternoModel = $checkListInternoModel;
    }

    /**
     * @param array $dadosCheckListInterno
     * @return array
     * @throws \Exception
     */
    public function salvarDados($dadosCheckListInterno = []){

        if($dadosCheckListInterno['id_cliente'] != 0){
            $checkListInterno = $this->salvar(new CheckListInternoModel(), $dadosCheckListInterno);
        }else{
            dd($dadosCheckListInterno);
        }
        return $checkListInterno->ckli_id;
    }

    /**
     * @param array $dadosCheckListInterno
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function atualizarDados($dadosCheckListInterno = [], int $id){

        $interno = $this->findById($id);

        return $this->salvar($interno, $dadosCheckListInterno)->ckli_id;

    }

    /**
     * @param int $idInterno
     * @return mixed
     */
    public function findById($idInterno = 0){
        return CheckListInternoModel::where('ckli_id', $idInterno)->first();
    }

    /**
     * @param $mecanicos
     * @param $id_ckli
     * @throws \Exception
     */
    private function salvarMecanico($mecanicos, $id_ckli ){

        $ChMec = CheckListMecanicoModel::where('ckli_id', $id_ckli)->get();

        if($ChMec){
            foreach ($ChMec as $checkListMecanicoModel) {
                $checkListMecanicoModel->delete();
            }
        }


        foreach ($mecanicos as $mec) {
            $checkListMecanicoModel = new CheckListMecanicoModel();
            $checkListMecanicoModel->ckli_id = $id_ckli;
            $checkListMecanicoModel->cola_id = $mec;

            if (!$checkListMecanicoModel->save()) {
                Common::setError('Erro ao salvar os dados do mecânico!');
            }
        }


    }

    /**
     * @param CheckListInternoModel $checkListInternoModel
     * @param array $dadosCheckListInterno
     * @return CheckListInternoModel
     * @throws \Exception
     */
    public function salvar(CheckListInternoModel $checkListInternoModel, $dadosCheckListInterno = []){

        /* Salvando os dados do CheckList Interno */
        $checkListInternoModel->ckli_km = $dadosCheckListInterno['km_local'];
        $checkListInternoModel->ckli_oleo = $dadosCheckListInterno['km_ultimo_oleo'] ?? null;
        $checkListInternoModel->ckli_filtro = $dadosCheckListInterno['status_filtro']?? null;
        $checkListInternoModel->ckli_status = $dadosCheckListInterno['status'];
        $checkListInternoModel->ckli_observacao = $dadosCheckListInterno['obs'];
        $checkListInternoModel->ckli_objetos_veiculo = $dadosCheckListInterno['objetos'];
        $checkListInternoModel->ckli_servico = $dadosCheckListInterno['servico'];
        $checkListInternoModel->ckli_previsao_entrega = (isset($dadosCheckListInterno['dt_entrega'])) ? Carbon::createFromFormat('d/m/Y', $dadosCheckListInterno['dt_entrega'])->format('Y-m-d H:i:s') : null;
        $checkListInternoModel->ckli_hora_entrega = $dadosCheckListInterno['hora_entrega'] ?? null;
        $checkListInternoModel->ckli_valor = ($dadosCheckListInterno['valor']) ? Mask::removerMascaraDinheiro($dadosCheckListInterno['valor']) : null;
        $checkListInternoModel->ckli_combustivel = $dadosCheckListInterno['status_combustivel'] ?? null;
        $checkListInternoModel->ckli_os_externo = $dadosCheckListInterno['os_externo'];
        $checkListInternoModel->ckli_os_externo_servico = $dadosCheckListInterno['os_externo_servico'];
        $checkListInternoModel->clie_id = $dadosCheckListInterno['id_cliente'];
        $checkListInternoModel->veic_id = $dadosCheckListInterno['id_veiculo'];
        $checkListInternoModel->empr_id = Session::get('empr_id');

        if(!$checkListInternoModel->save()){
            Common::setError('Erro ao salvar os dados do CheckList Interno!');
        }
        $dadosCheckListInterno['mecanicos'] = explode(',',$dadosCheckListInterno['colaboradores']);

        $this->salvarMecanico( $dadosCheckListInterno['mecanicos'], $checkListInternoModel->ckli_id);


        return $checkListInternoModel;
    }

    /**
     * @param array $filtro
     * @return mixed
     * @throws \Exception
     */
    public function tabela(array $filtro){

        $dadosChecklistInterno = CheckListInternoModel::query()
            ->with(['checklistInternoVeiculo', 'checklistInternoCliente', 'checklistMecanico'])
            ->where('ckli_status', $filtro['status'] ?? SituacaoUtils::_ORDEM_SERVICO )
            ->where('empr_id', Session::get('empr_id'));

        return Datatables::eloquent($dadosChecklistInterno)
            ->filter(function($instance) use ($filtro){

                if(isset($filtro['id_cliente']) && !empty($filtro['id_cliente'])){
                    $instance->where('clie_id', $filtro['id_cliente']);
                }

                /* Selecionando o Mecanico */
                if(isset($filtro['id_colaborador']) && !empty($filtro['id_colaborador'])){
                    $instance->whereHas('checklistMecanico', function($query) use ($filtro){
                        $query->where('checklist_mecanico.cola_id', $filtro['id_colaborador']);
                    });
                }

                if(isset($filtro['id_veiculo']) && !empty($filtro['id_veiculo'])){
                    $instance->where('veic_id', $filtro['id_veiculo']);
                }

                /* Tipo da Check List */
                if(isset($filtro['status'])){
                    $instance->where('ckli_status', $filtro['status']);
                }

                if(isset($filtro['data_inicio']) && !empty($filtro['data_inicio']) && isset($filtro['data_fim']) && !empty($filtro['data_fim'])){
                    $dataInicial = Carbon::createFromFormat('d/m/Y', $filtro['data_inicio'])->format('Y-m-d 00:00:00');
                    $dataFinal  = Carbon::createFromFormat('d/m/Y', $filtro['data_fim'])->format('Y-m-d 23:59:59');

                    $instance->whereBetween('created_at', [$dataInicial, $dataFinal]);
                }

                /* Filtrar utilizando o Procurar */
                if($filtro['search'] && !empty($filtro['search']['value']))
                {
                    if(is_numeric($filtro['search']['value'])){
                        $instance->where('ckli_os_externo', 'like', $filtro['search']['value'] .'%')
                            ->orwhere('ckli_os_externo_servico', 'like', $filtro['search']['value'] .'%')
                            ->orWhere('ckli_id', 'like', $filtro['search']['value'] .'%');
                    } else {
                        $instance->whereHas('checklistInternoVeiculo', function($query) use ($filtro){
                            return $query->where('veiculo.veic_placa', 'like', '%'. Mask::removerMascara($filtro['search']['value']).'%');
                        });

                        $instance->orWhereIn('veic_id',
                            VeiculoModel::select('veic_id')->where('empr_id', Session::get('empr_id'))
                                ->where('veic_placa', 'like', '%'. $filtro['search']['value'] .'%')
                                ->get()->toArray()
                        );

                        $instance->orWhereIn('clie_id',
                            ClienteModel::select('clie_id')->where('empr_id', Session::get('empr_id'))
                                ->where('clie_nome_razao_social', 'like', '%'. $filtro['search']['value'] .'%')
                                ->orWhere('clie_nome_fantasia', 'like', '%'. $filtro['search']['value'] .'%')
                                ->get()->toArray()
                        );

                        $instance->orWhere('ckli_servico', 'like', '%'. $filtro['search']['value'] .'%');
                    }
                }

                return $instance;
            })
            ->editColumn('ckli_os_externo', function($interno){
                return  $interno->ckli_os_externo_servico . ' / ' . $interno->ckli_os_externo;
            })
            ->editColumn('clie_id', function($interno){
                $cliente = $interno->checklistInternoCliente()->first();
                $retorno = "";

                if(isset($cliente->clie_nome_fantasia) && !empty($cliente->clie_nome_fantasia)){
                    $retorno = mb_strtoupper($cliente->clie_nome_fantasia,'UTF-8');
                } else if(isset($cliente->clie_nome_razao_social) && !empty($cliente->clie_nome_razao_social)){
                    $retorno = mb_strtoupper($cliente->clie_nome_razao_social,'UTF-8');
                }

                return $retorno;
            })
            ->editColumn('veic_id', function($interno){
                $veiculo = $interno->checklistInternoVeiculo()->first();
                $retorno = "";

                if(isset($veiculo->veic_modelo) && !empty($veiculo->veic_modelo)){
                    $retorno =  mb_strtoupper($veiculo->veic_modelo .' / '. Mask::placaVeiculo($veiculo->veic_placa),'UTF-8');
                }

                return $retorno;
            })
            ->editColumn('ckli_servico', function($interno){
                $retorno = "";

                if(isset($interno->ckli_observacao) && !empty($interno->ckli_observacao)){
                    $retorno = $interno->ckli_servico .' # '.$interno->ckli_observacao;
                } else if(!empty($interno->ckli_servico)) {
                    $retorno = mb_strtoupper($interno->ckli_servico,'UTF-8');
                }

                return $retorno;
            })
            ->editColumn('ckli_status', function($interno){
                return SituacaoUtils::getTituloCheckList($interno->ckli_status);
            })
            ->addColumn('action', function ($interno){
                $urlEdit = "oficina/checklist/interno/". $interno->ckli_id ."/edit";

                return '<div style="text-align: center">
                                <a href="'. url($urlEdit) .'" title="Editar CheckList Interno" data-interno="'. $interno->ckli_id .'" class="btn btn-default btn-circle margin-right-5"><i class="fa fa-edit"></i></a>
                                <button title="Excluir CheckList Interno" data-interno="'.$interno->ckli_id.'" class="btn btn-default btn-circle margin-right-5" onclick="jQueryCheckListInterno.deletarCheckListInterno($(this))"><i class="fa fa-trash-o"></i></button>
                                <a target="_blank" title="Imprimir CheckList Interno" href="'. url('oficina/checklist/interno/imprimir?id=') . $interno->ckli_id.'" style="margin: 4px" class="btn btn-default btn-circle"><i class="fa fa-print"></i></a>
                        </div>';
            })
            ->make(true);
    }

    /**
     * @param $dados
     * @return array
     */
    private function montarFiltro($dados){
        $filtro = [
            ['empr_id', '=', Session::get('empr_id')]
        ];

        if(isset($dados['status']) != 0 && !empty($dados['status'])){
            $filtro[] = ['ckli_status', '=', $dados['status'] ];
        }

        if(isset($dados['id_cliente']) && !empty($dados['id_cliente'])){
            $filtro[] = ['clie_id', '=',  $dados['id_cliente']];
        }

        if(isset($dados['id_veiculo']) && !empty($dados['id_veiculo'])){
            $filtro[] = ['veic_id','=', $dados['id_veiculo']];
        }
        return $filtro;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deletarCheckListInterno($id){

        $interno = $this->findById($id);

        return $interno->delete();
    }

    /**
     * @param array $dadosCliente
     * @return mixed
     * @throws \Exception
     */
    public function cadastroCliente($dadosCliente = []){

        // Reservando id na tabela endereço
        $endereco = new EnderecoModel();

        $endereco->save();

        // Salvando o contato
        $contato = new ContatoModel();

        $contato->cont_cel_1 = Mask::removerMascara($dadosCliente['cad_celular_1']) ?? null;
        if(!$contato->save()){
            Common::setError('Erro ao salvar os dados do Contato!');
        }

        $cliente = new ClienteModel();
        $cliente->clie_nome_razao_social = $dadosCliente['cad_nome'];
        $cliente->clie_tipo = '3';
        $cliente->clie_status = '1';
        $cliente->ende_id = $endereco->ende_id;
        $cliente->cont_id = $contato->cont_id;
        $cliente->empr_id = Session::get('empr_id');

        if(!$cliente->save()){
            Common::setError('Erro ao salvar os dados do Cliente!');
        }

        $veiculo = new VeiculoModel();

        $veiculo->veic_marca           = $dadosCliente['vei_marca'];
        $veiculo->veic_modelo          = $dadosCliente['vei_modelo'];
        $veiculo->veic_placa           = Mask::removerMascara($dadosCliente['vei_placa']);
        $veiculo->clie_id              = $cliente->clie_id;
        $veiculo->empr_id              =(int) Session::get('empr_id');

        if(!$veiculo->save()){
            Common::setError('Houve um erro ao salvar os dados!');
        }
        $clienteNovo = array_merge($cliente->toArray(), $veiculo->toArray());
        return $clienteNovo;
    }

    /**
     * @param array $dadosVeiculo
     * @return mixed
     * @throws \Exception
     */
    public function cadastroVeiculo($dadosVeiculo = []){

        $veiculo = new VeiculoModel();

        $veiculo->veic_marca           = $dadosVeiculo['cad_marca'];
        $veiculo->veic_modelo          = $dadosVeiculo['cad_modelo'];
        $veiculo->veic_placa           = Mask::removerMascara($dadosVeiculo['cad_placa']);
        $veiculo->clie_id              = $dadosVeiculo['id_cliente'];
        $veiculo->empr_id              =(int) Session::get('empr_id');

        if(!$veiculo->save()){
            Common::setError('Houve um erro ao salvar os dados!');
        }

        return $veiculo;
    }

    /**
     * @param $id
     * @return void
     */
    public function imprimirCheckList($id){
        $file = new CheckListPDF('P','mm','A4');
        $file->SetCreator('LMS', true);
        $file->SetFont('Times','B',12);
        $file->AddPage();
        $interno = CheckListInternoModel::with(['checklistInternoVeiculo', 'checklistInternoCliente'])
            ->where('ckli_id', '=', $id)
            ->firstOrFail()->toArray();
        $mecanicos = CheckListMecanicoModel::with('mecanicoColaborador')->where('ckli_id', $interno['ckli_id'] )->get()->toArray();

        $file->SetY(8);
        $file->SetX(150);
        $file->SetFont('Times','B',10);
        $file->Cell(3,3,'Data: '.Mask::dataPtBr($interno['created_at']));
        $file->ln(2);
        $file->SetX(150);
        $file->SetFont('Times','B',10);
        $file->Cell(2,10,utf8_decode('Serviços:'));
        $file->SetX($file->GetX()+15);
        $file->SetFont('Times','B',12);
        $file->Cell(15,10,$interno['ckli_os_externo_servico']);
        $file->ln(5);
        $file->SetX(150);
        $file->SetFont('Times','B',10);
        $file->Cell(2,10,utf8_decode('Peças:'));
        $file->SetX($file->GetX()+10);
        $file->SetFont('Times','B',12);
        $file->Cell(15,10,$interno['ckli_os_externo']);
        $file->ln(12);
        $file->SetX(8);
        $file->SetFont('Times','B',12);
        $file->Cell(190, 7,utf8_decode('CHECK-LIST DE ENTRADA  /  ' . mb_strtoupper(SituacaoUtils::getTituloCheckList($interno['ckli_status']),mb_internal_encoding())) ,1,1, 'C');
        $file->SetFont('Times','B',10);
        $file->Cell(15,10,'Cliente:');
        $file->SetFont('Times','B',10);
        $file->Cell(110,10,utf8_decode(strtoupper($interno['checklist_interno_cliente']['clie_nome_razao_social'] ?? $interno['checklist_interno_cliente']['clie_nome_fantasia'])));
        $file->SetFont('Times','B',10);
        $file->Cell(15,10,utf8_decode('Veículo: ' . strtoupper($interno['checklist_interno_veiculo']['veic_marca'] .' '. $interno['checklist_interno_veiculo']['veic_modelo'])));
        $file->SetFont('Times','B',10);
        $file->Cell(30,10,'');
        $file->ln(5);
        $file->SetFont('Times','B',10);
        $file->Cell(190,10,utf8_decode('Cor: ' . strtoupper(($interno['checklist_interno_veiculo']['veic_cor']) ? VeiculoCorModel::where('veco_id',$interno['checklist_interno_veiculo']['veic_cor'])->get()->toArray()[0]['veco_titulo'] : '') .
            '                                                               Placa: ' . strtoupper(Mask::placaVeiculo($interno['checklist_interno_veiculo']['veic_placa'])).
            '                                                                           KM: '.$interno['ckli_km']));
        $file->ln(5);
        $file->Rect(8, 34, 190, 80, 'D');
        $file->SetFont('Times','B',10);
        $file->Cell(13,10,utf8_decode('Serviços:'));
        $file->SetFont('Times','B',12);
        $file->ln(8);
        $file->MultiCell(185,5,utf8_decode(mb_strtoupper($interno['ckli_servico'],mb_internal_encoding())));
        $file->SetFont('Times','B',10);
        $mecRelacao = '';
        foreach ($mecanicos as $date){
            $mecRelacao = $mecRelacao . strtoupper(explode(" ",$date['mecanico_colaborador']['cola_nome'])[0]).', ';
        }
        $file->Cell(17,10,utf8_decode('Mecânico: ' . $mecRelacao));
        $file->ln(8);
        $file->Cell(33,10,utf8_decode('Previsão de entrega : ' . Mask::dataPtBr($interno['ckli_previsao_entrega'], false)),0,1);
        $file->SetFont('Times','B',12);
        $file->Cell(190,10,utf8_decode('DEVOLVER NO ESCRITÓRIO E ASSINAR A NOTA!'), 0 ,0, 'C');

        $file->Output();
        exit;
    }

    /**
     * @param $filtro
     * @return \Illuminate\Http\JsonResponse
     */
    public function relatorio($filtro){
        $dataInicial = ($filtro['data_inicio']) ? Carbon::createFromFormat('d/m/Y', $filtro['data_inicio'])->format('Y-m-d 00:00:00') : null;
        $dataFinal  = ($filtro['data_fim']) ? Carbon::createFromFormat('d/m/Y', $filtro['data_fim'])->format('Y-m-d 00:00:00') : null;

        $dadosChecklistInterno = CheckListInternoModel::with(['checklistInternoVeiculo', 'checklistInternoCliente','checklistMecanico' ])
            ->where($this->montarFiltro($filtro))->orderBy('ckli_servico')
            ->where(function ($query) use($dataInicial,$dataFinal){
                if(isset($dataInicial) && !empty($dataInicial) && isset($dataFinal) && !empty($dataFinal)){
                    $query->whereBetween('created_at', [$dataInicial, $dataFinal]);
                }
            })
            ->whereHas('checklistMecanico', function($query) use ($filtro){
                if(isset($filtro['id_colaborador']) && !empty($filtro['id_colaborador'])) {
                    $query->where('checklist_mecanico.cola_id', $filtro['id_colaborador']);
                }
            })
            ->get()->toArray();

        $pdf = new checklistPdf('L','mm','A4');
        $pdf->SetCreator('Leandro', true);

        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetLineWidth(0,5);
        $pdf->SetY(10);
        $pdf->SetX(198);
        $pdf->SetFont('Times','B',10);
        $pdf->Cell(80,3,'Data: ' . Carbon::now()->format('d/m/Y H:i:s'),0,0,'C' );
        $pdf->ln(5);
        $pdf->SetX(150);
        $pdf->SetFont('Times','B',10);
        $pdf->SetX(198);
        $pdf->Cell(80,10,'Total: ' . (string) count($dadosChecklistInterno),0,0,'C' );
        $pdf->SetX($pdf->GetX()+10);
        $pdf->ln(11);
        $pdf->SetFont('Times','B',12);
        $pdf->Cell(278, 7,utf8_decode('RELATÓRIO DE CHECK-LIST'),1,1, 'C');
        $pdf->SetFillColor(192);


        $pdf->Cell(20,5,utf8_decode('OS'),1,0,'C',1);
        $pdf->Cell(35,5,utf8_decode('Cliente'),1,0,'C',1);
        $pdf->Cell(40,5,utf8_decode('Veículo'),1,0,'C',1);
        $pdf->Cell(133,5,utf8_decode('Serviços'),1,0,'C',1);
        $pdf->Cell(30,5,utf8_decode('Mecânicos'),1,0,'C',1);
        $pdf->Cell(20,5,utf8_decode('Valor'),1,1,'C',1);

        $pdf->SetFont('Arial','B',7);
        $pdf->SetWidths(array(20,35,40,133,30,20));
        $pdf->SetAligns(array('C', 'L', 'L','L','L','C',));
        foreach ($dadosChecklistInterno as $interno){

            if(isset($filtro['id_colaborador']) && !empty($filtro['id_colaborador'])) {
                $mecanicos = CheckListMecanicoModel::with('mecanicoColaborador')
                    ->where('ckli_id', $interno['ckli_id'] )
                    ->where('cola_id', $filtro['id_colaborador'])
                    ->get()->toArray();
            }else{
                $mecanicos = CheckListMecanicoModel::with('mecanicoColaborador')->where('ckli_id', $interno['ckli_id'] )->get()->toArray();
            }


            $mecRelacao = '';
            foreach ($mecanicos as $date){
                $mecRelacao = $mecRelacao . strtoupper(explode(" ",$date['mecanico_colaborador']['cola_nome'])[0])."\n";
            }
            $pdf->Row(array($interno['ckli_os_externo_servico'].' / '.$interno['ckli_os_externo'],utf8_decode(strtoupper($interno['checklist_interno_cliente']['clie_nome_razao_social'] ?? $interno['checklist_interno_cliente']['clie_nome_fantasia'])),
                utf8_decode(strtoupper(Mask::placaVeiculo($interno['checklist_interno_veiculo']['veic_placa']). " / " . mb_strtoupper($interno['checklist_interno_veiculo']['veic_modelo'],mb_internal_encoding()) )),
                utf8_decode(mb_strtoupper($interno['ckli_servico'],mb_internal_encoding())),
                utf8_decode($mecRelacao),
                ($interno['ckli_valor']) ? Mask::dinheiro($interno['ckli_valor']) : ''
            ));
        }

        return response()->json(['success'=>1, 'pdf' => $pdf->Output()]);
    }
}
