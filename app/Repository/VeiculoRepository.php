<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 30/09/18
 * Time: 01:11
 */

namespace App\Repository;


use App\Model\VeiculoModel;
use App\Utils\ClienteUtils;
use App\Utils\Common;
use App\Utils\Mask;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class VeiculoRepository
{
    /**
     * @param array $veiculo
     * @return array
     * @throws \Exception
     */
    public function salvarDados($veiculo = array()){
        return $this->salvar(new VeiculoModel(), $veiculo);
    }

    /**
     * @param array $veiculo
     * @param int $idVeiculo
     * @return array
     * @throws \Exception
     */
    public function atualizarVeiculo(array $veiculo, int $idVeiculo){
        $dadosVeiculo = $this->findById($idVeiculo);

        return $this->salvar($dadosVeiculo, $veiculo);
    }

    /**
     * @param null $dados
     * @return mixed
     * @throws \Exception
     */
    public function tabela($dados = null){
        $dadosTabela = VeiculoModel::with(['veiculoCliente'])
            ->where('empr_id', Session::get('empr_id'))
            ->get();

        return DataTables::of($dadosTabela)
            ->addColumn('action', function ($veiculo) use ($dados) {
                return  '<a href="'. url('oficina/veiculo/' . $veiculo['veic_id']) .'/edit" class="btn btn-default btn-circle" data-veiculo="'. $veiculo['veic_id'] .'"><i class="fa fa-pencil"></i></a>
                        <button class="btn btn-default btn-circle" data-veiculo="'. $veiculo['veic_id'] .'" onclick="jQueryVeiculo.deletarVeiculo($(this))"><i class="fa fa-trash-o"></i></button>';
            })
            ->editColumn('cliente', function($veiculo){
                return $veiculo->veiculoCliente->clie_nome_razao_social;
            })
            ->editColumn('placa', function($veiculo){
                return Mask::placaVeiculo($veiculo->veic_placa);
            })
            ->editColumn('marca_modelo', function($veiculo){
                return strtoupper($veiculo->veic_marca) . ' / ' . strtoupper($veiculo->veic_modelo);
            })
            ->make(true);
    }

    /**
     * @param int $idVeiculo
     * @return VeiculoModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function findById(int $idVeiculo){
        return VeiculoModel::with(['veiculoCliente'])
            ->where('veic_id', $idVeiculo)
            ->where('empr_id', Session::get('empr_id'))
            ->first();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function deletarVeiculo(int $id){
        return VeiculoModel::where('veic_id', $id)->delete();
    }

    /**
     * @param VeiculoModel $veiculoModel
     * @param array $dadosVeiculo
     * @return array
     * @throws \Exception
     */
    private function salvar(VeiculoModel $veiculoModel, array $dadosVeiculo){

        $veiculoModel->veic_marca           = $dadosVeiculo['marca'];
        $veiculoModel->veic_modelo          = $dadosVeiculo['modelo'];
        $veiculoModel->veic_placa           = Mask::removerMascara($dadosVeiculo['placa']);
        $veiculoModel->veic_cor             =(int) $dadosVeiculo['cor'];
        $veiculoModel->veic_combustivel     =(int) $dadosVeiculo['combustivel'];
        $veiculoModel->veic_ano_fabricacao  =(int) $dadosVeiculo['ano_fabricacao'];
        $veiculoModel->veic_ano_modelo      =(int) $dadosVeiculo['ano_modelo'];
        $veiculoModel->veic_chassi          = $dadosVeiculo['chassi'];
        $veiculoModel->veic_km_inicial      = $dadosVeiculo['km_inicial'];
        $veiculoModel->clie_id              =(int) $dadosVeiculo['id_cliente'];
        $veiculoModel->empr_id              =(int) Session::get('empr_id');

        if(!$veiculoModel->save()){
            Common::setError('Houve um erro ao salvar os dados!');
        }

        return ['success' => 1];
    }

    public function autocomplete($valor, $idCliente){
        if($idCliente){
            if($valor != '**') {
                $veiculos = VeiculoModel::with('veiculoCliente')
                    ->where(function ($query) use ($valor) {
                        $query->where('veic_placa', 'like', $valor . '%')->orwhere('veic_modelo', 'like', $valor . '%');
                    })
                    ->where('clie_id', $idCliente)
                    ->where('empr_id', Session::get('empr_id'))
                    ->get()
                    ->toArray();
            }else{
                $veiculos = VeiculoModel::with('veiculoCliente')
                    ->where('clie_id', $idCliente)
                    ->where('empr_id', Session::get('empr_id'))
                    ->get()
                    ->toArray();
            }
        }

        if($idCliente == 0 && $valor != '**'){
            $veiculos = VeiculoModel::with('veiculoCliente')
                ->where(function($query) use ($valor){
                    $query->where('veic_placa', 'like', $valor . '%')->orwhere('veic_modelo', 'like', $valor . '%');
                })
                ->where('empr_id', Session::get('empr_id'))
                ->get()
                ->toArray();
        }

        if($idCliente == 0 && $valor == '**'){
            $veiculos = VeiculoModel::with('veiculoCliente')
                ->where('empr_id', Session::get('empr_id'))
                ->get()
                ->toArray();
        }

        return $veiculos;
    }
}
