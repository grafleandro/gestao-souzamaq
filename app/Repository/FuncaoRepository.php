<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 14/09/18
 * Time: 23:19
 */

namespace App\Repository;


use App\Model\FuncaoModel;
use App\Utils\Common;
use App\Utils\FuncaoUtils;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class FuncaoRepository
{
    /**
     * @param $dadosFuncao
     * @return array
     * @throws \Exception
     */
    public function salvarDados($dadosFuncao){

        $resposta = FuncaoModel::create([
            'func_nome'     => $dadosFuncao['funcao_titulo'],
            'func_descricao'=> $dadosFuncao['funcao_descricao'],
            'func_sigla'    => FuncaoUtils::prefixoSigla($dadosFuncao['funcao_titulo']),
            'func_padrao'   => 1,
            'empr_id'       => Session::get('empr_id'),
        ]);

        if(!$resposta->save()){
            Common::setError('Erro ao salvar os dados da Categoria!');
        }

        return ['success' => 1];
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $dadosFuncao = FuncaoModel::where('empr_id', '=', Session::get('empr_id'))
            ->orWhere('func_padrao', 0)
            ->orderBy('func_nome')
            ->get()
            ->toArray();

        return Datatables::of($dadosFuncao)
            ->addColumn('action', function ($funcao){

                if($funcao['func_padrao']){
                    return '<div style="text-align: center">
                                <button title="Editar Categoria" data-funcao="'. $funcao['func_id'] .'" style="margin: 4px" class="btn btn-default" onclick="jQueryFuncao.editarFuncao($(this))"><i class="fa fa-edit"></i></button>
                                <button title="Excluir Categoria" data-funcao="'. $funcao['func_id'] .'" style="margin: 4px" class="btn btn-default" onclick="jQueryFuncao.deletarFuncao($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
                }

                return '<span class="label bg-color-blueDark">Função Padrão</span>';
            })
            ->make(true);
    }


    /**
     * @param $dadosFuncao
     * @param $idFuncao
     * @return mixed
     */
    public function atualizarDados($dadosFuncao, $idFuncao){
        $funcao = $this->findById($idFuncao);

        $funcao->func_nome = $dadosFuncao['funcao_titulo'];
        $funcao->func_sigla = FuncaoUtils::prefixoSigla($dadosFuncao['funcao_titulo']);
        $funcao->func_descricao = $dadosFuncao['funcao_descricao'];

        return $funcao->save();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id){
        return FuncaoModel::get()->find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deletarFuncao($id){
        $funcao = $this->findById($id);

        return $funcao->delete();
    }
}
