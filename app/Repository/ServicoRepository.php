<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 20/11/18
 * Time: 08:32
 */

namespace App\Repository;


use App\Model\ServicoModel;
use App\Model\ServicoProdutoModel;
use App\Utils\Common;
use App\Utils\Mask;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class ServicoRepository
{
    /**
     * @param $dadosServico
     * @return array
     * @throws \Exception
     */
    public function salvarDados($dadosServico){
        return $this->salvar(new ServicoModel(), new ServicoProdutoModel(), $dadosServico);
    }

    /**
     * @param $dadosServico
     * @param $idServico
     * @return array
     * @throws \Exception
     */
    public function atualizarDados($dadosServico, $idServico){
        $servico = $this->findById($idServico);

        return $this->salvar($servico, new ServicoProdutoModel(), $dadosServico);
    }


    /**
     * @param ServicoModel $servicoModel
     * @param ServicoProdutoModel $servicoProdutoModel
     * @param array $dadosServico
     * @return array
     * @throws \Exception
     */
    private function salvar(ServicoModel $servicoModel, ServicoProdutoModel $servicoProdutoModel, array $dadosServico){

        $servicoModel->serv_titulo = $dadosServico['serv_titulo'];
        $servicoModel->serv_aliq_iss = Mask::removarMascaraPorcent($dadosServico['serv_aliq_iss']);
        $servicoModel->serv_valor = Mask::removerMascaraDinheiro($dadosServico['serv_valor']);
        $servicoModel->serv_cota_comissao   = Mask::removarMascaraPorcent($dadosServico['cota_comissao']);
        $servicoModel->serv_cota_empresa    = Mask::removarMascaraPorcent($dadosServico['cota_empresa']);
        $servicoModel->empr_id =(int) Session::get('empr_id');

        if(!$servicoModel->save()){
            Common::setError('Erro ao salvar os dados do Serviço!');
        }

        /* Salvando os Produtos do Servico */
        if(isset($dadosServico['tabela_prod_qtd']) && is_array($dadosServico['tabela_prod_qtd']) && count($dadosServico['tabela_prod_qtd'])){
            /* Remove todos os Produto do respectivo Produto */
            ServicoProdutoModel::where('serv_id', '=', $servicoModel->serv_id)->delete();

            /* Adicionando Produto ao respectivo Servico */
            $dadosProduto = [];
            foreach ($dadosServico['tabela_prod_qtd'] as $index => $produto){
                $dadosProduto[] = [
                    'prod_id'   =>(int) $produto['prod_id'], // ID do Produto
                    'sepr_qtd'  =>(int) $produto['qtd'], // Quantidade
                    'serv_id'   =>(int) $servicoModel->serv_id,
                    'created_at'=> Carbon::now(),
                    'updated_at'=> Carbon::now()
                ];
            }

            if(!$servicoProdutoModel->insert($dadosProduto)){
                Common::setError('Erro ao tentar salvar os dados do Produto deste serviço!');
            }
        }

        return ['success' => 1];
    }

    /**
     * @param int $idServico
     * @return ServicoModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function findById(int $idServico){
        return ServicoModel::with(['servicoEmpresa', 'servicoProduto' => function($query){
                return $query->with(['produto', 'produtoValores']);
            }])
            ->where('serv_id', $idServico)
            ->firstOrFail();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $dadosServico = ServicoModel::with(['servicoEmpresa', 'servicoProduto' => function($query){
                return $query->with(['produto', 'produtoValores']);
            }])
            ->where('empr_id', Session::get('empr_id'))
            ->get();

        return DataTables::of($dadosServico->toArray())
            ->addColumn('action', function ($servico) {
                return '<div style="text-align: center">
                                <a href="'. url('/oficina/servico/' . $servico['serv_id']) .'/edit" class="btn btn-default btn-circle" data-servico="'. $servico['serv_id'] .'"><i class="fa fa-pencil"></i></a>
                                <button class="btn btn-default btn-circle" data-servico="'. $servico['serv_id'] .'" onclick="jQueryServicos.deletarServico($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->editColumn('serv_aliq_iss', function($servico){
                return Mask::porcentagem($servico['serv_aliq_iss']);
            })
            ->editColumn('serv_valor', function($servico){
                return Mask::dinheiro($servico['serv_valor']);
            })
            ->make(true);
    }

    /**
     * @param $idServico
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function deletarServico($idServico){
        $servico = $this->findById($idServico);

        return $servico->delete();
    }

    public function autocomplete($search){

        if( $search != '**'){
            return ServicoModel::with(['servicoEmpresa', 'servicoProduto' => function($query){
                return $query->with(['produto', 'produtoValores']);
            }])
                ->where('empr_id', '=', Session::get('empr_id'))
                ->where('serv_titulo', 'like', $search. '%')
                ->get()
                ->toArray();
        }
        if($search == '**'){
            return ServicoModel::with(['servicoEmpresa', 'servicoProduto' => function($query){
                return $query->with(['produto', 'produtoValores']);
            }])
                ->where('empr_id', '=', Session::get('empr_id'))
                ->get()
                ->toArray();
        }
    }
}
