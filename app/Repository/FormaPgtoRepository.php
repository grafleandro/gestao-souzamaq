<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 24/11/18
 * Time: 09:29
 */

namespace App\Repository;


use App\Model\FormaPgtoModel;
use App\Utils\Common;
use App\Utils\Mask;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class FormaPgtoRepository
{
    /**
     * @param array $formaPgto
     * @return array
     * @throws \Exception
     */
    public function salvarDados(array $formaPgto){
        return $this->salvar(new FormaPgtoModel(), $formaPgto);
    }

    /**
     * @param array $formaPgto
     * @param int $idFormaPgto
     * @return array
     * @throws \Exception
     */
    public function atualizarDados(array $formaPgto, int $idFormaPgto){
        $dadosFormaPgto = $this->findById($idFormaPgto);

        return $this->salvar($dadosFormaPgto, $formaPgto);
    }

    /**
     * @param FormaPgtoModel $formaPgtoModel
     * @param array $dadosFormPgto
     * @return array
     * @throws \Exception
     */
    private function salvar(FormaPgtoModel $formaPgtoModel, array $dadosFormPgto){

        $formaPgtoModel->cofp_titulo = $dadosFormPgto['form-titulo'];
        $formaPgtoModel->cofp_descricao = $dadosFormPgto['form-descricao'];
        $formaPgtoModel->cofp_avista = $dadosFormPgto['form-tipo-pgto'];
        $formaPgtoModel->cofp_parcelas = $dadosFormPgto['form-parcelas'] ?? null;
        $formaPgtoModel->cofp_desconto_max = Mask::removarMascaraPorcent($dadosFormPgto['form-desconto-max']);
        $formaPgtoModel->cofp_maquininha_fornecedor = $dadosFormPgto['form-fornecedor'] ?? null;
        $formaPgtoModel->cofp_juros_parcela = Mask::removarMascaraPorcent($dadosFormPgto['form-juros-parcela'] ?? null);
        $formaPgtoModel->cofp_limite_parcela = Mask::removerMascaraDinheiro($dadosFormPgto['form-limite-parcela'] ?? null);
        $formaPgtoModel->cocb_id = $dadosFormPgto['conta_bancaria'];
        $formaPgtoModel->empr_id = Session::get('empr_id');

        if(!$formaPgtoModel->save()){
            Common::setError('Houve um erro ao salvar os dados da Forma de Pagamento!');
        }

        return ['success' => $formaPgtoModel->cofp_id];
    }

    /**
     * @param int $idFormaPgto
     * @return mixed
     */
    public function findById(int $idFormaPgto){
        return FormaPgtoModel::where('cofp_id', $idFormaPgto)->first();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $formaPgto = FormaPgtoModel::with(['contaBancaria' => function($query){
                return $query->with(['banco'])->get();
            }])
            ->where('empr_id', '=', Session::get('empr_id'))
            ->get()
            ->toArray();

        return Datatables::of($formaPgto)
            ->editColumn('cofp_tipo_pgto', function($pagamento){
                return $pagamento['cofp_avista'] ? 'à Vista' : 'a Prazo';
            })
            ->addColumn('action', function ($pagamento){
                return '<div style="text-align: center">
                                <button title="Editar Forma de Pagamento" data-form-pgto="'. $pagamento['cofp_id'] .'" class="btn btn-default btn-circle margin-right-5" onclick="jQueryConta.editarFormaPgto($(this))"><i class="fa fa-edit"></i></button>
                                <button title="Excluir Forma de Pagamento" data-form-pgto="'. $pagamento['cofp_id'] .'" class="btn btn-default btn-circle" onclick="jQueryFormaPgto.deletarFormaPgto($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->make(true);
    }

    /**
     * @param int $idFormaPgto
     * @return mixed
     */
    public function deletarFormaPgto(int $idFormaPgto){
        $objFormaPgto = $this->findById($idFormaPgto);

        return $objFormaPgto->delete();
    }
}
