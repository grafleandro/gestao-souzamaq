<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 30/10/18
 * Time: 15:45
 */

namespace App\Repository;


use App\Model\ContatoModel;
use App\Model\EnderecoModel;
use App\Model\ProdutoFabricanteModel;
use App\Utils\ClienteUtils;
use App\Utils\Common;
use App\Utils\FabricanteUtils;
use App\Utils\Mask;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class ProdutoFabricanteRepository
{
    /**
     * @param array $dadosFabricante
     * @return array
     * @throws \Exception
     */
    public function salvarDados($dadosFabricante = []){
        $respostaFab = ProdutoFabricanteModel::orWhere('fabr_cpf_cnpj', Mask::removerMascara($dadosFabricante['fab_cpf']))
            ->orWhere('fabr_cpf_cnpj', Mask::removerMascara($dadosFabricante['fab_cnpj']))
            ->where('empr_id', '=', Session::get('empr_id'))
            ->get();

        if($respostaFab->count()){
            $sigla = ($dadosFabricante['fab_tipo_pessoa'] == ClienteUtils::_P_FISICA) ? 'CPF' : 'CNPJ';

            Common::setError('Já possui um Fabricante cadastrado com esse '. $sigla .': ' . $respostaFab->fabr_cpf_cnpj);
        }

        return $this->salvarDadosFabricante(new ProdutoFabricanteModel(),
            new EnderecoModel(),
            new ContatoModel(),
            $dadosFabricante
        );
    }

    /**
     * @param array $dadosFabricante
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function atualizarDados($dadosFabricante = [], int $id){

        $fabricante = $this->findById($id);

        return $this->salvarDadosFabricante($fabricante,
            $fabricante->fabricanteEndereco()->getResults() ?? new EnderecoModel(),
            $fabricante->fabricanteContato()->getResults() ?? new ContatoModel(),
            $dadosFabricante
        );
    }

    /**
     * @param ProdutoFabricanteModel $fabricanteModel
     * @param EnderecoModel $enderecoModel
     * @param ContatoModel $contatoModel
     * @param array $dadosFabricante
     * @return array
     * @throws \Exception
     */
    public function salvarDadosFabricante(ProdutoFabricanteModel $fabricanteModel, EnderecoModel $enderecoModel, ContatoModel $contatoModel, $dadosFabricante = []){
        /* Salvando os dados de Endereco */
        $respostaEndereco = $this->salvarEndereco($enderecoModel, $dadosFabricante);

        /* Salvando os dados de Contato */
        $respostaContato = $this->salvarContato($contatoModel, $dadosFabricante);

        /* Salvando os dados do Fabricante */
        $fabricanteModel->fabr_nome_nome_fantazia = FabricanteUtils::getNomeOuRazaoSocial($dadosFabricante);
        $fabricanteModel->fabr_cpf_cnpj = Mask::removerMascara(FabricanteUtils::getCpfOuCnpj($dadosFabricante));;
        $fabricanteModel->fabr_rg_insc_estadual = FabricanteUtils::getRgOuInscEstadual($dadosFabricante);
        $fabricanteModel->fabr_razao_social = ($dadosFabricante['fab_razao_social']) ?? null;
        $fabricanteModel->fabr_tipo = ($dadosFabricante['fab_tipo_pessoa']) ?? null;
        $fabricanteModel->cont_id = $respostaContato->cont_id;
        $fabricanteModel->ende_id = $respostaEndereco->ende_id;
        $fabricanteModel->empr_id = Session::get('empr_id');

        if(!$fabricanteModel->save()){
            Common::setError('Erro ao salvar os dados do Fabricante!');
        }

        return ['success' => 1];
    }

    /**
     * @param EnderecoModel $enderecoModel
     * @param array $dadosFabricante
     * @return EnderecoModel
     * @throws \Exception
     */
    public function salvarEndereco(EnderecoModel $enderecoModel, $dadosFabricante = []){
        $enderecoModel->enlo_id = $dadosFabricante['tipo_logradouro'];
        $enderecoModel->ende_logradouro_titulo = $dadosFabricante['rua'] ;
        $enderecoModel->ende_bairro = $dadosFabricante['bairro'];
        $enderecoModel->ende_cidade = $dadosFabricante['cidade'];
        $enderecoModel->ende_estado = $dadosFabricante['estado'];
        $enderecoModel->ende_complemento = ($dadosFabricante['complemento']) ? $dadosFabricante['complemento'] : null;
        $enderecoModel->ende_numero = $dadosFabricante['numero'];
        $enderecoModel->ende_cep = Mask::removerMascara($dadosFabricante['cep']);

        if(!$enderecoModel->save()){
            Common::setError('Erro ao salvar os dados do Endereço!');
        }

        return $enderecoModel;
    }

    /**
     * @param ContatoModel $contatoModel
     * @param array $dadosFabricante
     * @return ContatoModel
     * @throws \Exception
     */
    public function salvarContato(ContatoModel $contatoModel, $dadosFabricante = []){
        $contatoModel->cont_cel_1 = (isset($dadosFabricante['celular_1'])) ? Mask::removerMascara($dadosFabricante['celular_1']) : null;
        $contatoModel->cont_cel_2 = (isset($dadosFabricante['celular_2'])) ? Mask::removerMascara($dadosFabricante['celular_2']) : null;
        $contatoModel->cont_tel_fixo = (isset($dadosFabricante['tel_fixo'])) ? Mask::removerMascara($dadosFabricante['tel_fixo']) : null;
        $contatoModel->cont_email = (isset($dadosFabricante['email'])) ? $dadosFabricante['email'] : null;

        if(!$contatoModel->save()){
            Common::setError('Erro ao salvar os dados do Contato!');
        }

        return $contatoModel;
    }

    /**
     * @param int $idFabricante
     * @return ProdutoFabricanteModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function findById(int $idFabricante){
        return ProdutoFabricanteModel::with(['fabricanteContato', 'fabricanteEndereco', 'fabricanteEmpresa'])
            ->where('fabr_id', $idFabricante)
            ->first();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $dadosFabricante = ProdutoFabricanteModel::with(['fabricanteContato', 'fabricanteContato', 'fabricanteEmpresa'])
            ->where('empr_id', '=', Session::get('empr_id'))
            ->get()
            ->toArray();

        return Datatables::of($dadosFabricante)
            ->editColumn('fabr_tipo', function($fabricante){
                return ClienteUtils::tituloTiposPessoaCliente($fabricante['fabr_tipo']);
            })
            ->editColumn('fabr_nome_razao', function($fabricante){
                $nomeRazao = '';

                if($fabricante['fabr_razao_social']){
                    $nomeRazao = $fabricante['fabr_razao_social'];
                } else if($fabricante['fabr_nome_nome_fantazia']){
                    $nomeRazao = $fabricante['fabr_nome_nome_fantazia'];
                }

                return $nomeRazao;
            })
            ->editColumn('fabr_cpf_cnpj', function($fabricante){
                $cpfCnpj = '';

                if($fabricante['fabr_tipo'] == ClienteUtils::_P_FISICA){
                    $cpfCnpj = Mask::cpf($fabricante['fabr_cpf_cnpj']);
                } else if($fabricante['fabr_tipo'] == ClienteUtils::_P_JURIDICA){
                    $cpfCnpj = Mask::cnpj($fabricante['fabr_cpf_cnpj']);
                }

                return $cpfCnpj;
            })
            ->editColumn('fabr_contato', function($fabricante){
                $contato = [];

                if($fabricante['fabricante_contato']['cont_cel_1']){
                    $contato[] = Mask::telCelular($fabricante['fabricante_contato']['cont_cel_1']);
                }

                if($fabricante['fabricante_contato']['cont_cel_2']){
                    $contato[] = Mask::telCelular($fabricante['fabricante_contato']['cont_cel_2']);
                }

                if($fabricante['fabricante_contato']['cont_tel_fixo']){
                    $contato[] = Mask::telCelular($fabricante['fabricante_contato']['cont_tel_fixo']);
                }

                return implode(' / ', $contato);
            })
            ->addColumn('action', function ($fabricante){
                $urlEdit = "empresa/fabricante/". $fabricante['fabr_id'] ."/edit";

                return '<div style="text-align: center">
                                <a href="'. url($urlEdit) .'" title="Editar Fabricante" data-fabricante="'.$fabricante['fabr_id'].'" style="margin: 4px" class="btn btn-default btn-circle"><i class="fa fa-edit"></i></a>
                                <button title="Excluir Fabricante" data-fabricante="'.$fabricante['fabr_id'].'" style="margin: 4px" class="btn btn-default btn-circle" onclick="jQueryFabricante.deletarFabricante($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->make(true);
    }

    /**
     * @param $id
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function deletarFabricante($id){
        $fabricante = $this->findById($id);

        $fabricante->fabricanteEndereco()->delete();
        $fabricante->fabricanteContato()->delete();

        return $fabricante->delete();
    }

    /**
     * @param $search
     * @return mixed
     */
    public function autocomplete($search){

        if(strlen($search) == 2 && $search == '**'){
            $dadosGrupo = ProdutoFabricanteModel::where('empr_id', '=', Session::get('empr_id'))
                ->get()
                ->toArray();
        } else {
            $dadosGrupo = ProdutoFabricanteModel::where('empr_id', '=', Session::get('empr_id'))
                ->orWhere('fabr_nome_nome_fantazia', 'like', "{$search}%")
                ->orWhere('fabr_cpf_cnpj', 'like', "{$search}%")
                ->get()
                ->toArray();
        }

        return $dadosGrupo;
    }
}
