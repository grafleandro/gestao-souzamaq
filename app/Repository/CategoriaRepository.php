<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 13/09/18
 * Time: 23:00
 */

namespace App\Repository;


use App\Model\CategoriaModel;
use App\Utils\Common;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class CategoriaRepository
{
    /**
     * @param $dadosCategoria
     * @return array
     * @throws \Exception
     */
    public function salvarDados($dadosCategoria){
        $resposta = CategoriaModel::create([
            'cate_titulo' => $dadosCategoria['categoria_titulo'],
            'cate_descricao' => $dadosCategoria['categoria_descricao'],
            'empr_id' => Session::get('empr_id'),
        ]);

        if(!$resposta->save()){
            Common::setError('Erro ao salvar os dados da Categoria!');
        }

        return ['success' => 1];
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $dadosCategoria = CategoriaModel::where('empr_id', '=', Session::get('empr_id'))->get()->toArray();

        return Datatables::of($dadosCategoria)
            ->addColumn('action', function ($categoria){
                return '<div style="text-align: center">
                                <button title="Editar Categoria" data-categoria="'.$categoria['cate_id'].'" style="margin: 4px" class="btn btn-default" onclick="jQueryCategoria.editarCategoria($(this))"><i class="fa fa-edit"></i></button>
                                <button title="Excluir Categoria" data-categoria="'.$categoria['cate_id'].'" style="margin: 4px" class="btn btn-default" onclick="jQueryCategoria.deletarCategoria($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->make(true);
    }

    /**
     * @param $dadosCategoria
     * @param $idCategoria
     * @return mixed
     */
    public function atualizarDados($dadosCategoria, $idCategoria){
        $categoria = $this->findById($idCategoria);

        $categoria->cate_titulo = $dadosCategoria['categoria_titulo'];
        $categoria->cate_descricao = $dadosCategoria['categoria_descricao'];

        return $categoria->save();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id){
        return CategoriaModel::get()->find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deletarCategoria($id){
        $categoria = $this->findById($id);

        return $categoria->delete();
    }
}
