<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 16/11/18
 * Time: 10:01
 */

namespace App\Repository;


use App\Model\CheckListInternoModel;
use App\Model\ClienteModel;
use App\Model\ConfiguracaoNumeracaoModel;
use App\Model\ContasReceberModel;
use App\Model\ContatoModel;
use App\Model\EnderecoModel;
use App\Model\OperacaoPagamentoModel;
use App\Model\OperacaoParcelasModel;
use App\Model\OrdemServicoMecanicoModel;
use App\Model\OrdemServicoModel;
use App\Model\OSPagamentoModel;
use App\Model\OSRelacaoProdutoModel;
use App\Model\OSRelacaoServicoModel;
use App\Model\VeiculoModel;
use App\Utils\checklistPdf;
use App\Utils\Common;
use App\Utils\ConfiguracaoUtils;
use App\Utils\Mask;
use App\Utils\MoneyUtils;
use App\Utils\ordemServicoPDF;
use App\Utils\SituacaoUtils;
use App\Utils\VeiculoUtils;
use App\Utils\VendaUtils;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class OrdemServicoRepository
{
    /* Identificar a origem da Contas a Receber Lancada
     * OS: Ordem de Serviço
     **/
    const OPERACAO_ORIGEM_OS = 1;


    private $OrdemServicoModel;
    private $produtoRepository;
    private $checklistInternoRepository;

    /**
     * OrdemServicoRepository constructor.
     * @param OrdemServicoModel $ordemServicoModel
     */
    public function __construct(OrdemServicoModel  $ordemServicoModel, ProdutoRepository $produtoRepository, CheckListInternoRepository $checkListInternoRepository)
    {
        $this->OrdemServicoModel = $ordemServicoModel;
        $this->produtoRepository = $produtoRepository;
        $this->checklistInternoRepository = $checkListInternoRepository;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function selecionaChecklist(){

        $dadosChecklistInterno = CheckListInternoModel::with(['checklistInternoVeiculo', 'checklistInternoCliente', 'checklistMecanico'])
            ->where('empr_id', '=', Session::get('empr_id'))
            ->where('ckli_status', SituacaoUtils::_ORDEM_SERVICO)
            ->get()->toArray();

        return Datatables::of($dadosChecklistInterno)

            ->editColumn('cliente', function($interno){
                return ($interno['checklist_interno_cliente']['clie_nome_fantasia']) ? mb_strtoupper($interno['checklist_interno_cliente']['clie_nome_fantasia'],'UTF-8') : mb_strtoupper($interno['checklist_interno_cliente']['clie_nome_razao_social'],'UTF-8') ;
            })
            ->editColumn('veiculo', function($interno){
                return mb_strtoupper($interno['checklist_interno_veiculo']['veic_modelo'].' / '.Mask::placaVeiculo($interno['checklist_interno_veiculo']['veic_placa']),'UTF-8');
            })
            ->editColumn('prev_entrega', function($interno){
                return ($interno['ckli_previsao_entrega']) ? Carbon::createFromFormat('Y-m-d', $interno['ckli_previsao_entrega'])->format('d/m/Y') : '';
            })

            ->make(true);
    }

    /**
     * @param array $dadosOrdemServico
     * @return mixed
     * @throws \Exception
     */
    public function salvarDados($dadosOrdemServico = []){
        if($dadosOrdemServico['id_cliente'] != 0 &&  $dadosOrdemServico['id_veiculo'] != 0){
            $ordemServico = $this->salvar(new OrdemServicoModel(), $dadosOrdemServico, false);
        }else{
            dd($dadosOrdemServico);
        }
        return $ordemServico->orse_id;
    }

    /**
     * @param array $dadosOrdemServico
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function atualizarDados($dadosOrdemServico = [], int $id){

        $ordemServico = $this->findById($id);

        return $this->salvar($ordemServico, $dadosOrdemServico, true)->orse_id;

    }

    /**
     * @param int $idOS
     * @return mixed
     */
    public function findById($idOS = 0){
        return OrdemServicoModel::where('orse_id', $idOS)->first();
    }

    /**
     * @param $mecanicos
     * @param $id_orse
     * @throws \Exception
     */
    private function salvarMecanico($mecanicos, $id_orse ){

        foreach ($mecanicos as $mec) {
            $OSMecanico = new OrdemServicoMecanicoModel();
            $OSMecanico->orse_id = $id_orse;
            $OSMecanico->cola_id = $mec;

            if (!$OSMecanico->save()) {
                Common::setError('Erro ao salvar os dados do mecânico!');
            }
        }
    }

    /**
     * @param $servicos
     * @param $id_orse
     * @throws \Exception
     */
    private function salvarProdutoOS($produtos, $id_orse){

        foreach ($produtos as $prod) {
            $Produto = new OSRelacaoProdutoModel();
            $Produto->orse_id = $id_orse;
            $Produto->osrp_prod_valor = (float) $prod['prod_valor'];
            $Produto->osrp_prod_qtd = (float)  $prod['prod_qtd'];
            $Produto->prod_id = $prod['prod_id'];


            if (!$Produto->save()) {
                Common::setError('Erro ao salvar os dados o produto!');
            }
        }
    }

    /**
     * @param $servicos
     * @param $id_orse
     * @throws \Exception
     */
    private function salvarServicoOS($servicos, $id_orse){

        foreach ($servicos as $serv) {
            $Servico = new OSRelacaoServicoModel();
            $Servico->orse_id = $id_orse;
            $Servico->osrs_valor_servico = (float) $serv['serv_valor'];
            $Servico->osrs_descricao_servico = $serv['desc_servico'] ?? null;
            $Servico->osrs_qtd_servico = (float) $serv['serv_qtd'];
            $Servico->serv_id = $serv['serv_id'];

            if (!$Servico->save()) {
                Common::setError('Erro ao salvar os dados o serviço!');
            }
        }
    }

    /**
     * @param OrdemServicoModel $ordemServicoModel
     * @param array $dadosOrdemServico
     * @return OrdemServicoModel|array
     * @throws \Exception
     */
    public function salvar(OrdemServicoModel $ordemServicoModel, $dadosOrdemServico = [], $edit = false){

        /* Salvando os dados do OrdemServico */
        $ordemServicoModel->orse_situacao = $dadosOrdemServico['situacao'];
        $ordemServicoModel->orse_prev_entrega = (isset($dadosOrdemServico['dt_entrega'])) ? Carbon::createFromFormat('d/m/Y', $dadosOrdemServico['dt_entrega'])->format('Y-m-d H:i:s') : null;
        $ordemServicoModel->orse_descri_serv_cliente = $dadosOrdemServico['desc_servico'] ?? null;
        $ordemServicoModel->orse_observacao = $dadosOrdemServico['observacao'] ?? null;
        $ordemServicoModel->orse_numero = ($edit) ? $ordemServicoModel->orse_numero : ConfiguracaoUtils::atualizaNumero('conu_ordem_servico');
        $ordemServicoModel->orse_total_servico = $dadosOrdemServico['totalservico'];
        $ordemServicoModel->orse_total_produtos = $dadosOrdemServico['totalProduto'];
        $ordemServicoModel->orse_total_os = $dadosOrdemServico['total_os'];
        $ordemServicoModel->empr_id = (int) Session::get('empr_id');
        $ordemServicoModel->clie_id = $dadosOrdemServico['id_cliente'];
        $ordemServicoModel->ckli_id = $dadosOrdemServico['checklist_id'] ?? null;
        $ordemServicoModel->veic_id = $dadosOrdemServico['id_veiculo'];
        $ordemServicoModel->conu_id = null;
        $ordemServicoModel->created_at = Carbon::now();
        $ordemServicoModel->updated_at = Carbon::now();

        if(!$ordemServicoModel->save()){
            Common::setError('Erro ao salvar os dados da Ordem de Serviço!');
        }

        /*Removendo todas os mecanicos relação serviço e produto caso seja edi~ao de OS*/
        if($edit){
            $this->removeRelacoesOS($ordemServicoModel->orse_id);
        }

        if(isset($dadosOrdemServico['mecanico'])) {
            $this->salvarMecanico($dadosOrdemServico['mecanico'], $ordemServicoModel->orse_id);
        }

        /** Salvando os dados do Produto da Ordem de Servico */
        if(isset($dadosOrdemServico['produto']) && !empty($dadosOrdemServico['produto'])) {
            $this->produtoRepository->salvarMovimentacao($dadosOrdemServico['produto'], $ordemServicoModel->orse_id, self::OPERACAO_ORIGEM_OS);
        }

        $this->salvarServicoOS( $dadosOrdemServico['servicos'], $ordemServicoModel->orse_id);


        return $ordemServicoModel;
    }

    /**
     * @param array $dadosOrdemServico
     * @return mixed
     * @throws \Exception
     */
    public function finalizarOS($dadosOrdemServico = []){
        if(isset($dadosOrdemServico['edit'])){

            $ordemServico = OrdemServicoRepository::findById($dadosOrdemServico['edit']);

            $this->salvar( $ordemServico, $dadosOrdemServico, true);

            foreach ($dadosOrdemServico['pagamento'] as $index => $pagamento){

                /** Salvando os dados das Parcelas, caso tenha */
                if(!empty($pagamento['parcela'] ))
                {
                    $this->finalizarPgtoPrazo($dadosOrdemServico, $pagamento, $dadosOrdemServico['edit']);

                } else {
                    $this->finalizarPgtoVista($pagamento, $dadosOrdemServico['edit']);
                }

                $ordemServicoModel = $this->findById($dadosOrdemServico['edit']);

                $ordemServicoModel->orse_situacao = SituacaoUtils::_FINALIZADA;

                if(!$ordemServicoModel->save()){
                    Common::setError('Erro ao finalizar a Ordem de Serviço!');
                }
            }

            return $dadosOrdemServico['edit'];

        }else{

            $ordemServico = $this->salvar(new OrdemServicoModel(), $dadosOrdemServico, false);


            foreach ($dadosOrdemServico['pagamento'] as $index => $pagamento){

                /** Salvando os dados das Parcelas, caso tenha */
                if(!empty($pagamento['parcela'] ))
                {
                    $this->finalizarPrazo($dadosOrdemServico, $pagamento, $ordemServico->orse_id);

                } else {
                    $this->finalizarVista($pagamento, $ordemServico->orse_id);
                }

            }

            $ordemServico->orse_situacao = SituacaoUtils::_FINALIZADA;

            if(!$ordemServico->save()){
                Common::setError('Erro ao finalizar a Ordem de Serviço!');
            }

            return $ordemServico->orse_id;
        }
    }

    /**
     * @param array $pagamento
     * @param $id_os
     */
    private function finalizarPgtoVista($pagamento = [], $id_os){
        /* Salvando as formas de pagamento Avista */
        $pagamentoModel = OSPagamentoModel::create([
            'ospa_valor' => (float) $pagamento['valor'],
            'ospa_parcelas' => (int) $pagamento['parcela'],
            'cofp_id' => (int) $pagamento['tipo_pgto'],
            'orse_id' => $id_os,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    /**
     * @param array $dadosOrdemServico
     * @param array $pagamento
     */
    private function finalizarPgtoPrazo($dadosOrdemServico = [], $pagamento = [], $id_origem){
        /** Criar lancamento em Contas a Receber */
        $contasReceber = ContasReceberModel::create([
            'core_titulo' => 'Contas a Receber - Ordem de Servico',
            'core_valor' => $pagamento['valor'],
            'core_dt_ocorrencia' => Carbon::now(),
            'core_origem_id' => $id_origem,
            'opor_id' => self::OPERACAO_ORIGEM_OS,
            'core_juros' => 0,
            'vest_id' => VendaUtils::VEND_ANDAMENTO,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $opPagamento = OperacaoPagamentoModel::create([
            'oppa_valor' => $pagamento['valor'],
            'oppa_parcelas' => $pagamento['parcela'] ?? 0,
            'oppa_desconto' => $pagamento['desconto'] ?? 0,
            'cofp_id' => (int) $pagamento['tipo_pgto'],
            'core_id' => $contasReceber->core_id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        $valorParcela = MoneyUtils::dividir($pagamento['valor'], $pagamento['parcela']);

        for($i = 1; $i <= $pagamento['parcela']; $i++){
            $opPgtoParcelas = OperacaoParcelasModel::create([
                'oppr_valor' => $valorParcela,
                'oppr_dt_vencimento' => Carbon::now()->addMonths($i),
                'oppa_id' => $opPagamento->oppa_id,
            ]);
        }
    }

    /**
     * @param int $id
     */
    private function removeRelacoesOS($id){
        $OSServico = OSRelacaoServicoModel::where('orse_id', $id)->get();
        $OSProduto = OSRelacaoProdutoModel::where('orse_id', $id)->get();
        $OSMec = OrdemServicoMecanicoModel::where('orse_id', $id)->get();

        if($OSServico){
            foreach ($OSServico as $servico) {
                $servico->delete();
            }
        }

        if($OSProduto){
            foreach ($OSProduto as $produto) {
                $produto->delete();
            }
        }

        if($OSMec){
            foreach ($OSMec as $OSMecanico) {
                $OSMecanico->delete();
            }
        }
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(array $dados){

        $queryOrdemServico = OrdemServicoModel::query()->with(['ordemServicoVeiculo', 'ordemServicoCliente', 'ordemServicoServicos'])
            ->where($this->montarFiltro($dados));

        return Datatables::eloquent($queryOrdemServico)
            ->editColumn('veic_id', function($veiculo){
                return mb_strtoupper($veiculo->ordemServicoVeiculo->veic_modelo.' / '.Mask::placaVeiculo($veiculo->ordemServicoVeiculo->veic_placa),'UTF-8');
            })
            ->editColumn('clie_id', function($cliente){
                return ($cliente->ordemServicoCliente->clie_nome_fantasia) ? mb_strtoupper($cliente->ordemServicoCliente->clie_nome_fantasia,'UTF-8') : mb_strtoupper($cliente->ordemServicoCliente->clie_nome_razao_social,'UTF-8') ;
            })
            ->editColumn('servico', function($servico){
                $servicoTitulo = [];

                foreach ($servico->ordemServicoServicos()->get() as $index => $servico){
                    $servicoTitulo[] = (new ServicoRepository())->findById($servico->serv_id)->serv_titulo;
                }

                return implode('; ', $servicoTitulo);
            })
            ->editColumn('situacao', function($situacao){
                return '<span class="badge '.SituacaoUtils::getClassSituacao($situacao->orse_situacao).'">'.SituacaoUtils::getTitulo($situacao->orse_situacao) .'</span>';
            })
            ->addColumn('action', function ($os){
                $urlEdit = "oficina/ordem_servico/". $os->orse_id ."/edit";

                return '<div style="text-align: center">
                                <a href="'. url($urlEdit) .'" title="Editar Ordem de Serviço" data-interno="'.$os->orse_id.'" style="margin: 4px" class="btn btn-default btn-circle"><i class="fa fa-edit"></i></a>
                                <button title="Excluir Ordem de Serviço" data-os="'.$os->orse_id.'" style="margin: 4px" class="btn btn-default btn-circle" onclick="jQueryOrdemServico.deletarOrdemServico($(this))"><i class="fa fa-trash-o"></i></button>
                                <a target="_blank" title="Imprimir Ordem de Serviço" href="'.url('oficina/ordem_servico/imprimir_os?id=') . $os['orse_id'].'" style="margin: 4px" class="btn btn-default btn-circle" ><i class="fa fa-print"></i></a>
                        </div>';
            })
            ->rawColumns(['situacao', 'action'])
            ->make(true);
    }

    /**
     * @param $dados
     * @return array
     */
    private function montarFiltro($dados){
        $filtro = [
            ['empr_id', '=', Session::get('empr_id')]
        ];

        if($dados['situacao'] != 0 && !empty($dados['situacao'])){
            $filtro[] = ['orse_situacao', '=', $dados['situacao'] ];
        }

        return $filtro;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deletarOrdemServico($id){

        $os = $this->findById($id);

        return $os->delete();
    }

    /**
     * @param $id
     */
    public function imprimir_os($id){
        $os = OrdemServicoModel::with(['ordemServicoVeiculo' => function($query){
            return $query->with(['veiculoCor'])->get();
        }, 'ordemServicoCliente' => function($query){
            return $query->with(['clienteEndereco', 'clienteContato'])->get();
        }, 'ordemServicoServicos' => function($query){
            return $query->with(['servico'])->get();
        }, 'ordemServicoProduto' => function($query){
            return $query->with(['produto'])->get();
        }])
            ->where('orse_id', $id)->where('empr_id', Session::get('empr_id'))->firstOrFail();

        $file = new ordemServicoPDF('P','mm','A4');
        $file->SetCreator('LMS', true);
        $file->SetFont('Arial','B',12);
        $file->AddPage();
        $file->SetY(8);
        $file->SetX(146);
        $file->SetFont('Arial','B',10);
        $file->Cell(54,3,utf8_decode('Situação: '. mb_strtoupper(SituacaoUtils::getTitulo($os->orse_situacao), mb_internal_encoding())),0, 0, 'C');
        $file->ln(4);
        $file->SetX(146);
        $file->SetFont('Arial','B',10);
        $file->Cell(54,10,($os->orse_situacao != SituacaoUtils::OS_ORCAMENTO) ? utf8_decode('ORDEM DE SERVIÇO') : utf8_decode('ORÇAMENTO'),0, 1, 'C');
        $file->SetX(146);
        $file->SetFont('Arial','B',20);
        $file->Cell(54,10,$os['orse_numero'],0, 0, 'C');
        $file->ln(10);
        $file->SetFont('Arial','B',9);
        $file->SetFillColor(192);
        $file->Cell(190, 6, utf8_decode('DADOS DO CLIENTE'), 1,1, 'L', 1);


        $file->Cell(120, 6, utf8_decode('Cliente: ') . utf8_decode(substr(strtoupper($os->ordemServicoCliente->clie_nome_razao_social),0,50)), 0);
        $file->Cell(70, 6,  ($os->ordemServicoCliente->clie_tipo == 3) ? 'CPF/CNPJ: ' .Mask::cpf($os->ordemServicoCliente->clie_cpf_cnpj) : 'CPF/CNPJ: ' . Mask::cnpj($os->ordemServicoCliente->clie_cpf_cnpj), 0, 1);

        $file->Cell(95, 6, utf8_decode('Endereço: ') . utf8_decode(substr(strtoupper($os->ordemServicoCliente->clienteEndereco->ende_logradouro_titulo.', ' . $os->ordemServicoCliente->clienteEndereco->ende_numero),0,50)), 0,0);
        $file->Cell(95, 6, utf8_decode('Cidade/UF: ') . utf8_decode(substr(strtoupper($os->ordemServicoCliente->clienteEndereco->ende_cidade.'/' . $os->ordemServicoCliente->clienteEndereco->ende_estado),0,50)), 0,1);


        $file->Cell(95, 6, utf8_decode('Telefone: ') . utf8_decode(substr( Mask::telCelular($os->ordemServicoCliente->clienteContato->cont_cel_1),0,50)), 0,0);
        $file->Cell(95, 6, utf8_decode('Email: ') . utf8_decode(substr($os->ordemServicoCliente->clienteContato->cont_email,0,50)), 0,1);

        $file->ln(2);

        $file->Cell(190, 6, utf8_decode('DADOS DO VEÍCULO'), 1,1, 'L', 1);

        $file->Cell(75, 6, utf8_decode('Modelo: ') . utf8_decode(substr(strtoupper($os->ordemServicoVeiculo->veic_modelo ?? ""),0,50)), 0,0);
        $file->Cell(40, 6, utf8_decode('Placa: ') . utf8_decode(substr(strtoupper(($os->ordemServicoVeiculo->veic_placa) ? Mask::placaVeiculo($os->ordemServicoVeiculo->veic_placa)  : ""),0,50)), 0,0);
        $file->Cell(75, 6, utf8_decode('Marca: ') . utf8_decode(substr(strtoupper($os->ordemServicoVeiculo->veic_marca ?? ""),0,50)), 0,1);

        $file->Cell(47, 6, utf8_decode('Cor: ') . utf8_decode(substr(strtoupper(($os->ordemServicoVeiculo->veic_cor) ? $os->ordemServicoVeiculo->veiculoCor->veco_titulo: ""),0,50)), 0,0);
        $file->Cell(47, 6, utf8_decode('Ano: ') . utf8_decode(($os->ordemServicoVeiculo->veic_ano_fabricacao && $os->ordemServicoVeiculo->veic_ano_modelo) ? $os->ordemServicoVeiculo->veic_ano_fabricacao .' / '. $os->ordemServicoVeiculo->veic_ano_modelo : ""), 0,0);
        $file->Cell(48, 6, utf8_decode('KM Atual: ') . utf8_decode(substr($this->checklistInternoRepository->findById($os->ckli_id)->ckli_km ?? "",0,50)), 0,0);
        $file->Cell(48, 6, utf8_decode('Combustivel: ') . utf8_decode(substr(strtoupper(($os->ordemServicoVeiculo->veic_combustivel) ? VeiculoUtils::tituloCombustivel($os->ordemServicoVeiculo->veic_combustivel) : ""),0,50)), 0,1);

        $file->ln(2);
        $file->Cell(190, 5, utf8_decode('SERVIÇOS'), 1,1, 'L', 1);
        $file->Cell(10, 5, utf8_decode('Cod.'), 1,0, 'C');
        $file->Cell(105, 5, utf8_decode('Descrição'), 1,0, 'C');
        $file->Cell(20, 5, utf8_decode('Qtd.'), 1,0, 'C');
        $file->Cell(25, 5, utf8_decode('Valor Un.'), 1,0, 'C');
        $file->Cell(30, 5, utf8_decode('Subtotal'), 1,1, 'C');

        foreach ($os->ordemServicoServicos as  $index => $servico){
            $file->Cell(10, 5, utf8_decode($servico->orse_id), 1,0, 'C');
            $file->Cell(105, 5, utf8_decode($servico->servico->serv_titulo), 1,0, 'L');
            $file->Cell(20, 5, utf8_decode($servico->osrs_qtd_servico), 1,0, 'C');
            $file->Cell(25, 5, utf8_decode(Mask::dinheiro($servico->osrs_valor_servico)), 1,0, 'C');
            $file->Cell(30, 5, utf8_decode(Mask::dinheiro($servico->osrs_qtd_servico * $servico->osrs_valor_servico)), 1,1, 'C');
        }

        $file->Cell(160, 5, utf8_decode('Total dos Serviço'), 1,0, 'R');
        $file->Cell(30, 5, utf8_decode(Mask::dinheiro($os->orse_total_servico)), 1,1, 'C');

        if(count($os->ordemServicoProduto)) {
            $file->ln(2);
            $file->Cell(190, 5, utf8_decode('PRODUTOS'), 1, 1, 'L', 1);
            $file->Cell(10, 5, utf8_decode('Cod.'), 1, 0, 'C');
            $file->Cell(105, 5, utf8_decode('Descrição'), 1, 0, 'C');
            $file->Cell(20, 5, utf8_decode('Qtd.'), 1, 0, 'C');
            $file->Cell(25, 5, utf8_decode('Valor Un.'), 1, 0, 'C');
            $file->Cell(30, 5, utf8_decode('Subtotal'), 1, 1, 'C');

            foreach ($os->ordemServicoProduto as $index => $produto) {
                $file->Cell(10, 5, utf8_decode($produto->osrp_id), 1, 0, 'C');
                $file->Cell(105, 5, utf8_decode($produto->produto->prod_titulo), 1, 0, 'L');
                $file->Cell(20, 5, utf8_decode($produto->osrp_prod_qtd), 1, 0, 'C');
                $file->Cell(25, 5, utf8_decode(Mask::dinheiro($produto->osrp_prod_valor)), 1, 0, 'C');
                $file->Cell(30, 5, utf8_decode(Mask::dinheiro($produto->osrp_prod_qtd * $produto->osrp_prod_valor)), 1, 1, 'C');
            }

            $file->Cell(160, 5, utf8_decode('Total dos Produtos'), 1, 0, 'R');
            $file->Cell(30, 5, utf8_decode(Mask::dinheiro($os->orse_total_produtos)), 1, 1, 'C');
        }
        $file->ln(2);
        $file->Cell(190, 5, utf8_decode('OBSERVAÇÕES'), 1,1, 'L', 1);
        $file->MultiCell(190, 5, utf8_decode($os->orse_observacao), 1);

        $file->ln(2);
        $file->Cell(160, 5, utf8_decode('TOTAL DA ORDEM DE SERVIÇO'), 1,0, 'R', 1);
        $file->Cell(30, 5, utf8_decode(Mask::dinheiro($os->orse_total_os)), 1,0, 'C');

        $file->Ln(8);

        $file->Cell(95, 6, utf8_decode('Entrada: '.Mask::dataPtBr($os['created_at'] )), 0,0, 'L');
        $file->Cell(95, 6, utf8_decode('Sainda: '.Mask::dataPtBr($os['updated_at'])), 0,1, 'L');

        $file->Ln(15);

        $file->Cell(95, 6, utf8_decode('____________________________________________________'), 0,0, 'L');
        $file->Cell(95, 6, utf8_decode('____________________________________________________'), 0,1, 'L');

        $file->Cell(95, 6, utf8_decode('Assinatura do Cliente/Responsável '), 0,0, 'C');
        $file->Cell(95, 6, utf8_decode('Assinatura do Gerente/Fiscal'), 0,1, 'C');

        $file->Output();
    }

    /**
     * @param array $dadosCliente
     * @return mixed
     * @throws \Exception
     */
    public function cadastroCliente($dadosCliente = []){

//        reservando id na tabela endereço
        $endereco = new EnderecoModel();

        $endereco->save();

//        salvando o contato
        $contato = new ContatoModel();

        $contato->cont_cel_1 = Mask::removerMascara($dadosCliente['cad_celular_1']) ?? null;
        if(!$contato->save()){
            Common::setError('Erro ao salvar os dados do Contato!');
        }

        $cliente = new ClienteModel();
        $cliente->clie_nome_razao_social = $dadosCliente['cad_nome'];
        $cliente->clie_tipo = '3';
        $cliente->clie_status = '1';
        $cliente->ende_id = $endereco->ende_id;
        $cliente->cont_id = $contato->cont_id;
        $cliente->empr_id = Session::get('empr_id');

        if(!$cliente->save()){
            Common::setError('Erro ao salvar os dados do Cliente!');
        }

        $veiculo = new VeiculoModel();

        $veiculo->veic_marca           = $dadosCliente['vei_marca'];
        $veiculo->veic_modelo          = $dadosCliente['vei_modelo'];
        $veiculo->veic_placa           = Mask::removerMascara($dadosCliente['vei_placa']);
        $veiculo->clie_id              = $cliente->clie_id;
        $veiculo->empr_id              =(int) Session::get('empr_id');

        if(!$veiculo->save()){
            Common::setError('Houve um erro ao salvar os dados!');
        }

        return $cliente->clie_id;
    }

    /**
     * @param array $dadosVeiculo
     * @return mixed
     * @throws \Exception
     */
    public function cadastroVeiculo($dadosVeiculo = []){

        $veiculo = new VeiculoModel();

        $veiculo->veic_marca           = $dadosVeiculo['cad_marca'];
        $veiculo->veic_modelo          = $dadosVeiculo['cad_modelo'];
        $veiculo->veic_placa           = Mask::removerMascara($dadosVeiculo['cad_placa']);
        $veiculo->clie_id              = $dadosVeiculo['id_cliente'];
        $veiculo->empr_id              =(int) Session::get('empr_id');

        if(!$veiculo->save()){
            Common::setError('Houve um erro ao salvar os dados!');
        }

        return $veiculo->clie_id;
    }
}
