<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 14/01/19
 * Time: 18:18
 */

namespace App\Repository;


use App\Model\CCColaboradolModel;
use App\Utils\Mask;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class CCColaboradorRepository
{
    public function tabela(Request $request)
    {
        $queryExtrato = CCColaboradolModel::query()->with(['colaborador']);

        return DataTables::eloquent($queryExtrato)
            ->filter(function($instance) use ($request){

                /* Filtrar registro da Respectiva Empresa logada */
                $instance->where('empr_id', Session::get('empr_id'));

                /* Filtrar por finalidade - Consumo ou Emprestimo */
                if ($request->has('dt_inicio') && !empty($request->get('dt_inicio')) && $request->has('dt_termino') && !empty($request->get('dt_termino')))
                {

                        $instance->whereBetween('created_at', array(Carbon::createFromFormat('d/m/Y', $request->dt_inicio)->format('Y-m-d 00:00:00'), Carbon::createFromFormat('d/m/Y', $request->dt_termino)->format('Y-m-d 00:00:00')));
                }

                /* Filtrar por Colaborador */
                if ($request->has('filtro_colaborador_id') && !empty($request->get('filtro_colaborador_id')) && !empty($request->get('filtro_colaborador')))
                {
                    $instance->whereHas('colaborador', function($query) use ($request){
                        $query->where('cola_id', $request->get('filtro_colaborador_id'));
                    });
                }

                /* Filtrar utilizando o Procurar */
                if($request->has('search') && !empty($request->get('search')['value']))
                {
                    $instance->where('cocc_descricao', 'like', '%'.$request->get('search')['value'].'%')
                        ->orWhere('cocc_id', 'like', '%'.$request->get('search')['value'].'%');
                }

                return $instance;
            })
            ->editColumn('cocc_valor', function($extrato){
                return ($extrato->cocc_valor = Mask::dinheiro($extrato->cocc_valor));
            })
            ->make(true);
    }
}