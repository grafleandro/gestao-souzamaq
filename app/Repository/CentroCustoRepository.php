<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 05/12/18
 * Time: 10:01
 */

namespace App\Repository;


use App\Model\CentroCustoModel;
use App\Utils\Common;
use Illuminate\Support\Facades\Session;

class CentroCustoRepository
{
    private $centroCustoModel;

    public function __construct(CentroCustoModel $centroCustoModel)
    {
        $this->centroCustoModel = $centroCustoModel;
    }

    /**
     * @param array $dadosCentroCusto
     * @return array
     * @throws \Exception
     */
    public function salvarDados(array $dadosCentroCusto){

        $this->centroCustoModel->cocc_titulo = $dadosCentroCusto['cc-titulo'];
        $this->centroCustoModel->cocc_descricao = $dadosCentroCusto['cc-descricao'];
        $this->centroCustoModel->cocc_situacao =(int) $dadosCentroCusto['cc-situacao'];
        $this->centroCustoModel->empr_id = Session::get('empr_id');

        if(!$this->centroCustoModel->save()){
            Common::setError('Houve um erro ao salvar os dados do Centro de Custo!');
        }

        return ['success' => 1];
    }
}
