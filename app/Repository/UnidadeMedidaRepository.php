<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 14/11/18
 * Time: 17:38
 */

namespace App\Repository;


use App\Model\UnidadeMedidaModel;

class UnidadeMedidaRepository
{
    public function unidadeMedida(){
        $dadosUnidMedida = UnidadeMedidaModel::get()->toArray();

        foreach ($dadosUnidMedida as $index => &$value){
            $value['unme_titulo'] = $value['unme_sigla'] . ' - ' . $value['unme_titulo'];
        }

        return $dadosUnidMedida;
    }
}
