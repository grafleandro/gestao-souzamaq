<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FormaPgtoModel extends Model
{
    protected $table = 'configuracao_forma_pagamento';

    protected $primaryKey = 'cofp_id';

    protected $fillable = [
        'cofp_titulo',
        'cofp_descricao',
        'cofp_avista',
        'cofp_parcelas',
        'cofp_troco',
        'cofp_desconto_max',
        'cofp_cod_nfe',
        'cofp_maquininha_fornecedor',
        'cofp_juros_parcela',
        'cofp_limite_parcela',
        'cocb_id',
        'empr_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function formaPgtoEmpresa(){
        return $this->hasOne(EmpresaModel::class, 'empr_id', 'empr_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contaBancaria(){
        return $this->hasOne(ContaBancariaModel::class, 'cocb_id', 'cocb_id');
    }
}
