<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmpresaModel extends Model
{
    protected $table = 'empresa';

    protected $primaryKey = 'empr_id';

    protected $fillable = [
        'empr_nome',
        'empr_razao_social',
        'empr_insc_estadual',
        'empr_status',
        'empr_cnpj',
        'ende_id',
        'cont_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empresaEndereco(){
        return $this->hasOne(EnderecoModel::class, 'ende_id', 'ende_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empresaContato(){
        return $this->hasOne(ContatoModel::class, 'cont_id', 'cont_id');
    }
}
