<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ColaboradorModel extends Model
{
    protected $table = 'colaborador';

    protected $primaryKey = 'cola_id';

    protected $fillable = [
        'cola_nome',
        'cola_data_nasc',
        'cola_cpf',
        'cola_rg',
        'cola_status',
        'cola_foto',
        'cola_comissao',
        'ende_id',
        'empr_id',
        'cont_id',
        'func_id',
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pessoaEndereco(){
        return $this->hasOne(EnderecoModel::class, 'ende_id', 'ende_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pessoaContato(){
        return $this->hasOne(ContatoModel::class, 'cont_id', 'cont_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pessoaFuncao(){
        return $this->hasOne(FuncaoModel::class, 'func_id', 'func_id');
    }

    /** Dados do Login do Usuario
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pessoaLogin(){
        return $this->hasOne(User::class, 'cola_id', 'cola_id');
    }
}
