<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InventarioModel extends Model
{
    protected $table = 'inventario';

    protected $primaryKey = 'inve_id';

    protected $fillable = [
        'inve_item',
        'inve_cod_barra',
        'inve_num_serie',
        'inve_observacao',
        'inve_qtd',
        'inve_dt_aquisicao',
        'inve_vida_util',
        'inve_valor_unit',
        'inve_valor_sucata',
        'inve_intervalo_manutencao',
        'inve_ultima_manutencao',
        'inve_periodo_garantia',
        'inve_arquivo',
        'grup_id',
        'empr_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function grupo()
    {
        return $this->hasOne(ProdutoGrupoModel::class, 'grup_id', 'grup_id');
    }
}
