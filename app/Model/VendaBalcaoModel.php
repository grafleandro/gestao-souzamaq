<?php

namespace App\Model;

use App\Repository\VendaBalcaoRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VendaBalcaoModel extends Model
{
    protected $table = 'venda_balcao';

    protected $primaryKey = 'veba_id';

    protected $fillable = [
        'veba_venda_total',
        'veba_venda_troco',
        'veba_total_pago',
        'veba_qtd_produto',
        'vest_id',
        'cola_id',
        'empr_id',
        'clie_id'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function colaborador(){
        return $this->hasOne(ColaboradorModel::class, 'cola_id', 'cola_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empresa(){
        return $this->hasOne(EmpresaModel::class, 'empr_id', 'empr_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pagamento(){
        return $this->hasMany(VendaBalcaoPagamentoModel::class, 'veba_id', 'veba_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contasReceber(){
        return $this->hasMany(ContasReceberModel::class, 'core_origem_id', 'veba_id')
            ->where('opor_id', VendaBalcaoRepository::OPERACAO_ORIGEM_VB);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function produto(){
        return $this->hasMany(ProdutoMovimentacaoModel::class, 'prmo_origem_id', 'veba_id')
            ->where('prmo_origem', VendaBalcaoRepository::OPERACAO_ORIGEM_VB);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function parcelas(){
        return DB::table('operacao_parcelas')
            ->join('operacao_pagamento', 'operacao_parcelas.oppa_id', '=', 'operacao_pagamento.oppa_id')
            ->join('contas_receber', 'operacao_pagamento.core_id', '=', 'contas_receber.core_id')
            ->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status(){
        return $this->hasOne(VendaStatusModel::class, 'vest_id', 'vest_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cliente(){
        return $this->hasOne(ClienteModel::class, 'clie_id', 'clie_id');
    }
}
