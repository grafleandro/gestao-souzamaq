<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProdutoMarcaModel extends Model
{
    protected $table = 'marca';

    protected $primaryKey = 'marc_id';

    protected $fillable = [
        'marc_titulo',
        'marc_descricao',
        'empr_id',
    ];
}
