<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OperacaoParcelasModel extends Model
{
    protected $table = 'operacao_parcelas';

    protected $primaryKey = 'oppr_id';

    protected $fillable = [
        'oppr_valor',
        'oppr_dt_vencimento',
        'oppr_status',
        'oppa_id',
    ];

    /** Operacao Pagamento
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function opPagamento()
    {
        return $this->hasOne(OperacaoPagamentoModel::class, 'oppa_id', 'oppa_id');
    }
}
