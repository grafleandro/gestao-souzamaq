<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FiscalTributoModel extends Model
{
    protected $table = 'fiscal_tributos';

    protected $primaryKey = 'fitr_id';

    protected $fillable = [
        'fitr_cofins_entrada',
        'fitr_cofins_saida',
        'fitr_pis_entrada',
        'fitr_pis_saida',
        'fincm_id',
        'ficst_id',
        'ficsosn_id',
        'empr_id',
    ];

    /** Tributos vinculados ao NCM
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function tributoNcm(){
        return $this->hasOne(FiscalNCMModel::class, 'finc_id', 'fincm_id');
    }
}
