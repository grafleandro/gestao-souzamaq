<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProdutoModel extends Model
{
    protected $table = 'produto';

    protected $primaryKey = 'prod_id';

    protected $fillable = [
        'prod_titulo',
        'prod_cod_barra',
        'prod_unid_venda',
        'grup_id',
        'fabr_id',
        'marc_id',
        'empr_id',
        'fincm_id',
        'cola_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function unidadeMedida(){
        return $this->hasOne(UnidadeMedidaModel::class, 'unme_id', 'prod_unid_venda');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produtoNcm(){
        return $this->hasOne(FiscalNCMModel::class, 'finc_id', 'fincm_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produtoValores(){
        return $this->hasOne(ProdutoValoresModel::class, 'prod_id', 'prod_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produtoEstoque(){
        return $this->hasOne(ProdutoEstoqueModel::class, 'prod_id', 'prod_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produtoGrupo(){
        return $this->hasOne(ProdutoGrupoModel::class, 'grup_id', 'grup_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produtoMarca(){
        return $this->hasOne(ProdutoMarcaModel::class, 'marc_id', 'marc_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produtoFabricante(){
        return $this->hasOne(ProdutoFabricanteModel::class, 'fabr_id', 'fabr_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function colaborador(){
        return $this->hasOne(ColaboradorModel::class, 'cola_id', 'cola_id');
    }
}
