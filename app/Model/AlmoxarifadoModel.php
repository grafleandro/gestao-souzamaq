<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AlmoxarifadoModel extends Model
{
    protected $table = 'almoxarifado';

    protected $primaryKey = 'almo_id';

    protected $fillable = [
        'almo_cod_barra',
        'almo_titulo',
        'almo_valor',
        'almo_qtd',
        'almo_qtd_emprestimo',
        'almo_finalidade',
        'almo_dt_vencimento',
        'almo_multa',
        'almo_sessao',
        'almo_categoria',
        'almo_subcategoria',
        'grup_id',
        'unme_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function grupo()
    {
        return $this->hasOne(ProdutoGrupoModel::class, 'grup_id', 'grup_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function unidadeMedida()
    {
        return $this->hasOne(UnidadeMedidaModel::class, 'unme_id', 'unme_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function emprestimo()
    {
        return $this->hasMany(AlmoxarifadoEmprestimoModel::class, 'almo_id', 'almo_id');
    }
}
