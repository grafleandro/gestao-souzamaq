<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VendaStatusModel extends Model
{
    protected $table = 'venda_status';

    protected $primaryKey = 'vest_id';

    protected $fillable = [
        'vest_titulo',
        'vest_descricao',
        'vest_class',
        'empr_id',
    ];
}
