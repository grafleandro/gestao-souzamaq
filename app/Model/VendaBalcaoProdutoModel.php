<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VendaBalcaoProdutoModel extends Model
{
    protected $table = 'venda_balcao_produto';

    protected $primaryKey = 'vebp_id';

    protected $fillable = [
        'vebp_valor',
        'vebp_quantidade',
        'prod_id',
        'veba_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function vendaBalcao(){
        return $this->hasOne(VendaBalcaoModel::class, 'veba_id', 'veba_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produto(){
        return $this->hasOne(ProdutoModel::class, 'prod_id', 'prod_id');
    }
}
