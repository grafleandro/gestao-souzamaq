<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FiscalCSOSNModel extends Model
{
    protected $table = 'fiscal_csosn';

    protected $primaryKey = 'fics_id';

    protected $fillable = [
        'fics_titulo',
        'fics_descricao',
        'fics_codigo',
    ];
}
