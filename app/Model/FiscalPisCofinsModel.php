<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FiscalPisCofinsModel extends Model
{
    protected $table = 'fiscal_pis_cofins';

    protected $primaryKey = 'fipc_id';

    protected $fillable = [
        'fipc_titulo',
        'fipc_descricao',
        'fipc_entrada',
        'fipc_codigo',
    ];
}
