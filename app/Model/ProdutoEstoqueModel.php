<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProdutoEstoqueModel extends Model
{
    protected $table = 'produto_estoque';

    protected $primaryKey = 'pres_id';

    protected $fillable = [
        'pres_controlar_estoque',
        'pres_estoq_mini',
        'pres_estoq_max',
        'prest_estoq_atual',
        'prod_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produtoEstoque(){
        return $this->hasOne(ProdutoModel::class, 'prod_id', 'prod_id');
    }
}
