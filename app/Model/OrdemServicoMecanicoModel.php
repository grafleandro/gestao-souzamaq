<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrdemServicoMecanicoModel extends Model
{
    protected $table = 'ordem_servico_mecanico';

    protected $primaryKey = 'osme_id';

    protected $fillable = [
        'cola_id',
        'orse_id',
    ];
}
