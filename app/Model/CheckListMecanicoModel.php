<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CheckListMecanicoModel extends Model
{
    protected $table = 'checklist_mecanico';

    protected $primaryKey = 'chme_id';

    protected $fillable = [
        'ckli_id',
        'cola_id'
    ];

    public $timestamps = false;

    public function mecanicoColaborador(){
        return $this->hasOne(ColaboradorModel::class, 'cola_id', 'cola_id');
    }
}


