<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FiscalCSTModel extends Model
{
    protected $table = "fiscal_cst";

    protected $primaryKey = "fics_id";

    protected $fillable = [
        'fics_titulo',
        'fics_descricao',
        'fics_codigo',
    ];
}
