<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EnderecoModel extends Model
{
    protected $table = 'endereco';

    protected $primaryKey = 'ende_id';

    protected $fillable = [
        'enlo_id',
        'ende_logradouro_titulo',
        'ende_bairro',
        'ende_cidade',
        'ende_estado',
        'ende_complemento',
        'ende_numero',
        'ende_cep',
        'created_at',
        'updated_at',
    ];
}
