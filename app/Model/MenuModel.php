<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model
{
    protected $table = 'menu';

    protected $primaryKey = 'menu_id';

    protected $fillable = [
        'menu_texto',
        'menu_titulo',
        'menu_ordem',
        'menu_sequencia',
        'menu_href',
        'menu_class',
        'menu_icone',
        'menu_pai'
    ];
}
