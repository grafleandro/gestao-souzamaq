<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FiscalNCMModel extends Model
{
    protected $table = 'fiscal_ncm';

    protected $primaryKey = 'finc_id';

    protected $fillable = [
        'finc_cod_ncm',
        'finc_cod_cest',
        'finc_descricao',
        'finc_ipi',
        'finc_inicio_vigencia',
        'finc_fim_vigencia',
        'finc_gtin_producao',
        'finc_gtin_homologacao',
        'finc_observacao',
        'unme_id',
    ];
}
