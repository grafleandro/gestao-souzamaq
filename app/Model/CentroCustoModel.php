<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CentroCustoModel extends Model
{
    protected $table = 'configuracao_centro_custo';

    protected $primaryKey = 'cocc_id';

    protected $fillable = [
        'cocc_titulo',
        'cocc_descricao',
        'cocc_situacao',
        'empr_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empresa(){
        return $this->hasOne(EmpresaModel::class, 'empr_id', 'empr_id');
    }
}
