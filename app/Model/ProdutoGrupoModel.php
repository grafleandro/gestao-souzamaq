<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProdutoGrupoModel extends Model
{
    protected $table = "grupo";

    protected $primaryKey = "grup_id";

    protected $fillable = [
        'grup_titulo',
        'grup_descricao',
        'empr_id'
    ];
}
