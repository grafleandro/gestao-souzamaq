<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UnidadeMedidaModel extends Model
{
    protected $table = 'unidade_medida';

    protected $primaryKey = 'unme_id';

    protected $fillable = [
        'unme_titulo',
        'unme_sigla',
    ];
}
