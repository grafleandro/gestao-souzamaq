<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContaBancariaModel extends Model
{
    protected $table = 'configuracao_conta_bancaria';

    protected $primaryKey = 'cocb_id';

    protected $fillable = [
        'cocb_tipo_conta',
        'cocb_agencia',
        'cocb_conta_corrente',
        'cocb_conta_poupanca',
        'cocb_variacao',
        'banc_id',
        'empr_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function banco(){
        return $this->hasOne(BancosModel::class, 'banc_id', 'banc_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empresa(){
        return $this->hasOne(EmpresaModel::class, 'empr_id', 'empr_id');
    }
}
