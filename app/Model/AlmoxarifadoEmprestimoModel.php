<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AlmoxarifadoEmprestimoModel extends Model
{
    protected $table = 'almoxarifado_emprestimo';

    protected $primaryKey = 'alem_id';

    protected $fillable = [
        'alem_responsavel',
        'alem_destino',
        'alem_qtd',
        'alem_dt_ocorrencia',
        'alem_status',
        'cola_id',
        'empr_id',
        'almo_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function colaborador()
    {
        return $this->hasOne(ColaboradorModel::class, 'cola_id', 'cola_id');
    }

    public function almoxarifado()
    {
        return $this->hasOne(AlmoxarifadoModel::class, 'almo_id', 'almo_id');
    }
}
