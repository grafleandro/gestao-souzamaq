<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OSRelacaoProdutoModel extends Model
{
    protected $table = 'ordem_servico_relacao_produto';

    protected $primaryKey = 'osrp_id';

    protected $fillable = [
        'osrp_prod_valor',
        'osrp_prod_qtd',
        'prod_id',
        'orse_id',
    ];

    public function produto(){
        return $this->hasOne(ProdutoModel::class, 'prod_id', 'prod_id');
    }

    public function produtoValores(){
        return $this->hasOne(ProdutoValoresModel::class, 'prod_id', 'prod_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produtoEstoque(){
        return $this->hasOne(ProdutoEstoqueModel::class, 'prod_id', 'prod_id');
    }
}
