<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OSRelacaoServicoModel extends Model
{
    protected $table = 'ordem_servico_relacao_servico';

    protected $primaryKey = 'osrs_id';

    protected $fillable = [
        'osrs_valor_servico',
        'osrs_descricao_servico',
        'osrs_qtd_servico',
        'serv_id',
        'orse_id',
    ];

    public function servico(){
        return $this->hasOne(ServicoModel::class, 'serv_id', 'serv_id');
    }
}
