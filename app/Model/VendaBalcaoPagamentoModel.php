<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VendaBalcaoPagamentoModel extends Model
{
    protected $table = 'venda_balcao_pagamento';

    protected $primaryKey = 'vbpa_id';

    protected $fillable = [
        'vbpa_valor',
        'vbpa_parcelas',
        'cofp_id',
        'veba_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function vendaBalcao(){
        return $this->hasOne(VendaBalcaoModel::class, 'veba_id', 'veba_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function formaPagamento(){
        return $this->hasOne(FormaPgtoModel::class, 'cofp_id', 'cofp_id');
    }
}
