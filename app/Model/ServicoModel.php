<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServicoModel extends Model
{
    protected $table = 'servico';

    protected $primaryKey = 'serv_id';

    protected $fillable = [
        'serv_titulo',
        'serv_aliq_iss',
        'serv_valor',
        'serv_cota_comissao',
        'serv_cota_empresa',
        'empr_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function servicoEmpresa(){
        return $this->hasOne(EmpresaModel::class, 'empr_id', 'empr_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function servicoProduto(){
        return $this->hasMany(ServicoProdutoModel::class, 'serv_id', 'serv_id');
    }
}
