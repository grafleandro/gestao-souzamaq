<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CheckListInternoModel extends Model
{
    protected $table = 'checklist_interno';

    protected $primaryKey = 'ckli_id';

    protected $fillable = [
    'ckli_filtro',
    'veic_id',
    'cola_id',
    'checklist_interno',
    'ckli_servico',
    'ckli_km',
    'ckli_observacao',
    'empr_id',
    'clie_id',
    'ckli_previsao_entrega',
    'ckli_hora_entrega',
    'ckli_objetos_veiculo',
    'ckli_valor',
    'ckli_combustivel',
    'ckli_status',
    'ckli_oleo',
    'ckli_os_externo',
    'ckli_os_externo_servico',
    ];

    public function checklistInternoVeiculo(){
        return $this->hasOne(VeiculoModel::class, 'veic_id', 'veic_id');
    }

    public function checklistInternoCliente(){
        return $this->hasOne(ClienteModel::class, 'clie_id', 'clie_id');
    }

    public function checklistMecanico(){
        return $this->hasManyThrough(ColaboradorModel::class, CheckListMecanicoModel::class, 'ckli_id', 'cola_id', 'ckli_id', 'cola_id');
    }

}
