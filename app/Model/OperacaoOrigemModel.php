<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OperacaoOrigemModel extends Model
{
    protected $table = 'operacao_origem';

    protected $primaryKey = 'opor_id';

    protected $fillable = [
        'opor_titulo',
        'opor_descricao'
    ];
}
