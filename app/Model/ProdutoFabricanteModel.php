<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProdutoFabricanteModel extends Model
{
    protected $table = 'fabricante';

    protected $primaryKey = 'fabr_id';

    protected $fillable = [
        'fabr_nome_nome_fantazia',
        'fabr_cpf_cnpj',
        'fabr_rg_insc_estadual',
        'fabr_razao_social',
        'fabr_tipo',
        'cont_id',
        'ende_id',
        'empr_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function fabricanteContato(){
        return $this->hasOne(ContatoModel::class, 'cont_id', 'cont_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function fabricanteEndereco(){
        return $this->hasOne(EnderecoModel::class, 'ende_id', 'ende_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function fabricanteEmpresa(){
        return $this->hasOne(EmpresaModel::class, 'empr_id', 'empr_id');
    }
}
