<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContasAPagarCategoriaModel extends Model
{
    protected $table = 'contas_pagar_categoria';

    protected $primaryKey = 'copc_id';

    protected $fillable = [
        'copc_titulo',
        'copc_descricao',
    ];
}
