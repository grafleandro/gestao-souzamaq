<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CheckListExternoModel extends Model
{
    protected $table = 'checklist_externo';

    protected $primaryKey = 'chex_id';

    protected $fillable = [
        'chex_cliente',
        'chex_responsavel',
        'chex_placa',
        'chex_num_os',
        'chex_status',
        'chex_descricao',
    ];
}
