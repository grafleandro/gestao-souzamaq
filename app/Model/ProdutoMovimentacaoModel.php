<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProdutoMovimentacaoModel extends Model
{
    protected $table = 'produto_movimentacao';

    protected $primaryKey = 'prmo_id';

    protected $fillable = [
        'prmo_valor',
        'prmo_qtd',
        'prmo_status',
        'prmo_origem',
        'prmo_origem_id',
        'prod_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produto(){
        return $this->hasOne(ProdutoModel::class, 'prod_id', 'prod_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function estoque(){
        return $this->hasOne(ProdutoEstoqueModel::class, 'prod_id', 'prod_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function valores(){
        return $this->hasOne(ProdutoValoresModel::class, 'prod_id', 'prod_id');
    }
}
