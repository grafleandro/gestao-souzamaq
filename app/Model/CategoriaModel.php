<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoriaModel extends Model
{
    protected $table = 'categoria';

    protected $primaryKey = 'cate_id';

    protected $fillable = [
        'cate_titulo',
        'cate_descricao',
        'empr_id'
    ];
}
