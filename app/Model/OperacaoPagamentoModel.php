<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OperacaoPagamentoModel extends Model
{
    protected $table = 'operacao_pagamento';

    protected $primaryKey = 'oppa_id';

    protected $fillable = [
        'oppa_valor',
        'oppa_parcelas',
        'oppa_desconto',
        'oppa_origem_id',
        'opor_id',
        'cofp_id',
        'core_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function formaPgto()
    {
        return $this->hasOne(FormaPgtoModel::class, 'cofp_id', 'cofp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contasReceber()
    {
        return $this->hasOne(ContasReceberModel::class, 'core_id', 'core_id');
    }
}
