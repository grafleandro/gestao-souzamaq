<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ComissaoModel extends Model
{
    protected $table = 'configuracao_comissao';

    protected $primaryKey = 'coco_id';

    protected $fillable = [
        'coco_liquido_bruto',
        'coco_cota_comissao',
        'coco_cota_empresa',
        'coco_modulo_venda',
        'coco_modulo_os',
        'empr_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empresa(){
        return $this->hasOne(EmpresaModel::class, 'empr_id', 'empr_id');
    }
}
