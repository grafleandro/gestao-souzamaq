<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContasAPagarParcelasModel extends Model
{
    protected $table = 'contas_pagar_parcelas';

    protected $primaryKey = 'copp_id';

    protected $fillable = [
        'copp_valor',
        'copp_dt_vencimento',
        'copp_status',
        'copa_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contaPagar(){
        return $this->hasOne(ContasAPagarModel::class, 'copa_id', 'copa_id');
    }
}
