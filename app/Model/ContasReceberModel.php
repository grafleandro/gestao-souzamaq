<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContasReceberModel extends Model
{
    protected $table = 'contas_receber';

    protected $primaryKey = 'core_id';

    protected $fillable = [
        'core_titulo',
        'core_valor',
        'core_dt_ocorrencia',
        'core_juros',
        'core_origem_id', // ID do item que se encontra na tabela de origem: Venda Balca; Ordem de Servico; Contas a Receber
        'opor_id',
        'vest_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function origem()
    {
        return $this->hasOne(OperacaoOrigemModel::class, 'opor_id', 'opor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status()
    {
        return $this->hasOne(VendaStatusModel::class, 'vest_id', 'vest_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function opPagamento()
    {
        return $this->hasOne(OperacaoPagamentoModel::class, 'core_id', 'core_id');
    }
}
