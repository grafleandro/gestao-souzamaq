<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FuncaoModel extends Model
{
    protected $table = 'funcao';

    protected $primaryKey = 'func_id';

    protected $fillable = [
        'func_nome',
        'func_sigla',
        'func_descricao',
        'func_padrao',
        'empr_id'
    ];
}
