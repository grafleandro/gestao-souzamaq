<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContasAPagarModel extends Model
{
    protected $table = 'contas_pagar';

    protected $primaryKey = 'copa_id';

    protected $fillable = [
        'copa_descricao',
        'copa_dt_competencia',
        'copa_dt_vencimento',
        'copa_valor',
        'copa_parcelas',
        'copa_parcelas_qtd',
        'copa_observacao',
        'copc_id',
        'cocb_id',
        'cocc_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function categoria(){
        return $this->hasOne(ContasAPagarCategoriaModel::class, 'copc_id', 'copc_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contaBancaria(){
        return $this->hasOne(ContaBancariaModel::class, 'cocb_id', 'cocb_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function centroCusto(){
        return $this->hasOne(CentroCustoModel::class, 'cocc_id', 'cocc_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parcelas(){
        return $this->hasMany( ContasAPagarParcelasModel::class, 'copa_id', 'copa_id');
    }
}
