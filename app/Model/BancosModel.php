<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BancosModel extends Model
{
    protected $table = 'bancos';

    protected $primaryKey = 'banc_id';

    protected $fillable = [
        'banc_id',
        'banc_titulo',
        'banc_codigo',
    ];
}
