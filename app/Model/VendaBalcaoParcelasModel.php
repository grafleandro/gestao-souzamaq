<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VendaBalcaoParcelasModel extends Model
{
    protected $table = 'venda_balcao_parcelas';

    protected $primaryKey = 'vbap_id';

    protected $fillable = [
        'vbap_valor',
        'vbap_pago',
        'vbap_vencimento',
        'vbpa_id',
    ];
}
