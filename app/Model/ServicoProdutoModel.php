<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServicoProdutoModel extends Model
{
    protected $table = 'servico_produto';

    protected $primaryKey = 'sepr_id';

    protected $fillable = [
        'sepr_qtd',
        'prod_id',
        'serv_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function servicoProdutoServico(){
        return $this->hasOne(ServicoModel::class, 'serv_id', 'serv_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produto(){
        return $this->hasOne(ProdutoModel::class, 'prod_id', 'prod_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produtoValores(){
        return $this->hasOne(ProdutoValoresModel::class, 'prod_id', 'prod_id');
    }
}
