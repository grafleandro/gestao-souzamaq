<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OSPagamentoModel extends Model
{
    protected $table = 'ordem_servico_pagamento';

    protected $primaryKey = 'ospa_id';

    protected $fillable = [
        'ospa_valor',
        'ospa_desconto',
        'cofp_id',
        'orse_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ordemServico(){
        return $this->hasOne(OrdemServicoModel::class, 'orse_id', 'orse_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function formaPagamento(){
        return $this->hasOne(FormaPgtoModel::class, 'cofp_id', 'cofp_id');
    }
}
