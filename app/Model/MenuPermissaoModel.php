<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MenuPermissaoModel extends Model
{
    protected $table = 'menu_permissao';

    protected $primaryKey = 'mepe_id';

    protected $fillable = [
        'mepe_apelido',
        'mepe_padrao',
        'cola_id',
        'menu_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function menu(){
        return $this->hasOne(MenuModel::class, 'menu_id', 'menu_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function colaborador(){
        return $this->hasOne(ColaboradorModel::class, 'cola_id', 'cola_id');
    }
}
