<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClienteModel extends Model
{
    protected $table = 'cliente';

    protected $primaryKey = 'clie_id';

    protected $fillable = [
        'clie_nome_razao_social',
        'clie_nome_fantasia',
        'clie_cpf_cnpj',
        'clie_rg_insc_estadual',
        'clie_tipo',
        'clie_status',
        'ende_id',
        'cont_id',
        'empr_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function clienteEndereco(){
        return $this->hasOne(EnderecoModel::class, 'ende_id', 'ende_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function clienteContato(){
        return $this->hasOne(ContatoModel::class, 'cont_id', 'cont_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function clienteEmpresa(){
        return $this->hasOne(EmpresaModel::class, 'empr_id', 'empr_id');
    }
}
