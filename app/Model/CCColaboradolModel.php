<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CCColaboradolModel extends Model
{
    protected $table = 'colaborador_conta_corrente';

    protected $primaryKey = 'cocc_id';

    protected $fillable = [

        'cocc_valor',
        'cocc_tipo',
        'cocc_origem',
        'cocc_descricao',
        'cola_id',
    ];

    public function colaborador(){
        return $this->hasOne(ColaboradorModel::class, 'cola_id', 'cola_id' );
    }
}
