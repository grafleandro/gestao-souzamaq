<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VeiculoModel extends Model
{
    protected $table = 'veiculo';

    protected $primaryKey = 'veic_id';

    protected $fillable = [
        'veic_marca',
        'veic_modelo',
        'veic_placa',
        'veic_cor',
        'veic_combustivel',
        'veic_ano_fabricacao',
        'veic_ano_modelo',
        'veic_chassi',
        'veic_km_inicial',
        'clie_id',
        'empr_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function veiculoCliente(){
        return $this->hasOne(ClienteModel::class, 'clie_id', 'clie_id');
    }

    public function veiculoCor(){
        return $this->hasOne(VeiculoCorModel::class, 'veco_id', 'veic_cor');
    }
}
