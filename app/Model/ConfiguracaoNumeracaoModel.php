<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ConfiguracaoNumeracaoModel extends Model
{
    protected $table = 'configuracao_numeracao';

    protected $primaryKey = 'conu_id';

    protected $fillable = [
        'conu_checklist_interno',
        'conu_produto',
        'conu_servicos',
        'conu_venda',
        'conu_ordem_servico',
        'conu_orcamento',
        'conu_nfe',
        'conu_nfce',
        'empr_id',
    ];

    public $timestamps = false;
}
