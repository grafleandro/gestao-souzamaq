<?php

namespace App\Model;

use App\Repository\OrdemServicoRepository;
use Illuminate\Database\Eloquent\Model;

class OrdemServicoModel extends Model
{
    protected $table = 'ordem_servico';

    protected $primaryKey = 'orse_id';

    protected $fillable = [
        'orse_situacao',
        'orse_prev_entrega',
        'orse_descri_serv_cliente',
        'orse_observacao',
        'orse_numero',
        'orse_total_servico',
        'orse_total_produtos',
        'orse_total_os',
        'empr_id',
        'clie_id',
        'ckli_id',
        'veic_id',
        'conu_id',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ordemServicoVeiculo(){
        return $this->hasOne(VeiculoModel::class, 'veic_id', 'veic_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ordemServicoCliente(){
        return $this->hasOne(ClienteModel::class, 'clie_id', 'clie_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ordemServicoProduto(){
        return $this->hasMany(ProdutoMovimentacaoModel::class, 'prmo_origem_id', 'orse_id')
            ->where('prmo_origem', OrdemServicoRepository::OPERACAO_ORIGEM_OS);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ordemServicoServicos(){
        return $this->hasMany(OSRelacaoServicoModel::class, 'orse_id', 'orse_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function ordemServicoMecanico(){
        return $this->hasManyThrough(ColaboradorModel::class, OrdemServicoMecanicoModel::class, 'orse_id', 'cola_id', 'orse_id', 'cola_id');
    }
}
