<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProdutoValoresModel extends Model
{
    protected $table = 'produto_valores';

    protected $primaryKey = 'prva_id';

    protected $fillable = [
        'prva_custo',
        'prva_markup',
        'prva_preco_sugerido',
        'prva_preco_venda',
        'prva_cota_comissao',
        'prva_cota_empresa',
        'prod_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function produtoValores(){
        return $this->hasOne(ProdutoModel::class, 'prod_id', 'prod_id');
    }
}
