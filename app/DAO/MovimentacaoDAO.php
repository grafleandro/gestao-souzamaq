<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 29/01/19
 * Time: 16:07
 */

namespace App\DAO;

class MovimentacaoDAO
{
    private $produto = [];

    /**
     * @param null $idProduto
     * @return array|mixed
     */
    public function getProduto($idProduto = null){
        return isset($this->produto[$idProduto]) ? $this->produto[$idProduto] : $this->produto;
    }

    /**
     * @param MovimentacaoProdutoDAO $produtoDAO
     */
    public function addProduto(MovimentacaoProdutoDAO $produtoDAO){
        if(isset($this->produto[$produtoDAO->getProdId()])){
            $produto = $this->produto[$produtoDAO->getProdId()];

            $produto['valor'] += ($produtoDAO->getValor()*$produtoDAO->getQuantidade());
            $produto['quantidade'] += $produtoDAO->getQuantidade();

            $this->produto[$produtoDAO->getProdId()] = $produto;
        } else {
            $produto['prod_id']     = $produtoDAO->getProdId();
            $produto['titulo']      = $produtoDAO->getTitulo();
            $produto['ultimaVenda'] = $produtoDAO->getUtimaVenda();
            $produto['descricao']   = $produtoDAO->getDescricao();
            $produto['estoqueAtual']= $produtoDAO->getEstoqueAtual();
            $produto['valor']       =  ($produtoDAO->getValor()*$produtoDAO->getQuantidade());
            $produto['valorVenda']  = $produtoDAO->getValor();
            $produto['valorCusto'] = $produtoDAO->getValorDeCusto();
            $produto['quantidade']  = $produtoDAO->getQuantidade();
            $produto['created']     = $produtoDAO->getCreated();
            $produto['idOrigem']    = $produtoDAO->getOrigemId();
            $produto['colaborador']     = $produtoDAO->getColaNome();
            $produto['cliente']         = $produtoDAO->getClieNomeRazaoSocial();
            $produto['cadastradoPor']   = $produtoDAO->getCadastradoPor();
            $produto['cadastradoPorId']     = $produtoDAO->getCadastradoPorId();
            $produto['unidMedidaSigla']     = $produtoDAO->getUnidMedidaSigla();
            $produto['unidMedidaTtitulo']   = $produtoDAO->getUnidMedidaTitulo();
            $produto['status']  = $produtoDAO->getStatus();

            $this->produto[$produtoDAO->getProdId()] = $produto;
        }
    }

    /**
     * @param MovimentacaoProdutoDAO $produtoDAO
     */
    public function addProdutoDetalhe(MovimentacaoProdutoDAO $produtoDAO){
        $index = "{$produtoDAO->getOrigemId()}-{$produtoDAO->getId()}";

        if(isset($this->produto['cabecalho'])){
            $this->produto['cabecalho']['total'] += ($produtoDAO->getValor()*$produtoDAO->getQuantidade());
            $this->produto['cabecalho']['qtdVendido'] += $produtoDAO->getQuantidade();
        } else {
            $cabecalho['criadoEm'] = $produtoDAO->getCreated();
            $cabecalho['total'] =  ($produtoDAO->getValor()*$produtoDAO->getQuantidade());
            $cabecalho['qtdVendido'] = $produtoDAO->getQuantidade();
            $cabecalho['ultimaVenda'] = $produtoDAO->getUtimaVenda();
            $cabecalho['titulo'] = $produtoDAO->getTitulo();
            $cabecalho['cadastradoPor'] = $produtoDAO->getCadastradoPor();
            $cabecalho['cadastradoPorId'] = $produtoDAO->getCadastradoPorId();
            $cabecalho['unidMedidaSigla']     = $produtoDAO->getUnidMedidaSigla();
            $cabecalho['unidMedidaTtitulo']   = $produtoDAO->getUnidMedidaTitulo();
            $cabecalho['estoqueAtual'] = $produtoDAO->getEstoqueAtual();

            $this->produto['cabecalho'] = $cabecalho;
        }

        $produto['prod_id']     = $produtoDAO->getProdId();
        $produto['descricao']   = $produtoDAO->getDescricao();
        $produto['valor']       = $produtoDAO->getValor();
        $produto['estoqueAtual']    = $produtoDAO->getEstoqueAtual();
        $produto['valorVenda']      = $produtoDAO->getValor();
        $produto['valorDeCusto']    = $produtoDAO->getValorDeCusto();
        $produto['quantidade']  = $produtoDAO->getQuantidade();
        $produto['idOrigem']    = $produtoDAO->getOrigemId();
        $produto['colaborador'] = $produtoDAO->getColaNome();
        $produto['cliente'] = $produtoDAO->getClieNomeRazaoSocial();
        $produto['status']  = $produtoDAO->getStatus();

        $this->produto['produto'][$index] = $produto;
    }
}
