<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 29/01/19
 * Time: 15:27
 */

namespace App\DAO;


class MovimentacaoProdutoDAO
{
    private $clieId;
    private $clieNomeRazaoSocial;
    private $colaId;
    private $colaNome;
    private $id;
    private $valor;
    private $quantidade;
    private $titulo = '';
    private $prodId;
    private $origemId;
    private $origem = [];
    private $utimaVenda;
    private $descricao;
    private $estoqueAtual;
    private $status;
    private $cadastradoPor;
    private $cadastradoPorId;
    private $unidMedidaTitulo;
    private $unidMedidaSigla;
    private $valorDeCusto;
    private $created;
    private $updated;

    /**
     * @return mixed
     */
    public function getValorDeCusto()
    {
        return $this->valorDeCusto;
    }

    /**
     * @param mixed $valorDeCusto
     */
    public function setValorDeCusto($valorDeCusto): void
    {
        $this->valorDeCusto = $valorDeCusto;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUnidMedidaTitulo()
    {
        return $this->unidMedidaTitulo;
    }

    /**
     * @param mixed $unidMedidaTitulo
     */
    public function setUnidMedidaTitulo($unidMedidaTitulo): void
    {
        $this->unidMedidaTitulo = $unidMedidaTitulo;
    }

    /**
     * @return mixed
     */
    public function getUnidMedidaSigla()
    {
        return $this->unidMedidaSigla;
    }

    /**
     * @param mixed $unidMedidaSigla
     */
    public function setUnidMedidaSigla($unidMedidaSigla): void
    {
        $this->unidMedidaSigla = $unidMedidaSigla;
    }

    /**
     * @return mixed
     */
    public function getCadastradoPor()
    {
        return $this->cadastradoPor;
    }

    /**
     * @param mixed $cadastradoPor
     */
    public function setCadastradoPor($cadastradoPor): void
    {
        $this->cadastradoPor = $cadastradoPor;
    }

    /**
     * @return mixed
     */
    public function getCadastradoPorId()
    {
        return $this->cadastradoPorId;
    }

    /**
     * @param mixed $cadastradoPorId
     */
    public function setCadastradoPorId($cadastradoPorId): void
    {
        $this->cadastradoPorId = $cadastradoPorId;
    }

    /**
     * @return mixed
     */
    public function getClieId()
    {
        return $this->clieId;
    }

    /**
     * @param mixed $clieId
     */
    public function setClieId($clieId): void
    {
        $this->clieId = $clieId;
    }

    /**
     * @return mixed
     */
    public function getClieNomeRazaoSocial()
    {
        return $this->clieNomeRazaoSocial;
    }

    /**
     * @param mixed $clieNomeRazaoSocial
     */
    public function setClieNomeRazaoSocial($clieNomeRazaoSocial): void
    {
        $this->clieNomeRazaoSocial = $clieNomeRazaoSocial;
    }

    /**
     * @return mixed
     */
    public function getColaId()
    {
        return $this->colaId;
    }

    /**
     * @param mixed $colaId
     */
    public function setColaId($colaId): void
    {
        $this->colaId = $colaId;
    }

    /**
     * @return mixed
     */
    public function getColaNome()
    {
        return $this->colaNome;
    }

    /**
     * @param mixed $colaNome
     */
    public function setColaNome($colaNome): void
    {
        $this->colaNome = $colaNome;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getEstoqueAtual()
    {
        return $this->estoqueAtual;
    }

    /**
     * @param mixed $estoque
     */
    public function setEstoqueAtual($estoque): void
    {
        $this->estoqueAtual = $estoque;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao): void
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getUtimaVenda()
    {
        return $this->utimaVenda;
    }

    /**
     * @param mixed $utimaVenda
     */
    public function setUtimaVenda($utimaVenda): void
    {
        $this->utimaVenda = $utimaVenda;
    }

    /**
     * @return string
     */
    public function getTitulo(): string
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo(string $titulo): void
    {
        $this->titulo = $titulo;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param float $valor
     * @return void
     */
    public function setValor(float $valor): void
    {
        $this->valor = $valor;
    }

    /**
     * @return mixed
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * @param float $quantidade
     * @return void
     */
    public function setQuantidade(float $quantidade): void
    {
        $this->quantidade = $quantidade;
    }

    /**
     * @return mixed
     */
    public function getProdId()
    {
        return $this->prodId;
    }

    /**
     * @param int $prodId
     * @return void
     */
    public function setProdId(int $prodId): void
    {
        $this->prodId = $prodId;
    }

    /**
     * @return mixed
     */
    public function getOrigemId()
    {
        return $this->origemId;
    }

    /**
     * @param int $origemId
     * @return void
     */
    public function setOrigemId(int $origemId): void
    {
        $this->origemId = $origemId;
    }

    /**
     * @return array
     */
    public function getOrigem(): array
    {
        return $this->origem;
    }

    /**
     * @param array $origem
     * @return array
     */
    public function setOrigem(array $origem): array
    {
        $this->origem = $origem;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created): void
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated): void
    {
        $this->updated = $updated;
    }
}
