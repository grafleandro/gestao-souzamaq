<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 06/08/18
 * Time: 10:35
 */

namespace App\Mail;

use Illuminate\Support\Facades\View;
use PHPMailer\PHPMailer\PHPMailer;

class EnviarNotificacaoMail extends PHPMailer
{
    protected $user = 'no-report@outputweb.com.br';

    protected $pass = 'OutPutWeb58NR';

    protected $port = '587';

    public function __construct(?bool $exceptions = null)
    {
        parent::__construct($exceptions);

        $this->isSMTP();

        /**
         *  Enable SMTP debugging
         *  0 = off (for production use)
         *  1 = client messages
         *  2 = client and server messages
         *  */
        $this->SMTPDebug = 0;

        $this->Host = 'smtp.'.substr(strstr($this->user, '@'), 1);; // SMTP server

        $this->SMTPAuth = true; // enable SMTP authentication

        $this->Port = $this->port; // set the SMTP port for the service server

        $this->Username = $this->user; // account username

        $this->Password = $this->pass; // account password

        $this->CharSet = 'UTF-8';

        $this->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
    }

    /**
     * @param $name
     * @param $subject
     * @param $email
     * @param $body
     * @param array $option
     * @return array|bool|string
     * @throws \PHPMailer\PHPMailer\Exception
     * @throws \Throwable
     */
    public function enviarNotificacao($name, $subject, $email, $body, $option = []){
        /* View */
        $view = (View::exists($body)) ? \view($body, $option)->render() : $body;

        $this->SetFrom($this->user, $name);

        $this->Subject = $subject;

        $this->MsgHTML($view);

        $this->AddAddress($email, "");

        if(!$this->Send()){
            return "Mailer Error: " . $this->ErrorInfo;
        }else{
            return array("success" => 1);
        }

        $this->ClearAllRecipients();
        $this->ClearAttachments();

        return $this->send();
    }
}